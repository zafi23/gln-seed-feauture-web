the_silver_shilling_there be once 1 shilling which come forth from the mint spring and shout hurrah
now i be go out into the wide world and truly it do go out into the wide world
the child hold it with warm hand the miser with 1 cold and convulsive grasp and the old people turn it about goodness know how_many time while the young people soon allow it to roll away from them
the shilling be make of silver it contain very little copper and consider itself quite out in the world when it have be circulate for 1 year in the country in which it have be coin
one day it really do go out into the world for it belong to 1 gentleman who be about to travel in foreign land
this gentleman be not aware that the shilling lie at_the_bottom of his purse when he start till he one day find it between his finger
why cry he here be 1 shilling from home well it must go on its travel with me now
and the shilling jump and rattle for joy when it be put back again into the purse
here it lie among 1 number of foreign companion who be always come and go one take the place of another but the shilling from home be always put back and have to remain in the purse which be certainly $_DEM:1 of distinction
many week pass during which the shilling have travel 1 long distance in the purse without in the little know where he be
he have find out that the other coin be french and italian and one coin say they be in this town and another say they be in that but the shilling be unable to make out or imagine what they mean
1 man certainly can not see much of the world if he be tie up in 1 bag and this be really the shilling fate
but one day as he be lie in the purse he notice that it be not quite close and so he slip near to the opening to have 1 little peep into society
he certainly have not the little idea of what would follow but he be curious and curiosity often bring its own punishment
in his eagerness he come so near the edge of the purse that he slip out into the pocket of the trouser and when in the evening the purse be take out the shilling be leave behind in the corner to which it have fall
as the clothes be be carry into the hall the shilling fall out on the floor unheard and unnoticed by any one
the next morning the clothes be take back to the room the gentleman put them on and start on his journey again but the shilling remain behind on the floor
after 1 time it be find and be consider 1 good coin be place with 3 other coin
ah think the shilling this be pleasant i shall now see the world become acquaint with other people and learn other custom do you call that 1 shilling
say some one the next moment
that be not 1 genuine coin of the country it be false it be good for nothing now begin the story as it be afterwards relate by the shilling himself
false
good for nothing
say he
that remark go through and through me like 1 dagger
i know that i have 1 true ring and that mine be 1 genuine stamp
these people must at all event be wrong or they can not mean me
but yes i be the one they call false and good for nothing then_i must pay it away in the dark say the man who have receive me
so i be to be get rid of in the darkness and be again insult in broad daylight
false
good for nothing
oh i must contrive to get lose think i_and_i tremble between the finger of the people every time they try to pass me off slyly as 1 coin of the country
ah
unhappy shilling that i be
of what use be my silver my stamp and my real value here where all these quality be worthless
in the eye of the world 1 man be value just according_to the opinion form of him
it must be 1 shock thing to have 1 guilty conscience and to be sneak about on account of wicked deed
as for me innocent as i be i can not help shudder before their eye whenever they bring me out for i know i should be throw back again up the table as 1 false pretender
at length i be pay away to 1 poor old woman who receive me as wage for 1 hard day work
but she can not again get rid of me no_one would take me
i be to the woman 1 many unlucky shilling
i be positively oblige to pass this shilling to somebody say she i can not with the good intention lie by 1 bad shilling
the rich baker shall have it he can bear the loss good than i can
but after all it be not 1 right thing to do ah
sigh i to myself be i also to be 1 burden on the conscience of this poor woman
be i then in my old day so completely change
the woman offer me to the rich baker but he know the current money too well and as soon as he receive me he throw me almost in the womans face
she can get no bread for me and i feel quite grieve to the heart that i should be cause of so much trouble to another and be treat as 1 castoff coin
i who in my young day feel so joyful in the certainty of my own value and know so well that i bear 1 genuine stamp
i be as sorrowful now as 1 poor shilling can be when nobody will have him
the woman take me home again with her and look at me very earnestly she say no_i will not try to deceive any one with thee again
i will bear 1 hole through thee that everyone may know that thou art 1 false and worthless thing and yet why should i do that
very likely thou art 1 lucky shilling
1 thought have just strike me that it be so and i believe it
yes i will make 1 hole in the shilling say she and run 1 string through it and then give it to my neighbour little one to hang round her neck as 1 lucky shilling so she drill 1 hole through me
it be really not at all pleasant to have 1 hole bore through one but we can submit to 1 great deal when it be do with 1 good intention
1 string be draw through the hole and i become 1 kind of medal
they hang me round the neck of 1 little child and the child laugh at me and kiss me and i rest for one whole night on the warm innocent breast of 1 child
in the morning the childs mother take me between her finger and have certain thought about me which i very soon find out
first she look for 1 pair of scissor and cut the string
lucky shilling
say she certainly this be what i mean to try then she lay me in vinegar till i become quite green and after that she fill up the hole with cement rub me 1 little to brighten me up and go out in the twilight hour to the lottery collector to buy herself 1 ticket with 1 shilling that should bring luck
how everything seem to cause me trouble
the lottery collector press me so hard that i think i should crack
i have be call false i have be throw away that i know and there be many shilling and coin with inscription and stamp of all kind lie about
i well know how proud they be so i avoid them from very shame
with the collector be several man who seem to have 1 great deal to do so i fall unnoticed into 1 chest among several other coin
whether the lottery ticket gain 1 prize i know not but this i know that in 1 very few day after i be recognize as 1 bad shilling and lay aside
everything that happen seem always to add to my sorrow
even_if 1 man have 1 good character it be of no use for him to deny what be say of him for he be not consider 1 impartial judge of himself
1 year pass and in this way i have be change from hand to hand always abuse always look at with displeasure and trust by no_one but i trust in myself and have no confidence in the world
yes that be 1 very dark time
at length one day i be pass to 1 traveller 1 foreigner the very same who have bring me away from home and he be simple and truehearted enough to take me for current coin
but would he also attempt to pass me
and should i again hear the outcry false
goodfornothing
the traveller examine me attentively i take thee for_good coin say he then suddenly 1 smile spread all over his face
i have never see such 1 smile on any other face as on his
now this be singular say he it be 1 coin from my own country 1 good true shilling from home
some one have bore 1 hole through it and people have no_doubt call it false
how curious that it should come into my hand
i will take it home with me to my own house joy thrill through me when i hear this
i have be once many call 1 good honest shilling and i be to go back to my own home where each and all would recognize me and know that i be make of good silver and bear 1 true genuine stamp
i should have be glad in my joy to throw out spark of fire but it have never at any time be my nature to sparkle
steel can do so but not silver
i be wrap up in fine white paper that i may not mix with the other coin and be lose and on special occasion when people from my own country happen to be present i be bring forward and speak of very kindly
they say i be very interest and it be really quite worth while to notice that those who be interest have often not 1 single word to say for themselves
at length i reach home
all my care be at 1 end
joy again overwhelm me for be i not good silver and have i not 1 genuine stamp
i have no many insult or disappointment to endure although indeed there be 1 hole through me as_if i be false but suspicion be nothing when 1 man be really true and every one should persevere in act honestly for 1 will be make right in time
that be my firm belief say the shilling
the_end
