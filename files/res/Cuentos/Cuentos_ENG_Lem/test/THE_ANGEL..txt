the_angel_whenever 1 good child die 1 angel of god come down from heaven take the dead child in his arm spread out his great white wing and fly with him over all the place which the child have love during his life
then he gather 1 large handful of flower which he carry up to the almighty that they may bloom many brightly in heaven than they do on earth
and the almighty press the flower to his heart but he kiss the flower that please him good and it receive 1 voice and be able to join the song of the chorus of bliss these word be speak by 1 angel of god as he carry 1 dead child up to heaven and the child listen as_if in 1 dream
then they pass over wellknown spot where the little one have often play and through beautiful garden full of lovely flower
which of these shall we take with us to heaven to be transplant there
ask the angel
close by grow 1 slender beautiful rosebush but some wicked hand have break the stem and the halfopened rosebud hang fade and wither on the trail branch
poor rosebush
say the child let us take it with us to heaven that it may bloom above in gods garden the angel take up the rosebush then he kiss the child and the little 1/2 open his eye
the angel gather also some beautiful flower as_well as 1 few humble buttercup and heartsease
now we have flower enough say the child but the angel only nod he do not fly upward to heaven
it be night and quite still in the great town
here they remain and the angel hover over 1 small narrow street in which lie 1 large heap of straw ash and sweeping from the house of people who have remove
there lie fragment of plate piece of plaster rag old hat and other rubbish not pleasant to see
amidst all this confusion the angel point to the piece of 1 break flowerpot and to 1 lump of earth which have fall out of it
the earth have be keep from fall to piece by the root of 1 wither fieldflower which have be throw amongst the rubbish
we will take this with us say the angel i will tell you why as we fly along and as they fly the angel relate the history
down in that narrow lane in 1 low cellar live 1 poor sick boy he have be afflict from his childhood and even in his good day he can just manage to walk up and down the room on crutch once or twice but no many
during some day in summer the sunbeam would lie on the floor of the cellar for about half 1 hour
in this spot the poor sick boy would sit warm himself in the sunshine and watch the red blood through his delicate finger as he hold them before his face
then he would say he have be out yet he know nothing of the green forest in its spring verdure till 1 neighbour son bring him 1 green bough from 1 beechtree
this he would place over his head and fancy that he be in the beechwood while the sun shine and the bird carol gayly
one spring day the neighbour boy bring him some fieldflowers and among them be 1 to which the root still adhere
this he carefully plant in 1 flowerpot and place in 1 windowseat near his bed
and the flower have be plant by 1 fortunate hand for it grow put forth fresh shoot and blossom every year
it become 1 splendid flowergarden to the sick boy and his little treasure upon earth
he water it and cherish it and take care it should have the benefit of every sunbeam that find its way into the cellar from the earliest morning ray to the evening sunset
the flower entwine itself even in his dream for him it bloom for him spread its perfume
and it gladden his eye and to the flower he turn even in death when the lord call him
he have be 1 year with god
during that time the flower have stand in the window wither and forget till at length cast out among the sweeping into the street on the day of the lodger removal
and this poor flower wither and fade as it be we have add to our nosegay because it give many real joy than the many beautiful flower in the garden of 1 queen but how do you know all this
ask the child whom the angel be carry to heaven
i know it say the angel because i myself be the poor sick boy who walk upon crutch and i know my own flower well then the child open his eye and look into the glorious happy face of the angel and at the same moment they find themselves in that heavenly home where all be happiness and joy
and god press the dead child to his heart and wing be give him so_that he can fly with the angel hand_in_hand
then the almighty press all the flower to his heart but he kiss the wither fieldflower and it receive 1 voice
then it join in the song of the angel who surround the throne some near and other in 1 distant circle but all equally happy
they all join in the chorus of praise both great and small the good happy child and the poor fieldflower that once lay wither and cast away on 1 heap of rubbish in 1 narrow dark street
the_end
