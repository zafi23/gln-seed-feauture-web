by_the_almshouse_window_near the grasscovered rampart which encircle copenhagen lie 1 great red house
balsam and other flower greet us from the long row of window in the house whose interior be sufficiently povertystricken and poor and old be the people who inhabit it
the building be the warton_almshouse
look
at the window there lean 1 old maid
she pluck the wither leaf from the balsam and look at the grasscovered rampart on which many child be play
what be the old maid think of
1 whole life drama be unfold itself before her inward gaze
the poor little child how happy they be how merrily they play and romp together
what red cheek and what angel eye
but they have no shoe nor stocking
they dance on the green rampart just on the place where according_to the old story the ground always sink in and where 1 sportive frolicsome child have be lure by mean of flower toy and sweetmeat into 1 open grave ready dig for it and which be afterwards close over the child and from that moment the old story say the ground give way no_longer the mound remain firm and fast and be quickly cover with the green turf
the little people who now play on that spot know nothing of the old tale else would they fancy they hear 1 child cry deep below the earth and the dewdrops on each blade of grass would be to them tear of woe
nor do they know anything of the danish_king who here in the face of the coming foe take 1 oath before all his tremble courtier that he would hold out with the citizen of his capital and die here in his nest they know nothing of the man who have fight here or of the woman who from here have drench with boil water the enemy clad in white and bide in the snow to surprise the city
no the poor little one be play with light childish spirit
play on play on thou little maiden
soon the year will come yes those glorious year
the priestly hand have be lay on the candidate for confirmation hand_in_hand they walk on the green rampart
thou hast 1 white frock on it have cost thy mother much labor and yet it be only cut down for thee out of 1 old large dress
you will also wear 1 red shawl and what if it hang too far down
people will only see how large how very large it be
you be think of your dress and of the giver of all good so glorious be it to wander on the green rampart
and the year roll by they have no lack of dark day but you have your cheerful young spirit and you have gain 1 friend you know not how
you meet oh how often
you walk together on the rampart in the fresh spring on the high day and holiday when all the world come out to walk upon the rampart and all the bell of the church steeple seem to be sing 1 song of praise for the coming spring
scarcely have the violet come forth but there on the rampart just opposite the beautiful castle_of_rosenberg there be 1 tree bright with the 1 green bud
every year this tree send forth fresh green shoot
alas
it be not so with the human heart
dark mist many in number than those that cover the northern sky cloud the human heart
poor child
thy friend bridal chamber be 1 black coffin and thou becomest 1 old maid
from the almshouse window behind the balsam thou shalt look on the merry child at play and shalt see thine own history renew and that be the life drama that pass before the old maid while she look out upon the rampart the green sunny rampart where the child with their red cheek and bare shoeless foot be rejoice merrily like the other free little bird
the_end
