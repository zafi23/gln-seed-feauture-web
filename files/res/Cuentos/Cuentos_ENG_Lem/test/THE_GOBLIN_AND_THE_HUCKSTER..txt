the_goblin_and_the_huckster_there be once 1 regular student who live in 1 garret and have no possession
and there be also 1 regular huckster to whom the house belong and who occupy the ground floor
1 goblin live with the huckster because at christmas he always have 1 large dish full of jam with 1 great piece of butter in the middle
the huckster can afford this and therefore the goblin remain with the huckster which be very cunning of him
one evening the student come into the shop through the back door to buy candle and cheese for himself he have no_one to send and therefore he come himself he obtain what he wish and then the huckster and his wife nod good evening to him and she be 1 woman who can do many than merely nod for she have usually plenty to say for herself
the student nod in return as he turn to leave then suddenly stop and begin read the piece of paper in which the cheese be wrap
it be 1 leaf tear out of 1 old book 1 book that ought not to have be tear up for it be full of poetry
yonder lie some many of the same sort say the huckster i give 1 old woman 1 few coffee berry for it you shall have the rest for sixpence if you will indeed_i will say the student give me the book instead_of the cheese i can eat my bread and butter without cheese
it would be 1 sin to tear up 1 book like this
you be 1 clever man and 1 practical man but you understand no many about poetry than that cask yonder this be 1 very rude speech especially against the cask but the huckster and the student both laugh for it be only say in fun
but the goblin feel very angry that any man should venture to say such thing to 1 huckster who be 1 householder and sell the good butter
as soon as it be night and the shop close and every one in bed except the student the goblin step softly into the bedroom where the huckster wife sleep and take away her tongue which of course she do not then want
whatever object in the room he place his tongue upon immediately receive voice and speech and be able to express its thought and feeling as readily as the lady herself can do
it can only be use by one object at 1 time which be 1 good thing as 1 number speak at once would have cause great confusion
the goblin lay the tongue upon the cask in which lie 1 quantity of old newspaper
be it really true he ask that you do not know what poetry be
of course i know reply the cask poetry be something that always stand in the corner of 1 newspaper and be sometimes cut out and i may venture to affirm that i have many of it in me than the student have and i be only 1 poor tub of the huckster then the goblin place the tongue on the coffee mill and how it do go to be sure
then he put it on the butter tub and the cash box and they all express the same opinion as the wastepaper tub and 1 majority must always be respect
now i shall go and tell the student say the goblin and with these word he go quietly up the back stair to the garret where the student live
he have 1 candle burning still and the goblin peep through the keyhole and saw that he be read in the tear book which he have bring out of the shop
but how light the room be
from the book shoot forth 1 ray of light which grow broad and full like the stem of 1 tree from which bright ray spread upward and over the student head
each leaf be fresh and each flower be like 1 beautiful female head some with dark and sparkle eye and other with eye that be wonderfully blue and clear
the fruit gleam like star and the room be fill with sound of beautiful music
the little goblin have never imagine much less see or hear of any sight so glorious as this
he stand still on tiptoe peep in till the light go out in the garret
the student no_doubt have blow out his candle and go to bed but the little goblin remain stand there nevertheless and listen to the music which still sound on soft and beautiful 1 sweet cradlesong for the student who have lie down to rest this be 1 wonderful place say the goblin i never expect such 1 thing
i should like to stay here with the student and the little man think it over for he be 1 sensible little spirit
at_last he sigh but the student have no jam
so he go down stair again into the huckster shop and it be 1 good thing he get back when he do for the cask have almost wear out the ladys tongue he have give 1 description of all that he contain on one side and be just about to turn himself over to the other side to describe what be there when the goblin enter and restore the tongue to the lady
but from that time forward the whole shop from the cash box down to the pinewood log form their opinion from that of the cask and they all have such confidence in him and treat him with so much respect that when the huckster read the criticism on theatricals and art of 1 evening they fancy it must all come from the cask
but after what he have see the goblin can no_longer sit and listen quietly to the wisdom and understand down stair so as soon as the evening light glimmer in the garret he take courage for it seem to him as_if the ray of light be strong cable draw him up and oblige him to go and peep through the keyhole and while there 1 feeling of vastness come over him such as we experience by the evermoving sea when the storm break forth and it bring tear into his eye
he do not himself know why he weep yet 1 kind of pleasant feeling mingle with his tear
how wonderfully glorious it would be to sit with the student under such 1 tree but that be out of the question he must be content to look through the keyhole and be thankful for even that
there he stand on the old landing with the autumn wind blow down upon him through the trapdoor
it be very cold but the little creature do not really feel it till the light in the garret go out and the tone of music die away
then how he shiver and creep down stair again to his warm corner where it feel homelike and comfortable
and when christmas come again and bring the dish of jam and the great lump of butter he like the huckster good of all
soon after in the middle of the night the goblin be awake by 1 terrible noise and knock against the window shutter and the house door and by the sound of the watchmans horn for 1 great fire have break out and the whole street appear full of flame
be it in their house or 1 neighbour
no_one can tell for terror have seize upon all
the huckster wife be so bewilder that she take her gold earring out of her ear and put them in her pocket that she may save something at_least
the huckster run to get his business paper and the servant resolve to save her blue silk mantle which she have manage to buy
each wish to keep the good thing they have
the goblin have the same wish for with one spring he be up stair and in the student room whom he find stand by the open window and look quite calmly at the fire which be rage at the house of 1 neighbour opposite
the goblin catch up the wonderful book which lie on the table and pop it into his red cap which he hold tightly with both hand
the great treasure in the house be save and he run away with it to the roof and seat himself on the chimney
the flame of the burning house opposite illuminate him as he sit both hand press tightly over his cap in which the treasure lay and then he find out what feeling really reign in his heart and know exactly which way they tend
and yet when the fire be extinguish and the goblin again begin to reflect he hesitate and say at_last i must divide myself between the 2 i can not quite give up the huckster because_of the jam and this be 1 representation of human nature
we be like the goblin we all go to visit the huckster because_of the jam the_end
