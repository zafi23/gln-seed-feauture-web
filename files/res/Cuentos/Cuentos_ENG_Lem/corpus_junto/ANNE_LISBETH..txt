anne_lisbeth_anne_lisbeth be 1 beautiful young woman with 1 red and white complexion glitter white tooth and clear soft eye and her footstep be light in the dance but her mind be lighter still
she have 1 little child not at all pretty so he be put out to be nurse by 1 laborer wife and his mother go to the count castle
she sit in splendid room richly decorate with silk and velvet not 1 breath of air be allow to blow upon her and no_one be allow to speak to her harshly for she be nurse to the count child
he be fair and delicate as 1 prince and beautiful as 1 angel and how she love this child
her own boy be provide for by being at the laborer where the mouth water many frequently than the pot boil and where in general no_one be at home to take care of the child
then he would cry but what nobody know nobody care for so he would cry till he be tire and then fall asleep and while we be asleep we can feel neither hunger nor thirst
ah yes sleep be 1 capital invention
as year go on anne_lisbeths child grow apace like weed although they say his growth have be stunt
he have become quite 1 member of the family in which he dwell they receive money to keep him so_that his mother get rid of him altogether
she have become quite 1 lady she have 1 comfortable home of her own in the town and out of door when she go for 1 walk she wear 1 bonnet but she never walk out to see the laborer that be too far from the town and indeed she have nothing to go for the boy now belong to these labor people
he have food and he can also do something towards earn his living he take care of marys red cow for he know how to tend cattle and make himself useful
the great dog by the yard gate of 1 noblemans mansion sit proudly on the top of his kennel when the sun shine and bark at every one that pass but if it rain he creep into his house and there he be warm and dry
anne_lisbeths boy also sit in the sunshine on the top of the fence cut out 1 little toy
if it be springtime he know of 3 strawberryplants in blossom which would certainly bear fruit
this be his many hopeful thought though it often come to nothing
and he have to sit out in the rain in the bad weather and get wet to the skin and let the cold wind dry the clothes on his back afterwards
if he go near the farmyard belonging to the count he be push and knock about for the man and the maid say he be so horrible ugly but he be use to all this for nobody love him
this be how the world treat anne_lisbeths boy and how can it be otherwise
it be his fate to be beloved by no_one
hitherto he have be 1 land crab the land at_last cast him adrift
he go to sea in 1 wretched vessel and sit at the helm while the skipper sit over the grogcan
he be dirty and ugly halffrozen and halfstarved he always look as_if he never have enough to eat which be really the case
late in the autumn when the weather be rough windy and wet and the cold penetrate through the thick clothing especially at sea 1 wretched boat go out to sea with only 2 man on board or many correctly 1 man and 1/2 for it be the skipper and his boy
there have only be 1 kind of twilight all day and it soon grow quite dark and so bitterly cold that the skipper take 1 dram to warm him
the bottle be old and the glass too
it be perfect in the upper part but the foot be break off and it have therefore be fix upon 1 little carve block of wood paint blue
1 dram be 1 great comfort and 2 be well still think the skipper while the boy sit at the helm which he hold fast in his hard seam hand
he be ugly and his hair be mat and he look cripple and stunt they call him the fieldlaborers boy though in the church register he be enter as anne_lisbeths son
the wind cut through the rig and the boat cut through the sea
the sail fill by the wind swell out and carry them along in wild career
it be wet and rough above and below and may still be bad
hold
what be that
what have strike the boat
be it 1 waterspout or 1 heavy sea roll suddenly upon them
heaven help us
cry the boy at the helm as the boat heel over and lay on its beam end
it have strike on 1 rock which rise from the depth of the sea and sink at once like 1 old shoe in 1 puddle
it sink at once with mouse and man as the saying be
there may have be mouse on board but only one man and 1/2 the skipper and the laborer boy
no_one saw it but the skim seagull and the fish beneath the water and even they do not see it properly for they dart back with terror as the boat fill with water and sink
there it lie scarcely 1 fathom below the surface and those 2 be provide for bury and forget
the glass with the foot of blue wood be the only thing that do not sink for the wood float and the glass drift away to be cast upon the shore and break where and when be indeed of no consequence
it have serve its purpose and it have be love which anne_lisbeths boy have not be
but in heaven no soul will be able to say never love anne_lisbeth have now live in the town many year she be call madame and feel dignify in consequence she remember the old noble day in which she have drive in the carriage and have associate with countess and baroness
her beautiful noble child have be 1 dear angel and possess the kind heart he have love her so much and she have love him in return they have kiss and love each other and the boy have be her joy her 2 life
now he be 14 year of age tall handsome and clever
she have not see him since she carry him in her arm neither have she be for year to the count palace it be quite 1 journey thither from the town
i must make one effort to go say anne_lisbeth to see my darling the count sweet child and press him to my heart
certainly he must long to see me too the young count no_doubt he think of me and love me as in those day when he would fling his angelarms round my neck and lisp anne_liz_it be music to my ear
yes i must make 1 effort to see him again she drive across the country in 1 grazier cart and then get out and continue her journey on foot and thus reach the count castle
it be as great and magnificent as it have always be and the garden look the same as ever all the servant be stranger to her not one of them know anne_lisbeth nor of what consequence she have once be there but she feel sure the countess would soon let them know it and her darling boy too how she long to see him
now that anne_lisbeth be at her journey end she be keep wait 1 long time and for those who wait time pass slowly
but before the great people go in to dinner she be call in and speak to very graciously
she be to go in again after dinner and then she would see her sweet boy once many
how tall and slender and thin he have grow but the eye and the sweet angel mouth be still beautiful
he look at her but he do not speak he certainly do not know who she be
he turn round and be go away but she seize his hand and press it to her lip
well well he say and with that he walk out of the room
he who fill her every think
he whom she love good and who be her whole earthly pride
anne_lisbeth go forth from the castle into the public road feeling mournful and sad he whom she have nurse day and night and even now carry about in her dream have be cold and strange and have not 1 word or think respect her
1 great black raven dart down in front of her on the high road and croak dismally
ah say she what bird of ill omen art thou
presently she pass the laborer hut his wife stand at the door and the 2 woman speak to each other
you look well say the woman youre fat and plump you be well off oh yes answer anne_lisbeth
the boat go down with them continue the woman hans the skipper and the boy be both drown so theres 1 end of them
i always think the boy would be able to help me with 1 few dollar
hell never cost you anything many anne_lisbeth_so they be drown repeat anne_lisbeth but she say no many and the subject be drop
she feel very lowspirited because her countchild have show no inclination to speak to her who love him so well and who have travel so far to see him
the journey have cost money too and she have derive no great pleasure from it
still she say not 1 word of all this she can not relieve her heart by tell the laborer wife lest the latter should think she do not enjoy her former position at the castle
then the raven fly over her scream again as he fly
the black wretch
say anne_lisbeth he will end by frighten me today she have bring coffee and chicory with her for she think it would be 1 charity to the poor woman to give them to her to boil 1 cup of coffee and then she would take 1 cup herself
the woman prepare the coffee and in_the_meantime anne_lisbeth seat her in 1 chair and fall asleep
then she dream of something which she have never dream before singularly enough she dream of her own child who have weep and hunger in the laborer hut and have be knock about in heat and in cold and who be now lie in the depth of the sea in 1 spot only know by god
she fancy she be still sit in the hut where the woman be busy prepare the coffee for she can smell the coffeeberries roast
but suddenly it seem to her that there stand on the threshold 1 beautiful young form as beautiful as the count child and this apparition say to her the world be pass away hold fast to me for you be my mother after all you have 1 angel in heaven hold me fast and the childangel stretch out his hand and seize her
then there be 1 terrible crash as of 1 world crumble to piece and the angelchild be rise from the earth and hold her by the sleeve so tightly that she feel herself lift from the ground but on_the_other_hand something heavy hang to her foot and drag her down and it seem as_if hundred of woman be cling to her and cry if thou art to be save we must be save too
hold fast hold fast and then they all hang on her but there be too many and as they cling the sleeve be tear and anne_lisbeth fall down in horror and awake
indeed she be on the point of fall over in reality with the chair on which she sit but she be so startle and alarm that she can not remember what she have dream only that it be something very dreadful
they drink their coffee and have 1 chat together and then anne_lisbeth go away towards the little town where she be to meet the carrier who be to drive her back to her own home
but when she come to him she find that he would not be ready to start till the evening of the next day
then she begin to think of the expense and what the distance would be to walk
she remember that the route by the seashore be LN_mi:2 short than by the high road and as the weather be clear and there would be moonlight she determine to make her way on foot and to start at once that she may reach home the next day
the sun have set and the evening bell sound through the air from the tower of the village church but to her it be not the bell but the cry of the frog in the marsh
then they cease and all around become still not 1 bird can be hear they be all at rest even the owl have not leave her hide place deep silence reign on the margin of the wood by the seashore
as anne_lisbeth walk on she can hear her own footstep in the sand even the wave of the sea be at rest and all in the deep water have sink into silence
there be quiet among the dead and the living in the deep sea
anne_lisbeth walk on think of nothing at all as people say or rather her thought wander but not away from her for thought be never absent from us it only slumber
many thought that have lie dormant be rouse at the proper time and begin to stir in the mind and the heart and seem even to come upon us from above
it be write that 1 good deed bear 1 blessing for its fruit and it be also write that the wage of sin be death
much have be say and much write which we pass over or know nothing of
1 light arise within us and then forget thing make themselves remember and thus it be with anne_lisbeth
the germ of every vice and every virtue lie in our heart in yours and in mine they lie like little grain of seed till 1 ray of sunshine or the touch of 1 evil hand or you turn the corner to the right or to the left and the decision be make
the little seed be stir it swell and shoot up and pour its sap into your blood direct your course either for_good or evil
troublesome thought often exist in the mind ferment there which be not realize by us while the sense be as it be slumber but still they be there
anne_lisbeth walk on thus with her sense half asleep but the thought be ferment within her
from one shrove [M:??/??/??:????:??] to another much may occur to weigh down the heart it be the reckoning of 1 whole year much may be forget sin against heaven in word and think sin against our neighbour and against our own conscience
we be scarcely aware of their existence and anne_lisbeth do not think of any of her error
she have commit no crime against the law of the land she be 1 honorable person in 1 good position that she know
she continue her walk along by the margin of the sea
what be it she saw lie there
1 old hat 1 man hat
now when may that have be wash overboard
she draw near she stop to look at the hat ha
what be lie yonder
she shudder yet it be nothing save 1 heap of grass and tangle seaweed fling across 1 long stone but it look like 1 corpse
only tangle grass and yet she be frighten at it
as she turn to walk away much come into her mind that she have hear in her childhood old superstition of spectre by the seashore of the ghost of drown but unburied people whose corpse have be wash up on the desolate beach
the body she know can do no harm to any one but the spirit can pursue the lonely wanderer attach itself to him and demand to be carry to the churchyard that it may rest in consecrate ground
hold fast
hold fast
the spectre would cry and as anne_lisbeth murmur these word to herself the whole of her dream be suddenly recall to her memory when the mother have cling to her and utter these word when amid the crash of world her sleeve have be tear and she have slip from the grasp of her child who want to hold her up in that terrible hour
her child her own child which she have never love lie now bury in the sea and may rise up like 1 spectre from the water and cry hold fast carry me to consecrate ground
as these thought pass through her mind fear give speed to her foot so_that she walk fast and fast
fear come upon her as_if 1 cold clammy hand have be lay upon her heart so_that she almost faint
as she look across the sea all there grow dark 1 heavy mist come roll onwards and cling to bush and tree distort them into fantastic shape
she turn and glance at the moon which have rise behind her
it look like 1 pale rayless surface and 1 deadly weight seem to hang upon her limb
hold think she and then she turn round TM_s:1 time to look at the moon
1 white face appear quite close to her with 1 mist hanging like 1 garment from its shoulder
stop
carry me to consecrate earth sound in her ear in strange hollow tone
the sound do not come from frog or raven she saw no sign of such creature
1 grave
dig me 1 grave
be repeat quite loud
yes it be indeed the spectre of her child
the child that lie beneath the ocean and whose spirit can have no rest until it be carry to the churchyard and until 1 grave have be dig for it in consecrate ground
she would go there at once and there she would dig
she turn in the direction of the church and the weight on her heart seem to grow lighter and even to vanish altogether but when she turn to go home by the short way it return
stop
stop
and the word come quite clear though they be like the croak of 1 frog or the wail of 1 bird
1 grave
dig me 1 grave
the mist be cold and damp her hand and face be moist and clammy with horror 1 heavy weight again seize her and cling to her her mind become clear for thought that have never before be there
in these northern region 1 beechwood often bud in 1 single night and appear in the morning sunlight in its full glory of youthful green
so in 1 single instant can the consciousness of the sin that have be commit in thought word and action of our past life be unfold to us
when once the conscience be awaken it spring up in the heart spontaneously and god awaken the conscience when we little expect it
then we can find no excuse for ourselves the deed be there and bear witness against us
the thought seem to become word and to sound far out into the world
we be horrify at the thought of what we have carry within us and at the consciousness that we have not overcome the evil which have its origin in thoughtlessness and pride
the heart conceal within itself the vice as_well as the virtue and they grow in the shallow ground
anne_lisbeth now experience in think what we have clothe in word
she be overpower by them and sink down and creep along for some distance on the ground
1 grave
dig me 1 grave
sound again in her ear and she would have gladly bury herself if in the grave she can have find forgetfulness of her action
it be the 1 hour of her awakening full of anguish and horror
superstition make her alternately shudder with cold or burn with the heat of fever
many thing of which she have fear even to speak come into her mind
silently as the cloudshadows in the moonshine 1 spectral apparition flit by her she have hear of it before
close by her gallop 4 snort steeds with fire flash from their eye and nostril
they drag 1 burning coach and within it sit the wicked lord of the manor who have rule there 100 year before
the legend say that every night at 12 oclock he drive into his castleyard and out again
he be not as pale as dead man be but black as 1 coal
he nod and point to anne_lisbeth cry out hold fast
hold fast
and then you may ride again in 1 noblemans carriage and forget your child she gather herself up and hasten to the churchyard but black cross and black raven dance before her eye and she can not distinguish one from the other
the raven croak as the raven have do which she saw in the daytime but now she understand what they say
i be the ravenmother i be the ravenmother each raven croak and anne_lisbeth feel that the name also apply to her and she fancy she should be transform into 1 black bird and have to cry as they cry if she do not dig the grave
and she throw herself upon the earth and with her hand dig 1 grave in the hard ground so_that the blood run from her finger
1 grave
dig me 1 grave
still sound in her ear she be fearful that the cock may crow and the 1 red streak appear in the east before she have finish her work and then she would be lose
and the cock crow and the day dawn in the east and the grave be only half dig
1 icy hand pass over her head and face and down towards her heart
only half 1 grave 1 voice wail and flee away
yes it flee away over the sea it be the ocean spectre and exhaust and overpower anne_lisbeth sink to the ground and her sense leave her
it be 1 bright day when she come to herself and 2 man be raise her up but she be not lie in the churchyard but on the seashore where she have dig 1 deep hole in the sand and cut her hand with 1 piece of break glass whose sharp stern be stick in 1 little block of paint wood
anne_lisbeth be in 1 fever
conscience have rouse the memory of superstition and have so act upon her mind that she fancy she have only half 1 soul and that her child have take the other half down into the sea
never would she be able to cling to the mercy of heaven till she have recover this other half which be now hold fast in the deep water
anne_lisbeth return to her home but she be no_longer the woman she have be
her thought be like 1 confuse tangle skein only one thread only one think be clear to her namely that she must carry the spectre of the seashore to the churchyard and dig 1 grave for him there that by so do she may win back her soul
many 1 night she be miss from her home and be always find on the seashore wait for the spectre
in this way 1 whole year pass and then one night she vanish again and be not to be find
the whole of the next day be spend in 1 useless search after her
towards evening when the clerk enter the church to toll the vesper bell he saw by the altar anne_lisbeth who have spend the whole day there
her power of body be almost exhaust but her eye flash brightly and on her cheek be 1 rosy flush
the last ray of the setting sun shine upon her and gleam over the altar upon the shine clasp of the bible which lay open at the word of the prophet joel_rend your heart and not your garment and turn unto the lord_that be just 1 chance people say but do thing happen by_chance
in the face of anne_lisbeth light up by the evening sun can be see peace and rest
she say she be happy now for she have conquer
the spectre of the shore her own child have come to her the night before and have say to her thou hast dig me only half 1 grave but thou hast now for 1 year and 1 day bury me altogether in thy heart and it be there 1 mother can good hide her child
and then he give her back her lose soul and bring her into the church
now i be in the house of god she say and in that house we be happy when the sun set anne_lisbeths soul have rise to that region where there be no many pain and anne_lisbeths trouble be at 1 end
the_end
