the_little_matchseller_it be terribly cold and nearly dark on the last evening of the old year and the snow be fall fast
in the cold and the darkness 1 poor little girl with bare head and naked foot roam through the street
it be true she have on 1 pair of slipper when she leave home but they be not of much use
they be very large so large indeed that they have belong to her mother and the poor little creature have lose them in run across the street to avoid 2 carriage that be roll along at 1 terrible rate
one of the slipper she can not find and 1 boy seize upon the other and run away with it say that he can use it as 1 cradle when he have child of his own
so the little girl go on with her little naked foot which be quite red and blue with the cold
in 1 old apron she carry 1 number of match and have 1 bundle of them in her hand
no_one have buy anything of her the whole day nor have any one give here even 1 penny
shiver with cold and hunger she creep along poor little child she look the picture of misery
the snowflake fall on her long fair hair which hang in curl on her shoulder but she regard them not
light be shine from every window and there be 1 savory smell of roast goose for it be newyears eve yes she remember that
in 1 corner between 2 house one of which project beyond the other she sink down and huddle herself together
she have draw her little foot under her but she can not keep off the cold and she dare not go home for she have sell no match and can not take home even 1 penny of money
her father would certainly beat her besides it be almost as cold at home as here for they have only the roof to cover them through which the wind howl although the large hole have be stop up with straw and rag
her little hand be almost freeze with the cold
ah
perhaps 1 burning match may be some good if she can draw it from the bundle and strike it against the wall just to warm her finger
she draw one outscratch
how it sputter as it burn
it give 1 warm bright light like 1 little candle as she hold her hand over it
it be really 1 wonderful light
it seem to the little girl that she be sit by 1 large iron stove with polish brass foot and 1 brass ornament
how the fire burn
and seem so beautifully warm that the child stretch out her foot as_if to warm them when lo
the flame of the match go out the stove vanish and she have only the remain of the halfburnt match in her hand
she rub another match on the wall
it burst into 1 flame and where its light fall upon the wall it become as transparent as 1 veil and she can see into the room
the table be cover with 1 snowy white tablecloth on which stand 1 splendid dinner service and 1 steam roast goose stuff with apple and dry plum
and what be still many wonderful the goose jump down from the dish and waddle across the floor with 1 knife and fork in its breast to the little girl
then the match go out and there remain nothing but the thick damp cold wall before her
she light another match and then she find herself sitting under 1 beautiful christmastree
it be large and many beautifully decorate than the one which she have see through the glass door at the rich merchant
thousand of taper be burn upon the green branch and color picture like those she have see in the showwindows look down upon it all
the little one stretch out her hand towards them and the match go out
the christmas light rise high and high till they look to her like the star in the sky
then she saw 1 star fall leave behind it 1 bright streak of fire
some one be die think the little girl for her old grandmother the only one who have ever love her and who be now dead have tell her that when 1 star fall 1 soul be go up to god
she again rub 1 match on the wall and the light shine round her in the brightness stand her old grandmother clear and shine yet mild and love in her appearance
grandmother cry the little one o take me with you i know you will go away when the match burn out you will vanish like the warm stove the roast goose and the large glorious christmastree_and she make haste to light the whole bundle of match for she wish to keep her grandmother there
and the match glow with 1 light that be bright than the noonday and her grandmother have never appear so large or so beautiful
she take the little girl in her arm and they both fly upwards in brightness and joy far above the earth where there be neither cold nor hunger nor pain for they be with god
in the dawn of morning there lie the poor little one with pale cheek and smile mouth leaning against the wall she have be freeze to death on the last evening of the year and the newyears sun rise and shine upon 1 little corpse
the child still sit in the stiffness of death hold the match in her hand one bundle of which be burn
she try to warm herself say some
no_one imagine what beautiful thing she have see nor into what glory she have enter with her grandmother on newyears day
the_end
