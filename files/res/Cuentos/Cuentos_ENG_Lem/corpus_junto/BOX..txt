the_moneybox_in 1 nursery where 1 number of toy lie scatter about 1 moneybox stand on the top of 1 very high wardrobe
it be make of clay in the shape of 1 pig and have be buy of the potter
in the back of the pig be 1 slit and this slit have be enlarge with 1 knife so_that dollar or crown piece may slip through and indeed there be 2 in the box besides 1 number of penny
the moneypig be stuff so full that it can no_longer rattle which be the high state of perfection to which 1 moneypig can attain
there he stand upon the cupboard high and lofty look down upon everything else in the room
he know very well that he have enough inside him to buy up all the other toy and this give him 1 very good opinion of his own value
the rest think of this fact also although they do not express it for there be so many other thing to talk about
1 large doll still handsome though rather old for her neck have be mend lay inside one of the drawer which be partly open
she call out to the other let us have 1 game at be man and woman that be something worth play at upon this there be 1 great uproar even the engraving which hang in frame on the wall turn round in their excitement and show that they have 1 wrong side to them although they have not the little intention to expose themselves in this way or to object to the game
it be late at night but as the moon shine through the window they have light at 1 cheap rate
and as the game be now to begin all be invite to take part in it even the childrens wagon which certainly belong to the coarse plaything
each have its own value say the wagon we can not all be nobleman there must be some to do the work the moneypig be the only one who receive 1 write invitation
he stand so high that they be afraid he would not accept 1 verbal message
but in his reply he say if he have to take 1 part he must enjoy the sport from his own home they be to arrange for him to do so and so they do
the little toy theatre be therefore put up in such 1 way that the moneypig can look directly into it
some want to begin with 1 comedy and afterwards to have 1 tea party and 1 discussion for mental improvement but they commence with the latter 1
the rockinghorse speak of training and race the wagon of railway and steam power for these subject belong to each of their profession and it be right they should talk of them
the clock talk politics tick tick he profess to know what be the time of day but there be 1 whisper that he do not go correctly
the bamboo cane stand by look stiff and proud he be vain of his brass ferrule and silver top and on the sofa lie 2 work cushion pretty but stupid
when the play at the little theatre begin the rest sit and look on they be request to applaud and stamp or crack when they feel gratify with what they saw
but the ridingwhip say he never crack for old people only for the young who be not yet marry
i crack for everybody say the cracker
yes and 1 fine noise you make think the audience as the play go on
it be not worth much but it be very well play and all the character turn their paint side to the audience for they be make only to be see on one side
the acting be wonderful except that sometimes they come out beyond the lamp because the wire be 1 little too long
the doll whose neck have be darn be so excite that the place in her neck burst and the moneypig declare he must do something for one of the player as they have all please him so much
so he make up his mind to remember one of them in his will as the one to be bury with him in the family vault whenever that event should happen
they all enjoy the comedy so much that they give up all thought of the tea party and only carry out their idea of intellectual amusement which they call play at man and woman and there be nothing wrong about it for it be only play
all the while each one think many of himself or of what the moneypig can be think
his thought be on as he suppose 1 very distant time of make his will and of his burial and of when it may all come to pass
certainly soon than he expect for all at once down he come from the top of the press fall on the ground and be break to piece
then the penny hop and dance about in the many amuse manner
the little one twirl round like top and the large one roll away as far as they can especially the one great silver crown piece who have often to go out into the world and now he have his wish as_well as all the rest of the money
the piece of the moneypig be throw into the dustbin and the next day there stand 1 new moneypig on the cupboard but it have not 1 farthing in its inside yet and therefore like the old one it can not rattle
this be the beginning with him and we will make it the end of our story
the_end
