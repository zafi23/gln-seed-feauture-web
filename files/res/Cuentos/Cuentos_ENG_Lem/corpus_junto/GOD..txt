olelukoie_the_dreamgod_there be nobody in the world who know so many story as olelukoie or who can relate them so nicely
in the evening while the child be seat at the table or in their little chair he come up the stair very softly for he walk in his sock then he open the door without the slight noise and throw 1 small quantity of very fine dust in their eye just enough to prevent them from keep them open and so they do not see him
then he creep behind them and blow softly upon their neck till their head begin to droop
but olelukoie doe not wish to hurt them for he be very fond of child and only want them to be quiet that he may relate to them pretty story and they never be quiet until they be in bed and asleep
as soon as they be asleep olelukoie seat himself upon the bed
he be nicely dress his coat be make of silken stuff it be impossible to say of what color for it change from green to red and from red to blue as he turn from side to side
under each arm he carry 1 umbrella one of them with picture on the inside he spread over the good child and then they dream the many beautiful story the whole night
but the other umbrella have no picture and this he hold over the naughty child so_that they sleep heavily and wake in the morning without have dream at all
now we shall hear how olelukoie come every night during 1 whole week to the little boy name hjalmar and what he tell him
there be 7 story as there be 7 day in the week
[L:??/??/??:????:??] [L:??/??/??:????:??] now pay attention say olelukoie in the evening when hjalmar be in bed and i will decorate the room immediately all the flower in the flowerpot become large tree with long branch reach to the ceiling and stretch along the wall so_that the whole room be like 1 greenhouse
all the branch be load with flower each flower as beautiful and as fragrant as 1 rise and have any one taste them he would have find them sweet even than jam
the fruit glitter like gold and there be cake so full of plum that they be nearly burst
it be incomparably beautiful
at the same time sound dismal moan from the tabledrawer in which lie hjalmars school book
what can that be now
say olelukoie go to the table and pull out the drawer
it be 1 slate in such distress because_of 1 false number in the sum that it have almost break itself to piece
the pencil pull and tug at its string as_if it be 1 little dog that want to help but can not
and then come 1 moan from hjalmars copybook
oh it be quite terrible to hear
on each leaf stand 1 row of capital letter every one have 1 small letter by its side
this form 1 copy under these be other letter which hjalmar have write they fancy they look like the copy but they be mistake for they be lean on one side as_if they intend to fall over the pencillines
see this be the way you should hold yourselves say the copy
look here you should slope thus with 1 graceful curve oh we be very will to do so but we can not say hjalmars letter we be so wretchedly make you must be scratch out then say olelukoie
oh no they cry and then they stand up so gracefully it be quite 1 pleasure to look at them
now we must give up our story and exercise these letter say olelukoie one 2 one 2 so he drill them till they stand up gracefully and look as beautiful as 1 copy can look
but after olelukoie be go and hjalmar look at them in the morning they be as wretched and as awkward as ever
[M:??/??/??:????:??] [M:??/??/??:????:??] as soon as hjalmar be in bed olelukoie touch with his little magic wand all the furniture in the room which immediately begin to chatter and each article only talk of itself
over the chest of drawer hang 1 large picture in 1 gilt frame represent 1 landscape with fine old tree flower in the grass and 1 broad stream which flow through the wood past several castle far out into the wild ocean
olelukoie touch the picture with his magic wand and immediately the bird commence sing the branch of the tree rustle and the cloud move across the sky cast their shadow on the landscape beneath them
then olelukoie lift little hjalmar up to the frame and place his foot in the picture just on the high grass and there he stand with the sun shine down upon him through the branch of the tree
he run to the water and seat himself in 1 little boat which lie there and which be paint red and white
the sail glitter like silver and 6 swan each with 1 golden circlet round its neck and 1 bright blue star on its forehead draw the boat past the green wood where the tree talk of robber and witch and the flower of beautiful little elf and fairy whose history the butterfly have relate to them
brilliant fish with scale like silver and gold swim after the boat sometimes make 1 spring and splash the water round them while bird red and blue small and great fly after him in 2 long line
the gnat dance round them and the cockchafers cry buz buz they all want to follow hjalmar and all have some story to tell him
it be 1 many pleasant sail
sometimes the forest be thick and dark sometimes like 1 beautiful garden gay with sunshine and flower then he pass great palace of glass and of marble and on the balcony stand princess whose face be those of little girl whom hjalmar know well and have often play with
one of them hold out her hand in which be 1 heart make of sugar many beautiful than any confectioner ever sell
as hjalmar sail by he catch hold of one side of the sugar heart and hold it fast and the princess hold fast also so_that it break in 2 piece
hjalmar have one piece and the princess the other but hjalmars be the large
at each castle stand little prince act as sentinel
they present arm and have golden sword and make it rain plum and tin soldier so_that they must have be real prince
hjalmar continue to sail sometimes through wood sometimes as it be through large hall and then by large city
at_last he come to the town where his nurse live who have carry him in her arm when he be 1 very little boy and have always be kind to him
she nod and beckon to him and then sing the little verse she have herself compose and set to him how oft my memory turn to thee my own hjalmar ever dear
when i can watch thy infant glee or kiss away 1 pearly tear
twas in my arm thy lisp tongue first speak the halfremembered word while oer thy totter step i hang my fond protection to afford
farewell
i pray the heavenly_power_to keep thee till thy die hour and all the bird sing the same tune the flower dance on their stem and the old tree nod as_if olelukoie have be tell them story as_well
[X:??/??/??:????:??] [X:??/??/??:????:??] how the rain do pour down
hjalmar can hear it in his sleep
and when olelukoie open the window the water flow quite up to the windowsill
it have the appearance of 1 large lake outside and 1 beautiful ship lie close to the house
wilt thou sail with me tonight little hjalmar
say olelukoie then we shall see foreign country and thou shalt return here in the morning all in 1 moment there stand hjalmar in his good clothes on the deck of the noble ship and immediately the weather become fine
they sail through the street round by the church and on every side roll the wide great sea
they sail till the land disappear and then they saw 1 flock of stork who have leave their own country and be travel to warmer climate
the stork fly one behind the other and have already be 1 long long time on the wing
one of them seem so tire that his wing can scarcely carry him
he be the last of the row and be soon leave very far behind
at length he sink lower and lower with outstretch wing flap them in vain till his foot touch the rig of the ship and he slided from the sail to the deck and stand before them
then 1 sailorboy catch him and put him in the henhouse with the fowl the duck and the turkey while the poor stork stand quite bewilder amongst them
just look at that fellow say the chicken
then the turkeycock puff himself out as large as he can and inquire who he be and the duck waddle backwards cry quack quack then the stork tell them all about warm africa of the pyramid and of the ostrich which like 1 wild horse run across the desert
but the duck do not understand what he say and quack amongst themselves we be all of the same opinion namely that he be stupid yes to be sure he be stupid say the turkeycock and gobble
then the stork remain quite silent and think of his home in africa
those be handsome thin leg of yours say the turkeycock
what do they cost LN_yd:1
quack quack quack grin the duck but the stork pretend not to hear
you may as_well laugh say the turkey for that remark be rather witty or perhaps it be above you
ah ah be he not clever
he will be 1 great amusement to us while he remain here and then he gobble and the duck quack gobble gobble quack quack what 1 terrible uproar they make while they be have such fun among themselves
then hjalmar go to the henhouse and open the door call to the stork
then he hop out on the deck
he have rest himself now and he look happy and seem as_if he nod to hjalmar as_if to thank him
then he spread his wing and fly away to warmer country while the hen cluck the duck quack and the turkeycock turn quite scarlet in the head
tomorrow you shall be make into soup say hjalmar to the fowl and then he awake and find himself lie in his little bed
it be 1 wonderful journey which olelukoie have make him take this night
[J:??/??/??:????:??] [J:??/??/??:????:??] what do you think i have get here
say olelukoie_do not be frighten and you shall see 1 little mouse and then he hold out his hand to him in which lie 1 lovely little creature
it have come to invite you to 1 wedding
2 little mouse be go to enter into the marriage state tonight
they reside under the floor of your mother storeroom and that must be 1 fine dwellingplace but how can i get through the little mousehole in the floor
ask hjalmar
leave me to manage that say olelukoie
i will soon make you small enough and then he touch hjalmar with his magic wand whereupon he become less and less until at_last he be not long than 1 little finger
now you can borrow the dress of the tin soldier
i think it will just fit you
it look well to wear 1 uniform when you go into company yes certainly say hjalmar and in 1 moment he be dress as neatly as the neat of all tin soldier
will you be so good as to seat yourself in your mamma thimble say the little mouse that i may have the pleasure of draw you to the wedding will you really take so much trouble young lady
say hjalmar
and so in this way he ride to the mouses wedding
first they go under the floor and then pass through 1 long passage which be scarcely high enough to allow the thimble to drive under and the whole passage be light up with the phosphorescent light of rotten wood
doe it not smell delicious
ask the mouse as she draw him along
the wall and the floor have be smear with baconrind nothing can be nice very soon they arrive at the bridal hall
on the right stand all the little ladymice whisper and giggle as_if they be make game of each other
to the left be the gentlemenmice stroke their whisker with their forepaws and in the centre of the hall can be see the bridal pair stand side by side in 1 hollow cheeserind and kiss each other while all eye be upon them for they have already be betroth and be soon to be marry
many and many friend keep arrive till the mouse be nearly tread each other to death for the bridal pair now stand in the doorway and none can pass in or out
the room have be rub over with baconrind like the passage which be all the refreshment offer to the guest
but for dessert they produce 1 pea on which 1 mouse belonging to the bridal pair have bite the 1 letter of their name
this be something quite uncommon
all the mouse say it be 1 very beautiful wedding and that they have be very agreeably entertain
after this hjalmar return home
he have certainly be in grand society but he have be oblige to creep under 1 room and to make himself small enough to wear the uniform of 1 tin soldier
[V:??/??/??:????:??] [V:??/??/??:????:??] it be incredible how_many old people there be who would be glad to have me at night say olelukoie especially those who have do something wrong
good little ole say they to me we can not close our eye and we lie awake the whole night and see all our evil deed sit on our bed like little imps and sprinkle us with hot water
will you come and drive them away that we may have 1 good night rest
and then they sigh so deeply and say we would gladly pay you for it
goodnight_oleluk the money lie on the window but_i never do anything for gold what shall we do tonight
ask hjalmar
i do not know whether you would care to go to another wedding he reply although it be quite 1 different affair to the one we saw last night
your sister large doll that be dress like 1 man and be call herman intend to marry the doll bertha
it be also the doll birthday and they will receive many present yes_i know that already say hjalmar my sister always allow her doll to keep their birthday or to have 1 wedding when they require new clothes that have happen already Z_times i be quite sure yes so it may but tonight be the hundred and 1 wedding and when that have take place it must be the last therefore this be to be extremely beautiful
only look hjalmar look at the table and there stand the little cardboard doll house with light in_all the window and draw up before it be the tin soldier present arm
the bridal pair be seat on the floor leaning against the leg of the table look very thoughtful and with good reason
then olelukoie dress up in grandmother black gown marry them
as soon as the ceremony be conclude all the furniture in the room join in sing 1 beautiful song which have be compose by the lead pencil and which go to the melody of 1 military tattoo
what merry sound be on the wind as marriage rite together bind 1 quiet and 1 love pair though form of kid yet smooth and fair
hurrah
if they be deaf and blind well sing though weather prove unkind and now come the present but the bridal pair have nothing to eat for love be to be their food
shall we go to 1 country house or travel
ask the bridegroom
then they consult the swallow who have travel so far and the old hen in the yard who have bring up 5 brood of chicken
and the swallow talk to them of warm country where the grape hang in large cluster on the vine and the air be soft and mild and about the mountain glow with color many beautiful than we can think of
but they have no red cabbage like we have say the hen i be once in the country with my chicken for 1 whole summer there be 1 large sandpit in which we can walk about and scratch as we like
then we get into 1 garden in which grow red cabbage oh how nice it be i can not think of anything many delicious but one cabbage stalk be exactly like another say the swallow and here we have often bad weather yes but we be accustom to it say the hen
but it be so cold here and freeze sometimes cold weather be good for cabbage say the hen besides we do have it warm here sometimes
4 year ago we have 1 summer that last many than 5 week and it be so hot one can scarcely breathe
and then in this country we have no poisonous animal and we be free from robber
he must be wicked who do not consider our country the fine of all land
he ought not to be allow to live here and then the hen weep very much and say i have also travel
i once go LN_mi:12 in 1 coop and it be not pleasant travel at all the hen be 1 sensible woman say the doll bertha
i dont care for travel over mountain just to go up and come down again
no let us go to the sandpit in front of the gate and then take 1 walk in the cabbage garden and so they settle it
[S:??/??/??:????:??] [S:??/??/??:????:??] am_i to hear any many story
ask little hjalmar as soon as olelukoie have send him to sleep
we shall have no time this evening say he spread out his prettiest umbrella over the child
look at these chinese and then the whole umbrella appear like 1 large china bowl with blue tree and point bridge upon which stand little chinamen nod their head
we must make all the world beautiful for tomorrow morning say olelukoie for it will be 1 holiday it be [G:??/??/??:????:??]
i must now go to the church steeple and see if the little sprite who live there have polish the bell so_that they may sound sweetly
then i must go into the field and see if the wind have blow the dust from the grass and the leaf and the many difficult task of all which i have to do be to take down all the star and brighten them up
i have to number them 1 before i put them in my apron and also to number the place from which i take them so_that they may go back into the right hole or else they would not remain and we should have 1 number of fall star for they would all tumble down one after the other hark ye
mr_lukoie say 1 old portrait which hang on the wall of hjalmars bedroom
do you know me
i be hjalmars greatgrandfather
i thank you for tell the boy story but you must not confuse his idea
the star can not be take down from the sky and polish they be sphere like our earth which be 1 good thing for them thank you old greatgrandfather say olelukoie
i thank you you may be the head of the family as no_doubt you be but i be old than you
i be 1 ancient heathen
the old romans and greeks name me the dreamgod
i have visit the noble house and continue to do so still i know how to conduct myself both to high and low and now you may tell the story yourself and so olelukoie walk off take his umbrella with him
well well one be never to give 1 opinion i suppose grumble the portrait
and it wake hjalmar
[G:??/??/??:????:??] [G:??/??/??:????:??] good evening say olelukoie
hjalmar nod and then spring out of bed and turn his greatgrandfathers portrait to the wall so_that it may not interrupt them as it have do yesterday
now say he you must tell me some story about 5 green pea that live in one pod or of the chickseed that court the chickweed or of the darn needle who act so proudly because she fancy herself 1 embroidery needle you may have too much of 1 good thing say olelukoie
you know that i like good to show you something so i will show you my brother
he be also call olelukoie but he never visit any one but once and when he do come he take him away on his horse and tell him story as they ride along
he know only 2 story
one of these be so wonderfully beautiful that no_one in the world can imagine anything at all like it but the other be just as ugly and frightful so_that it would be impossible to describe it then_olelukoie lift hjalmar up to the window
there now you can see my brother the other olelukoie he be also call death
you perceive he be not so bad as they represent him in picture book there he be 1 skeleton but now his coat be embroider with silver and he wear the splendid uniform of 1 hussar and 1 mantle of black velvet fly behind him over the horse
look how he gallop along hjalmar saw that as this olelukoie ride on he lift up old and young and carry them away on his horse
some he seat in front of him and some behind but always inquire 1 how stand the markbook
good they all answer
yes but let me see for myself he reply and they be oblige to give him the book
then all those who have very good or exceedingly good come in front of the horse and hear the beautiful story while those who have middling or tolerably good in their book be oblige to sit behind and listen to the frightful tale
they tremble and cry and want to jump down from the horse but they can not get free for they seem fasten to the seat
why death be 1 many splendid lukoie say hjalmar
i be not in the little afraid of him you need have no fear of him say olelukoie if you take care and keep 1 good conduct book now_i call that very instructive murmur the greatgrandfathers portrait
it be useful sometimes to express 1 opinion so he be quite satisfy
these be some of the doing and saying of olelukoie
i hope he may visit you himself this evening and relate some many
the_end
