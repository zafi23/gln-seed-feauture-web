the_willothe_wisp_is_in_the_town_says_the_moor_woman_there be 1 man who once know many story but they have slip away from him so he say
the story that use to visit him of its own accord no_longer come and knock at his door
and why do it come no_longer
it be true enough that for day and year the man have not think of it have not expect it to come and knock and if he have expect it it would certainly not have come for without there be war and within be the care and sorrow that war bring with it
the stork and the swallow come back from their long journey for they think of no danger and behold when they arrive the nest be burn the habitation of man be burn the hedge be all in disorder and everything seem go and the enemys horse be stamp in the old grave
those be hard gloomy time but they come to 1 end
and now they be past and go so people say yet no story come and knock at the door or give any tiding of its presence
i suppose it must be dead or go away with many other thing say the man
but the story never die
and many than 1 whole year go by and he long oh so very much
for the story
i wonder if the story will ever come back again and knock
and he remember it so well in_all the various form in which it have come to him sometimes young and charm like spring itself sometimes as 1 beautiful maiden with 1 wreath of thyme in her hair and 1 beechen branch in her hand and with eye that gleam like deep woodland lake in the bright sunshine
sometimes it have come to him in the guise of 1 peddler and have open its box and let silver ribbon come flutter out with verse and inscription of old remembrance
but it be many charm of all when it come as 1 old grandmother with silvery hair and such large sensible eye
she know so well how to tell about the old time long before the princess spin with the golden spindle and the dragon lie outside the castle guard them
she tell with such 1 air of truth that black spot dance before the eye of all who hear her and the floor become black with human blood terrible to see and to hear and yet so entertain because such 1 long time have pass since it all happen
will it ever knock at my door again
say the man and he gaze at the door so_that black spot come before his eye and upon the floor he do not know if it be blood or mourn crape from the dark heavy day
and as he sit thus the thought come upon him whether the story may not have hide itself like the princess in the old tale
and he would now go in search of it if he find it it would beam in new splendor lovelier than ever
who know
perhaps it have hide itself in the straw that balance on the margin of the well
carefully carefully
perhaps it lie hide in 1 certain flower that flower in one of the great book on the bookshelf and the man go and open one of the new book to gain information on this point but there be no flower to be find
there he read about holger_danske and the man read that the tale have be invent and put together by 1 monk in france that it be 1 romance translate into danish and print in that language that holger_danske have never really live and consequently can never come again as we have sing and have be so glad to believe
and william_tell be treat just like holger_danske
these be all only myth nothing on which we can depend and yet it be all write in 1 very learn book
well i shall believe what i believe
say the man
there grow no plantain where no foot have tread and he close the book and put it back in its place and go to the fresh flower at the window
perhaps the story may have hide itself in the red tulip with the golden yellow edge or in the fresh rise or in the beam camellia
the sunshine lie among the flower but no story
the flower which have be here in the dark troublous time have be much many beautiful but they have be cut off one after another to be weave into wreath and place in coffin and the flag have wave over them
perhaps the story have be bury with the flower but then the flower would have know of it and the coffin would have hear it and every little blade of grass that shot forth would have tell of it
the story never die
perhaps it have be here once and have knock but who have eye or ear for it in those time
people look darkly gloomily and almost angrily at the sunshine of spring at the twitter bird and all the cheerful green the tongue can not even bear the old merry popular song and they be lay in the coffin with so much that our heart hold dear
the story may have knock without obtain 1 hearing there be none to bid it welcome and so it may have go away
i will go forth and seek it
out in the country
out in the wood
and on the open sea beach
out in the country lie 1 old manor house with red wall point gable and 1 red flag that float on the tower
the nightingale sing among the finelyfringed beechleaves look at the bloom apple tree of the garden and think that they bear rose
here the bee be mightily busy in the summertime and hover round their queen with their hum song
the autumn have much to tell of the wild chase of the leaf of the tree and of the race of man that be pass away together
the wild swan sing at christmastime on the open water while in the old hall the guest by the fireside gladly listen to song and to old legend
down into the old part of the garden where the great avenue of wild chestnut tree lure the wanderer to tread its shade go the man who be in search of the story for here the wind have once murmur something to him of waldemar_daa and his daughters_the_dryad in the tree who be the storymother herself have here tell him the dream_of_the_old_oak_tree_here in the time of the ancestral mother have stand clip hedge but now only fern and sting nettle grow there hide the scatter fragment of old sculpture figure the moss be grow in their eye but they can see as_well as ever which be many than the man can do who be in search of the story for he can not find that
where can it be
the crow fly past him by hundred across the old tree and scream krah
da
krah
da
and he go out of the garden and over the grassplot of the yard into the alder grove there stand 1 little sixsided house with 1 poultryyard and 1 duckyard
in the middle of the room sit the old woman who have the management of the whole and who know accurately about every egg that be lay and about every chicken that can creep out of 1 egg
but she be not the story of which the man be in search that she can attest with 1 christian certificate of baptism and of vaccination that lie in her drawer
without not far from the house be 1 hill cover with redthorn and broom
here lie 1 old gravestone which be bring here many year ago from the churchyard of the provincial town 1 remembrance of one of the many honor councillor of the place his wife and his 5 daughter all with fold hand and stiff ruff stand round him
one can look at them so long that it have 1 effect upon the thought and these react upon the stone as_if they be tell of old time at_least it have be so with the man who be in search of the story
as he come near he notice 1 living butterfly sitting on the forehead of the sculpture councillor
the butterfly flap its wing and fly 1 little bit far and then return fatigue to sit upon the gravestone as_if to point out what grow there
fourleaved shamrock grow there there be 7 specimen close to each other
when fortune come it come in 1 heap
he pluck the shamrock and put them in his pocket
fortune be as good as red gold but 1 new charm story would be well still think the man but he can not find it here
and the sun go down round and large the meadow be cover with vapor
the moorwoman be at her brew
it be even
he stand alone in his room and look out upon the sea over the meadow over moor and coast
the moon shine bright 1 mist be over the meadow make it look like 1 great lake and indeed it be once so as the legend tell and in the moonlight the eye realize these myth
then the man think of what he have be read in the town that william_tell and holger_danske never really live but yet live in popular story like the lake yonder 1 living evidence for such myth
yes holger_danske will return again
as he stand thus and think something beat quite strongly against the window
be it 1 bird 1 bat or 1 owl
those be not let in even when they knock
the window fly open of itself and 1 old woman look in at the man
whats your pleasure
say he
who be you
youre look in at the 1 floor window
be you stand on 1 ladder
you have 1 fourleaved shamrock in your pocket she reply
indeed you have 7 and 1 of them be 1 sixleaved one who be you
ask the man again
the moorwoman she reply
the moorwoman who brew
i be at it
the bung be in the cask but one of the little moorimps pull it out in his mischief and fling it up into the yard where it beat against the window and now the beer run out of the cask and that wont do good to anybody pray tell me some many
say the man
yes wait 1 little answer the moorwoman
ive something else to do just now and she be go
the man be go to shut the window when the woman already stand before him again
now its do she say but i shall have half the beer to brew over again tomorrow if the weather be suitable
well what have you to ask me
ive come back for i always keep my word and you have 7 fourleaved shamrock in your pocket and one of them be 1 sixleaved one
that inspire respect for thats 1 order that grow beside the sandy way but that every one do not find
what have you to ask me
dont stand there like 1 ridiculous oaf for i must go back again directly to my bung and my cask and the man ask about the story and inquire if the moorwoman have meet it in her journeyings
by the big brewingvat
exclaim the woman havent you get story enough
i really believe that many people have enough of them
here be other thing to take notice of other thing to examine
even the child have go beyond that
give the little boy 1 cigar and the little girl 1 new crinoline they like that much good
to listen to story
no indeed there be many important thing to be do here and other thing to notice
what do you mean by that
ask the man and what do you know of the world
you dont see anything but frog and willothewisps
yes beware of the willothewisps say the moorwoman for theyre out theyre let loose thats what we must talk about
come to me in the moor where my presence be necessary and i will tell you all about it but you must make haste and come while your 7 fourleaved shamrock for which one have 6 leaf be still fresh and the moon stand high
and the moorwoman be go
it strike 12 in the town and before the last stroke have die away the man be out in the yard out in the garden and stand in the meadow
the mist have vanish and the moorwoman stop her brew
youve be 1 long time coming
say the moorwoman
witch get forward fast than man and im glad that i belong to the witch folk
what have you to say to me now
ask the man
be it anything about the story
can you never get beyond ask about that
retort the woman
can you tell me anything about the poetry of the future
resume the man
dont get on your stilt say the crone and ill answer you
you think of nothing but poetry and only ask about that story as_if she be the lady of the whole troop
shes the old of us all but she take precedence of the young
i know her well
ive be young too and shes no chicken now
i be once quite 1 pretty elfmaiden and have dance in my time with the other in the moonlight and have hear the nightingale and have go into the forest and meet the storymaiden who be always to be find out there run about
sometimes she take up her night lodge in 1 halfblown tulip or in 1 field flower sometimes she would slip into the church and wrap herself in the mourn crape that hang down from the candle on the altar you be capitally wellinformed say the man
i ought at_least to know as much as you answer the moorwoman
story and poetry yes theyre like LN_yd:2 of the same piece of stuff they can go and lie down where they like and one can brew all their prattle and have it all the good and cheap
you shall have it from me for nothing
i have 1 whole cupboardfull of poetry in bottle
it make essence and thats the good of it bitter and sweet herb
i have everything that people want of poetry in bottle so_that i can put 1 little on my handkerchief on holiday to smell why these be wonderful thing that youre tell
say the man
you have poetry in bottle
many than you can require say the woman
i suppose you know the history of the girl who trod on the loaf so_that she may not soil her shoe
that have be write and print too i tell that story myself say the man
yes then you must know it and you must know also that the girl sink into the earth directly to the moorwoman just as old_bogeys grandmother be pay her morning visit to inspect the brewery
she saw the girl glide down and ask to have her as 1 remembrance of her visit and get her too while i receive 1 present thats of no use to me 1 travel druggist shop 1 whole cupboardfull of poetry in bottle
grandmother tell me where the cupboard be to be place and there its standing still
just look
youve your 7 fourleaved shamrock in your pocket one of which be 1 sixleaved one and so you will be able to see it and really in the midst of the moor lie something like 1 great knot block of alder and that be the old grandmother cupboard
the moorwoman say that this be always open to her and to every one in the land if they only know where the cupboard stand
it can be open either at the front or at the back and at every side and corner 1 perfect work of art and yet only 1 old alder stump in appearance
the poet of all land and especially those of our own country have be arrange here the spirit of them have be extract refine criticise and renovate and then store up in bottle
with what may be call great aptitude if it be not genius the grandmother have take as it be the flavor of this and of that poet and have add 1 little devilry and then cork up the bottle for use during all future time
pray let me see say the man
yes but there be many important thing to hear reply the moorwoman
but now we be at the cupboard
say the man
and he look in
here be bottle of all size
what be in this one
and what in that one yonder
here be what they call maybalm reply the woman
i have not try it myself
but i have not yet tell you the many important thing you be to hear
the_willothewisps_in_the_town
thats of much many consequence than poetry and story
i ought indeed to hold my tongue but there must be 1 necessity 1 fate 1 something that stick in my throat and that want to come out
take care you mortal
i dont understand 1 word of all this
cry the man
be kind enough to seat yourself on that cupboard she retort but take care you dont fall through and break the bottle you know whats inside of them
i must tell of the great event
it occur no_longer ago than the day before yesterday
it do not happen earlier
it have now 300 sixtythree day to run about
i suppose you know how_many day there be in 1 year
and this be what the moorwoman tell there be 1 great commotion yesterday out here in the marsh
there be 1 christening feast
1 little willothewisp be bear here in fact 12 of them be bear all together and they have permission if they choose to use it to go abroad among man and to move about and command among them just as_if they be bear mortal
that be 1 great event in the marsh and accordingly all the willothewisps male and female go dance like little light across the moor
there be some of them of the dog species but those be not worth mention
i sit there on the cupboard and have all the 12 little newborn willothewisps upon my lap
they shine like glowworms they already begin to hop and increase in size every moment so_that before 1/4 of 1 hour have elapse each of them look just as large as his father or his uncle
now its 1 oldestablished regulation and favor that when the moon stand just as it do yesterday and the wind blow just as it blow then it be allow and accord to all willothewisps that be to all those who be bear at that minute of time to become mortal and individually to exert their power for the space of one year
the willothewisp may run about in the country and through the world if it be not afraid of fall into the sea or of be blow out by 1 heavy storm
it can enter into 1 person and speak for him and make all the movement it please
the willothewisp may take whatever form he like of man or woman and can act in their spirit and in their disguise in such 1 way that he can effect whatever he wish to do
but he must manage in the course of the year to lead 300 sixtyfive people into 1 bad way and in 1 grand style too
to lead them away from the right and the truth and then he reach the high point
such 1 willothewisp can attain to the honor of be 1 runner before the devil state coach and then hell wear clothes of fiery yellow and breathe forth flame out of his throat
thats enough to make 1 simple willothewisp smack his lip
but theres some danger in this and 1 great deal of work for 1 willothewisp who aspire to play so distinguish 1 part
if the eye of the man be open to what he be and if the man can then blow him away its all over with him and he must come back into the marsh or if before the year be up the willothewisp be seize with 1 longing to see his family and so return to it and give the matter up it be over with him likewise and he can no_longer burn clear and soon become extinguish and can not be light up again and when the year have elapse and he have not lead 300 sixtyfive people away from the truth and from all that be grand and noble he be condemn to be imprison in decay wood and to lie glimmer there without be able to move and thats the many terrible punishment that can be inflict on 1 lively willothewisp
now all this i know and all this i tell to the 12 little willothewisps whom i have on my lap and who seem quite crazy with joy
i tell them that the safe and many convenient course be to give up the honor and do nothing at all but the little flame would not agree to this and already fancy themselves clad in fiery yellow clothes breathe flame from their throat
stay with us say some of the old one
carry on your sport with mortal say the other
the mortal be dry up our meadow theyve take to drain
what will our successor do
we want to flame we will flame flame
cry the newborn willothewisps
and thus the affair be settle
and now 1 ball be give 1 minute long it can not well be short
the little elfmaidens whirl round Z_times with the rest that they may not appear proud but they prefer dance with 1 another
and now the sponsor gift be present and present be throw them
these present fly like pebble across the seawater
each of the elfmaidens give 1 little piece of her veil
take that they say and then youll know the high dance the many difficult turn and twist that be to say if you should find them necessary
youll know the proper deportment and then you can show yourself in the very pick of society the night raven teach each of the young willothewisps to say googoogood and to say it in the right place and thats 1 great gift which bring its own reward
the owl and the stork but they say it be not worth mention and so we wont mention it
king waldemars wild chase be just then rush over the moor and when the great lord hear of the festivity that be go on they send 1 couple of handsome dog which hunt on the spoor of the wind as 1 present and these may carry 2 or 3 of the willothewisps
1 couple of old alpas spirit who occupy themselves with alppressing be also at the feast and from these the young willothewisps learn the art of slip through every keyhole as_if the door stand open before them
these alpas offer to carry the youngster to the town with which they be well acquaint
they usually ride through the atmosphere on their own back hair which be fasten into 1 knot for they love 1 hard seat but now they sit sideways on the wild hunting dog take the young willothewisps in their lap who want to go into the town to mislead and entice mortal and whisk
away they be
now this be what happen last night
today the willothewisps be in the town and have take the matter in_hand but where and how
ah can you tell me that
still ive 1 lightning conductor in my great toe and that will always tell me something why this be 1 complete story exclaim the man
yes but it be only the beginning reply the woman
can you tell me how the willothewisps deport themselves and how they behave
and in what shape they have aforetime appear and lead people into crooked path
i believe reply the man that one can tell quite 1 romance about the willothewisps in 12 part or well still one may make quite 1 popular play of them you may write that say the woman but its good let alone yes thats well and many agreeable the man reply for then we shall escape from the newspaper and not be tie up by them which be just as uncomfortable as for 1 willothewisp to lie in decay wood to have to gleam and not to be able to stir i dont care about it either way cry the woman
let the rest write those who can and those who can not likewise
ill grant you 1 old bung from my cask that will open the cupboard where poetrys keep in bottle and you may take from that whatever may be want
but you my good man seem to have blot your hand sufficiently with ink and to have come to that age of satiety that you need not be running about every year for story especially as there be much many important thing to be do
you must have understand what be go on
the willothewisp be in town say the man
ive hear it and i have understand it
but what do you think i ought to do
i should be thrash if i be to go to the people and say look yonder go 1 willothewisp in his good clothes
they also go in undress reply the woman
the willothewisp can assume all kind of form and appear in every place
he go into the church but not for the sake of the service and perhaps he may enter into one or other of the priest
he speak in the parliament not for the benefit of the country but only for himself
hes 1 artist with the colorpot as_well as in the theatre but when he get all the power into his own hand then the pot empty
i chatter and chatter but it must come out whats stick in my throat to the disadvantage of my own family
but i must now be the woman that will save 1 good many people
it be not do with my good will or for the sake of 1 medal
i do the many insane thing i possibly can and then i tell 1 poet about it and thus the whole town get to know of it directly the town will not take that to heart observe the man that will not disturb 1 single person for they will all think im only tell them 1 story if i say the_willothewisp be in the town say the moorwoman
take care of yourselves
the_end
