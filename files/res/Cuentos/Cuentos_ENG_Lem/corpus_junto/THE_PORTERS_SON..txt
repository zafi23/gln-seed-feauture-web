the_porters_son_the_general live in the grand 1 floor and the porter live in the cellar
there be 1 great distance between the 2 family the whole of the ground floor and the difference in rank but they live in the same house and both have 1 view of the street and of the courtyard
in the courtyard be 1 grassplot on which grow 1 bloom acacia tree when it be in bloom and under this tree sit occasionally the finelydressed nurse with the still many finelydressed child of the general little emily
before them dance about barefoot the little son of the porter with his great brown eye and dark hair and the little girl smile at him and stretch out her hand towards him and when the general saw that from the window he would nod his head and cry charming
the generals lady who be so young that she may very well have be her husband daughter from 1 early marriage never come to the window that look upon the courtyard
she have give order though that the boy may play his antic to amuse her child but must never touch it
the nurse punctually obey the gracious ladys order
the sun shine in upon the people in the grand 1 floor and upon the people in the cellar the acacia tree be cover with blossom and they fall off and next year new one come
the tree bloom and the porter little son bloom too and look like 1 fresh tulip
the generals little daughter become delicate and pale like the leaf of the acacia blossom
she seldom come down to the tree now for she take the air in 1 carriage
she drive out with her mamma and then she would always nod at the porter george yes she use even to kiss her hand to him till her mamma say she be too old to do that now
one morning george be send up to carry the general the letter and newspaper that have be deliver at the porter room in the morning
as he be run up stair just as he pass the door of the sandbox he hear 1 faint pipe
he think it be some young chicken that have stray there and be raise cry of distress but it be the generals little daughter deck out in lace and finery
dont tell papa and mamma she whimper they would be angry whats the matter little missie
ask george
its all on fire
she answer
its burning with 1 bright flame
george hurry up stair to the generals apartment he open the door of the nursery
the window curtain be almost entirely burn and the wooden curtainpole be one mass of flame
george spring upon 1 chair he bring in haste and pull down the burning article he then alarm the people
but for him the house would have be burn down
the general and his lady crossquestioned little emily
i only take just 1 lucifermatch she say and it be burn directly and the curtain be burn too
i spit at it to put it out i spit at it as much as ever i can but i can not put it out so i run away and hide myself for papa and mamma would be angry i spit
cry the generals lady what 1 expression
do you ever hear your papa and mamma talk about spit
you must have get that from down stair
and george have 1 penny give him
but this penny do not go to the baker shop but into the savingsbox and soon there be so many penny in the savingsbox that he can buy 1 paintbox and color the drawing he make and he have 1 great number of drawing
they seem to shoot out of his pencil and out of his finger end
his 1 color picture he present to emily
charm
say the general and even the generals lady acknowledge that it be easy to see what the boy have mean to draw
he have genius those be the word that be carry down into the cellar
the general and his gracious lady be grand people
they have 2 coat of arm on their carriage 1 coat of arm for each of them and the gracious lady have have this coat of arm embroider on both side of every bit of linen she have and even on her nightcap and her dressingbag
one of the coat of arm the one that belong to her be 1 very dear one it have be buy for hard cash by her father for he have not be bear with it nor have she she have come into the world too early 7 year before the coat of arm and many people remember this circumstance but the family do not remember it
1 man may well have 1 bee in his bonnet when he have such 1 coat of arm to carry as that let alone have to carry 2 and the generals wife have 1 bee in hers when she drive to the court ball as stiff and as proud as you please
the general be old and gray but he have 1 good seat on horseback and he know it and he ride out every day with 1 groom behind him at 1 proper distance
when he come to 1 party he look somehow as_if he be ride into the room upon his high horse and he have order too such 1 number that no_one would have believe it but that be not his fault
as 1 young man he have take part in the great autumn review which be hold in those day
he have 1 anecdote that he tell about those day the only one he know
1 subaltern under his order have cut off one of the prince and take him prisoner and the prince have be oblige to ride through the town with 1 little band of capture soldier himself 1 prisoner behind the general
this be 1 evermemorable event and be always tell over and over again every year by the general who moreover always repeat the remarkable word he have use when he return his sword to the prince those word be only my subaltern can have take your highness prisoner i can never have do it
and the prince have reply you be incomparable in $_BRL:1 war the general have never take part
when war come into the country he have go on 1 diplomatic career to foreign court
he speak the french language so fluently that he have almost forget his own he can dance well he can ride well and order grow on his coat in 1 astound way
the sentry present arm to him one of the many beautiful girl present arm to him and become the generals lady and in time they have 1 pretty charm child that seem as_if it have drop from heaven it be so pretty and the porter son dance before it in the courtyard as soon as it can understand it and give her all his color picture and little emily look at them and be please and tear them to piece
she be pretty and delicate indeed
my little roseleaf
cry the generals lady thou art bear to wed 1 prince the prince be already at the door but they know nothing of it people dont see far beyond the threshold
the day before yesterday our boy divide his bread and butter with her
say the porter wife
there be neither cheese nor meat upon it but she like it as_well as_if it have be roast beef
there would have be 1 fine noise if the general and his wife have see the feast but they do not see it
george have divide his bread and butter with little emily and he would have divide his heart with her if it would have please her
he be 1 good boy brisk and clever and he go to the night school in the academy now to learn to draw properly
little emily be get on with her education too for she speak french with her bonne and have 1 dance master
george will be confirm at easter say the porter wife for george have get so far as this
it would be the good thing now to make 1 apprentice of him say his father
it must be to some good call and then he would be out of the house he would have to sleep out of the house say georges mother
it be not easy to find 1 master who have room for him at night and we shall have to provide him with clothes too
the little bit of eat that he want can be manage for him for hes quite happy with 1 few boil potato and he get teach for nothing
let the boy go his own way
you will say that he will be our joy some day and the professor say so too the confirmation suit be ready
the mother have work it herself but the tailor who do repair have cut them out and 1 capital cutterout he be
if he have have 1 good position and be able to keep 1 workshop and journeyman the porter wife say he may have be 1 court tailor the clothes be ready and the candidate for confirmation be ready
on his confirmation day george receive 1 great pinchbeck watch from his godfather the old iron monger shopman the rich of his godfather
the watch be 1 old and try servant
it always go too fast but that be good than to be lag behind
that be 1 costly present
and from the generals apartment there arrive 1 hymnbook bind in morocco send by the little lady to whom george have give picture
at the beginning of the book his name be write and her name as his gracious patroness these word have be write at the dictation of the generals lady and the general have read the inscription and pronounce it charming
that be really 1 great attention from 1 family of such position say the porter wife and george be send up stair to show himself in his confirmation clothes with the hymnbook in his hand
the generals lady be sit very much wrap up and have the bad headache she always have when time hang heavy upon her hand
she look at george very pleasantly and wish him all prosperity and that he may never have her headache
the general be walk about in his dressinggown
he have 1 cap with 1 long tassel on his head and russian boot with red top on his foot
he walk Z_times up and down the room absorb in his own thought and recollection and then stop and say so little george be 1 confirm christian now
be 1 good man and honor those in authority over you
some day when you be 1 old man you can say that the general give you this precept that be 1 long speech than the general be accustom to make and then he go back to his rumination and look very aristocratic
but of all that george hear and saw up there little miss_emily remain many clear in his thought
how graceful she be how gentle and flutter and pretty she look
if she be to be draw it ought to be on 1 soapbubble
about her dress about her yellow curl hair there be 1 fragrance as of 1 freshblown rise and to think that he have once divide his bread and butter with her and that she have eat it with enormous appetite and nod to him at every 2 mouthful
do she remember anything about it
yes certainly for she have give him the beautiful hymnbook in remembrance of this and when the 1 new moon in the 1 new year after this event come round he take 1 piece of bread 1 penny and his hymnbook and go out into the open air and open the book to see what psalm he should turn up
it be 1 psalm of praise and thanksgiving
then he open the book again to see what would turn up for little emily
he take great pain not to open the book in the place where the funeral hymn be and yet he get one that refer to the grave and death
but then he think this be not 1 thing in which one must believe for all that he be startle when soon afterwards the pretty little girl have to lie in bed and the doctor carriage stop at the gate every day
they will not keep her with them say the porter wife
the good god know whom he will summon to himself_but they keep her after all and george draw picture and send them to her
he draw the czars palace the old kremlin at moscow just as it stand with tower and cupola and these cupola look like gigantic green and gold cucumber at_least in georges drawing
little emily be highly please and consequently when 1 week have elapse george send her 1 few many picture all with building in them for you see she can imagine all sort of thing inside the window and door
he draw 1 chinese house with bell hang from every one of 16 story
he draw 2 grecian temple with slender marble pillar and with step all round them
he draw 1 norwegian church
it be easy to see that this church have be build entirely of wood hew out and wonderfully put together every story look as_if it have rocker like 1 cradle
but the many beautiful of all be the castle draw on one of the leave and which he call emilys_castle_this be the kind of place in which she must live
that be what george have think and consequently he have put into this building whatever he think many beautiful in_all the other
it have carve woodwork like the norwegian church marble pillar like the grecian temple bell in every story and be crown with cupola green and gild like those of the kremlin_of_the_czar
it be $_BRL:1 childs castle and under every window be write what the hall or the room inside be intend to be for_instance here_emily sleep here_emily dance here_emily play at receive visitor it be $_BRL:1 pleasure to look at the castle and right well be the castle look at accordingly
charm
say the general
but the old count for there be 1 old count there who be still grand than the general and have 1 castle of his own say nothing at all he hear that it have be design and draw by the porter little son
not that he be so very little either for he have already be confirm
the old count look at the picture and have his own thought as he do so
one day when it be very gloomy gray wet weather the bright of day dawn for george_for_the_professor at the academy call him into his room
listen to me my friend say the professor_i want to speak to you
the lord have be good to you in give you ability and he have also be good in place you among kind people
the old count at the corner yonder have be speak to me about you
i have also see your sketch but we will not say any many about those for there be 1 good deal to correct in them
but from this time forward you may come twice aweek to my drawingclass and then you will soon learn how to do them well
i think theres many of the architect than of the painter in you
you will have time to think that over but go across to the old count this very day and thank god for have send you such 1 friend it be 1 great house the house of the old count at the corner
round the window elephant and dromedaries be carve all from the old time but the old count love the new time good and what it bring whether it come from the 1 floor or from the cellar or from the attic
i think say the porter wife the grand people be the few air do they give themselves
how kind and straightforward the old count be
and he talk exactly like you and me
now the general and his lady cant do that
and george be fairly wild with delight yesterday at the good reception he meet with at the counts and so be i today after speak to the great man
wasnt it 1 good thing that we didnt bind george apprentice to 1 handicraftsman
for he have ability of his own but they must be help on by other say the father
that help he have get now rejoin the mother for the count speak out quite clearly and distinctly but_i fancy it begin with the general say the father and we must thank them too let us do so with all my heart cry the mother though i fancy we have not much to thank them for
i will thank the good god and i will thank him too for let little emily get well emily be get on bravely and george get on bravely too
in the course of the year he win the little silver prize medal of the academy and afterwards he gain the great one too
it would have be good after all if he have be apprentice to 1 handicraftsman say the porter wife weep for then we can have keep him with us
what be he to do in rome
i shall never get 1 sight of him again not even_if he come back but that he wont do the dear boy it be fortune and fame for him say the father
yes thank you my friend say the mother you be say what you do not mean
you be just as sorrowful as i be and it be all true about the sorrow and the journey
but everybody say it be 1 great piece of good fortune for the young fellow
and he have to take leave and of the general too
the generals lady do not show herself for she have her bad headache
on this occasion the general tell his only anecdote about what he have say to the prince and how the prince have say to him you be incomparable and he hold out 1 languid hand to george
emily give george her hand too and look almost sorry and george be the many sorry of all
time go by when one have something to do and it go by too when one have nothing to do
the time be equally long but not equally useful
it be useful to george and do not seem long at all except when he happen to be think of his home
how may the good folk be get on up stair and down stair
yes there be writing about that and many thing can be put into 1 letter bright sunshine and dark heavy day
both of these be in the letter which bring the news that his father be dead and that his mother be alone now
she write that emily have come down to see her and have be to her like 1 angel of comfort and concern herself she add that she have be allow to keep her situation as porteress
the generals lady keep 1 diary and in this diary be record every ball she attend and every visit she receive
the diary be illustrate by the insertion of the visit card of the diplomatic circle and of the many noble family and the generals lady be proud of it
the diary keep grow through 1 long time and amid many severe headache and through 1 long course of halfnight that be to say of court ball
emily have now be to 1 court ball for the 1 time
her mother have wear 1 bright red dress with black lace in the spanish style the daughter have be attire in white fair and delicate green silk ribbon flutter like flagleaves among her yellow lock and on her head she wear 1 wreath of waterlillies
her eye be so blue and clear her mouth be so delicate and red she look like 1 little water spirit as beautiful as such 1 spirit can be imagine
the princes dance with her one after another of course and the generals lady have not 1 headache for 1 week afterwards
but the 1 ball be not the last and emily can not stand it it be 1 good thing therefore that summer bring with it rest and exercise in the open air
the family have be invite by the old count to visit him at him castle
that be 1 castle with 1 garden which be worth see
part of this garden be lay out quite in the style of the old day with stiff green hedge you walk as_if between green wall with peepholes in them
box tree and yew tree stand there trim into the form of star and pyramid and water spring from fountain in large grottoes line with shell
all around stand figure of the many beautiful stone that can be see in their clothes as_well as in their face every flowerbed have 1 different shape and represent 1 fish or 1 coat of arm or 1 monogram
that be the french part of the garden and from this part the visitor come into what appear like the green fresh forest where the tree may grow as they choose and accordingly they be great and glorious
the grass be green and beautiful to walk on and it be regularly cut and roll and sweep and tend
that be the english part of the garden
old time and new time say the count here they run well into 1 another
in 2 year the building itself will put on 1 proper appearance there will be 1 complete metamorphosis in beauty and improvement
i shall show you the drawing and i shall show you the architect for he be to dine here today charming
say the general
tis like paradise here say the generals lady and yonder you have 1 knight castle
thats my poultryhouse observe the count
the pigeon live in the tower the turkey in the 1 floor but old elsie rule in the ground floor
she have apartment on all side of her
the sitting hen have their own room and the hen with chicken have theirs and the duck have their own particular door lead to the water charming
repeat the general
and all sail forth to see these wonderful thing
old elsie stand in the room on the ground floor and by her side stand architect_george
he and emily now meet for the 1 time after several year and they meet in the poultryhouse
yes there he stand and be handsome enough to be look at
his face be frank and energetic he have black shine hair and 1 smile about his mouth which say i have 1 brownie that sit in my ear and know every one of you inside and out old_elsie have pull off her wooden shoe and stand there in her stocking to do honor to the noble guest
the hen cluck and the cock crow and the duck waddle to_and_fro and say quack quack
but the fair pale girl the friend of his childhood the daughter of the general stand there with 1 rosy blush on her usually pale cheek and her eye open wide and her mouth seem to speak without utter 1 word and the greeting he receive from her be the many beautiful greet 1 young man can desire from 1 young lady if they be not relate or have not dance many time together and she and the architect have never dance together
the count shake hand with him and introduce him
he be not altogether 1 stranger our young friend george_the_generals lady bow to him and the generals daughter be very nearly give him her hand but she do not give it to him
our little master_george
say the general
old friend
charm
you have become quite 1 italian say the generals lady and i presume you speak the language like 1 native
my wife sing the language but she do not speak it observe the general
at dinner george sit at the right hand of emily whom the general have take down while the count lead in the generals lady
mr_george talk and tell of his travel and he can talk well and be the life and soul of the table though the old count can have be it too
emily sit silent but she listen and her eye gleam but she say nothing
in the verandah among the flower she and george stand together the rosebushes conceal them
and george be speak again for he take the lead now
many thank for the kind consideration you show my old mother he say
i know that you go down to her on the night when my father die and you stay with her till his eye be close
my heartiest thank
he take emilys hand and kiss it he may do so_on such 1 occasion
she blush deeply but press his hand and look at him with her dear blue eye
your mother be 1 dear soul
she say
how fond she be of her son
and she let me read all your letter so_that i almost believe i know you
how kind you be to me when i be little girl
you use to give me picture which you tear in 2 say george
no i have still your drawing of the castle i must build the castle in reality now say george and he become quite warm at his own word
the general and the generals lady talk to each other in their room about the porter son how he know how to behave and to express himself with the great propriety
he may be 1 tutor say the general
intellect
say the generals lady but she do not say anything many
during the beautiful summertime mr_george several time visit the count at his castle and he be miss when he do not come
how_much the good god have give you that he have not give to us poor mortal say emily to him
be you sure you be very grateful for it
it flatter george that the lovely young girl should look up to him and he think then that emily have unusually good ability
and the general feel many and many convince that george be no cellarchild
his mother be 1 very good woman he observe
it be only right i should do her that justice now she be in her grave the summer pass away and the winter come again there be talk about mr_george
he be highly respect and be receive in the 1 circle
the general have meet him at 1 court ball
and now there be 1 ball to be give in the generals house for emily and can mr_george be invite to it
he whom the king invite can be invite by the general also say the general and draw himself up till he stand quite LN_in:1 high than before
mr_george be invite and he come prince and count come and they dance 1 good than the other
but emily can only dance one dance the 1 for she make 1 false step nothing of consequence but her foot hurt her so_that she have to be careful and leave off dance and look at the other
so she sit and look on and the architect stand by her side
i suppose you be give her the whole history of st_peters say the general as he pass by and smile like the personification of patronage
with the same patronize smile he receive mr_george 1 few day afterwards
the young man come no_doubt to return thank for the invitation to the ball
what else can it be
but indeed there be something else something very astonish and startle
he speak word of sheer lunacy so_that the general can hardly believe his own ear
it be the height of rhodomontade 1 offer quite 1 inconceivable offer mr_george come to ask the hand of emily in marriage
man
cry the general and his brain seem to be boil
i dont understand you at all
what be it you say
what be it you want
i dont know you
sir
man
what possess you to break into my house
and be i to stand here and listen to you
he step backwards into his bedroom lock the door behind him and leave mr_george standing alone
george stand still for 1 few minute and then turn round and leave the room
emily be standing in the corridor
my father have answer
she say and her voice tremble
george press her hand
he have escape me he reply but 1 good time will come there be tear in emilys eye but in the young man eye shine courage and confidence and the sun shine through the window and cast his beam on the pair and give them his blessing
the general sit in his room burst hot
yes he be still boil until he boil over in the exclamation lunacy
porter
madness
not 1 hour be over before the generals lady know it out of the generals own mouth
she call emily and remain alone with her
you poor child she say to insult you so
to insult us so
there be tear in your eye too but they become you well
you look beautiful in tear
you look as i look on my weddingday
weep on my sweet emily_yes that i must say emily if you and my father do not say yes child
scream the generals lady you be ill you be talk wildly and i shall have 1 many terrible headache
oh what 1 misfortune be come upon our house
dont make your mother die emily or you will have no mother and the eye of the generals lady be wet for she can not bear to think of her own death
in the newspaper there be 1 announcement
mr_george have be elect professor_of_the_fifth_class number 8 its 1 pity that his parent be dead and can not read it say the new porter people who now live in the cellar under the generals apartment
they know that the professor have be bear and grow up within their 4 wall
now hell get 1 salary say the man
yes thats not much for 1 poor child say the woman
$_USD:18 1 year say the man
why its 1 good deal of money no_i mean the honor of it reply the wife
do you think he care for the money
those few dollar he can earn Z_times over and many likely hell get 1 rich wife into the bargain
if we have child of our own husband our child should be 1 architect and 1 professor too george be speak well of in the cellar and he be speak well of in the 1 floor
the old count take upon himself to do that
the picture he have draw in his childhood give occasion for it
but how do the conversation come to turn on these picture
why they have be talk of russia and of moscow and thus mention be make of the kremlin which little george have once draw for miss_emily
he have draw many picture but the count especially remember one emilys_castle where she be to sleep and to dance and to play at receive guest
the professor be 1 true man say the count and would be 1 privy councillor before he die it be not at all unlikely and he may build $_BRL:1 castle for the young lady before that time come why not
that be 1 strange jest remark the generals lady when the count have go away
the general shake his head thoughtfully and go out for 1 ride with his groom behind him at 1 proper distance and he sit many stiffly than ever on his high horse
it be emilys birthday
flower book letter and visit card come pour in
the generals lady kiss her on the mouth and the general kiss her on the forehead they be affectionate parent and they and emily have to receive grand visitor 2 of the princes
they talk of ball and theatre of diplomatic mission of the government of empire and nation and then they speak of talent native talent and so the discourse turn upon the young architect
he be build up 1 immortality for himself say one and he will certainly build his way into one of our 1 family
one of our 1 family
repeat the general and afterwards the generals lady what be mean by one of our 1 family
i know for whom it be intend say the generals lady but i shall not say it
i dont think it
heaven dispose but i shall be astonish i be astonish also
say the general
i havent 1 idea in my head
and he fall into 1 reverie wait for idea
there be 1 power 1 nameless power in the possession of favor from above the favor of providence and this favor little george have
but we be forget the birthday
emilys room be fragrant with flower send by male and female friend on the table lie beautiful present for greeting and remembrance but none can come from george none can come from him but it be not necessary for the whole house be full of remembrance of him
even out of the ashbin the blossom of memory peep forth for emily have sit whimper there on the day when the windowcurtain catch fire and george arrive in the character of fire engine
1 glance out of the window and the acacia tree remind of the day of childhood
flower and leaf have fall but there stand the tree cover with hoar frost look like 1 single huge branch of coral and the moon shine clear and large among the twig unchanged in its changings as it be when george divide his bread and butter with little emily
out of 1 box the girl take the drawing of the czars palace and of her own castle remembrance of george
the drawing be look at and many thought come
she remember the day when unobserved by her father and mother she have go down to the porter wife who lie die
once again she seem to sit beside her hold the die womans hand in hers hear the die womans last word blessing_george
the mother be think of her son and now emily give her own interpretation to those word
yes george be certainly with her on her birthday
it happen that the next day be another birthday in that house the generals birthday
he have be bear the day after his daughter but before her of course many year before her
many present arrive and among them come 1 saddle of exquisite workmanship 1 comfortable and costly saddle one of the princes have just such another
now from whom may this saddle come
the general be delight
there be 1 little note with the saddle
now if the word on the note have be many thank for yesterday reception we may easily have guess from whom it come
but the word be from somebody whom the general do not know whom in the world do i not know
exclaim the general
i know everybody and his thought wander all through society for he know everybody there
that saddle come from my wife
he say at_last
she be tease me charm
but she be not tease him those time be past
again there be 1 feast but it be not in the generals house it be 1 fancy ball at the princes and mask be allow too
the general go as rubens in 1 spanish costume with 1 little ruff round his neck 1 sword by his side and 1 stately manner
the generals lady be madame_rubens in black velvet make high round the neck exceedingly warm and with 1 millstone round her neck in the shape of 1 great ruff accurately dress after 1 dutch picture in the possession of the general in which the hand be especially admire
they be just like the hand of the generals lady
emily be psyche
in white crape and lace she be like 1 float swan
she do not want wing at all
she only wear them as emblematic of psyche
brightness splendor light and flower wealth and taste appear at the ball there be so much to see that the beautiful hand of madame_rubens make no sensation at all
1 black domino with 1 acacia blossom in his cap dance with psyche
who be that
ask the generals lady
his royal_highness reply the general
i be quite sure of it
i know him directly by the pressure of his hand the_generals lady doubt it
general rubens have no doubt about it
he go up to the black domino and write the royal letter in the mask hand
these be deny but the mask give him 1 hint
the word that come with the saddle one whom you do not know general_but_i do know you say the general
it be you who send me the saddle the domino raise his hand and disappear among the other guest
who be that black domino with whom you be dance emily
ask the generals lady
i do not ask his name she reply because you know it
it be the professor
your protege be here count
she continue turn to that nobleman who stand close by
1 black domino with acacia blossom in his cap very likely my dear lady reply the count
but one of the princes wear just the same costume i know the pressure of the hand say the general
the saddle come from the prince
i be so certain of it that i can invite that domino to dinner do so
if it be the prince he will certainly come reply the count
and if it be the other he will not come say the general and approach the black domino who be just speak with the king
the general give 1 very respectful invitation that they may make each other acquaintance and he smile in his certainty concern the person he be invite
he speak loud and distinctly
the domino raise his mask and it be george
do you repeat your invitation general
he ask
the general certainly seem to grow LN_in:1 tall assume 1 many stately demeanor and take 2 step backward and one step forward as_if he be dance 1 minuet and then come as much gravity and expression into the face of the general as the general can contrive to infuse into it but he reply i never retract my word
you be invite professor
and he bow with 1 glance at the king who must have hear the whole dialogue
now there be 1 company to dinner at the generals but only the old count and his protege be invite
i have my foot under his table think george
thats lay the foundation stone and the foundation stone be really lay with great ceremony at the house of the general and of the generals lady
the man have come and have speak quite like 1 person in good society and have make himself very agreeable so_that the general have often to repeat his charming
the general talk of this dinner talk of it even to 1 court lady and this lady one of the many intellectual person about the court ask to be invite to meet the professor the next time he should come
so he have to be invite again and he be invite and come and be charm again he can even play chess
hes not out of the cellar say the general hes quite 1 distinguish person
there be many distinguish person of that kind and its no fault of his the_professor who be receive in the kings palace may very well be receive by the general but that he can ever belong to the house be out of the question only the whole town be talk of it
he grow and grow
the dew of favor fall from above so no_one be surprise after all that he should become 1 privy_councillor and emily_a_privy_councillors lady
life be either 1 tragedy or 1 comedy say the general
in tragedy they die in comedy they marry one another in this case they marry
and they have 3 clever boy but not all at once
the sweet child ride on their hobbyhorses through all the room when they come to see the grandparent
and the general also ride on his stick he ride behind them in the character of groom to the little privy_councillors
and the generals lady sit on her sofa and smile at them even when she have her severe headache
so far do george get and much far else it have not be worth while to tell the story of the_porters_son
the_end
