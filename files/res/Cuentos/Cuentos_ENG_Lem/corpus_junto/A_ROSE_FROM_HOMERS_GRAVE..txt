1 rose_from_homers_grave_all the song of the east speak of the love of the nightingale for the rise in the silent starlight night
the wing songster serenade the fragrant flower
not far from smyrna where the merchant drive his load camel proudly arch their long neck as they journey beneath the lofty pine over holy ground i saw 1 hedge of rose
the turtledove fly among the branch of the tall tree and as the sunbeam fall upon her wing they glisten as_if they be motherofpearl
on the rosebush grow 1 flower many beautiful than them all and to her the nightingale sing of his woe but the rise remain silent not even 1 dewdrop lie like 1 tear of sympathy on her leaf
at_last she bow her head over 1 heap of stone and say here rest the great singer in the world over his tomb will i spread my fragrance and on it i will let my leaf fall when the storm scatter them
he who sing of troy become earth and from that earth i have spring
i 1 rise from the grave of homer be too lofty to bloom for 1 nightingale then the nightingale sing himself to death
1 cameldriver come by with his load camel and his black slave his little son find the dead bird and bury the lovely songster in the grave of the great homer while the rise tremble in the wind
the evening come and the rise wrap her leaf many closely round her and dream and this be her dream
it be 1 fair sunshiny day 1 crowd of stranger draw near who have undertake 1 pilgrimage to the grave of homer
among the stranger be 1 minstrel from the north the home of the cloud and the brilliant light of the aurora borealis
he pluck the rise and place it in 1 book and carry it away into 1 distant part of the world his fatherland
the rise fade with grief and lay between the leaf of the book which he open in his own home saying here be 1 rise from the grave of homer_then the flower awake from her dream and tremble in the wind
1 drop of dew fall from the leaf upon the singer grave
the sun rise and the flower bloom many beautiful than ever
the day be hot and she be still in her own warm asia
then footstep approach stranger such as the rise have see in her dream come by and among them be 1 poet from the north he pluck the rise press 1 kiss upon her fresh mouth and carry her away to the home of the cloud and the northern light
like 1 mummy the flower now rest in his iliad and as in her dream she hear him say as he open the book here be 1 rise from the grave of homer_the_end
