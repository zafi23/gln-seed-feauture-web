the_snail_and_the_rosetree_round about the garden run 1 hedge of hazelbushes beyond the hedge be field and meadow with cow and sheep but in the middle of the garden stand 1 rosetree in bloom under which sit 1 snail whose shell contain 1 great deal that be himself
only wait till my time come he say i shall do many than grow rose bear nut or give milk like the hazelbush the cow and the sheep i expect 1 great deal from you say the rosetree
may i ask when it will appear
i take my time say the snail
youre always in such 1 hurry
that do not excite expectation the following year the snail lie in almost the same spot in the sunshine under the rosetree which be again bud and bear rose as fresh and beautiful as ever
the snail creep half out of his shell stretch out his horn and draw them in again
everything be just as it be last year
no progress at all the rosetree stick to its rose and get no far the summer and the autumn pass the rosetree bore rose and bud till the snow fall and the weather become raw and wet then it bend down its head and the snail creep into the ground
1 new year begin the rose make their appearance and the snail make his too
you be 1 old rosetree now say the snail
you must make haste and die
you have give the world all that you have in you whether it be of much importance be 1 question that i have not have time to think about
but this much be clear and plain that you have not do the little for your inner development or you would have produce something else
have you anything to say in defense
you will now soon be nothing but 1 stick
do you understand what i say
you frighten me say the rise tree
i have never think of that no you have never take the trouble to think at all
have you ever give yourself 1 account why you bloom and how your bloom come about why just in that way and in no other
no say the rosetree
i bloom in gladness because i can not do otherwise
the sun shine and warm me and the air refresh me i drink the clear dew and the invigorate rain
i breathe and i live
out of the earth there arise 1 power within me whilst from above i also receive strength i feel 1 everrenewed and everincreasing happiness and therefore i be oblige to go on bloom
that be my life i can not do otherwise you have lead 1 very easy life remark the snail
certainly
everything be give me say the rosetree
but still many be give to you
yours be one of those deepthinking nature one of those highly gift mind that astonish the world i have not the slight intention of doing so say the snail
the world be nothing to me
what have i to do with the world
i have enough to do with myself and enough in myself but must we not all here on earth give up our good part to other and offer as much as lie in our power
it be true i have only give rose
but you you who be so richly endow what have you give to the world
what will you give it
what have i give
what be i go to give
i spit at it its good for nothing and do not concern me
for my part you may go on bearing rose you can not do anything else
let the hazel bush bear nut and the cow and sheep give milk they have each their public
i have mine in myself
i retire within myself and there i stop
the world be nothing to me with this the snail withdraw into his house and block up the entrance
thats very sad say the rise tree
i can not creep into myself however much i may wish to do so i have to go on bearing rose
then they drop their leaf which be blow away by the wind
but i once saw how 1 rise be lay in the mistresss hymnbook and how one of my rose find 1 place in the bosom of 1 young beautiful girl and how another be kiss by the lip of 1 child in the glad joy of life
that do me good it be $_BRL:1 blessing
those be my recollection my life and the rise tree go on bloom in innocence while the snail lie idle in his house the world be nothing to him
year pass by
the snail have turn to earth in the earth and the rise tree too
even the souvenir rise in the hymnbook be fade but in the garden there be other rise tree and other snail
the latter creep into their house and spit at the world for it do not concern them
shall we read the story all over again
it will be just the same
the_end
