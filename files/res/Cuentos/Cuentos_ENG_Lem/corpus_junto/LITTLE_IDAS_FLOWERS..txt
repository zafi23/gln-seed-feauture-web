little_idas_flowers_my poor flower be quite dead say little ida they be so pretty yesterday evening and now all the leaf be hang down quite wither
what do they do that for she ask of the student who sit on the sofa she like him very much he can tell the many amuse story and cut out the prettiest picture heart and lady dance castle with door that open as_well as flower he be 1 delightful student
why do the flower look so fade today
she ask again and point to her nosegay which be quite wither
dont you know what be the matter with them
say the student
the flower be at 1 ball last night and therefore it be no wonder they hang their head but flower can not dance
cry little ida
yes indeed they can reply the student
when it grow dark and everybody be asleep they jump about quite merrily
they have 1 ball almost every night can child go to these ball
yes say the student little daisy and lily of the valley where do the beautiful flower dance
ask little ida
have you not often see the large castle outside the gate of the town where the king life in summer and where the beautiful garden be full of flower
and have you not feed the swan with bread when they swim towards you
well the flower have capital ball there believe me i be in the garden out there yesterday with my mother say ida but all the leaf be off the tree and there be not 1 single flower leave
where be they
i use to see so many in the summer they be in the castle reply the student
you must know that as soon as the king and all the court be go into the town the flower run out of the garden into the castle and you should see how merry they be
the 2 many beautiful rose seat themselves on the throne and be call the king and queen then all the red cockscombs range themselves on each side and bow these be the lordsinwaiting
after that the pretty flower come in and there be 1 grand ball
the blue violet represent little naval cadet and dance with hyacinth and crocus which they call young lady
the tulip and tigerlilies be the old lady who sit and watch the dance so_that everything may be conduct with order and propriety but say little ida be there no_one there to hurt the flower for dance in the king castle
no_one know anything about it say the student
the old steward of the castle who have to watch there at night sometimes come in but he carry 1 great bunch of key and as soon as the flower hear the key rattle they run and hide themselves behind the long curtain and stand quite still just peep their head out
then the old steward say i smell flower here but he can not see them oh how capital say little ida clap her hand
should i be able to see these flower
yes say the student mind you think of it the next time you go out no_doubt you will see them if you peep through the window
i do so today and i saw 1 long yellow lily lie stretch out on the sofa
she be 1 court lady can the flower from the botanical_gardens go to these ball
ask ida
it be such 1 distance
oh yes say the student whenever they like for they can fly
have you not see those beautiful red white
and yellow butterfly that look like flower
they be flower once
they have fly off their stalk into the air and flap their leaf as_if they be little wing to make them fly
then if they behave well they obtain permission to fly about during the day instead_of be oblige to sit still on their stem at home and so in time their leave become real wing
it may be however that the flower in the botanical_gardens have never be to the king palace and therefore they know nothing of the merry doing at night which take place there
i will tell you what to do and the botanical professor who live close by here will be so surprise
you know him very well do you not
well next time you go into his garden you must tell one of the flower that there be go to be 1 grand ball at the castle then that flower will tell all the other and they will fly away to the castle as_soon_as_possible
and when the professor walk into his garden there will not be 1 single flower leave
how he will wonder what have become of them
but how can one flower tell another
flower can not speak
no certainly not reply the student but they can make sign
have you not often see that when the wind blow they nod at one another and rustle all their green leaf
can the professor understand the sign
ask ida
yes to be sure he can
he go one morning into his garden and saw 1 sting nettle make sign with its leaf to 1 beautiful red carnation
it be say you be so pretty i like you very much but the professor do not approve of such nonsense so he clap his hand on the nettle to stop it
then the leaf which be its finger sting him so sharply that he have never venture to touch 1 nettle since oh how funny
say ida and she laugh
how can anyone put such notion into 1 childs head
say 1 tiresome lawyer who have come to pay 1 visit and sit on the sofa
he do not like the student and would grumble when he saw him cut out droll or amuse picture
sometimes it would be 1 man hanging on 1 gibbet and hold 1 heart in his hand as_if he have be steal heart
sometimes it be 1 old witch ride through the air on 1 broom and carry her husband on her nose
but the lawyer do not like such joke and he would say as he have just say how can anyone put such nonsense into 1 childs head
what absurd fancy there be
but to little ida all these story which the student tell her about the flower seem very droll and she think over them 1 great deal
the flower do hang their head because they have be dance all night and be very tire and many likely they be ill then she take them into the room where 1 number of toy lie on 1 pretty little table and the whole of the table drawer besides be full of beautiful thing
her doll sophy lay in the doll bed asleep and little ida say to her you must really get up sophy and be content to lie in the drawer tonight the poor flower be ill and they must lie in your bed then perhaps they will get well again so she take the doll out who look quite cross and say not 1 single word for she be angry at be turn out of her bed
ida place the flower in the doll bed and draw the quilt over them
then she tell them to lie quite still and be good while she make some tea for them so_that they may be quite well and able to get up the next morning
and she draw the curtain close round the little bed so_that the sun may not shine in their eye
during the whole evening she can not help think of what the student have tell her
and before she go to bed herself she be oblige to peep behind the curtain into the garden where all her mother beautiful flower grow hyacinth and tulip and many other
then she whisper to them quite softly i know you be go to 1 ball tonight but the flower appear as_if they do not understand and not 1 leaf move still ida feel quite sure she know all about it
she lie awake 1 long time after she be in bed think how pretty it must be to see all the beautiful flower dance in the king garden
i wonder if my flower have really be there she say to herself and then she fall asleep
in the night she awake she have be dream of the flower and of the student as_well as of the tiresome lawyer who find fault with him
it be quite still in idas bedroom the nightlamp burn on the table and her father and mother be asleep
i wonder if my flower be still lie in sophys bed she think to herself how_much i should like to know she raise herself 1 little and glance at the door of the room where all her flower and plaything lie it be partly open and as she listen it seem as_if some one in the room be play the piano but softly and many prettily than she have ever before hear it
now all the flower be certainly dance in there she think oh how_much i should like to see them but she do not dare move for fear of disturb her father and mother
if they would only come in here she think but they do not come and the music continue to play so beautifully and be so pretty that she can resist no_longer
she creep out of her little bed go softly to the door and look into the room
oh what 1 splendid sight there be to be sure
there be no nightlamp burning but the room appear quite light for the moon shine through the window upon the floor and make it almost like day
all the hyacinth and tulip stand in 2 long row down the room not 1 single flower remain in the window and the flowerpot be all empty
the flower be dance gracefully on the floor make turn and hold each other by their long green leaf as they swing round
at the piano sit 1 large yellow lily which little ida be sure she have see in the summer for she remember the student saying she be very much like miss_lina one of idas friend
they all laugh at him then but now it seem to little ida as_if the tall yellow flower be really like the young lady
she have just the same manner while play bend her long yellow face from side to side and nod in time to the beautiful music
then she saw 1 large purple crocus jump into the middle of the table where the plaything stand go up to the doll bedstead and draw back the curtain there lie the sick flower but they get up directly and nod to the other as 1 sign that they wish to dance with them
the old rough doll with the break mouth stand up and bow to the pretty flower
they do not look ill at all now but jump about and be very merry yet none of them notice little ida
presently it seem as_if something fall from the table
ida look that way and saw 1 slight carnival rod jump down among the flower as_if it belong to them it be however very smooth and neat and 1 little wax doll with 1 broad brim hat on her head like the one wear by the lawyer sit upon it
the carnival rod hop about among the flower on its 3 red stilt foot and stamp quite loud when it dance the mazurka the flower can not perform this dance they be too light to stamp in that manner
all at once the wax doll which ride on the carnival rod seem to grow large and tall and it turn round and say to the paper flower how can you put such thing in 1 childs head
they be all foolish fancy and then the doll be exactly like the lawyer with the broad brim hat and look as yellow and as cross as he do but the paper doll strike him on his thin leg and he shrink up again and become quite 1 little wax doll
this be very amuse and ida can not help laugh
the carnival rod go on dance and the lawyer be oblige to dance also
it be no use he may make himself great and tall or remain 1 little wax doll with 1 large black hat still he must dance
then at_last the other flower intercede for him especially those who have lie in the doll bed and the carnival rod give up his dance
at the same moment 1 loud knock be hear in the drawer where idas doll sophy lay with many other toy
then the rough doll run to the end of the table lay himself flat down upon it and begin to pull the drawer out 1 little way
then sophy raise himself and look round quite astonish there must be 1 ball here tonight say sophy
why do not somebody tell me
will you dance with me
say the rough doll
you be the right sort to dance with certainly say she turn her back upon him
then she seat herself on the edge of the drawer and think that perhaps one of the flower would ask her to dance but none of them come
then she cough hem hem ahem but for all that not one come
the shabby doll now dance quite alone and not very badly after all
as none of the flower seem to notice sophy she let herself down from the drawer to the floor so_as_to make 1 very great noise
all the flower come round her directly and ask if she have hurt herself especially those who have lie in her bed
but she be not hurt at all and idas flower thank her for the use of the nice bed and be very kind to her
they lead her into the middle of the room where the moon shine and dance with her while all the other flower form 1 circle round them
then sophy be very happy and say they may keep her bed she do not mind lie in the drawer at all
but the flower thank her very much and say we can not live long
tomorrow morning we shall be quite dead and you must tell little ida to bury us in the garden near to the grave of the canary then in the summer we shall wake up and be many beautiful than ever no you must not die say sophy as she kiss the flower
then the door of the room open and 1 number of beautiful flower dance in
ida can not imagine where they can come from unless they be the flower from the king garden
first come 2 lovely rose with little golden crown on their head these be the king and queen
beautiful stock and carnation follow bow to every one present
they have also music with them
large poppy and peony have peashells for instrument and blow into them till they be quite red in the face
the bunch of blue hyacinth and the little white snowdrop jingle their belllike flower as_if they be real bell
then come many many flower blue violet purple heartsease daisy and lily of the valley and they all dance together and kiss each other
it be very beautiful to behold
at_last the flower wish each other goodnight
then little ida creep back into her bed again and dream of all she have see
when she arise the next morning she go quickly to the little table to see if the flower be still there
she draw aside the curtain of the little bed
there they all lay but quite fade much many so than the day before
sophy be lie in the drawer where ida have place her but she look very sleepy
do you remember what the flower tell you to say to me
say little ida
but sophy look quite stupid and say not 1 single word
you be not kind at all say ida and yet they all dance with you then she take 1 little paper box on which be paint beautiful bird and lay the dead flower in it
this shall be your pretty coffin she say and by and by when my cousin come to visit me they shall help me to bury you out in the garden so_that next summer you may grow up again many beautiful than ever her cousin be 2 goodtempered boy whose name be james and adolphus
their father have give them each 1 bow and arrow and they have bring them to show ida
she tell them about the poor flower which be dead and as soon as they obtain permission they go with her to bury them
the 2 boy walk 1 with their crossbow on their shoulder and little ida follow carry the pretty box contain the dead flower
they dig 1 little grave in the garden
ida kiss her flower and then lay them with the box in the earth
james and adolphus then fire their crossbow over the grave as they have neither gun nor cannon
the_end
