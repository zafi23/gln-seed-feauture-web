the_portuguese_duck_a_duck once arrive from portugal but there be some who say she come from spain which be almost the same thing
at all event she be call the portuguese and she lay egg be kill and cook and there be 1 end of her
but the duckling which creep forth from the egg be also call portuguese and about that there may be some question
but of all the family one only remain in the duckyard which may be call 1 farmyard as the chicken be admit and the cock strut about in 1 very hostile manner
he annoy me with his loud crow say the portuguese duck but still hes 1 handsome bird theres no deny that although hes not 1 drake
he ought to moderate his voice like those little bird who be sing in the limetrees over there in our neighbour garden but that be 1 art only acquire in polite society
how sweetly they sing there it be quite 1 pleasure to listen to them
i call it portuguese sing
if i have only such 1 little singingbird id be kind and good as 1 mother to him for its in my nature in my portuguese blood while she be speak one of the little singingbirds come tumble head over heel from the roof into the yard
the cat be after him but he have escape from her with 1 break wing and so come tumble into the yard
thats just like the cat shes 1 villain say the portuguese duck
i remember her way when i have child of my own
how can such 1 creature be allow to live and wander about upon the roof
i dont think they allow such thing in portugal_she pity the little singingbird and so do all the other duck who be not portuguese
poor little creature
they say one after another as they come up
we cant sing certainly but we have 1 soundingboard or something of the kind within us we can feel that though we dont talk about it but_i can talk say the portuguese duck and ill do something for the little fellow its my duty and she step into the watertrough and beat her wing upon the water so strongly that the bird be nearly drown by 1 showerbath but the duck mean it kindly
that be 1 good deed she say i hope the other will take example by it tweet tweet
say the little bird for one of his wing be break he find it difficult to shake himself but he quite understand that the bath be mean kindly and he say you be very kindhearted madam but he do not wish for TM_s:1 bath
i have never think about my heart reply the portuguese duck but i know that i love all my fellowcreatures except the cat and nobody can expect me to love her for she eat up 2 of my duckling
but pray make yourself at home it be easy to make one self comfortable
i be myself from 1 foreign country as you may see by my feathery dress
my drake be 1 native of these part hes not of my race but i be not proud on that account
if any one here can understand you i may say positively i be that person shes quite full of portulak say 1 little common duck who be witty
all the common duck consider the word portulak 1 good joke for it sound like portugal
they nudge each other and say quack
that be witty
then the other duck begin to notice the little bird
the portuguese have certainly 1 great flow of language they say to the little bird
for our part we dont care to fill our beak with such long word but we sympathize with you quite as much
if we dont do anything else we can walk about with you everywhere and we think that be the good thing we can do you have 1 lovely voice say one of the old duck it must be great satisfaction to you to be able to give so much pleasure as you do
i be certainly no judge of your sing so i keep my beak shut which be good than talk nonsense as other do dont plague him so interpose the portuguese duck he require rest and nursing
my little singingbird do you wish me to prepare another bath for you
oh no no pray let me dry implore the little bird
the watercure be the only remedy for me when i be not well say the portuguese
amusement too be very beneficial
the fowl from the neighbourhood will soon be here to pay you 1 visit
there be 2 cochin_chinese amongst them they wear feather on their leg and be well educate
they have be bring from 1 great distance and consequently i treat them with great respect than i do the other then the fowl arrive and the cock be polite enough today to keep from be rude
you be $_BRL:1 songster he say you do as much with your little voice as it be possible to do but there require many noise and shrillness in any one who wish it to be know who he be the 2 chinese be quite enchant with the appearance of the singingbird
his feather have be much ruffle by his bath so_that he seem to them quite like 1 tiny chinese fowl
hes charm they say to each other and begin 1 conversation with him in whisper use the many aristocratic chinese dialect we be of the same race as yourself they say
the duck even the portuguese be all aquatic bird as you must have notice
you do not know us yet very few know us or give themselves the trouble to make our acquaintance not even any of the fowl though we be bear to occupy 1 high grade in society than many of them
but that do not disturb us we quietly go on in our own way among the rest whose idea be certainly not ours for we look at the bright side of thing and only speak what be good although that be sometimes very difficult to find where none exist
except ourselves and the cock there be not one in the yard who can be call talented or polite
it can not even be say of the duck and we warn you little bird not to trust that one yonder with the short tail feather for she be cunning that curiously mark one with the crooked stripe on her wing be 1 mischiefmaker and never let any one have the last word though she be always in the wrong
that fat duck yonder speak evil of every one and that be against our principle
if we have nothing good to tell we close our beak
the portuguese be the only one who have have any education and with whom we can associate but she be passionate and talk too much about portugal_i wonder what those 2 chinese be whisper about whisper one duck to another they be always do it and it annoy me
we never speak to them now the drake come up and he think the little singingbird be 1 sparrow
well i dont understand the difference he say it appear to me all the same
hes only 1 plaything and if people will have plaything why let them i say dont take any notice of what he say whisper the portuguese hes very well in matter of business and with him business be place before everything
but now i shall lie down and have 1 little rest
it be 1 duty we owe to ourselves that we may be nice and fat when we come to be embalm with sage and onion and apple so she lay herself down in the sun and wink with one eye she have 1 very comfortable place and feel so comfortable that she fall asleep
the little singingbird busy himself for_some_time with his break wing and at_last he lie down too quite close to his protectress
the sun shine warm and bright and he find out that it be 1 very good place
but the fowl of the neighbourhood be all awake and to tell the truth they have pay 1 visit to the duckyard simply and solely to find food for themselves
the chinese be the 1 to leave and the other fowl soon follow them
the witty little duck say of the portuguese that the old lady be get quite 1 dote ducky all the other duck laugh at this
dote ducky they whisper
oh thats too witty
and then they repeat the former joke about portulak and declare it be many amuse
then they all lie down to have 1 nap
they have be lie asleep for_some_time when suddenly something be throw into the yard for them to eat
it come down with such 1 bang that the whole company start up and clap their wing
the portuguese awake too and rush over to the other side in so do she tread upon the little singingbird
tweet he cry you tread very hard upon me madam well then why do you lie in my way
she retort you must not be so touchy
i have nerve of my own but i do not cry tweet dont be angry say the little bird the tweet slip out of my beak unawares the_portuguese do not listen to him but begin eat as fast as she can and make 1 good meal
when she have finish she lie down again and the little bird who wish to be amiable begin to sing chirp and twitter the dewdrops glitter in the hour of sunny spring ill sing my good till_i go to rest with my head behind my wing now_i want rest after my dinner say the portuguese you must conform to the rule of the house while you be here
i want to sleep now the little bird be quite take aback for he mean it kindly
when madam awake afterwards there he stand before her with 1 little corn he have find and lay it at her foot but as she have not sleep well she be naturally in 1 bad temper
give that to 1 chicken she say and dont be always stand in my way why be you angry with me
reply the little singingbird what have i do
do
repeat the portuguese duck your mode of express yourself be not very polite
i must call your attention to that fact it be sunshine here yesterday say the little bird but today it be cloudy and the air be close you know very little about the weather i fancy she retort the day be not over yet
dont stand there look so stupid but you be look at me just as the wicked eye look when i fall into the yard yesterday impertinent creature
exclaim the portuguese duck would you compare me with the cat that beast of prey
theres not 1 drop of malicious blood in me
ive take your part and now ill teach you good manner so saying she make 1 bite at the little singingbirds head and he fall dead on the ground
now whatever be the meaning of this
she say can he not bear even such 1 little peck as i give him
then certainly he be not make for this world
ive be like 1 mother to him i know that for ive 1 good heart then the cock from the neighbor yard stick his head in and crow with steamengine power
youll kill me with your crow she cry its all your fault
hes lose his life and im very near lose mine theres not much of him lie there observe the cock
speak of him with respect say the portuguese duck for he have manner and education and he can sing
he be affectionate and gentle and that be as rare 1 quality in animal as in those who call themselves human being then all the duck come crowd round the little dead bird
duck have strong passion whether they feel envy or pity
there be nothing to envy here so they all show 1 great deal of pity even the 2 chinese
we shall never have another singingbird again amongst us he be almost 1 chinese they whisper and then they weep with such 1 noisy cluck sound that all the other fowl cluck too but the duck go about with red eye afterwards
we have heart of our own they say nobody can deny that hearts
repeat the portuguese indeed you have almost as tender as the duck in portugal_let us think of get something to satisfy our hunger say the drake thats the many important business
if one of our toy be break why we have plenty many the_end
