the_shirtcollar_there be once 1 fine gentleman who possess among other thing 1 bootjack and 1 hairbrush but he have also the fine shirtcollar in the world and of this collar we be about to hear 1 story
the collar have become so old that he begin to think about get marry and one day he happen to find himself in the same washingtub as 1 garter
upon my word say the shirtcollar i have never see anything so slim and delicate so neat and soft before
may i venture to ask your name
i shall not tell you reply the garter
where do you reside when you be at home
ask the shirtcollar
but the garter be naturally shy and do not know how to answer such 1 question
i presume you be 1 girdle say the shirtcollar 1 sort of under girdle
i see that you be useful as_well as ornamental my little lady you must not speak to me say the garter i do not think i have give you any encouragement to do so oh when any one be as beautiful as you be say the shirtcollar be not that encouragement enough
get away dont come so near me say the garter you appear to me quite like 1 man i be 1 fine gentleman certainly say the shirtcollar i possess 1 bootjack and 1 hairbrush this be not true for these thing belong to his master but he be 1 boaster
dont come so near me say the garter i be not accustom to it affectation
say the shirtcollar
then they be take out of the washtub starch and hang over 1 chair in the sunshine and then lay on the ironingboard
and now come the glow iron
mistress widow say the shirtcollar little mistress widow i feel quite warm
i be change i be lose all my crease
you be burn 1 hole in me
ugh
i propose to you you old rag say the flatiron drive proudly over the collar for she fancy herself 1 steamengine which roll over the railway and draw carriage
you old rag
say she
the edge of the shirtcollar be 1 little fray so the scissor be bring to cut them smooth
oh
exclaim the shirtcollar what 1 firstrate dancer you would make you can stretch out your leg so well
i never saw anything so charm i be sure no human being can do the same i should think not reply the scissor
you ought to be 1 countess say the shirt collar but all i possess consist of 1 fine gentleman 1 bootjack and 1 comb
i wish i have 1 estate for your sake what
be he go to propose to me
say the scissor and she become so angry that she cut too sharply into the shirt collar and it be oblige to be throw by as useless
i shall be oblige to propose to the hairbrush think the shirt collar so he remark one day it be wonderful what beautiful hair you have my little lady
have you never think of be engage
you may know i should think of it answer the hair brush i be engage to the bootjack engaged
cry the shirt collar now there be no_one leave to propose to and then he pretend to despise all lovemaking
1 long time pass and the shirt collar be take in 1 bag to the papermill
here be 1 large company of rag the fine one lie by themselves separate from the coarser as it ought to be
they have all many thing to relate especially the shirt collar who be 1 terrible boaster
i have have 1 immense number of love affair say the shirt collar no_one leave me any peace
it be true i be 1 very fine gentleman quite stick up
i have 1 bootjack and 1 brush that i never use
you should have see me then when i be turn down
i shall never forget my 1 love she be 1 girdle so charm and fine and soft and she throw herself into 1 washing tub for my sake
there be 1 widow too who be warmly in love with me but i leave her alone and she become quite black
the next be 1 firstrate dancer she give me the wound from which i still suffer she be so passionate
even my own hairbrush be in love with me and lose all her hair through neglect love
yes i have have great experience of this kind but my great grief be for the garter the girdle i mean to say that jump into the washtub
i have 1 great deal on my conscience and it be really time i should be turn into white paper and the shirt collar come to this at_last
all the rag be make into white paper and the shirt collar become the very identical piece of paper which we now see and on which this story be print
it happen as 1 punishment to him for have boast so shockingly of thing which be not true
and this be 1 warning to us to be careful how we act for we may some day find ourselves in the ragbag to be turn into white paper on which our whole history may be write even its many secret action
and it would not be pleasant to have to run about the world in the form of 1 piece of paper tell everything we have do like the boast shirt collar
the_end
