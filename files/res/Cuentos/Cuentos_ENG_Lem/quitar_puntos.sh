#!/bin/bash

mkdir $2

cleanText(){
for f in $1/*
do
  filename=$(basename "$f")
  if [ -f "$f" ]; then
  	cat $f | tr -d "." | sed 's/ *$//'> $2/$filename

  else
	direc="$2/$filename"
	mkdir -m 777 $direc
	cleanText $f $direc 
	
  fi
done
}


cleanText $1 $2
