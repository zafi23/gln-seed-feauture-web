the_flax_the flax be in full bloom it have pretty little blue flower as delicate as the wing of 1 moth or even many so
the sun shine and the shower water it and this be just as good for the flax as it be for little child to be wash and then kiss by their mother
they look much prettier for it and so do the flax
people say that i look exceedingly well say the flax and that i be so fine and long that i shall make 1 beautiful piece of linen
how fortunate i be it make me so happy it be such 1 pleasant thing to know that something can be make of me
how the sunshine cheer me and how sweet and refresh be the rain my happiness overpower me no_one in the world can feel happier than i be ah yes no_doubt say the fern but you do not know the world yet as_well as i do for my stick be knotty and then it sing quite mournfully snip snap snurre basse lurre the song be end no it be not end say the flax
tomorrow the sun will shine or the rain descend
i feel that i be grow
i feel that i be in full blossom
i be the happiest of all creature well one day some people come who take hold of the flax and pull it up by the root this be painful then it be lay in water as_if they intend to drown it and after that place near 1 fire as_if it be to be roast all this be very shock
we can not expect to be happy always say the flax by experience evil as_well as good we become wise and certainly there be plenty_of evil in store for the flax
it be steep and roast and break and comb indeed it scarcely know what be do to it
at_last it be put on the spin wheel
whirr whirr go the wheel so quickly that the flax can not collect its thought
well i have be very happy he think in the midst of his pain and must be content with the past and content he remain till he be put on the loom and become 1 beautiful piece of white linen
all the flax even to the last stalk be use in make this one piece
well this be quite wonderful i can not have believe that i should be so favor by fortune
the fern be not wrong with its song of snip snap snurre basse lurre but the song be not end yet i be sure it be only just begin
how wonderful it be that after all i have suffer i be make something of at_last i be the luckiest person in the world so strong and fine and how white and what 1 length
this be something different to be 1 mere plant and bearing flower
then i have no attention nor any water unless it rain now i be watch and take care of
every morning the maid turn me over and i have 1 showerbath from the wateringpot every evening
yes and the clergymans wife notice me and say i be the good piece of linen in the whole parish
i can not be happier than i be now after some time the linen be take into the house place under the scissor and cut and tear into piece and then prick with needle
this certainly be not pleasant but at_last it be make into 12 garment of that kind which people do not like to name and yet everybody should wear one
see now then say the flax i have become something of importance
this be my destiny it be quite 1 blessing
now i shall be of some use in the world as everyone ought to be it be the only way to be happy
i be now divide into 12 piece and yet we be all one and the same in the whole dozen
it be many extraordinary good fortune years pass away and at_last the linen be so wear it can scarcely hold together
it must end very soon say the piece to each other we would gladly have hold together 1 little long but it be useless to expect impossibility and at length they fall into rag and tatter and think it be all over with them for they be tear to shred and steep in water and make into 1 pulp and dry and they know not what besides till all at once they find themselves beautiful white paper
well now this be 1 surprise 1 glorious surprise too say the paper
i be now fine than ever and i shall be write upon and who can tell what fine thing i may have write upon me
this be wonderful luck
and sure enough the many beautiful story and poetry be write upon it and only once be there 1 blot which be very fortunate
then people hear the story and poetry read and it make them wise and good for all that be write have 1 good and sensible meaning and 1 great blessing be contain in the word on this paper
i never imagine anything like this say the paper when i be only 1 little blue flower grow in the field
how can i fancy that i should ever be the mean of bring knowledge and joy to man
i can not understand it myself and yet it be really so
heaven know that i have do nothing myself but what i be oblige to do with my weak power for my own preservation and yet i have be promote from one joy and honor to another
each time i think that the song be end and then something high and good begin for me
i suppose now i shall be send on my travel about the world so_that people may read me
it can not be otherwise indeed it be many than probable for i have many splendid thought write upon me than i have pretty flower in olden time
i be happier than ever but the paper do not go on its travel it be send to the printer and all the word write upon it be set up in type to make 1 book or rather many hundred of book for so many many person can derive pleasure and profit from 1 print book than from the write paper and if the paper have be send around the world it would have be wear out before it have get half through its journey
this be certainly the wise plan say the write paper i really do not think of that
i shall remain at home and be hold in honor like some old grandfather as i really be to all these new book
they will do some good
i can not have wander about as they do
yet he who write all this have look at me as every word flow from his pen upon my surface
i be the many honor of all then the paper be tie in 1 bundle with other paper and throw into 1 tub that stand in the washhouse
after work it be well to rest say the paper and 1 very good opportunity to collect one thought
now i be able for the 1 time to think of my real condition and to know one self be true progress
what will be do with me now i wonder
no_doubt i shall still go forward
i have always progress hitherto as i know quite well now it happen one day that all the paper in the tub be take out and lay on the hearth to be burn
people say it can not be sell at the shop to wrap up butter and sugar because it have be write upon
the child in the house stand round the stove for they want to see the paper burn because it flame up so prettily and afterwards among the ash so many red spark can be see run one after the other here and there as quick as the wind
they call it see the child come out of school and the last spark be the schoolmaster
they often think the last spark have come and one would cry there go the schoolmaster but the next moment another spark would appear shine so beautifully
how they would like to know where the spark all go to
perhaps we shall find out some day but we dont know now
the whole bundle of paper have be place on the fire and be soon alight
ugh cry the paper as it burst into 1 bright flame ugh it be certainly not very pleasant to be burn but when the whole be wrap in flame the flame mount up into the air high than the flax have ever be able to raise its little blue flower and they glisten as the white linen never can have glisten
all the write letter become quite red in 1 moment and all the word and thought turn to fire
now i be mount straight up to the sun say 1 voice in the flame and it be as_if 1000 voice echo the word and the flame dart up through the chimney and go out at the top
then 1 number of tiny being as many in number as the flower on the flax have be and invisible to mortal eye float above them
they be even lighter and many delicate than the flower from which they be bear and as the flame be extinguish and nothing remain of the paper but black ash these little being dance upon it and whenever they touch it bright red spark appear
the child be all out of school and the schoolmaster be the last of all say the child
it be good fun and they sing over the dead ash snip snap snurre basse lure the song be end but the little invisible being say the song be never end the many beautiful be yet to come but the child can neither hear nor understand this nor should they for child must not know everything
the_end
