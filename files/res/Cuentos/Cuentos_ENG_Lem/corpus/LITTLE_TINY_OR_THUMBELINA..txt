little_tiny_or_thumbelina_there be once 1 woman who wish very much to have 1 little child but she can not obtain her wish
at_last she go to 1 fairy and say i should so very much like to have 1 little child can you tell me where i can find one
oh that can be easily manage say the fairy
here be 1 barleycorn of 1 different kind to those which grow in the farmer field and which the chicken eat put it into 1 flowerpot and see what will happen thank you say the woman and she give the fairy 12 shilling which be the price of the barleycorn
then she go home and plant it and immediately there grow up 1 large handsome flower something like 1 tulip in appearance but with its leave tightly close as_if it be still 1 bud
it be 1 beautiful flower say the woman and she kiss the red and goldencolored leaf and while she do so the flower open and she can see that it be $_BRL:1 tulip
within the flower upon the green velvet stamen sit 1 very delicate and graceful little maiden
she be scarcely half as long as 1 thumb and they give her the name of thumbelina or tiny because she be so small
1 walnutshell elegantly polish serve her for 1 cradle her bed be form of blue violetleaves with 1 roseleaf for 1 counterpane
here she sleep at night but during the day she amuse herself on 1 table where the woman have place 1 plateful of water
round this plate be wreath of flower with their stem in the water and upon it float 1 large tulipleaf which serve tiny for 1 boat
here the little maiden sit and row herself from side to side with 2 oar make of white horsehair
it really be 1 very pretty sight
tiny can also sing so softly and sweetly that nothing like her sing have ever before be hear
one night while she lie in her pretty bed 1 large ugly wet toad creep through 1 break pane of glass in the window and leap right upon the table where tiny lay sleep under her roseleaf quilt
what 1 pretty little wife this would make for my son say the toad and she take up the walnutshell in which little tiny lay asleep and jump through the window with it into the garden
in the swampy margin of 1 broad stream in the garden live the toad with her son
he be uglier even than his mother and when he saw the pretty little maiden in her elegant bed he can only cry croak croak croak dont speak so loud or she will wake say the toad and then she may run away for she be as light as swan down
we will place her on one of the waterlily leave out in the stream it will be like 1 island to her she be so light and small and then she can not escape and while she be away we will make haste and prepare the stateroom under the marsh in which you be to live when you be marry far out in the stream grow 1 number of waterlily with broad green leaf which seem to float on the top of the water
the large of these leaf appear far off than the rest and the old toad swim out to it with the walnutshell in which little tiny lay still asleep
the tiny little creature wake very early in the morning and begin to cry bitterly when she find where she be for she can see nothing but water on every side of the large green leaf and no way of reach the land
meanwhile the old toad be very busy under the marsh deck her room with rush and wild yellow flower to make it look pretty for her new daughterinlaw
then she swim out with her ugly son to the leaf on which she have place poor little tiny
she want to fetch the pretty bed that she may put it in the bridal chamber to be ready for her
the old toad bow low to her in the water and say here be my son he will be your husband and you will live happily in the marsh by the stream croak croak croak be all her son can say for himself so the toad take up the elegant little bed and swim away with it leave tiny all alone on the green leaf where she sit and weep
she can not bear to think of living with the old toad and have her ugly son for 1 husband
the little fish who swim about in the water beneath have see the toad and hear what she say so they lift their head above the water to look at the little maiden
as soon as they catch sight of her they saw she be very pretty and it make them very sorry to think that she must go and live with the ugly toad
no it must never be
so they assemble together in the water round the green stalk which hold the leaf on which the little maiden stand and gnaw it away at the root with their tooth
then the leaf float down the stream carry tiny far away out of reach of land
tiny sail past many town and the little bird in the bush saw her and sing what 1 lovely little creature so the leaf swim away with her far and far till it bring her to other land
1 graceful little white butterfly constantly flutter round her and at_last alight on the leaf
tiny please him and she be glad of it for_now the toad can not possibly reach her and the country through which she sail be beautiful and the sun shine upon the water till it glitter like liquid gold
she take off her girdle and tie one end of it round the butterfly and the other end of the ribbon she fasten to the leaf which now glide on much fast than ever take little tiny with it as she stand
presently 1 large cockchafer fly by the moment he catch sight of her he seize her round her delicate waist with his claw and fly with her into 1 tree
the green leaf float away on the brook and the butterfly fly with it for he be fasten to it and can not get away
oh how frighten little tiny feel when the cockchafer fly with her to the tree
but especially be she sorry for the beautiful white butterfly which she have fasten to the leaf for if he can not free himself he would die of hunger
but the cockchafer do not trouble himself at all about the matter
he seat himself by her side on 1 large green leaf give her some honey from the flower to eat and tell her she be very pretty though not in the little like 1 cockchafer
after 1 time all the cockchafers turn up their feeler and say she have only 2 leg
how ugly that look she have no feeler say another
her waist be quite slim
pooh
she be like 1 human being oh
she be ugly say all the lady cockchafers although tiny be very pretty
then the cockchafer who have run away with her believe all the other when they say she be ugly and would have nothing many to say to her and tell her she may go where she like
then he fly down with her from the tree and place her on 1 daisy and she weep at the thought that she be so ugly that even the cockchafers would have nothing to say to her
and all the while she be really the loveliest creature that one can imagine and as tender and delicate as 1 beautiful roseleaf
during the whole summer poor little tiny live quite alone in the wide forest
she weave herself 1 bed with blade of grass and hang it up under 1 broad leaf to protect herself from the rain
she suck the honey from the flower for food and drink the dew from their leave every morning
so pass away the summer and the autumn and then come the winter the long cold winter
all the bird who have sing to her so sweetly be fly away and the tree and the flower have wither
the large clover leaf under the shelter of which she have live be now roll together and shrivel up nothing remain but 1 yellow wither stalk
she feel dreadfully cold for her clothes be tear and she be herself so frail and delicate that poor little tiny be nearly freeze to death
it begin to snow too and the snowflake as they fall upon her be like 1 whole shovelful fall upon one of us for we be tall but she be only LN_in:1 high
then she wrap herself up in 1 dry leaf but it crack in the middle and can not keep her warm and she shiver with cold
near the wood in which she have be living lie 1 cornfield but the corn have be cut 1 long time nothing remain but the bare dry stubble stand up out of the freeze ground
it be to her like struggle through 1 large wood
oh
how she shiver with the cold
she come at_last to the door of 1 fieldmouse who have 1 little den under the cornstubble
there dwell the fieldmouse in warmth and comfort with 1 whole roomful of corn 1 kitchen and 1 beautiful dine room
poor little tiny stand before the door just like 1 little beggargirl and beg for 1 small piece of barleycorn for she have be without 1 morsel to eat for 2 day
you poor little creature say the fieldmouse who be really 1 good old fieldmouse come into my warm room and dine with me she be very please with tiny so she say you be quite welcome to stay with me all the winter if you like but you must keep my room clean and neat and tell me story for i shall like to hear them very much and_tiny do all the fieldmouse ask her and find herself very comfortable
we shall have 1 visitor soon say the fieldmouse one day my neighbour pay me 1 visit once 1 week
he be good off than i be he have large room and wear 1 beautiful black velvet coat
if you can only have him for 1 husband you would be well provide for indeed
but he be blind so you must tell him some of your prettiest story
but tiny do not feel at all interest about this neighbour for he be 1 mole
however he come and pay his visit dress in his black velvet coat
he be very rich and learn and his house be Z_times large than mine say the fieldmouse
he be rich and learn no_doubt but he always speak slightingly of the sun and the pretty flower because he have never see them
tiny be oblige to sing to him ladybird ladybird fly away home and many other pretty song
and the mole fall in love with her because she have such 1 sweet voice but he say nothing yet for he be very cautious
1 short time before the mole have dig 1 long passage under the earth which lead from the dwelling of the fieldmouse to his own and here she have permission to walk with tiny whenever she like
but he warn them not to be alarm at the sight of 1 dead bird which lie in the passage
it be 1 perfect bird with 1 beak and feather and can not have be dead long and be lie just where the mole have make his passage
the mole take 1 piece of phosphorescent wood in his mouth and it glitter like fire in the dark then he go before them to light them through the long dark passage
when they come to the spot where lie the dead bird the mole push his broad nose through the ceiling the earth give way so_that there be 1 large hole and the daylight shine into the passage
in the middle of the floor lie 1 dead swallow his beautiful wing pull close to his side his foot and his head draw up under his feather the poor bird have evidently die of the cold
it make little tiny very sad to see it she do so love the little bird all the summer they have sing and twitter for her so beautifully
but the mole push it aside with his crooked leg and say he will sing no many now
how miserable it must be to be bear 1 little bird
i be thankful that none of my child will ever be bird for they can do nothing but cry tweet tweet and always die of hunger in the winter yes you may well say that as 1 clever man
exclaim the fieldmouse what be the use of his twitter for when winter come he must either starve or be freeze to death
still bird be very high breed tiny say nothing but when the 2 other have turn their back on the bird she stoop down and stroke aside the soft feather which cover the head and kiss the close eyelid
perhaps this be the one who sing to me so sweetly in the summer she say and how_much pleasure it give me you dear pretty bird the mole now stop up the hole through which the daylight shine and then accompany the lady home
but during the night tiny can not sleep so she get out of bed and weave 1 large beautiful carpet of hay then she carry it to the dead bird and spread it over him with some down from the flower which she have find in the fieldmouses room
it be as soft as wool and she spread some of it on each side of the bird so_that he may lie warmly in the cold earth
farewell you pretty little bird say she farewell thank you for your delightful sing during the summer when all the tree be green and the warm sun shine upon us
then she lay her head on the bird breast but she be alarm immediately for it seem as_if something inside the bird go thump thump it be the bird heart he be not really dead only benumbed with the cold and the warmth have restore him to life
in autumn all the swallow fly away into warm country but if one happen to linger the cold seize it it become freeze and fall down as_if dead it remain where it fall and the cold snow cover it
tiny tremble very much she be quite frighten for the bird be large 1 great deal large than herself she be only LN_in:1 high
but she take courage lay the wool many thickly over the poor swallow and then take 1 leaf which she have use for her own counterpane and lay it over the head of the poor bird
the next morning she again steal out to see him
he be alive but very weak he can only open his eye for 1 moment to look at tiny who stand by hold 1 piece of decay wood in her hand for she have no other lantern
thank you pretty little maiden say the sick swallow i have be so nicely warm that i shall soon regain my strength and be able to fly about again in the warm sunshine oh say she it be cold out of door now it snow and freeze
stay in your warm bed i will take care of you then she bring the swallow some water in 1 flowerleaf and after he have drink he tell her that he have wound one of his wing in 1 thornbush and can not fly as fast as the other who be soon far away on their journey to warm country
then at_last he have fall to the earth and can remember no many nor how he come to be where she have find him
the whole winter the swallow remain underground and tiny nurse him with care and love
neither the mole nor the fieldmouse know anything about it for they do not like swallow
very soon the spring time come and the sun warm the earth
then the swallow bid farewell to tiny and she open the hole in the ceiling which the mole have make
the sun shine in upon them so beautifully that the swallow ask her if she would go with him she can sit on his back he say and he would fly away with her into the green wood
but tiny know it would make the fieldmouse very grieve if she leave her in that manner so she say no_i can not farewell then farewell you good pretty little maiden say the swallow and he fly out into the sunshine
tiny look after him and the tear rise in her eye
she be very fond of the poor swallow
tweet tweet sing the bird as he fly out into the green wood and tiny feel very sad
she be not allow to go out into the warm sunshine
the corn which have be sow in the field over the house of the fieldmouse have grow up high into the air and form 1 thick wood to tiny who be only LN_in:1 in height
you be go to be marry tiny say the fieldmouse
my neighbour have ask for you
what good fortune for 1 poor child like you
now we will prepare your wedding clothes
they must be both woollen and linen
nothing must be want when you be the mole wife tiny have to turn the spindle and the fieldmouse hire 4 spider who be to weave day and night
every evening the mole visit her and be continually speak of the time when the summer would be over
then he would keep his weddingday with tiny but now the heat of the sun be so great that it burn the earth and make it quite hard like 1 stone
as soon as the summer be over the wedding should take place
but tiny be not at all please for she do not like the tiresome mole
every morning when the sun rise and every evening when it go down she would creep out at the door and as the wind blow aside the ear of corn so_that she can see the blue sky she think how beautiful and bright it seem out there and wish so much to see her dear swallow again
but he never return for by this time he have fly far away into the lovely green forest
when autumn arrive tiny have her outfit quite ready and the fieldmouse say to her in 4 week the wedding must take place then_tiny weep and say she would not marry the disagreeable mole
nonsense reply the fieldmouse
now dont be obstinate or i shall bite you with my white tooth
he be 1 very handsome mole the queen herself do not wear many beautiful velvets and fur
his kitchen and cellar be quite full
you ought to be very thankful for such good fortune so the weddingday be fix on which the mole be to fetch tiny away to live with him deep under the earth and never again to see the warm sun because he do not like it
the poor child be very unhappy at the thought of say farewell to the beautiful sun and as the fieldmouse have give her permission to stand at the door she go to look at it once many
farewell bright sun she cry stretch out her arm towards it and then she walk 1 short distance from the house for the corn have be cut and only the dry stubble remain in the field
farewell farewell she repeat twine her arm round 1 little red flower that grow just by her side
greet the little swallow from me if you should see him again tweet tweet sound over her head suddenly
she look up and there be the swallow himself fly close by
as soon as he spy tiny he be delight and then she tell him how unwilling she feel to marry the ugly mole and to live always beneath the earth and never to see the bright sun any many
and as she tell him she weep
cold winter be come say the swallow and i be go to fly away into warmer country
will you go with me
you can sit on my back and fasten yourself on with your sash
then we can fly away from the ugly mole and his gloomy room far away over the mountain into warmer country where the sun shine many brightly than here where it be always summer and the flower bloom in great beauty
fly now with me dear little tiny you save my life when i lie freeze in that dark passage yes_i will go with you say tiny and she seat herself on the bird back with her foot on his outstretch wing and tie her girdle to one of his strong feather
then the swallow rise in the air and fly over forest and over sea high above the high mountain cover with eternal snow
tiny would have be freeze in the cold air but she creep under the bird warm feather keep her little head uncover so_that she may admire the beautiful land over which they pass
at length they reach the warm country where the sun shine brightly and the sky seem so much high above the earth
here on the hedge and by the wayside grow purple green and white grape lemon and orange hang from tree in the wood and the air be fragrant with myrtles and orange blossom
beautiful child run along the country lane play with large gay butterfly and as the swallow fly far and far every place appear still many lovely
at_last they come to 1 blue lake and by the side of it shade by tree of the deep green stand 1 palace of dazzle white marble build in the olden time
vine cluster round its lofty pillar and at the top be many swallow nest and one of these be the home of the swallow who carry tiny
this be my house say the swallow but it would not do for you to live there you would not be comfortable
you must choose for yourself one of those lovely flower and i will put you down upon it and then you shall have everything that you can wish to make you happy that will be delightful she say and clap her little hand for joy
1 large marble pillar lie on the ground which in fall have be break into 3 piece
between these piece grow the many beautiful large white flower so the swallow fly down with tiny and place her on one of the broad leaf
but how surprise she be to see in the middle of the flower 1 tiny little man as white and transparent as_if he have be make of crystal
he have 1 gold crown on his head and delicate wing at his shoulder and be not much large than tiny herself
he be the angel of the flower for 1 tiny man and 1 tiny woman dwell in every flower and this be the king of them all
oh how beautiful he be
whisper tiny to the swallow
the little prince be at 1 quite frighten at the bird who be like 1 giant compare to such 1 delicate little creature as himself but when he saw tiny he be delight and think her the prettiest little maiden he have ever see
he take the gold crown from his head and place it on hers and ask her name and if she would be his wife and queen over all the flower
this certainly be 1 very different sort of husband to the son of 1 toad or the mole with my black velvet and fur so she say yes to the handsome prince
then all the flower open and out of each come 1 little lady or 1 tiny lord all so pretty it be quite 1 pleasure to look at them
each of them bring tiny 1 present but the good gift be 1 pair of beautiful wing which have belong to 1 large white fly and they fasten them to tinys shoulder so_that she may fly from flower to flower
then there be much rejoicing and the little swallow who sit above them in his nest be ask to sing 1 wedding song which he do as_well as he can but in his heart he feel sad for he be very fond of tiny and would have like never to part from her again
you must not be call tiny any many say the spirit of the flower to her
it be 1 ugly name and you be so very pretty
we will call you maia_farewell farewell say the swallow with 1 heavy heart as he leave the warm country to fly back into denmark
there he have 1 nest over the window of 1 house in which dwell the writer of fairy tale
the swallow sing tweet tweet and from his song come the whole story
the_end
