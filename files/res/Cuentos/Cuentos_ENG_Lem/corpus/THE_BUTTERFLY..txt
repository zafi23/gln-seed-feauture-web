the_butterfly_there be once 1 butterfly who wish for 1 bride and as may be suppose he want to choose 1 very pretty one from among the flower
he glance with 1 very critical eye at all the flowerbeds and find that the flower be seat quietly and demurely on their stalk just as maiden should sit before they be engage but there be 1 great number of them and it appear as_if his search would become very wearisome
the butterfly do not like to take too much trouble so he fly off on 1 visit to the daisy
the french call this flower marguerite and they say that the little daisy can prophesy
lover pluck off the leaf and as they pluck each leaf they ask 1 question about their lover thus does he or she love me
ardently
distractedly
very much
1 little
not at all
and so_on
every one speak these word in his own language
the butterfly come also to marguerite to inquire but he do not pluck off her leaf he press 1 kiss on each of them for he think there be always many to be do by kindness
darling marguerite daisy he say to her you be the wise woman of all the flower
pray tell me which of the flower i shall choose for my wife
which will be my bride
when i know i will fly directly to her and propose but_marguerite do not answer him she be offend that he should call her 1 woman when she be only 1 girl and there be 1 great difference
he ask her TM_s:1 time and then 1/3 but she remain dumb and answer not 1 word
then he would wait no_longer but fly away to commence his woo at once
it be in the early spring when the crocus and the snowdrop be in full bloom
they be very pretty think the butterfly charm little lasses but they be rather formal then as the young lad often do he look out for the old girl
he next fly to the anemone these be rather sour to his taste
the violet 1 little too sentimental
the limeblossoms too small and besides there be such 1 large family of them
the appleblossoms though they look like rose bloom today but may fall off tomorrow with the 1 wind that blow and he think that 1 marriage with one of them may last too short 1 time
the peablossom please him many of all she be white and red graceful and slender and belong to those domestic maiden who have 1 pretty appearance and can yet be useful in the kitchen
he be just about to make her 1 offer when close by the maiden he saw 1 pod with 1 wither flower hanging at the end
who be that
he ask
that be my sister reply the peablossom
oh indeed and you will be like her some day say he and he fly away directly for he feel quite shock
1 honeysuckle hang forth from the hedge in full bloom but there be so many girl like her with long face and sallow complexion
no he do not like her
but which one do he like
spring go by and summer draw towards its close autumn come but he have not decide
the flower now appear in their many gorgeous robe but all in vain they have not the fresh fragrant air of youth
for the heart ask for fragrance even when it be no_longer young and there be very little of that to be find in the dahlia or the dry chrysanthemum therefore the butterfly turn to the mint on the ground
you know this plant have no blossom but it be sweetness all over full of fragrance from head to foot with the scent of 1 flower in every leaf
i will take her say the butterfly and he make her 1 offer
but the mint stand silent and stiff as she listen to him
at_last she say friendship if you please nothing many
i be old and you be old but we may live for each other just the same as to marry no dont let us appear ridiculous at our age and so it happen that the butterfly get no wife at all
he have be too long choose which be always 1 bad plan
and the butterfly become what be call 1 old bachelor
it be late in the autumn with rainy and cloudy weather
the cold wind blow over the bow back of the willow so_that they creak again
it be not the weather for fly about in summer clothes but fortunately the butterfly be not out in it
he have get 1 shelter by_chance
it be in 1 room heat by 1 stove and as warm as summer
he can exist here he say well enough
but it be not enough merely to exist say he i need freedom sunshine and 1 little flower for 1 companion then he fly against the windowpane and be see and admire by those in the room who catch him and stick him on 1 pin in 1 box of curiosity
they can not do many for him
now i be perch on 1 stalk like the flower say the butterfly
it be not very pleasant certainly i should imagine it be something like be marry for here i be stick fast and with this think he console himself 1 little
that seem very poor consolation say one of the plant in the room that grow in 1 pot
ah think the butterfly one cant very well trust these plant in pot they have too much to do with mankind the_end
