the_puppetshow_man_on board 1 steamer i once meet 1 elderly man with such 1 merry face that if it be really 1 index of his mind he must have be the happiest fellow in creation and indeed he consider himself so for i hear it from his own mouth
he be 1 dane the owner of 1 travel theatre
he have all his company with him in 1 large box for he be the proprietor of 1 puppetshow
his inborn cheerfulness he say have be test by 1 member of the polytechnic_institution and the experiment have make him completely happy
i do not at 1 understand all this but afterwards he explain the whole story to me and here it be i be give 1 representation he say in the hall of the postinghouse in the little town of slagelse there be 1 splendid audience entirely juvenile except 2 respectable matron
all at once 1 person in black of studentlike appearance enter the room and sit down he laugh aloud at the tell point and applaud quite at the proper time
this be 1 very unusual spectator for me and i feel anxious to know who he be
i hear that he be 1 member of the polytechnic_institution in copenhagen who have be send out to lecture to the people in the province
punctually at 8 oclock my performance close for child must go early to bed and 1 manager must also consult the convenience of the public
at 9 oclock the lecturer commence his lecture and his experiment and then i form 1 part of his audience
it be wonderful both to hear and to see
the great part of it be beyond my comprehension but it lead me to think that if we man can acquire so much we must surely be intend to last long than the little span which extend only to the time when we be hide away under the earth
his experiment be quite miracle on 1 small scale and yet the explanation flow as naturally as water from his lip
at the time of moses and the prophet such 1 man would have be place among the sage of the land in the middle age they would have burn him at the stake
all night long i can not sleep and the next evening when i give another performance and the lecturer be present i be in one of my good mood
i once hear of 1 actor who when he have to act the part of 1 lover always think of one particular lady in the audience he only play for her and forget all the rest of the house and now the polytechnic lecturer be my she my only auditor for whom alone i play
when the performance be over and the puppet remove behind the curtain the polytechnic lecturer invite me into his room to take 1 glass of wine
he talk of my comedy and i of his science and i believe we be both equally please
but i have the good of it for there be much in what he do that he can not always explain to me
for_instance why 1 piece of iron which be rub on 1 cylinder should become magnetic
how do this happen
the magnetic spark come to it but how
it be the same with people in the world they be rub about on this spherical globe till the electric spark come upon them and then we have 1 napoleon or 1 luther or some one of the kind
the whole world be but 1 series of miracle say the lecturer but we be so accustom to them that we call them everyday matter and he go on explain thing to me till my skull seem lift from my brain and i declare that be i not such 1 old fellow i would at once become 1 member of the polytechnic_institution that i may learn to look at the bright side of everything although i be one of the happiest of man
one of the happiest
say the lecturer as_if the idea please him be you really happy
yes i reply for i be welcome in every town when i arrive with my company but i certainly have 1 wish which sometimes weigh upon my cheerful temper like 1 mountain of lead
i should like to become the manager of $_BRL:1 theatre and the director of $_BRL:1 troupe of man and woman i understand he say you would like to have life breathe into your puppet so_that they may be live actor and you their director
and would you then be quite happy
i say i believe so
but he do not and we talk it over in_all manner of way yet can not agree on the subject
however the wine be excellent and we clank our glass together as we drink
there must have be magic in it or i should many certainly become tipsy but that do not happen for my mind seem quite clear and indeed 1 kind of sunshine fill the room and beam from the eye of the polytechnic lecturer
it make me think of the old story when the god in their immortal youth wander upon this earth and pay visit to mankind
i say so to him and he smile and i can have swear that he be one of these ancient deity in disguise or at all event that he belong to the race of the god
the result seem to prove i be right in my suspicion for it be arrange that my high wish should be grant that my puppet be to be gift with life and that i be to be the manager of $_BRL:1 company
we drink to my success and clank our glass
then he pack all my doll into the box and fasten it on my back and i feel as_if i be spin round in 1 circle and presently find myself lie on the floor
i remember that quite well
and then the whole company spring from the box
the spirit have come upon us all the puppet have become distinguish actor at_least so they say themselves and i be their director
when all be ready for the 1 representation the whole company request permission to speak to me before appear in public
the dance lady say the house can not be support unless she stand on one leg for she be 1 great genius and beg to be treat as such
the lady who act the part of the queen expect to be treat as 1 queen off the stage as_well as on it or else she say she should get out of practice
the man whose duty it be to deliver 1 letter give himself as many air as he who take the part of 1 lover in the piece he declare that the inferior part be as important as the great one and deserve equal consideration as part of 1 artistic whole
the hero of the piece would only play in 1 part contain point likely to bring down the applause of the house
the prima donna would only act when the light be red for she declare that 1 blue light do not suit her complexion
it be like 1 company of fly in 1 bottle and i be in the bottle with them for i be their director
my breath be take away my head whirl and i be as miserable as 1 man can be
it be quite 1 novel strange set of being among whom i now find myself
i only wish i have them all in my box again and that i have never be their director
so i tell them roundly that after all they be nothing but puppet and then they kill me
after 1 while i find myself lie on my bed in my room but how i get there or how i get away at all from the polytechnic professor he may perhaps know i dont
the moon shine upon the floor the box lie open and the doll be all scatter about in great confusion but i be not idle
i jump off the bed and into the box they all have to go some on their head some on their foot
then i shut down the lid and seat myself upon the box
now youll have to stay say i and i shall be cautious how i wish you flesh and blood again i feel quite light my cheerfulness have return and i be the happiest of mortal
the polytechnic professor have fully cure me
i be as happy as 1 king and go to sleep on the box
next morning correctly speak it be [??:??/??/??:1200:pm] for i sleep remarkably late that day i find myself still sit there in happy consciousness that my former wish have be 1 foolish one
i inquire for the polytechnic professor but he have disappear like the greek and roman god from that time i have be the happiest man in the world
i be 1 happy director for none of my company ever grumble nor the public either for i always make them merry
i can arrange my piece just as i please
i choose out of every comedy what i like good and no_one be offend
play that be neglect nowadays by the great public be run after 30 year ago and listen to till the tear run down the cheek of the audience
these be the piece i bring forward
i place them before the little one who cry over them as papa and mamma use to cry 30 year ago
but i make them short for the youngster dont like long speech and if they have anything mournful they like it to be over quickly the_end
