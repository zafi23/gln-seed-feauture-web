the_elf_of_the_rose_in the midst of 1 garden grow 1 rosetree in full blossom and in the prettiest of all the rose live 1 elf
he be such 1 little wee thing that no human eye can see him
behind each leaf of the rise he have 1 sleep chamber
he be as_well form and as beautiful as 1 little child can be and have wing that reach from his shoulder to his foot
oh what sweet fragrance there be in his chamber
and how clean and beautiful be the wall
for they be the blush leaf of the rise
during the whole day he enjoy himself in the warm sunshine fly from flower to flower and dance on the wing of the fly butterfly
then he take it into his head to measure how_many step he would have to go through the road and crossroad that be on the leaf of 1 lindentree
what we call the vein on 1 leaf he take for road ay and very long road they be for him for before he have half finish his task the sun go down he have commence his work too late
it become very cold the dew fall and the wind blow so he think the good thing he can do would be to return home
he hurry himself as much as he can but he find the rose all close up and he can not get in not 1 single rise stand open
the poor little elf be very much frighten
he have never before be out at night but have always slumber secretly behind the warm roseleaves
oh this would certainly be his death
at the other end of the garden he know there be 1 arbor overgrow with beautiful honeysuckle
the blossom look like large paint horn and he think to himself he would go and sleep in one of these till the morning
he fly thither but hush
2 people be in the arbor 1 handsome young man and 1 beautiful lady
they sit side by side and wish that they may never be oblige to part
they love each other much many than the good child can love its father and mother
but we must part say the young man your brother do not like our engagement and therefore he send me so far away on business over mountain and sea
farewell my sweet bride for so you be to me and then they kiss each other and the girl weep and give him 1 rise but before she do so she press 1 kiss upon it so fervently that the flower open
then the little elf fly in and lean his head on the delicate fragrant wall
here he can plainly hear them say farewell farewell and he feel that the rise have be place on the young man breast
oh how his heart do beat
the little elf can not go to sleep it thump so loudly
the young man take it out as he walk through the dark wood alone and kiss the flower so often and so violently that the little elf be almost crush
he can feel through the leaf how hot the lip of the young man be and the rise have open as_if from the heat of the noonday sun
there come another man who look gloomy and wicked
he be the wicked brother of the beautiful maiden
he draw out 1 sharp knife and while the other be kiss the rise the wicked man stab him to death then he cut off his head and bury it with the body in the soft earth under the lindentree
now he be go and will soon be forget think the wicked brother he will never come back again
he be go on 1 long journey over mountain and sea it be easy for 1 man to lose his life in such 1 journey
my sister will suppose he be dead for he can not come back and she will not dare to question me about him then he scatter the dry leaf over the light earth with his foot and go home through the darkness but he go not alone as he think the little elf accompany him
he sit in 1 dry rolledup lindenleaf which have fall from the tree on to the wicked man head as he be dig the grave
the hat be on the head now which make it very dark and the little elf shudder with fright and indignation at the wicked deed
it be the dawn of morning before the wicked man reach home he take off his hat and go into his sister room
there lie the beautiful bloom girl dream of him whom she love so and who be now she suppose travel far away over mountain and sea
her wicked brother stop over her and laugh hideously as fiend only can laugh
the dry leaf fall out of his hair upon the counterpane but he do not notice it and go to get 1 little sleep during the early morning hour
but the elf slip out of the wither leaf place himself by the ear of the sleep girl and tell her as in 1 dream of the horrid murder describe the place where her brother have slay her lover and bury his body and tell her of the lindentree in full blossom that stand close by
that you may not think this be only 1 dream that i have tell you he say you will find on your bed 1 wither leaf then she awake and find it there
oh what bitter tear she shed
and she can not open her heart to any one for relief
the window stand open the whole day and the little elf can easily have reach the rose or any of the flower but he can not find it in his heart to leave one so afflict
in the window stand 1 bush bear monthly rose
he seat himself in one of the flower and gaze on the poor girl
her brother often come into the room and would be quite cheerful in spite of his base conduct so she dare not say 1 word to him of her heart grief
as soon as night come on she slip out of the house and go into the wood to the spot where the lindentree stand and after remove the leaf from the earth she turn it up and there find him who have be murder
oh how she weep and pray that she also may die
gladly would she have take the body home with her but that be impossible so she take up the poor head with the close eye kiss the cold lip and shake the mould out of the beautiful hair
i will keep this say she and as soon as she have cover the body again with the earth and leave she take the head and 1 little sprig of jasmine that bloom in the wood near the spot where he be bury and carry them home with her
as soon as she be in her room she take the large flowerpot she can find and in this she place the head of the dead man cover it up with earth and plant the twig of jasmine in it
farewell farewell whisper the little elf
he can not any long endure to witness all this agony of grief he therefore fly away to his own rise in the garden
but the rise be fade only 1 few dry leave still cling to the green hedge behind it
alas
how soon all that be good and beautiful pass away sigh the elf
after 1 while he find another rise which become his home for among its delicate fragrant leaf he can dwell in safety
every morning he fly to the window of the poor girl and always find her weep by the flower pot
the bitter tear fall upon the jasmine twig and each day as she become pale and pale the sprig appear to grow green and fresher
one shoot after another sprout forth and little white bud blossom which the poor girl fondly kiss
but her wicked brother scold her and ask her if she be go mad
he can not imagine why she be weep over that flowerpot and it annoy him
he do not know whose close eye be there nor what red lip be fade beneath the earth
and one day she sit and lean her head against the flowerpot and the little elf of the rise find her asleep
then he seat himself by her ear talk to her of that evening in the arbor of the sweet perfume of the rise and the love of the elf
sweetly she dream and while she dream her life pass away calmly and gently and her spirit be with him whom she love in heaven
and the jasmine open its large white bell and spread forth its sweet fragrance it have no other way of show its grief for the dead
but the wicked brother consider the beautiful bloom plant as his own property leave to him by his sister and he place it in his sleep room close by his bed for it be very lovely in appearance and the fragrance sweet and delightful
the little elf of the rise follow it and fly from flower to flower tell each little spirit that dwell in them the story of the murder young man whose head now form part of the earth beneath them and of the wicked brother and the poor sister
we know it say each little spirit in the flower we know it for have we not spring from the eye and lip of the murder one
we know it we know it and the flower nod with their head in 1 peculiar manner
the elf of the rise can not understand how they can rest so quietly in the matter so he fly to the bee who be gather honey and tell them of the wicked brother
and the bee tell it to their queen who command that the next morning they should go and kill the murderer
but during the night the 1 after the sister death while the brother be sleep in his bed close to where he have place the fragrant jasmine every flower cup open and invisibly the little spirit steal out arm with poisonous spear
they place themselves by the ear of the sleeper tell him dreadful dream and then fly across his lip and prick his tongue with their poison spear
now have we revenge the dead say they and fly back into the white bell of the jasmine flower
when the morning come and as soon as the window be open the rise elf with the queen bee and the whole swarm of bee rush in to kill him
but he be already dead
people be stand round the bed and saying that the scent of the jasmine have kill him
then the elf of the rise understand the revenge of the flower and explain it to the queen bee and she with the whole swarm buzz about the flowerpot
the bee can not be drive away
then 1 man take it up to remove it and one of the bee sting him in the hand so_that he let the flowerpot fall and it be break to piece
then every one saw the whiten skull and they know the dead man in the bed be 1 murderer
and the queen bee hum in the air and sing of the revenge of the flower and of the elf of the rise and say that behind the small leaf dwell one who can discover evil deed and punish them also
the_end
