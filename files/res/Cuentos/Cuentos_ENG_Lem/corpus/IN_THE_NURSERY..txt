in_the_nursery_father and mother and brother and sister be go to the play only little anna and her grandpapa be leave at home
well have 1 play too he say and it may begin immediately but we have no theatre cry little anna and we have no_one to act for us my old doll can not for she be 1 fright and my new one can not for she must not rumple her new clothes one can always get actor if one make use of what one have observe grandpapa
now well go into the theatre
here we will put up 1 book there another and there 1/3 in 1 slope row
now 3 on the other side so now we have the side scene
the old box that lie yonder may be the back stair and well lay the floor on top of it
the stage represent 1 room as every [??:1/5/??:????:??] see
now we want the actor
let us see what we can find in the playthingbox
first the personages and then we will get the play ready
one after the other that will be capital
heres 1 pipehead and yonder 1 odd glove they will do very well for father and daughter but those be only 2 character say little anna
heres my brother old waistcoat can not that play in our piece too
its big enough certainly reply grandpapa
it shall be the lover
theres nothing in the pocket and thats very interest for thats half of 1 unfortunate attachment
and here we have the nutcracker boot with spur to them
row dow dow
how they can stamp and strut
they shall represent the unwelcome wooer whom the lady do not like
what kind of 1 play will you have now
shall it be 1 tragedy or 1 domestic drama
1 domestic drama please say little anna for the other be so fond of that
do you know one
i know 100 say grandpapa
those that be many in favor be from the french but they be not good for little girl
in_the_meantime we may take one of the prettiest for inside theyre all very much alike
now i shake the pen
cockalorum
so now heres the play brinbranspan new
now listen to the playbill and grandpapa take 1 newspaper and read as_if he be read from it the_pipehead_and_the_good_head_a_family_drama in one act_characters_mr_pipehead 1 father
mr_waistcoat 1 lover
miss_glove 1 daughter
mr_de_boots 1 suitor
and now be go to begin
the curtain rise
we have no curtain so it have rise already
all the character be there and so we have them at_hand
now i speak as papa_pipehead
hes angry today
one can see that hes 1 color meerschaum
snik snak snurre bassellurre
im master of this house
im the father of my daughter
will you hear what i have to say
mr de boots be 1 person in whom [??:1/5/??:????:??] see one face his upper part be of morocco and he have spur into the bargain
snikke snakke snak
he shall have my daughter
now listen to what the waistcoat say little anna say grandpapa
now the waistcoats speak
the waistcoat have 1 laydown collar and be very modest but he know his own value and have quite 1 right to say what he say i havent 1 spot on me
goodness of material ought to be appreciate
i be of real silk and have string to me on the wedding day but no_longer you dont keep your color in the wash this be mr_pipehead who be speak
mr de boots be watertight of strong leather and yet very delicate he can creak and clank with his spur and have 1 italian physiognomy but they ought to speak in verse say anna_for_ive hear thats the many charm way of all they can do that too reply grandpapa and if the public demand it they will talk in that way
just look at little miss_glove how shes point her finger
can i but have my love who then so happy as glove
ah
if i from him must part im sure twill break my heart
bah
the last word be speak by mr_pipehead and now its mr_waistcoats turn o_glove my own dear though it cost thee 1 tear thou must be mine for_holger_danske have swear it
mr de boots hear this kick up jingle his spur and knock down 3 of the sidescenes thats exceedingly charm
cry little anna
silence
silence
say grandpapa
silent approbation will show that you be the educate public in the stall
now miss_glove sing her great song with startle effect i cant see heigho
and therefore ill crow
kikkeriki in the lofty hall
now come the excite part little anna
this be the many important in_all the play
mr_waistcoat undo himself and address his speech to you that you may applaud but leave it alone thats consider many genteel
i be drive to extremity
take care of yourself
now come the plot
you be the pipehead and i be the good head snap
there you go
do you notice this little anna
ask grandpapa
thats 1 many charm comedy
mr_waistcoat seize the old pipehead and put him in his pocket there he lie and the waistcoat say you be in my pocket you cant come out till you promise to unite me to your daughter glove on the left
i hold out my right hand thats awfully pretty say little anna
and now the old pipehead reply though_im all ear very stupid i appear wheres my humor
go i fear and_i feel my hollow stick not here ah
never my dear did_i feel so queer
oh
pray let me out and like 1 lamb lead to slaughter ill betroth you no_doubt to my daughter is the play over already
ask little anna
by no mean reply grandpapa
its only all over with mr de boots
now the lover kneel down and one of them sing father
and the other come do as you ought to do bless your son and daughter and they receive his blessing and celebrate their wedding and all the piece of furniture sing in chorus klink
clank
1000 thank and now the play be over
and now well applaud say grandpapa
well call them all out and the piece of furniture too for they be of mahogany and be not our play just as good as those which the other have in the real theatre
our play be much well say grandpapa
it be short the performer be natural and it have pass away the interval before teatime the_end
