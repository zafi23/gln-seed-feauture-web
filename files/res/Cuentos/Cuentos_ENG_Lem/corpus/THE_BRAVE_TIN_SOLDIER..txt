the_brave_tin_soldier_there be once fiveandtwenty tin soldier who be all brother for they have be make out of the same old tin spoon
they shoulder arm and look straight before them and wear 1 splendid uniform red and blue
the 1 thing in the world they ever hear be the word tin soldier
utter by 1 little boy who clap his hand with delight when the lid of the box in which they lie be take off
they be give him for 1 birthday present and he stand at the table to set them up
the soldier be all exactly alike except one who have only one leg he have be leave to the last and then there be not enough of the melt tin to finish him so they make him to stand firmly on one leg and this cause him to be very remarkable
the table on which the tin soldier stand be cover with other plaything but the many attractive to the eye be 1 pretty little paper castle
through the small window the room can be see
in front of the castle 1 number of little tree surround 1 piece of lookingglass which be intend to represent 1 transparent lake
swan make of wax swim on the lake and be reflect in it
all this be very pretty but the prettiest of all be 1 tiny little lady who stand at the open door of the castle she also be make of paper and she wear 1 dress of clear muslin with 1 narrow blue ribbon over her shoulder just like 1 scarf
in front of these be fix 1 glitter tinsel rise as large as her whole face
the little lady be 1 dancer and she stretch out both her arm and raise one of her leg so high that the tin soldier can not see it at all and he think that she like himself have only one leg
that be the wife for me he think but she be too grand and life in 1 castle while i have only 1 box to live in fiveandtwenty of us altogether that be no place for her
still i must try and make her acquaintance then he lay himself at full length on the table behind 1 snuffbox that stand upon it so_that he can peep at the little delicate lady who continue to stand on one leg without lose her balance
when even come the other tin soldier be all place in the box and the people of the house go to bed
then the plaything begin to have their own game together to pay visit to have sham fight and to give ball
the tin soldier rattle in their box they want to get out and join the amusement but they can not open the lid
the nutcracker play at leapfrog and the pencil jump about the table
there be such 1 noise that the canary wake up and begin to talk and in poetry too
only the tin soldier and the dancer remain in their place
she stand on tiptoe with her leg stretch out as firmly as he do on his one leg
he never take his eye from her for even 1 moment
the clock strike 12 and with 1 bounce up spring the lid of the snuffbox but instead_of snuff there jump up 1 little black goblin for the snuffbox be 1 toy puzzle
tin soldier say the goblin dont wish for what do not belong to you
but the tin soldier pretend not to hear
very well wait till tomorrow then say the goblin
when the child come in the next morning they place the tin soldier in the window
now whether it be the goblin who do it or the draught be not know but the window fly open and out fall the tin soldier heel over head from the 3 story into the street beneath
it be 1 terrible fall for he come head downwards his helmet and his bayonet stick in between the flagstone and his one leg up in the air
the servant maid and the little boy go down stair directly to look for him but he be nowhere to be see although once they nearly tread upon him
if he have call out here_i be it would have be all right but he be too proud to cry out for help while he wear 1 uniform
presently it begin to rain and the drop fall fast and fast till there be 1 heavy shower
when it be over 2 boy happen to pass by and one of them say look there be 1 tin soldier
he ought to have 1 boat to sail in so they make 1 boat out of 1 newspaper and place the tin soldier in it and send him sailing down the gutter while the 2 boy run by the side of it and clap their hand
good gracious what large wave arise in that gutter
and how fast the stream roll on
for the rain have be very heavy
the paper boat rock up and down and turn itself round sometimes so quickly that the tin soldier tremble yet he remain firm his countenance do not change he look straight before him and shoulder his musket
suddenly the boat shot under 1 bridge which form 1 part of 1 drain and then it be as dark as the tin soldier box
where be i go now
think he
this be the black goblin fault i be sure
ah well if the little lady be only here with me in the boat i should not care for any darkness suddenly there appear 1 great waterrat who live in the drain
have you 1 passport
ask the rat give it to me at once but the tin soldier remain silent and hold his musket tight than ever
the boat sail on and the rat follow it
how he do gnash his tooth and cry out to the bit of wood and straw stop him stop him he have not pay toll and have not show his pass but the stream rush on strong and strong
the tin soldier can already see daylight shine where the arch end
then he hear 1 roar sound quite terrible enough to frighten the brave man
at the end of the tunnel the drain fall into 1 large canal over 1 steep place which make it as dangerous for him as 1 waterfall would be to us
he be too close to it to stop so the boat rush on and the poor tin soldier can only hold himself as stiffly as possible without move 1 eyelid to show that he be not afraid
the boat whirl round 3 or Z_times and then fill with water to the very edge nothing can save it from sink
he now stand up to his neck in water while deep and deep sink the boat and the paper become soft and loose with the wet till at_last the water close over the soldier head
he think of the elegant little dancer whom he should never see again and the word of the song sound in his ear farewell warrior
ever brave drifting onward to thy grave then the paper boat fall to piece and the soldier sink into the water and immediately afterwards be swallow up by 1 great fish
oh how dark it be inside the fish
1 great deal dark than in the tunnel and narrow too but the tin soldier continue firm and lay at full length shoulder his musket
the fish swim to_and_fro make the many wonderful movement but at_last he become quite still
after 1 while 1 flash of lightning seem to pass through him and then the daylight approach and 1 voice cry out i declare here be the tin soldier the fish have be catch take to the market and sell to the cook who take him into the kitchen and cut him open with 1 large knife
she pick up the soldier and hold him by the waist between her finger and thumb and carry him into the room
they be all anxious to see this wonderful soldier who have travel about inside 1 fish but he be not at all proud
they place him on the table and how_many curious thing do happen in the world
there he be in the very same room from the window of which he have fall there be the same child the same plaything stand on the table and the pretty castle with the elegant little dancer at the door she still balance herself on one leg and hold up the other so she be as firm as himself
it touch the tin soldier so much to see her that he almost weep tin tear but he keep them back
he only look at her and they both remain silent
presently one of the little boy take up the tin soldier and throw him into the stove
he have no reason for do so therefore it must have be the fault of the black goblin who live in the snuffbox
the flame light up the tin soldier as he stand the heat be very terrible but whether it proceed from the real fire or from the fire of love he can not tell
then he can see that the bright color be fade from his uniform but whether they have be wash off during his journey or from the effect of his sorrow no_one can say
he look at the little lady and she look at him
he feel himself melt away but he still remain firm with his gun on his shoulder
suddenly the door of the room fly open and the draught of air catch up the little dancer she flutter like 1 sylph right into the stove by the side of the tin soldier and be instantly in flame and be go
the tin soldier melt down into 1 lump and the next morning when the maid servant take the ash out of the stove she find him in the shape of 1 little tin heart
but of the little dancer nothing remain but the tinsel rise which be burn black as 1 cinder
the_end
