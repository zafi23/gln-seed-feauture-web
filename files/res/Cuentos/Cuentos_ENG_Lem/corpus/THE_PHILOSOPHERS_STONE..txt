the_philosophers_stone_far away towards the east in india which seem in those day the world end stand the tree_of_the_sun 1 noble tree such as we have never see and perhaps never may see
the summit of this tree spread itself for mile like 1 entire forest each of its small branch form 1 complete tree
palm beechtrees pine planetrees and various other kind which be find in_all part of the world be here like small branch shoot forth from the great tree while the large bough with their knot and curve form valley and hill clothe with velvety green and cover with flower
everywhere it be like 1 bloom meadow or 1 lovely garden
here be bird from all quarter of the world assemble together bird from the primeval forest of america from the rise garden of damascus and from the desert of africa in which the elephant and the lion may boast of be the only ruler
bird from the polar region come fly here and of course the stork and the swallow be not absent
but the bird be not the only living creature
there be stag squirrel antelope and hundred of other beautiful and lightfooted animal here find 1 home
the summit of the tree be 1 widespreading garden and in the midst of it where the green bough form 1 kind of hill stand 1 castle of crystal with 1 view from it towards every quarter of heaven
each tower be erect in the form of 1 lily and within the stern be 1 winding staircase through which one can ascend to the top and step out upon the leaf as upon balcony
the calyx of the flower itself form 1 many beautiful glitter circular hall above which no other roof arise than the blue firmament and the sun and star
just as much splendor but of another kind appear below in the wide hall of the castle
here on the wall be reflect picture of the world which represent numerous and vary scene of everything that take place daily so_that it be useless to read the newspaper and indeed there be none to be obtain in this spot
all be to be see in live picture by those who wish it but all would have be too much for even the wise man and this man dwell here
his name be very difficult you would not be able to pronounce it so it may be omit
he know everything that 1 man on earth can know or imagine
every invention already in existence or yet to be be know to him and much many still everything on earth have 1 limit
the wise king solomon be not half so wise as this man
he can govern the power of nature and hold sway over potent spirit even death itself be oblige to give him every morning 1 list of those who be to die during the day
and king_solomon himself have to die at_last and this fact it be which so often occupy the thought of this great man in the castle on the tree_of_the_sun
he know that he also however high he may tower above other man in wisdom must one day die
he know that his child would fade away like the leaf of the forest and become dust
he saw the human race wither and fall like leaf from the tree he saw new man come to fill their place but the leaf that fall off never sprout forth again they crumble to dust or be absorb into other plant
what happen to man ask the wise man of himself when touch by the angel of death
what can death be
the body decay and the soul
yes what be the soul and whither do it go
to eternal life say the comfort voice of religion
but what be this change
where and how shall we exist
above in heaven answer the pious man it be there we hope to go above
repeat the wise man fix his eye upon the moon and star above him
he saw that to this earthly sphere above and below be constantly change place and that the position vary according_to the spot on which 1 man find himself
he know also that even_if he ascend to the top of the high mountain which rear its lofty summit on this earth the air which to us seem clear and transparent would there be dark and cloudy the sun would have 1 coppery glow and send forth no ray and our earth would lie beneath him wrap in 1 orangecolored mist
how narrow be the limit which confine the bodily sight and how little can be see by the eye of the soul
how little do the wise among us know of that which be so important to us all
in the many secret chamber of the castle lie the great treasure on earth the book_of_truth
the wise man have read it through page after page
every man may read in this book but only in fragment
to many eye the character seem so mix in confusion that the word can not be distinguish
on certain page the writing often appear so pale or so blur that the page become 1 blank
the wise 1 man become the many he will read and those who be wise read many
the wise man know how to unite the sunlight and the moonlight with the light of reason and the hide power of nature and through this strong light many thing in the page be make clear to him
but in the portion of the book entitle life after death not 1 single point can he see distinctly
this pain him
should he never be able here on earth to obtain 1 light by which everything write in the book_of_truth should become clear to him
like the wise king_solomon he understand the language of animal and can interpret their talk into song but that make him none the wise
he find out the nature of plant and metal and their power in cure disease and arrest death but none to destroy death itself
in_all create thing within his reach he seek the light that should shine upon the certainty of 1 eternal life but he find it not
the book_of_truth lay open before him but its page be to him as blank paper
christianity place before him in the bible 1 promise of eternal life but he want to read it in his book in which nothing on the subject appear to be write
he have 5 child 4 son educate as the child of such 1 wise father should be and 1 daughter fair gentle and intelligent but she be blind yet this deprivation appear as nothing to her her father and brother be outward eye to her and 1 vivid imagination make everything clear to her mental sight
the son have never go far from the castle than the branch of the tree extend and the sister have scarcely ever leave home
they be happy child in that home of their childhood the beautiful and fragrant tree_of_the_sun
like all child they love to hear story relate to them and their father tell them many thing which other child would not have understand but these be as clever as many grownup people be among us
he explain to them what they saw in the picture of life on the castle wall the doing of man and the progress of event in_all the land of the earth and the son often express 1 wish that they can be present and take 1 part in these great deed
then their father tell them that in the world there be nothing but toil and difficulty that it be not quite what it appear to them as they look upon it in their beautiful home
he speak to them of the true the beautiful and the good and tell them that these 3 hold together in the world and by that union they become crystallize into 1 precious jewel clear than 1 diamond of the 1 water 1 jewel whose splendor have 1 value even in the sight of god in whose brightness all thing be dim
this jewel be call the philosopher stone
he tell them that by search man can attain to 1 knowledge of the existence of god and that it be in the power of every man to discover the certainty that such 1 jewel as the philosopher stone really exist
this information would have be beyond the perception of other child but these child understand and other will learn to comprehend its meaning after 1 time
they question their father about the true the beautiful and the good and he explain it to them in many way
he tell them that god when he make man out of the dust of the earth touch his work Z_times leave 5 intense feeling which we call the 5 sense
through these the true the beautiful and the good be see understand and perceive and through these they be value protect and encourage
5 sense have be give mentally and corporeally inwardly and outwardly to body and soul
the child think deeply on all these thing and meditate upon them day and night
then the old of the brother dream 1 splendid dream
strange to say not only the 2 brother but also the 3 and 4 brother all dream exactly the same thing namely that each go out into the world to find the philosopher stone
each dream that he find it and that as he ride back on his swift horse in the morning dawn over the velvety green meadow to his home in the castle of his father that the stone gleam from his forehead like 1 beam light and throw such 1 bright radiance upon the page of the book_of_truth that every word be illuminate which speak of the life beyond the grave
but the sister have no dream of go out into the wide world it never enter her mind
her world be her father house
i shall ride forth into the wide world say the old brother
i must try what life be like there as i mix with man
i will practise only the good and true with these i will protect the beautiful
much shall be change for the good while i be there now these thought be great and dare as our thought generally be at home before we have go out into the world and encounter its storm and tempest its thorn and its thistle
in him and in_all his brother the 5 sense be highly cultivate inwardly and outwardly but each of them have 1 sense which in keenness and development surpass the other 4
in the case of the old this preeminent sense be sight which he hope would be of special service
he have eye for all time and all people eye that can discover in the depth of the earth hide treasure and look into the heart of man as through 1 pane of glass he can read many than be often see on the cheek that blush or grow pale in the eye that droop or smile
stag and antelope accompany him to the western boundary of his home and there he find the wild swan
these he follow and find himself far away in the north far from the land of his father which extend eastward to the end of the earth
how he open his eye with astonishment
how_many thing be to be see here
and so different to the mere representation of picture such as those in his father house
at 1 he nearly lose his eye in astonishment at the rubbish and mockery bring forward to represent the beautiful but he keep his eye and soon find full employment for them
he wish to go thoroughly and honestly to work in his endeavor to understand the true the beautiful and the good
but how be they represent in the world
he observe that the wreath which rightly belong to the beautiful be often give the hideous that the good be often pass by unnoticed while mediocrity be applaud when it should have be hiss
people look at the dress not at the wearer think many of 1 name than of do their duty and trust many to reputation than to real service
it be everywhere the same
i see i must make 1 regular attack on these thing say he and he accordingly do not spare them
but while look for the truth come the evil one the father of lie to intercept him
gladly would the fiend have pluck out the eye of this seer but that would have be 1 too straightforward path for him he work many cunningly
he allow the young man to seek for and discover the beautiful and the good but while he be contemplate them the evil spirit blow one mote after another into each of his eye and such 1 proceeding would injure the strong sight
then he blow upon the motes and they become beam so_that the clearness of his sight be go and the seer be like 1 blind man in the world and have no_longer any faith in it
he have lose his good opinion of the world as_well as of himself and when 1 man give up the world and himself too it be all over with him
all over say the wild swan who fly across the sea to the east
all over twitter the swallow who be also fly eastward towards the tree_of_the_sun
it be no good news which they carry home
i think the seer have be badly serve say the 2 brother but the hearer may be many successful this one possess the sense of hear to 1 very high degree so acute be this sense that it be say he can hear the grass grow
he take 1 fond leave of all at home and ride away provide with good ability and good intention
the swallow escort him and he follow the swan till he find himself out in the world and far away from home
but he soon discover that [??:1/5/??:????:??] have too much of 1 good thing
his hearing be too fine
he not only hear the grass grow but can hear every man heart beat whether in sorrow or in joy
the whole world be to him like 1 clockmakers great workshop in which all the clock be go tick tick and all the turret clock strike ding dong it be unbearable
for 1 long time his ear endure it but at_last all the noise and tumult become too much for one man to bear
there be rascally boy of 60 year old for year do not alone make 1 man who raise 1 tumult which may have make the hearer laugh but for the applause which follow echo through every street and house and be even hear in country road
falsehood thrust itself forward and play the hypocrite the bell on the fool cap jingle and declare they be churchbells and the noise become so bad for the hearer that he thrust his finger into his ear
still he can hear false note and bad sing gossip and idle word scandal and slander groan and moan without and within
heaven help us
he thrust his finger far and far into his ear till at_last the drum burst
and now he can hear nothing many of the true the beautiful and the good for his hearing be to have be the mean by which he hope to acquire his knowledge
he become silent and suspicious and at_last trust no_one not even himself and no_longer hope to find and bring home the costly jewel he give it up and give himself up too which be bad than all
the bird in their flight towards the east carry the tiding and the news reach the castle in the tree_of_the_sun
i will try now say the 3 brother i have 1 keen nose now that be not 1 very elegant expression but it be his way and we must take him as he be
he have 1 cheerful temper and be besides $_BRL:1 poet he can make many thing appear poetical by the way in which he speak of them and idea strike him long before they occur to the mind of other
i can smell he would say and he attribute to the sense of smell which he possess in 1 high degree 1 great power in the region of the beautiful
i can smell he would say and many place be fragrant or beautiful according_to the taste of the frequenter
one man feel at home in the atmosphere of the tavern among the flare tallow candle and when the smell of spirit mingle with the fume of bad tobacco
another prefer sit amidst the overpower scent of jasmine or perfume himself with scent olive oil
this man seek the fresh sea breeze while that one climb the lofty mountaintop to look down upon the busy life in miniature beneath him as he speak in this way it seem as_if he have already be out in the world as_if he have already know and associate with man
but this experience be intuitive it be the poetry within him 1 gift from heaven bestow on him in his cradle
he bid farewell to his parental roof in the tree_of_the_sun and depart on foot from the pleasant scene that surround his home
arrive at its confine he mount on the back of 1 ostrich which run fast than 1 horse and afterwards when he fall in with the wild swan he swing himself on the strong of them for he love change and away he fly over the sea to distant land where there be great forest deep lake lofty mountain and proud city
wherever he come it seem as_if sunshine travel with him across the field for every flower every bush exhale 1 renew fragrance as_if conscious that 1 friend and protector be near one who understand them and know their value
the stunt rosebush shot forth twig unfold its leaf and bear the many beautiful rose every one can see it and even the black slimy woodsnail notice its beauty
i will give my seal to the flower say the snail i have trail my slime upon it i can do no many
thus it always fare with the beautiful in this world say the poet
and he make 1 song upon it and sing it after his own fashion but nobody listen
then he give 1 drummer twopence and 1 peacock feather and compose 1 song for the drum and the drummer beat it through the street of the town and when the people hear it they say that be 1 capital tune the poet write many song about the true the beautiful and the good
his song be listen to in the tavern where the tallow candle flare in the fresh clover field in the forest and on the highseas and it appear as_if this brother be to be many fortunate than the other 2
but the evil spirit be angry at this so he set to work with soot and incense which he can mix so artfully as to confuse 1 angel and how_much many easily 1 poor poet
the evil one know how to manage such people
he so completely surround the poet with incense that the man lose his head forget his mission and his home and at_last lose himself and vanish in smoke
but when the little bird hear of it they mourn and for 3 day they sing not 1 song
the black woodsnail become black still not for grief but for envy
they should have offer me incense he say for it be i who give him the idea of the many famous of his song the drum song of the_way_of_the_world and it be i who spit at the rise i can bring 1 witness to that fact but no tiding of all this reach the poet home in india
the bird have all be silent for 3 day and when the time of mourn be over so deep have be their grief that they have forget for whom they weep
such be the way of the world
now i must go out into the world and disappear like the rest say the 4 brother
he be as goodtempered as the 3 but no poet though he can be witty
the 2 old have fill the castle with joyfulness and now the last brightness be go away
sight and hearing have always be consider 2 of the chief sense among man and those which they wish to keep bright the other sense be look upon as of less importance
but the young son have 1 different opinion he have cultivate his taste in every way and taste be very powerful
it rule over what go into the mouth as_well as over all which be present to the mind and consequently this brother take upon himself to taste everything store up in bottle or jar this he call the rough part of his work
every man mind be to him as 1 vessel in which something be concoct every land 1 kind of mental kitchen
there be no delicacy here he say so he wish to go out into the world to find something delicate to suit his taste
perhaps fortune may be many favorable to me than it be to my brother
i shall start on my travel but what conveyance shall i choose
be air balloon invent yet
he ask of his father who know of all invention that have be make or would be make
air balloon have not then be invent nor steamships nor railway
good say he then i shall choose 1 air balloon my father know how they be to be make and guide
nobody have invent 1 yet and the people will believe that it be 1 aerial phantom
when i have do with the balloon i shall burn it and for this purpose you must give me 1 few piece of another invention which will come next i mean 1 few chemical match he obtain what he want and fly away
the bird accompany him far than they have the other brother
they be curious to know how this flight would end
many many of them come swoop down they think it must be some new bird and he soon have 1 goodly company of follower
they come in cloud till the air become darken with bird as it be with the cloud of locust over the land of egypt
and now he be out in the wide world
the balloon descend over one of the great city and the aeronaut take up his station at the high point on the church steeple
the balloon rise again into the air which it ought not to have do what become of it be not know neither be it of any consequence for balloon have not then be invent
there he sit on the church steeple
the bird no_longer hover over him they have get tire of him and he be tire of them
all the chimney in the town be smoking
there be altar erect to my honor say the wind who wish to say something agreeable to him as he sit there boldly look down upon the people in the street
there be one step along proud of his purse another of the key he carry behind him though he have nothing to lock up another take 1 pride in his motheaten coat and another in his mortify body
vanity all vanity
he exclaim
i must go down there byandby and touch and taste but i shall sit here 1 little while long for the wind blow pleasantly at my back
i shall remain here as long as the wind blow and enjoy 1 little rest
it be comfortable to sleep late in the morning when one have 1 great deal to do say the sluggard so i shall stop here as long as the wind blow for it please me and there he stay
but as he be sit on the weathercock of the steeple which keep turn round and round with him he be under the false impression that the same wind still blow and that he can stay where he be without expense
but in india in the castle on the tree_of_the_sun all be solitary and still since the brother have go away one after the other
nothing go well with them say the father they will never bring the glitter jewel home it be not make for me they be all dead and go then he bend down over the book_of_truth and gaze on the page on which he should have read of the life after death but for him there be nothing to be read or learn upon it
his blind daughter be his consolation and joy she cling to him with sincere affection and for the sake of his happiness and peace she wish the costly jewel can be find and bring home
with long tenderness she think of her brother
where be they
where do they live
how she wish she may dream of them but it be strange that not even in dream can she be bring near to them
but at_last one night she dream that she hear the voice of her brother call to her from the distant world and she can not refrain herself but go out to them and yet it seem in her dream that she still remain in her father house
she do not see her brother but she feel as it be 1 fire burning in her hand which however do not hurt her for it be the jewel she be bring to her father
when she awake she think for 1 moment that she still hold the stone but she only grasp the knob of her distaff
during the long evening she have spin constantly and round the distaff be weave thread fine than the web of 1 spider human eye can never have distinguish these thread when separate from each other
but she have wet them with her tear and the twist be as strong as 1 cable
she rise with the impression that her dream must be 1 reality and her resolution be take
it be still night and her father sleep she press 1 kiss upon his hand and then take her distaff and fasten the end of the thread to her father house
but for this blind as she be she would never have find her way home again to this thread she must hold fast and trust not to other or even to herself
from the tree_of_the_sun she break 4 leaf which she give up to the wind and the weather that they may be carry to her brother as letter and 1 greeting in_case she do not meet them in the wide world
poor blind child what would become of her in those distant region
but she have the invisible thread to which she can hold fast and she possess 1 gift which all the other lack
this be 1 determination to throw herself entirely into whatever she undertake and it make her feel as_if she have eye even at the tip of her finger and can hear down into her very heart
quietly she go forth into the noisy bustle wonderful world and wherever she go the sky grow bright and she feel the warm sunbeam and 1 rainbow above in the blue heaven seem to span the dark world
she hear the song of the bird and smell the scent of the orange grove and apple orchard so strongly that she seem to taste it
soft tone and charm song reach her ear as_well as harsh sound and rough word thought and opinion in strange contradiction to each other
into the deep recess of her heart penetrate the echo of human thought and feeling
now she hear the following word sadly sing life be 1 shadow that flit away in 1 night of darkness and woe but then would follow bright thought life have the rose sweet perfume with sunshine light and joy and if one stanza sound painfully each mortal think of himself alone is 1 truth alas too clearly know then on_the_other_hand come the answer love like 1 mighty flow stream fills every heart with its radiant gleam she hear indeed such word as these in the pretty turmoil here below all be 1 vain and paltry show
then come also word of comfort great and good be the action do by many whose worth be never know and if sometimes the mock strain reach her why not join in the jest cry that contemn all gift from the throne on high
in the blind girl heart 1 strong voice repeat to trust in thyself and god be good in_his holy will forever to rest but the evil spirit can not see this and remain content
he have many cleverness than 10000 man and he find mean to compass his end
he betook himself to the marsh and collect 1 few little bubble of stagnant water
then he utter over them the echo of lie word that they may become strong
he mix up together song of praise with lie epitaph as many as he can find boil them in tear shed by envy put upon them rouge which he have scrape from fade cheek and from these he produce 1 maiden in form and appearance like the blind girl the angel of completeness as man call her
the evil one plot be successful
the world know not which be the true and indeed how should the world know
to trust in thyself and god be good in his holy will forever to rest so sing the blind girl in full faith
she have entrust the 4 green leaf from the tree_of_the_sun to the wind as letter of greet to her brother and she have full confidence that the leaf would reach them
she fully believe that the jewel which outshine all the glory of the world would yet be find and that upon the forehead of humanity it would glitter even in the castle of her father
even in my father house she repeat
yes the place in which this jewel be to be find be earth and i shall bring many than the promise of it with me
i feel it glow and swell many and many in my close hand
every grain of truth which the keen wind carry up and whirl towards me i catch and treasure
i allow it to be penetrate with the fragrance of the beautiful of which there be so much in the world even for the blind
i take the beating of 1 heart engage in 1 good action and add them to my treasure
all that i can bring be but dust still it be 1 part of the jewel we seek and there be plenty my hand be quite full of it she soon find herself again at home carry thither in 1 flight of think never have loosen her hold of the invisible thread fasten to her father house
as she stretch out her hand to her father the power of evil dash with the fury of 1 hurricane over the tree_of_the_sun 1 blast of wind rush through the open door and into the sanctuary where lie the book_of_truth
it will be blow to dust by the wind say the father as he seize the open hand she hold towards him
no she reply with quiet confidence it be indestructible
i feel its beam warm my very soul then her father observe that 1 dazzle flame gleam from the white page on which the shine dust have pass from her hand
it be there to prove the certainty of eternal life and on the book glow 1 shine word and only one the word believe
and soon the 4 brother be again with the father and daughter
when the green leaf from home fall on the bosom of each 1 longing have seize them to return
they have arrive accompany by the bird of passage the stag the antelope and all the creature of the forest who wish to take part in their joy
we have often see when 1 sunbeam burst through 1 crack in the door into 1 dusty room how 1 whirl column of dust seem to circle round
but this be not poor insignificant common dust which the blind girl have bring even the rainbow color be dim when compare with the beauty which shine from the page on which it have fall
the beam word believe from every grain of truth have the brightness of the beautiful and the good many bright than the mighty pillar of flame that lead moses and the child of israel to the land of canaan and from the word believe arise the bridge of hope reach even to the unmeasurable love in the realm of the infinite
the_end
