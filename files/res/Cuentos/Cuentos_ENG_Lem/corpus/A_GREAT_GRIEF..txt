1 great_grief_this story really consist of 2 part
the 1 part may be leave out but it give us 1 few particular and these be useful we be stay in the country at 1 gentlemans seat where it happen that the master be absent for 1 few day
in_the_meantime there arrive from the next town 1 lady she have 1 pug dog with her and come she say to dispose of share in her tanyard
she have her paper with her and we advise her to put them in 1 envelope and to write thereon the address of the proprietor of the estate general_warcommissary_knight c
she listen to us attentively seize the pen pause and beg us to repeat the direction slowly
we comply and she write but in the midst of the general_war she strike fast sigh deeply and say i be only 1 woman
her puggie have seat itself on the ground while she write and growl for the dog have come with her for amusement and for the sake of its health and then the bare floor ought not to be offer to 1 visitor
his outward appearance be characterize by 1 snub nose and 1 very fat back
he doesnt bite say the lady he have no tooth
he be like one of the family faithful and grumpy but the latter be my grandchildrens fault for they have tease him they play at wedding and want to give him the part of the bridesmaid and thats too much for him poor old fellow and she deliver her paper and take puggie upon her arm
and this be the 1 part of the story which may have be leave out
puggie_died
thats the 2 part
it be about 1 week afterwards we arrive in the town and put up at the inn
our window look into the tanyard which be divide into 2 part by 1 partition of plank in 1/2 be many skin and hide raw and tan
here be all the apparatus necessary to carry on 1 tannery and it belong to the widow
puggie have die in the morning and be to be bury in this part of the yard the grandchild of the widow that be of the tanner widow for puggie have never be marry fill up the grave and it be 1 beautiful grave it must have be quite pleasant to lie there
the grave be border with piece of flowerpot and strew over with sand quite at the top they have stick up half 1 beer bottle with the neck upwards and that be not at all allegorical
the child dance round the grave and the old of the boy among them 1 practical youngster of 7 year make the proposition that there should be 1 exhibition of puggies burialplace for all who live in the lane the price of admission be to be 1 trouser button for every boy would be sure to have one and each may also give one for 1 little girl
this proposal be adopt by acclamation
and all the child out of the lane yes even out of the little lane at the back flock to the place and each give 1 button
many be notice to go about on that afternoon with only one suspender but then they have see puggies grave and the sight be worth much many
but in front of the tanyard close to the entrance stand 1 little girl clothe in rag very pretty to look at with curly hair and eye so blue and clear that it be 1 pleasure to look into them
the child say not 1 word nor do she cry but each time the little door be open she give 1 long long look into the yard
she have not 1 button that she know right well and therefore she remain stand sorrowfully outside till all the other have see the grave and have go away then she sit down hold her little brown hand before her eye and burst into tear this girl alone have not see puggies grave
it be 1 grief as great to her as any grow person can experience
we saw this from above and look at from above how_many 1 grief of our own and of other can make us smile
that be the story and whoever do not understand it may go and purchase 1 share in the tanyard from the window
the_end
