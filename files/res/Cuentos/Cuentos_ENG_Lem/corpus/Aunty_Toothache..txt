where do we get this story would you like to know
we get it from the basket that the wastepaper be throw into
many 1 good and rare book have be take to the delicatessen store and the grocer not to be read but to be use as wrap paper for starch and coffee bean for salt herring butter and cheese used writing paper have also be find suitable
frequently 1 throw into the wastepaper basket what ought not to go there
i know 1 grocer assistant the son of 1 delicatessen store owner he have work his way up from serving in the cellar to serving in the front shop he be 1 wellread person his reading consist of the print and write matter to be find on the paper use for wrap he have 1 interest collection consist of several important official document from the wastepaper basket of busy and absentminded official 1 few confidential letter from one lady friend to another report of scandal which be not to go further not to be mention by 1 soul he be 1 living salvage institution for many than 1 little of our literature and his collection cover 1 wide field he have the run of his parent shop and that of his present master and have there save many 1 book or leave of 1 book well worth reading twice
he have show me his collection of print and write matter from the wastepaper basket the many value item of which have come from the delicatessen store 1 couple of leaf from 1 large composition book lie among the collection the unusually clear and neat handwriting attract my attention at once
this be write by the student he say the student who live opposite here and die about 1 month ago he suffer terribly from toothache as one can see it be quite amuse to read this be only 1 small part of what he write there be 1 whole book and many besides my parent give the student landlady half $_GBP:1 of green soap for it this be what i have be able to save of it
i borrow it i read it and now i tell it
the title be
aunty_toothache
i_aunty give me sweet when i be little my tooth can stand it then it didnt hurt them now_i be old be 1 student and still she go on spoil me with sweet she say i be 1 poet
i have something of the poet in me but not enough often when i go walk along the city street it seem to me as_if i be walk in 1 big library the house be the bookshelf and every floor be 1 shelf with book there stand 1 story of everyday life next to it be 1 good old comedy and there be work of all scientific branch bad literature and good reading i can dream and philosophize among all this literature
there be something of the poet in me but not enough no_doubt many people have just as much of it in them as i though they do not carry 1 sign or 1 necktie with the word poet on it they and i have be give 1 divine gift 1 blessing great enough to satisfy oneself but altogether too little to be portion out again to other it come like 1 ray of sunlight and fill one soul and thought it come like the fragrance of 1 flower like 1 melody that one know and yet can not remember from where
the other evening i sit in my room and feel 1 urge to read but i have no book no paper just then 1 leaf fresh and green fall from the lime tree and the breeze carry it in through the window to me i examine the many vein in it 1 little insect be crawl across them as_if it be make 1 thorough study of the leaf this make me think of man wisdom we also crawl about on 1 leaf our knowledge be limit to that only and yet we unhesitatingly deliver 1 lecture on the whole big tree the root the trunk and the crown the great tree comprise of god the world and immortality and of all this we know only 1 little leaf
as i be sit there i receive 1 visit from aunty_mille_i show her the leaf with the insect and tell her of my thought in connection with these and her eye light up
you be 1 poet she say perhaps the great we have if_i should live to see this i would go to my grave gladly ever since the brewer rasmussens funeral you have amaze me with your powerful imagination
so say aunty_mille and she then kiss me
who be aunty_mille and who be rasmussen the brewer
ii
we child always call our mother aunt aunty we have no other name for her
she give us jam and sweet although they be very injurious to our tooth but the dear child be her weakness she say it be cruel to deny them 1 few sweet when they be so fond of them and thats why we love aunty so much
she be 1 old maid as far back as i can remember she be always old her age never seem to change
in earlier year she have suffer 1 great deal from toothache and she always speak about it and so it happen that her friend the brewer rasmussen who be 1 great wit call her aunty_toothache
he have retire from the brew business some year before and be then live on the interest of his money he frequently visit aunty he be old than she he have no tooth at all only 1 few black stump when 1 child he have eat too much sugar he tell us child and thats how he come to look as he do
aunt can surely never have eat sugar in her childhood for she have the many beautiful white tooth she take great care of them and she do not sleep with them at night say rasmussen the brewer we child know that this be say in malice but aunty say he do not mean anything by it
one morning at the breakfast table she tell us of 1 terrible dream she have have during the night in which one of her tooth have fall out
that mean she say that i shall lose 1 true friend
be it 1 false tooth ask the brewer with 1 chuckle if so it can only mean that you will lose 1 false friend
you be 1 insolent old man say aunty angrier than i have see her before or ever have since
she later tell us that her old friend have only be tease her he be the fine man on earth and when he die he would become one of gods little angel in heaven
i think 1 good deal of this transformation and wonder if i would be able to recognize him in this new character
when aunty and he have be young he have propose to her she have settle down to think it over have think too long and have become 1 old maid but always remain his true friend
and then brewer_rasmussen die he be take to his grave in the many expensive hearse and be follow by 1 great number of folk include people with order and in uniform
aunt stand dress in mourn by the window together with all of us child except our little brother whom the stork have bring 1 week before when the hearse and the procession have pass and the street be empty aunty want to go away from the window but i do not want to i be wait for the angel rasmussen the brewer surely he have by now become one of gods bewinged little child and would appear
aunt i say dont you think that he will come now or that when the stork again bring us 1 little brother hell then bring us the angel rasmussen
aunt be quite overwhelm by my imagination and say that child will become 1 great poet and this she keep repeat all the time i go to school and even after my confirmation and yes still do now that i be 1 student
she be and be to me the many sympathetic of friend both in my poetical trouble and dental trouble for i have attack of both
just write down all your thought she say and put them in the table drawer thats what jean_paul do he become 1 great poet though i dont admire him he do not excite one you must be excite yes you will be excite
the night after she say this i lie awake full of longing and anguish with anxiety and fond hope to become the great poet that aunty saw and perceive in me i go through all the pain of 1 poet but there be 1 even great pain toothache and it be grind and crush me i become 1 writhe worm with 1 bag of herb and 1 mustard plaster
i know all about it say aunty_there be 1 sorrowful smile on her lip and her white tooth glisten
but i must begin 1 new chapter in my own and my aunt story
iii
i have move to 1 new flat and have be live there 1 month i be tell aunty about it
i live with 1 quiet family they pay no attention to me even_if i ring Z_times besides it be 1 noisy house full of sound and disturbance cause by the weather the wind and the people i live just above the street gate every carriage that drive out or in make the picture on the wall move about the gate bang and shake the house as_if there be 1 earthquake if_i be in bed the shock go right through all my limb but that be say to be strengthen to the nerve if the wind blow and it be always blow in this country the long window hook outside swing to_and_fro and strike against the wall the bell on the gate to the neighbour yard ring with every gust of wind
the people who live in the house come home at all hour from late in the evening until far into the night the lodger just above me who in the daytime give lesson on the trombone come home the late and do not go to bed before he have take 1 little [??:??/??/??:0000:am] promenade with heavy step and in iron heel shoe
there be no double window there be 1 break pane in my room over which the landlady have paste some paper but the wind blow through the crack despite that and produce 1 sound similar to that of 1 buzz wasp it be like the sort of music that make one go to sleep if at_last i fall asleep i be soon awaken by the crow of the cock from the cellarmans hencoop the cock and hen announce that it will soon be morning the small pony which have no stable but be tie up in the storeroom under the staircase kick against the door and the paneling as they move about
the day dawn the porter who live with his family in the attic come thunder down the stair his wooden shoe clatter the gate bang and the house shake and when all this be over the lodger above begin to occupy himself with gymnastic exercise he lift 1 heavy iron ball in each hand but he be not able to hold onto them and they be continually fall on the floor while at the same time the young folk in the house who be go to school come scream with all their may i go to the window and open it to get some fresh air and it be many refresh when i can get it and when the young woman in the back building be not wash glove in soapsuds by which she earn her livelihood otherwise it be 1 pleasant house and i live with 1 quiet family
this be the report i give aunty about my flat though it be livelier at the time for the speak word have 1 fresher sound than the write
you be 1 poet cry aunty_just write down all you have say and you will be as good as dickens_indeed to me you be much many interest you paint when you speak you describe your house so_that one can see it it make one shudder go on with your poetry put some living being into it people charm people especially unhappy one
i write down my description of the house as it stand with all its sound its noise but include only myself there be no plot in it that come later
iv
it be during wintertime late at night after theater hour it be terrible weather 1 snowstorm rage so_that one can hardly move along
aunt have go to the theater and i go there to take her home it be difficult for one to get anywhere to say nothing of help another all the hire carriage be engage aunty live in 1 distant section of the town while my dwelling be close to the theater had this not be the case we would have have to take refuge in 1 sentry box for 1 while
we trudge along in the deep snow while the snowflake whirl around us i have to lift her hold onto her and push her along only twice do we fall but we fall on the soft snow
we reach my gate where we shake some of the snow from ourselves on the stair too we shake some off and yet there be still enough almost to cover the floor of the anteroom
we take off our overcoat and boot and what other clothes may be remove the landlady lend aunty dry stocking and 1 nightcap this she would need say the landlady and add that it would be impossible for my aunt to get home that night which be true then she ask aunty to make use of her parlor where she would prepare 1 bed for her on the sofa in front of the door that lead into my room and that be always keep lock and so she stay
the fire burn in my stove the tea urn be place on the table and the little room become cozy if_not as cozy as auntys own room where in the wintertime there be heavy curtain before the door heavy curtain before the window and double carpet on the floor with 3 layer of thick paper underneath one sit there as_if in 1 wellcorked bottle full of warm air still as i have say it be also cozy at my place while outside the wind be whistle
aunt talk and reminisce she recall the day of her youth the brewer come back many old memory be revive
she can remember the time i get my 1 tooth and the familys delight over it my 1 tooth the tooth of innocence shine like 1 little drop of milk the milk tooth
when one have come several many come 1 whole rank of them side by side appear both above and below the fine of childrens tooth though these be only the vanguard not the real tooth which have to last one whole lifetime
then those also appear and the wisdom tooth as_well the flank man of each rank bear in pain and great tribulation
they disappear too sometimes every one of them they disappear before their time of service be up and when the very last one go that be far from 1 happy day it be 1 day for mourn and so then one consider himself old even_if he feel young
such thought and talk be not pleasant yet we come to talk about all this we go back to the day of my childhood and talk and talk it be 12 oclock before aunty go to rest in the room near by
good night my sweet child she call i shall now sleep as_if i be in my own bed
and she sleep peacefully but otherwise there be no peace either in the house or outside the storm rattle the window strike the long dangle iron hook against the house and ring the neighbour backyard bell the lodger upstairs have come home he be still take his little nightly tour up and down the room he then kick off his boot and go to bed and to sleep but he snore so_that anyone with good ear can hear him through the ceiling
i find no rest no peace the weather do not rest either it be lively the wind howl and sing in its own way my tooth also begin to be lively and they hum and sing in their way 1 awful toothache be come on
there be 1 draft from the window the moon shine in upon the floor the light come and go as the cloud come and go in the stormy weather there be 1 restless change of light and shadow but at_last the shadow on the floor begin to take shape i stare at the move form and feel 1 icycold wind against my face
on the floor sit 1 figure thin and long like something 1 child would draw with 1 pencil on 1 slate something suppose to look like 1 person 1 single thin line form the body another 2 line the arm each leg being but 1 single line and the head have 1 polygonal shape
the figure soon become many distinct it have 1 very thin very fine sort of cloth drape around it clearly show that the figure be that of 1 female
i hear 1 buzz sound was it she or the wind which be buzz like 1 hornet through the crack in the pane
no it be she madam_toothache herself her terrible highness satania_infernalis_god deliver and preserve us from her
it be good to be here she buzz these be nice quarter mossy ground fenny ground gnats have be buzz around here with poison in their sting and now i be here with such 1 sting it must be sharpen on human tooth those belong to the fellow in bed here shine so brightly they have defy sweet and sour thing heat and cold nutshell and plum stone but i shall shake them make them quake fee their root with drafty wind and give them cold foot
that be 1 frighten speech she be 1 terrible visitor
so you be 1 poet she say well_ill make you well versed in_all the poetry of toothache ill thrust iron and steel into your body ill seize all the fiber of your nerve
i then feel as_if 1 redhot awl be be drive into my jawbone i writhe and twist
1 splendid set of tooth she say just like 1 organ to play upon we shall have 1 grand concert with jewsharps kettledrums and trumpet piccoloflute and 1 trombone in the wisdom tooth grand poet grand music
and then she start to play she look terrible even_if one do not see many of her than her hand the shadowy gray icecold hand with the long thin point finger each of them be 1 instrument of torture the thumb and the forefinger be the pincer and wrench the middle finger end in 1 point awl the ring finger be 1 drill and the little finger squirt gnat poison
i be go to teach you meter she say 1 great poet must have 1 great toothache 1 little poet 1 little toothache
oh let me be 1 little poet i beg let me be nothing at all and_i be not 1 poet i have only fit of poetry like fit of toothache go away go away
will you acknowledge then that i be mightier than poetry philosophy mathematics and all the music she say mightier than all those notion that be paint on canvas or carve in marble i be old than every one of them i be bear close to the garden of paradise just outside where the wind blow and the wet toadstool grow it be i who make eve wear clothes in the cold weather and adam also believe me there be power in the 1 toothache
i believe it all i say but go away go away
yes if you will give up be 1 poet never put verse on paper slate or any sort of write material then i will let you off but ill come again if you write poetry
i swear i say only let me never see or feel you any many
see me you shall but in 1 many substantial shape in 1 shape many dear to you than i be now you shall see me as aunty_mille and i shall say write poetry my sweet boy you be 1 great poet perhaps the great we have but if you believe me and begin to write poetry then i will set music to your verse and play them on your mouth harp you sweet child remember me when you see aunty_mille
then she disappear
at our parting i receive 1 thrust through my jawbone like that of 1 redhot awl but it soon subside and then i feel as_if i be glide along the smooth water i saw the white water lily with their large green leave bend and sink down under me they wither and dissolve and i sink too and dissolve into peace and rest
to die and melt away like snow resound in the water to evaporate into air to drift away like the cloud
great glow name and inscription on wave banner of victory the letter patent of immortality write on the wing of 1 ephemera shine down to me through the water
the sleep be deep 1 sleep now without dream i do not hear the whistle wind the bang gate the ring of the neighbour gate bell or the lodger strenuous gymnastics
what happiness
then come 1 gust of wind so strong that the lock door to auntys room burst open aunty jump up put on her shoe get dress and come into my room i be sleep like one of gods angel she say and she have not the heart to awaken me
i later awake by myself and open my eye i have completely forget that aunty be in the house but i soon remember it and then remember my toothache vision dream and reality be blend
i suppose you do not write anything last night after we say good night she say i wish you have you be my poet and shall always be
it seem to me that she smile rather slyly i do not know if it be the kindly aunty_mille who love me or the terrible one to whom i have make the promise the night before
have you write any poetry sweet child
no no i shout you be aunty_mille arent you
who else she say and it be aunty_mille
she kiss me get into 1 carriage and drive home
i write down what be write here it be not in verse and it will never be print
yes here end the manuscript
my young friend the grocer assistant can not find the miss sheet they have go out into the world like the paper around the salt herring the butter and the green soap they have fulfil their destiny
the brewer be dead aunty be dead the student be dead he whose spark of genius go into the basket this be the end of the story the story of aunty_toothache
