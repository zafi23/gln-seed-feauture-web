the_sunbeam_and_the_captive_it be autumn
we stand on the rampart and look out over the sea
we look at the numerous ship and at the swedish coast on the opposite side of the sound rise far above the surface of the water which mirror the glow of the evening sky
behind us the wood be sharply define mighty tree surround us and the yellow leaf flutter down from the branch
below at the foot of the wall stand 1 gloomy look building enclose in palisade
the space between be dark and narrow but still many dismal must it be behind the iron grating in the wall which cover the narrow loophole or window for in these dungeon the many deprave of the criminal be confine
1 ray of the setting sun shoot into the bare cell of one of the captive for gods sun shine upon the evil and the good
the harden criminal cast 1 impatient look at the bright ray
then 1 little bird fly towards the grating for bird twitter to the just as_well as to the unjust
he only cry tweet tweet and then perch himself near the grating flutter his wing peck 1 feather from one of them puff himself out and set his feather on end round his breast and throat
the bad chain man look at him and 1 many gentle expression come into his hard face
in his breast there rise 1 thought which he himself can not rightly analyze but the thought have some connection with the sunbeam with the bird and with the scent of violet which grow luxuriantly in spring at the foot of the wall
then there come the sound of the hunter horn merry and full
the little bird start and fly away the sunbeam gradually vanish and again there be darkness in the room and in the heart of that bad man
still the sun have shine into that heart and the twitter of the bird have touch it
sound on ye glorious strain of the hunter horn continue your stirring tone for the evening be mild and the surface of the sea heave slowly and calmly be smooth as 1 mirror
the_end
