the_golden_treasure_the drummer wife go into the church
she saw the new altar with the paint picture and the carve angel
those upon the canvas and in the glory over the altar be just as beautiful as the carve one and they be paint and gilt into the bargain
their hair gleam golden in the sunshine lovely to behold but the real sunshine be many beautiful still
it shine red clear through the dark tree when the sun go down
it be lovely thus to look at the sunshine of heaven
and she look at the red sun and she think about it so deeply and think of the little one whom the stork be to bring and the wife of the drummer be very cheerful and look and look and wish that the child may have 1 gleam of sunshine give to it so_that it may at_least become like one of the shine angel over the altar
and when she really have the little child in her arm and hold it up to its father then it be like one of the angel in the church to behold with hair like gold the gleam of the setting sun be upon it
my golden treasure my riches my sunshine
say the mother and she kiss the shine lock and it sound like music and song in the room of the drummer and there be joy and life and movement
the drummer beat 1 roll 1 roll of joy
and the drum say the firedrum that be beat when there be 1 fire in the town red hair
the little fellow have red hair
believe the drum and not what your mother say
ruba dub ruba dub
and the town repeat what the firedrum have say
the boy be take to church the boy be christen
there be nothing much to be say about his name he be call peter
the whole town and the drum too call him peter the drummer boy with the red hair but his mother kiss his red hair and call him her golden treasure
in the hollow way in the clayey bank many have scratch their name as 1 remembrance
celebrity be always something
say the drummer and so he scratch his own name there and his little son name likewise
and the swallow come
they have on their long journey see many durable character engraven on rock and on the wall of the temple in hindostan mighty deed of great king immortal name so old that no_one now can read or speak them
remarkable celebrity
in the clayey bank the marten build their nest
they bore hole in the deep declivity and the splash rain and the thin mist come and crumble and wash the name away and the drummer name also and that of his little son
peter name will last 1 full year and 1/2 long
say the father
fool
think the firedrum but it only say dub dub dub rubadub
he be 1 boy full of life and gladness this drummer son with the red hair
he have 1 lovely voice
he can sing and he sing like 1 bird in the woodland
there be melody and yet no melody
he must become 1 chorister boy say his mother
he shall sing in the church and stand among the beautiful gild angel who be like him
fiery cat
say some of the witty one of the town
the drum hear that from the neighbour wife
dont go home peter cry the street boy
if you sleep in the garret therell be 1 fire in the house and the firedrum will have to be beat look out for the drumstick reply peter and small as he be he run up boldly and give the foremost such 1 punch in the body with his fist that the fellow lose his leg and tumble over and the other take their leg off with themselves very rapidly
the town musician be very genteel and fine
he be the son of the royal platewasher
he be very fond of peter and would sometimes take him to his home and he give him 1 violin and teach him to play it
it seem as_if the whole art lie in the boy finger and he want to be many than 1 drummer he want to become musician to the town
ill be 1 soldier say peter for he be still quite 1 little lad and it seem to him the fine thing in the world to carry 1 gun and to be able to [??:1/3/??:????:??] 2 one 2 and to wear 1 uniform and 1 sword
ah you learn to long for the drumskin drum dum dum
say the drum
yes if he can only [??:??/3/??:????:??] his way up to be 1 general
observe his father but before he can do that there must be war heaven forbid
say his mother
we have nothing to lose remark the father
yes we have my boy she retort
but suppose he come back 1 general
say the father
without arm and leg
cry the mother
no i would rather keep my golden treasure with me drum dum dum
the firedrum and all the other drum be beat for war have come
the soldier all set out and the son of the drummer follow them
redhead
golden treasure
the mother weep the father in fancy saw him famous the town musician be of opinion that he ought not to go to war but should stay at home and learn music
redhead say the soldier and little peter laugh but when one of them sometimes say to another foxey he would bite his tooth together and look another way into the wide world
he do not care for the nickname
the boy be active pleasant of speech and goodhumored that be the good canteen say his old comrade
and many 1 night he have to sleep under the open sky wet through with the drive rain or the fall mist but his good humor never forsake him
the drumstick sound rubadub all up all up
yes he be certainly bear to be 1 drummer
the day of battle dawn
the sun have not yet rise but the morning be come
the air be cold the battle be hot there be mist in the air but still many gunpowdersmoke
the bullet and shell fly over the soldier head and into their head into their body and limb but still they press forward
here or there one or other of them would sink on his knee with bleeding temple and 1 face as white as chalk
the little drummer still keep his healthy color he have suffer no damage he look cheerfully at the dog of the regiment which be jump along as merrily as_if the whole thing have be get up for his amusement and as_if the bullet be only fly about that he may have 1 game of play with them
[??:??/3/??:????:??]
forward
[??:??/3/??:????:??]
this be the word of command for the drum
the word have not yet be give to fall back though they may have do so and perhaps there would have be much sense in it and now at_last the word retire be give but our little drummer beat forward
[??:??/3/??:????:??]
for he have understand the command thus and the soldier obey the sound of the drum
that be 1 good roll and prove the summons to victory for the man who have already begin to give way
life and limb be lose in the battle
bombshells tear away the flesh in red strip bombshells light up into 1 terrible glow the strawheaps to which the wound have drag themselves to lie untended for many hour perhaps for all the hour they have to live
its no use think of it and yet one can not help think of it even far away in the peaceful town
the drummer and his wife also think of it for peter be at the war
now im tire of these complaint say the firedrum
again the day of battle dawn the sun have not yet rise but it be morning
the drummer and his wife be asleep
they have be talk about their son as indeed they do almost every night for he be out yonder in gods hand
and the father dream that the war be over that the soldier have return home and that peter wear 1 silver cross on his breast
but the mother dream that she have go into the church and have see the paint picture and the carve angel with the gild hair and her own dear boy the golden treasure of her heart who be standing among the angel in white robe sing so sweetly as surely only the angel can sing and that he have soar up with them into the sunshine and nod so kindly at his mother
my golden treasure
she cry out and she awake
now the good god have take him to himself
she fold her hand and hide her face in the cotton curtain of the bed and weep
where do he rest now
among the many in the big grave that they have dig for the dead
perhaps hes in the water in the marsh
nobody know his grave no holy word have be read over it
and the lords_prayer go inaudibly over her lip she bow her head and be so weary that she go to sleep
and the day go by in life as in dream
it be even
over the battlefield 1 rainbow spread which touch the forest and the deep marsh
it have be say and be preserve in popular belief that where the rainbow touch the earth 1 treasure lie bury 1 golden treasure and here there be one
no_one but his mother think of the little drummer and therefore she dream of him
and the day go by in life as in dream
not 1 hair of his head have be hurt not 1 golden hair
drummarum
drummarum
there he be
the drum may have say and his mother may have sing if she have see or dream it
with hurrah and song adorn with green wreath of victory they come home as the war be at 1 end and peace have be sign
the dog of the regiment spring on in front with large bound and make the way Z_times as long for himself as it really be
and day and week go by and peter come into his parent room
he be as brown as 1 wild man and his eye be bright and his face beam like sunshine
and his mother hold him in her arm she kiss his lip his forehead and his red hair
she have her boy back again he have not 1 silver cross on his breast as his father have dream but he have sound limb 1 thing the mother have not dream
and what 1 rejoicing be there
they laugh and they weep and peter embrace the old firedrum
there stand the old skeleton still
he say
and the father beat 1 roll upon it
one would think that 1 great fire have break out here say the firedrum
bright day
fire in the heart
golden treasure
skrat
skrrat
skrrrrat
and what then
what then
ask the town musician
peter far outgrow the drum he say
peter will be great than i_and yet he be the son of 1 royal platewasher but all that he have learn in half 1 lifetime peter learn in half 1 year
there be something so merry about him something so truly kindhearted
his eye gleam and his hair gleam too there be no deny that
he ought to have his hair dye say the neighbour wife
that answer capitally with the policemans daughter and she get 1 husband but her hair turn as green as duckweed and be always have to be color up she know how to manage for herself say the neighbour and so can peter
he come to the many genteel house even to the burgomasters where he give miss_charlotte pianoforte lesson he can play
he can play fresh out of his heart the many charm piece that have never be put upon musicpaper
he play in the bright night and in the dark night too
the neighbour declare it be unbearable and the firedrum be of the same opinion
he play until his thought soar up and burst forth in great plan for the future to be famous
and burgomasters charlotte sit at the piano
her delicate finger dance over the key and make them ring into peters heart
it seem too much for him to bear and this happen not once but many time and at_last one day he seize the delicate finger and the white hand and kiss it and look into her great brown eye
heaven know what he say but we may be allow to guess at it
charlotte blush to guess at it
she redden from brow to neck and answer not 1 single word and then stranger come into the room and one of them be the state councillor son
he have 1 lofty white forehead and carry it so high that it seem to go back into his neck
and peter sit by her 1 long time and she look at him with gentle eye
at home that even he speak of travel in the wide world and of the golden treasure that lie hide for him in his violin
to be famous
tummelum tummelum tummelum
say the firedrum
peter have go clear out of his wit
i think there must be 1 fire in the house next day the mother go to market
shall i tell you news peter
she ask when she come home
1 capital piece of news
burgomasters_charlotte have engage herself to the state councillor son the betrothal take place yesterday evening no cry peter and he spring up from his chair
but his mother persist in saying yes_she have hear it from the baker wife whose husband have it from the burgomasters own mouth and_peter become as pale as death and sit down again
good heaven
whats the matter with you
ask his mother
nothing nothing only leave me to myself he answer but the tear be run down his cheek
my sweet child my golden treasure
cry the mother and she weep but the firedrum sing not out loud but inwardly
charlottes go
charlottes go
and now the song be do but the song be not do there be many many verse in it long verse the many beautiful verse the golden treasure of 1 life
she behave like 1 mad woman say the neighbour wife
all the world be to see the letter she get from her golden treasure and to read the word that be write in the paper about his violin play
and he send her money too and thats very useful to her since she have be 1 widow he play before emperor and king say the town musician
i never have that fortune but hes my pupil and he do not forget his old master and his mother say his father dream that peter come home from the war with 1 silver cross
he do not gain one in the war but it be still many difficult to gain one in this way
now he have the cross of honor
if his father have only live to see it
hes grow famous
say the firedrum and all his native town say the same thing for the drummer son peter with the red hair peter whom they have know as 1 little boy running about in wooden shoe and then as 1 drummer play for the dancer be become famous
he play at our house before he play in the presence of king say the burgomasters wife
at that time he be quite smite with charlotte
he be always of 1 aspire turn
at that time he be saucy and 1 enthusiast
my husband laugh when he hear of the foolish affair and now our charlotte be 1 state councillor wife 1 golden treasure have be hide in the heart and soul of the poor child who have beat the roll as 1 drummer 1 roll of victory for those who have be ready to retreat
there be 1 golden treasure in his bosom the power of sound it burst forth on his violin as_if the instrument have be 1 complete organ and as_if all the elf of 1 midsummer night be dance across the string
in its sound be hear the pipe of the thrush and the full clear note of the human voice therefore the sound bring rapture to every heart and carry his name triumphant through the land
that be 1 great firebrand the firebrand of inspiration
and then he look so splendid
say the young lady and the old lady too and the old of all procure 1 album for famous lock of hair wholly and solely that she may beg 1 lock of his rich splendid hair that treasure that golden treasure
and the son come into the poor room of the drummer elegant as 1 prince happier than 1 king
his eye be as clear and his face be as radiant as sunshine and he hold his mother in his arm and she kiss his mouth and weep as blissfully as any one can weep for joy and he nod at every old piece of furniture in the room at the cupboard with the teacup and at the flowervase
he nod at the sleepingbench where he have sleep as 1 little boy but the old firedrum he bring out and drag it into the middle of the room and say to it and to his mother my father would have beat 1 famous roll this evening
now i must do it
and he beat 1 thunder rollcall on the instrument and the drum feel so highly honor that the parchment burst with exultation
he have 1 splendid touch
say the drum
ive 1 remembrance of him now that will last
i expect that the same thing will happen to his mother from pure joy over her golden treasure and this be the story of the golden_treasure
the_end
