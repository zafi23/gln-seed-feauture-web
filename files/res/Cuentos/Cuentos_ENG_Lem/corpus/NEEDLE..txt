the_darningneedle_there be once 1 darningneedle who think herself so fine that she fancy she must be fit for embroidery
hold me tight she would say to the finger when they take her up dont let me fall if you do i shall never be find again i be so very fine that be your opinion be it
say the finger as they seize her round the body
see i be come with 1 train say the darningneedle draw 1 long thread after her but there be no knot in the thread
the finger then place the point of the needle against the cook slipper
there be 1 crack in the upper leather which have to be sew together
what coarse work
say the darningneedle i shall never get through
i shall break
i be break
and sure enough she break
do i not say so
say the darningneedle i know i be too fine for such work as that this needle be quite useless for sew now say the finger but they still hold it fast and the cook drop some sealingwax on the needle and fasten her handkerchief with it in front
so now i be 1 breastpin say the darningneedle i know very well i should come to honor some day merit be sure to rise and she laugh quietly to herself for of course no_one ever saw 1 darningneedle laugh
and there she sit as proudly as_if she be in 1 state coach and look all around her
may i be allow to ask if you be make of gold
she inquire of her neighbour 1 pin you have 1 very pretty appearance and 1 curious head although you be rather small
you must take pain to grow for it be not every one who have sealingwax drop upon him and as she speak the darningneedle draw herself up so proudly that she fall out of the handkerchief right into the sink which the cook be clean
now i be go on 1 journey say the needle as she float away with the dirty water i do hope i shall not be lose but she really be lose in 1 gutter
i be too fine for this world say the darningneedle as she lie in the gutter but i know who i be and that be always some comfort so the darningneedle keep up her proud behavior and do not lose her good humor
then there float over her all sort of thing chip and straw and piece of old newspaper
see how they sail say the darningneedle they do not know what be under them
i be here and here i shall stick
see there go 1 chip think of nothing in the world but himself only 1 chip
theres 1 straw go by now how he turn and twist about
dont be think too much of yourself or you may chance to run against 1 stone
there swim 1 piece of newspaper what be write upon it have be forget long ago and yet it give itself air
i sit here patiently and quietly
i know who i be so i shall not move one day something lie close to the darningneedle glitter so splendidly that she think it be 1 diamond yet it be only 1 piece of break bottle
the darningneedle speak to it because it sparkle and represent herself as 1 breastpin
i suppose you be really 1 diamond
she say
why yes something of the kind he reply and so each believe the other to be very valuable and then they begin to talk about the world and the conceited people in it
i have be in 1 ladys workbox say the darningneedle and this lady be the cook
she have on each hand 5 finger and anything so conceited as these 5 finger i have never see and yet they be only employ to take me out of the box and to put me back again were they not highborn
highborn
say the darningneedle no indeed but so haughty
they be 5 brother all bear finger they keep very proudly together though they be of different length
the one who stand 1 in the rank be name the thumb he be short and thick and have only one joint in his back and can therefore make but one bow but he say that if he be cut off from 1 man hand that man would be unfit for 1 soldier
sweettooth his neighbour dip himself into sweet or sour point to the sun and moon and form the letter when the finger write
longman the middle finger look over the head of all the other
goldband the next finger wear 1 golden circle round his waist
and little playman do nothing at all and seem proud of it
they be boasters and boasters they will remain and therefore i leave them and now we sit here and glitter say the piece of break bottle
at the same moment many water stream into the gutter so_that it overflow and the piece of bottle be carry away
so he be promote say the darningneedle while i remain here i be too fine but that be my pride and what do i care
and so she sit there in her pride and have many such thought as these i can almost fancy that i come from 1 sunbeam i be so fine
it seem as_if the sunbeam be always look for me under the water
ah
i be so fine that even my mother can not find me
have i still my old eye which be break off i believe i should weep but no i would not do that it be not genteel to cry one day 1 couple of street boy be paddle in the gutter for they sometimes find old nail farthing and other treasure
it be dirty work but they take great pleasure in it
hallo
cry one as he prick himself with the darningneedle heres 1 fellow for you i be not 1 fellow i be 1 young lady say the darningneedle but no_one hear her
the sealingwax have come off and she be quite black but black make 1 person look slender so she think herself even fine than before
here come 1 eggshell sailing along say one of the boy so they stick the darningneedle into the eggshell
white wall and i be black myself say the darningneedle that look well now i can be see but i hope i shall not be seasick or i shall break again she be not seasick and she do not break
it be 1 good thing against seasickness to have 1 steel stomach and not to forget one own importance
now my seasickness have past delicate people can bear 1 great deal crack go the eggshell as 1 waggon pass over it
good heaven how it crush
say the darningneedle
i shall be sick now
i be break
but she do not break though the waggon go over her as she lie at full length and there let her lie
the_end
