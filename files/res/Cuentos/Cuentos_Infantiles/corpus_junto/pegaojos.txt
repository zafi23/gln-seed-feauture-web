 En todo el mundo no hay quien sepa tantos cuentos como Pegaojos
Señor los que sabe
 Al anochecer cuando los niños están aún sentados a la mesa o en su escabel viene un duende llamado Pegaojos sube la escalera quedito quedito pues va descalzo sólo en calcetines abre las puertas sin hacer ruido y chitón
 vierte en los ojos de los pequeñuelos leche dulce con cuidado con cuidado pero siempre bastante para que no puedan tener los ojos abiertos y por tanto verlo
Se desliza por detrás les sopla levemente en la nuca y los hace quedar dormidos
Pero no les duele pues Pegaojos es amigo de los niños sólo quiere que se estén quietecitos y para ello lo mejor es aguardar a que estén acostados
Deben estarse quietos y callados para que él pueda contarles sus cuentos
Cuando ya los niños están dormidos Pegaojos se sienta en la cama
Va bien vestido lleva un traje de seda pero es imposible decir de qué color pues tiene destellos verdes rojos y azules según como se vuelva
Y lleva dos paraguas uno debajo de cada brazo
Uno de estos paraguas está bordado con bellas imágenes y lo abre sobre los niños buenos entonces ellos durante toda la noche sueñan los cuentos más deliciosos el otro no tiene estampas y lo despliega sobre los niños traviesos los cuales se duermen como marmotas y por la mañana se despiertan sin haber tenido ningún sueño
Ahora veremos cómo Pegaojos visitó todas las noches de una semana a un muchachito que se llamaba Federico para contarle sus cuentos
Son siete pues siete son los días de la semana
Lunes Atiende dijo Pegaojos cuando ya Federico estuvo acostado verás cómo arreglo todo esto
Y todas las flores de las macetas se convirtieron en altos árboles que extendieron las largas ramas por debajo del techo y por las paredes de modo que toda la habitación parecía una maravillosa glorieta de follaje las ramas estaban cuajadas de flores y cada flor era más bella que una rosa y exhalaba un aroma delicioso y si te daba por comerla sabía más dulce que mermelada
Había frutas que relucían como oro y no faltaban pasteles llenos de pasas
Un espectáculo inolvidable
 Pero al mismo tiempo salían unas lamentaciones terribles del cajón de la mesa que guardaba los libros escolares de Federico
Qué pasa ahí
 inquirió Pegaojos y dirigiéndose a la mesa abrió el cajón
Algo se agitaba en la pizarra rascando y chirriando era una cifra equivocada que se había deslizado en la operación de aritmética y todo andaba revuelto que no parecía sino que la pizarra iba a hacerse pedazos
El pizarrín todo era saltar y brincar atado a la cinta como si fuese un perrillo ansioso de corregir la falta mas no lo lograba
Pero lo peor era el cuaderno de escritura
Qué de lamentos y quejas
 Partían el alma
De arriba abajo en cada página se sucedían las letras mayúsculas cada una con una minúscula al lado servían de modelo y a continuación venían unos garabatos que pretendían parecérseles y eran obra de Federico estaban como caídas sobre las líneas que debían servirles para tenerse en pie
Miren tienen que poner así decía la muestra
Ven
 Así inclinadas con un trazo vigoroso
Ay
 qué más quisiéramos nosotras
 gimoteaban las letras de Federico
Pero no podemos somos tan raquíticas
  Entonces les voy a dar un poco de aceite de hígado de bacalao dijo Pegaojos
Oh no
 exclamaron las letras y se enderezaron que era un primor
Pues ahora no hay cuento dijo el duende
Ejercicio es lo que conviene a esas mocosuelas
Un dos un dos
 Y siguió ejercitando a las letras hasta que estuvieron esbeltas y perfectas como la propia muestra
Mas por la mañana cuando Pegaojos se hubo marchado Federico las miró y vio que seguían tan raquíticas como la víspera
Martes No bien estuvo Federico en la cama Pegaojos con su jeringa encarnada roció los muebles de la habitación y enseguida se pusieron a charlar todos a la vez cada uno hablando de sí mismo
Sólo callaba la escupidera que muda en su rincón se indignaba al ver la vanidad de los otros que no sabían pensar ni hablar más que de sus propias personas sin ninguna consideración a ella que se estaba tan modesta en su esquina dejando que todo el mundo le escupiera
Encima de la cómoda colgaba un gran cuadro en un marco dorado representaba un paisaje y en él se veían viejos y corpulentos árboles y flores entre la hierba y un gran río que fluía por el bosque pasando ante muchos castillos para verterse finalmente en el mar encrespado
Pegaojos tocó el cuadro con su jeringa mágica y los pájaros empezaron a cantar las ramas a moverse y las nubes a desfilar según podía verse por las sombras que proyectaban sobre el paisaje
Entonces Pegaojos levantó a Federico hasta el nivel del marco y lo puso de pie sobre el cuadro entre la alta hierba y el sol le llegaba por entre el ramaje de los árboles
Echó a correr hacia el río y subió a una barquita estaba pintada de blanco y encarnado la vela brillaba como plata y seis cisnes todos con coronas de oro en torno al cuello y una radiante estrella azul en la cabeza arrastraban la embarcación a lo largo de la verde selva los árboles hablaban de bandidos y brujas y las flores de los lindos silfos enanos y de lo que les habían contado las mariposas
Peces magníficos de escamas de oro y plata nadaban junto al bote saltando de vez en cuando fuera del agua con un fuerte chapoteo mientras innúmeras aves rojas y azules grandes y chicas lo seguían volando en largas filas y los mosquitos danzaban y los abejorros no paraban de zumbar Bum bum

Todos querían seguir a Federico y todos tenían una historia que contarle
Vaya excursioncita
 Tan pronto el bosque era espeso y oscuro como se abría en un maravilloso jardín bañado de sol y cuajado de flores
Había vastos palacios de cristal y mármol con princesas en sus terrazas y todas eran niñas a quienes Federico conocía y con las cuales había jugado
Todas le alargaban la mano y le ofrecían pastelillos de mazapán mucho mejores que los que vendía la mujer de los pasteles
Federico agarraba el dulce por un extremo pero la princesa no lo soltaba del otro y así al avanzar la barquita se quedaban cada uno con una parte ella la más pequeña Federico la mayor
Y en cada palacio había príncipes de centinela que sables al hombro repartían pasas y soldaditos de plomo
Bien se veía que eran príncipes de veras
 El barquito navegaba ora por entre el bosque ora a través de espaciosos salones o por el centro de una ciudad y pasó también por la ciudad de su nodriza la que lo había llevado en brazos cuando él era muy pequeñín y lo había querido tanto y he aquí que la buena mujer le hizo señas con la cabeza y le cantó aquella bonita canción que había compuesto y enviado a Federico Cuánto te recuerdo mi niño querido Mi dulce Federico jamás te olvido
 Besé mil veces tu boquita sonriente Tus párpados suaves y tu blanca frente
Oí de tus labios la palabra primera Y hube de separarme de tu vera
Bendígate Dios en toda ocasión Ángel que llevé contra mi corazón
 Y todas las avecillas le hacían coro y las flores bailaban sobre sus peciolos y los viejos árboles inclinaban complacidos las copas como si también a ellos les contase historias Pegaojos
Miércoles Qué manera de llover
 Federico oía la lluvia en sueños y como a Pegaojos le dio por abrir una ventana el pequeño vio cómo el agua llegaba hasta el antepecho formando un lago inmenso
Pero junte a la casa flotaba un barco soberbio
Si quieres embarcar Federico dijo Pegaojos esta noche podrías irte por tierras extrañas y mañana estar de vuelta
Y ahí tenéis a Federico con sus mejores vestidos domingueros embarcado en la magnífica nave
En un tris se despejó el cielo y el barco con las velas desplegadas avanzó por las calles contorneó la iglesia y fue a salir a un mar inmenso
Y siguieron navegando hasta que desapareció toda tierra y vieron una bandada de cigüeñas que se marchaban de su país en busca de otro más cálido
Las aves volaban en fila una tras otra y estaban ya lejos muy lejos
Una de ellas se sentía tan cansada que sus alas casi no podían ya sostenerla era la última de la hilera y volaba muy rezagada
Finalmente la vio perder altura con las alas extendidas y aunque pegó unos aletazos todo fue inútil
Tocó con las patas el aparejo del barco se deslizó vela abajo y bum
 fue a caer sobre la cubierta
La cogió el grumete y la metió en el gallinero con los pollos los gansos y los pavos pero la pobre cigüeña se sentía cohibida entre aquella compañía
Miren a ésta
 exclamaron los pollos
El pavo se hinchó tanto como pudo y le preguntó quién era
Los patos todo era andar a reculones empujándose mutuamente y gritando Cuidado cuidado

La cigüeña se puso a hablarles de la tórrida África de las pirámides y las avestruces que corren por el desierto más veloces que un camello salvaje
Pero los patos no comprendían sus palabras y reanudaron los empujones Estamos todos de acuerdo en que es tonta verdad

Claro que es tonta
 exclamó el pavo y soltó unos graznidos
Entonces la cigüeña se calló y se quedó pensando en su África
Qué patas tan delgadas tiene usted
 dijo la pava
A cuánto la vara
 Cuac cuac cuac
 graznaron todos los gansos pero la cigüeña hizo como si no los oyera
Por qué no te ríes con nosotros
 le dijo la pava
No te parece graciosa mi pregunta
 O es que está por encima de tu inteligencia
 Bah
 Qué espíritu tan obtuso
 Mejor será dejarla
 Y soltó otro graznido mientras los patos coreaban Cuac cuac
 cuac cuac

Dios mío y cómo se divertían
 Pero Federico fue al gallinero abrió la puerta y llamó a la cigüeña que muy contenta lo siguió a la cubierta dando saltos
Estaba ya descansada y con sus inclinaciones de cabeza parecía dar las gracias a Federico
Desplegó luego las alas y emprendió nuevamente el vuelo hacia las tierras cálidas mientras las gallinas cloqueaban los patos graznaban y al pavo se le ponía toda la cabeza encendida
Mañana haremos una buena sopa contigo
 le dijo Federico y en esto se despertó y se encontró en su camita
Qué extraño viaje le había procurado aquella noche Pegaojos
Jueves Sabes qué
 dijo el duende
Voy a hacer salir un ratoncillo pero no tengas miedo
Y le tendió la mano mostrándole el lindo animalito
Ha venido a invitarte a una boda
Esta noche se casan dos ratoncillos
Viven abajo en la despensa de tu madre es una vivienda muy hermosa
 Pero cómo voy a pasar por la ratonera
 preguntó Federico
Déjalo por mi cuenta replicó Pegaojos verás cuán pequeño te vuelvo
Y lo tocó con su jeringuita mágica y enseguida Federico se fue reduciendo reduciendo hasta no ser más largo que un dedo
Ahora puedes pedirle su uniforme al soldado de plomo creo que te sentará bien y en sociedad lo mejor es presentarse de uniforme
Desde luego respondió Federico y en un momento estuvo vestido de soldado de plomo
Hace el favor de sentarse en el dedal de su madre
 preguntó el ratoncito
Será para mí un honor llevarlo
Si la señorita es tan amable dijo Federico y salieron para la boda
Primero llegaron a un largo corredor del sótano junto lo bastante alto para que pudiesen pasar con el dedal y en toda su longitud estaba alumbrado con la fosforescencia de madera podrida
Verdad que huele bien
 dijo el ratón que lo llevaba
Han untado todo el pasillo con corteza de tocino
Ay que cosa tan rica
 Así llegaron al salón de la fiesta
A la derecha se hallaban reunidas todas las ratitas cuchicheando y hablándose al oído qué no parecía sino que estuviesen a partir un piñón y a la izquierda quedaban los caballeros alisándose los bigotes con la patita
Y en el centro de la sala aparecía la pareja de novios de pie sobre la corteza de un queso vaciado besándose sin remilgos delante de toda la concurrencia pues estaban prometidos y dentro unos momentos quedarían unidos en matrimonio
Seguían llegando forasteros y más forasteros todo eran apreturas y pisotones los novios se habían plantado ante la misma puerta de modo que no dejaban entrar ni salir
Toda la habitación estaba untada de tocino como el pasillo y en este olor consistía el banquete para postre presentaron un guisante en el que un ratón de la familia había marcado con los dientes el nombre de los novios quiero decir las iniciales
Jamás se vio cosa igual
Todos los ratones afirmaron que había sido una boda hermosísima y el banquete magnífico
Federico regresó entonces a su casa estaba muy contento de haber conocido una sociedad tan distinguida lástima que hubiera tenido que reducirse tanto de tamaño y vestirse de soldadito de plomo
 