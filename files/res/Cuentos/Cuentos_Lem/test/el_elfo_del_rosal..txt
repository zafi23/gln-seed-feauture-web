en el centro de uno jardín crecer uno rosal cuajar de rosa y en uno de ellos el más hermoso de todo habitar uno elfo tan pequeñín que ninguno ojo humano poder distinguir lo
detrás_de cada pétalo de el rosa tener uno dormitorio
ser tan bien educar y tan guapo como poder ser lo uno niño y tener ala que le llegar desde el hombro hasta el pie
oh y qué aroma exhalar su habitación y qué claro y hermoso ser el pared
no ser otro cosa sino el pétalo de el flor de color rosa pálido
se pasar el día gozar de el luz de el sol volar de flor en flor bailar sobre el ala de el inquieto mariposa y medir el paso que necesitar dar para recorrer todo el camino y sendero que haber en uno solo hoja de tilo
ser lo que nosotros llamar el nervadura para él ser camino y senda y no poco largo
antes_de haber lo recorrer todo se haber poner el sol claro que haber empezar algo tarde
se enfriar el ambiente caer el rocío mientras soplar el viento el mejor ser retirar se a casa
el elfo echar a correr cuando poder pero el rosa se haber cerrar y no poder entrar y ninguno otro quedar abrir
el pobre elfo se asustar no poco
nunca haber salir de noche siempre haber permanecer en casita dormitar tras el tibio pétalo
ay su imprudencia le ir a costar el vida
saber que en el extremo oponer de el jardín haber uno glorieta recubrir de bello madreselva cuyo flor parecer trompetilla pintar decidir refugiar se en uno de ellos y aguardar el mañana
se trasladar volar a el glorieta
cuidado
dentro haber 2 persona uno hombre joven y guapo y uno hermoso muchacho sentar uno junto_a el otro desear no tener que separar se en todo el eternidad se querer con todo el alma mucho más de el que el mejor de el hijo poder querer a su madre y a su padre
y no_obstante tener que separar nos decir el joven ­
tu hermano nos odiar por ese me enviar con uno misión más_allá_de el montaña y el mar
adiós mi dulce prometido pues lo ser a_pesar_de_todo
se besar y el muchacho llorar le dar uno rosa después_de haber estampar en él uno beso tan intenso y sentido que el flor se abrir
el elfo aprovechar el ocasión para introducir se en él reclinar el cabeza en el suave pétalo fragante desde allí poder oír perfectamente el adiós de el pareja
y se dar_cuenta de que el rosa ser prender en el pecho de el doncel
ah cómo palpitar el corazón debajo
ser tan violento su latido que el elfo no poder pegar el ojo
pero el rosa no permanecer mucho tiempo prender en el pecho
el hombre lo tomar en su mano y mientras caminar solitario por el bosque oscuro lo besar con tanto frecuencia y fuerza que por poco ahogar a nuestro elfo
este poder percibir a_través_de el hoja el ardor de el labio de el joven y el rosa por su parte se haber abrir como al_calor_de el sol más cálido de [??:??/??/??:1200:pm]
se acercar entonces otro hombre sombrío y colérico ser el perverso hermano de el doncella
sacar uno afilar cuchillo de grande dimensión lo clavar en el pecho de el enamorar mientras este besar el rosa
luego le cortar el cabeza y lo enterrar junto_con el cuerpo en el tierra blando de el pie de el tilo
helo aquí olvidar y ausente pensar aquel malvado no volver jamás
deber emprender uno largo viaje a_través_de monte y océano
ser fácil perder el vida en este expedición y haber morir
no volver y mi hermano no se atrever a preguntar me por él
luego con el pie acumular hoja seco sobre el tierra mullir y se marchar a su casa a_través_de el noche oscuro
pero no ir solo como creer lo acompañar el minúsculo elfo montar en uno enrollar hoja seco de tilo que se haber adherir a el pelo de el criminal mientras enterrar a su víctima
llevar el sombrero poner y el elfo estar sumir en profundo tiniebla temblar de horror y de indignación por aquel abominable crimen
el malvado llegar a casa a el amanecer
se quitar el sombrero y entrar en el dormitorio de su hermano
el hermoso y lozano doncella yacer en su lecho soñar con aquel que tanto lo amar y que según él creer se encontrar en aquel momento caminar por bosque y montaña
el perverso hermano se inclinar sobre él con uno risa diabólico como sólo el demonio saber reír se
entonces el hoja seco se le caer de el pelo quedar sobre el cubrecama sin que él se dar_cuenta
luego salir de el habitación para acostar se uno hora
el elfo saltar de el hoja y entrar se en el oído de el dormir muchacho le contar como en sueño el horrible asesinato describir le el lugar donde el hermano lo haber perpetrar y aquel en que yacer el cadáver
le hablar también de el tilo florido que crecer allí y decir para que no pensar que el que acabar de contar te ser sólo uno sueño encontrar sobre tu cama uno hoja seco
y efectivamente a el despertar él el hoja estar allí
oh qué amargo lágrima verter
y sin tener a nadie a quien poder confiar su dolor
el ventana permanecer abrir todo el día a el elfo le haber ser fácil ir se a el rosa y a todo el flor de el jardín pero no tener valor para abandonar a el afligir joven
en el ventana haber uno rosal de bengala se instalar en uno de su flor y se estar contemplar a el pobre doncella
su hermano se presentar repetidamente en el habitación alegre a_pesar_de su crimen pero él no osar decir le uno palabra de su cuita
no bien haber oscurecer el joven salir disimuladamente de el casa se dirigir a el bosque a el lugar donde crecer el tilo y apartar el hoja y el tierra no tardar en encontrar el cuerpo de el asesinar
ah cómo llorar y cómo rogar a dios_nuestro_señor que le conceder el gracia de uno pronto muerte
haber querer llevar se el cadáver a casa pero a el ser le imposible coger el cabeza lívido con el cerrar ojo y besar el frío boca sacudir el tierra adherir a el hermoso cabello
lo guardar
decir y después_de haber cubrir el cuerpo con tierra y hoja volver a su casa con el cabeza y uno rama de jazmín que florecer en el sitio de el sepultura
llegar a su habitación coger el maceta más grande que poder encontrar depositar en él el cabeza de el muerto lo cubrir de tierra y plantar en él el rama de jazmín
adiós adiós
susurrar el geniecillo que no poder soportar por más tiempo aquel gran dolor volar a su rosa de el jardín
pero estar marchito sólo uno poco hoja amarillo colgar aun de el cáliz verde
ah qué pronto pasar el bello y el bueno
suspirar el elfo
por fin encontrar otro rosa y establecer en él su morada detrás_de su delicado y fragante pétalo
cada mañana se llegar volar a el ventana de el desdichado muchacho y siempre encontrar a este llorar junto_a su maceta
su amargo lágrima caer sobre el rama de jazmín el cual crecer y se poner verde y lozano mientras el palidez ir invadir el mejilla de el doncella
brotar nuevo ramilla y florecer blanco capullo que él besar
el perverso hermano no cesar de reñir le preguntar le si se haber volver loco
no poder soportar lo ni comprender por qué llorar continuamente sobre aquel maceta
ignorar qué ojo cerrar y qué rojo labio se estar convertir allí en tierra
el muchacho reclinar el cabeza sobre el maceta y el elfo de el rosa soler encontrar lo allí dormir entonces se deslizar en su oído y le contar de aquel anochecer en el glorieta de el aroma de el flor y de el amor de el elfo él soñar dulcemente
TM_d:1 mientras se hallar sumir en uno de este sueño se apagar su vida y el muerte lo acoger misericordioso
se encontrar en el cielo junto_a el ser amar
y el jazmín abrir su blanco flor y esparcir su maravilloso aroma característico ser su modo de llorar a el morir
el mal hermano se apropiar el hermoso planta florido y lo poner en su habitación junto_a el cama pues ser precioso y su perfume uno verdadero delicia
lo seguir el pequeño elfo de el rosa volar de flor en flor en cada uno de el cual habitar uno alma y le hablar de el joven inmolar cuyo cabeza ser ahora tierra entre el tierra y le hablar también de el malvado hermano y de el desdichado hermano
lo saber decir cada alma de el flor lo saber
no brotar acaso de el ojo y de el labio de el asesinar
lo saber lo saber
y hacer con el cabeza uno gesto significativo
el elfo no lograr comprender cómo poder estar se tan quieto y se ir volar en_busca_de el abeja que recoger miel y le contar el historia de el malvado hermano y el abeja lo decir a su reina el cual dar orden de que a el mañana siguiente dar muerte a el asesino
pero el noche anterior el 1 que seguir a el fallecimiento de el hermano a el quedar se dormir el malvado en su cama junto_a el oloroso jazmín se abrir todo el cáliz invisible pero armar de ponzoñoso dardo salir todo el alma de el flor y penetrar primero en su oído le contar sueño de pesadilla luego volar a su labio le herir en el lengua con su venenoso flecha
ya haber vengar a el muerto
decir y se retirar de nuevo a el flor blanco de el jazmín
a el amanecer y abrir se súbitamente el ventana de el dormitorio entrar el elfo de el rosa con el reina de el abeja y todo el enjambre que veníam a ejecutar su venganza
pero ya estar morir varios persona que rodear el cama decir el perfume de el jazmín lo haber matar
el elfo comprender el venganza de el flor y lo explicar a el reina de el abeja y él con todo el enjambre revolotear zumbar en_torno_a el maceta
no haber modo de ahuyentar a el insecto y entonces uno hombre se llevar el tiesto afuera mas a el picar le en el mano uno de el abeja soltar él el maceta que se romper a el tocar el suelo
entonces descubrir el lívido cráneo y saber que el muerto que yacer en el lecho ser uno homicida
el reina de el abeja seguir zumbar en el aire y cantar el venganza de el flor y cantar a el elfo de el rosa y pregonar que detrás_de el hoja más mínimo haber alguien que poder descubrir el maldad y vengar lo
