ser tan cariñoso listo y bueno el bisabuelo
nosotros sólo ver por su ojo
en realidad por el que poder recordar lo llamar abuelo pero cuando entrar a formar_parte de el familia el hijo de mi hermano federico él ascender a el categoría de bisabuelo más alto no poder llegar
nos querer mucho a todo aunque no parecer estar muy de_acuerdo_con nuestro época
el viejo tiempo ser el bueno
decir sensato y sólido
hoy todo ir a el galope todo estar revolver
el juventud llevar el voz cantante y hasta hablar de el rey como si ir su igual
el 1 que llegar poder mojar su trapo en agua sucio y escurrir lo sobre el cabeza de uno hombre honorable
cuando soltar uno de este discurso el bisabuelo se poner rojo como uno pavo pero al_cabo_de uno momento reaparecer su afable sonrisa y entonces decir bueno tal_vez me equivocar
ser de el tiempo antiguo y no conseguir acomodar me a el nuevo
dios querer encauzar lo y guiar lo
cuando el bisabuelo hablar de el tiempo pasar yo creer encontrar me en ellos
con el pensamiento me ver en uno dorar carroza con lacayo ver el corporación gremial con su escudo desfilar a el son de el banda y bajo el bandera y me encontrar en el alegre salón navideño disfrazar y jugar a prenda
cierto que en aquel época ocurrir también mucho cosa repugnante y horrible como el suplicio de el rueda y el derramamiento de sangre pero todo aquel horror tener algo de atrayente de estimulante
y también oír mucho cosa bueno sobre el noble danés que emancipar a el campesino y el príncipe heredero de dinamarca que abolir el trata de esclavo
ser magnífico oír a el bisabuelo hablar de todo aquel y de su año juvenil aunque el período mejor el más sobresaliente y grandioso haber ser el anterior
bárbaro ser
exclamar mi hermano federico
dios ser loar
pero ya pasar
y se lo decir a el bisabuelo
no estar bien y sin embargo yo sentir gran respeto por federico mi hermano mayor que haber poder ser mi padre según decir él
y decir también mucho cosa divertir
de estudiante llevar siempre el mejor nota y en el despacho de mi padre se aplicar tanto que muy pronto poder entrar en el negocio
ser el que tener más trato con el bisabuelo pero siempre discutir
no se comprender ni llegar nunca a comprender se afirmar todo el familia pero yo con ser tan pequeño no tardar en dar_cuenta de que el uno no poder prescindir de el otro
el bisabuelo escuchar con ojo brillante cuando federico hablar o leer en voz alto acerca_de el progreso de el ciencia de el descubrimiento de el fuerza natural de todo el notable que ocurrir en nuestro época
el hombre se volver más listo pero no mejor decir el bisabuelo
inventar arma terrible para destruir se mutuamente
así el guerra ser más corto replicar federico_no haber que aguardar 7 año para que venir el bendecir paz
el mundo estar pletórico y a vez le convenir uno sangría
TM_d:1 federico le contar uno suceso ocurrir en uno pequeño ciudad
el reloj de el alcalde ser decir el gran reloj de el ayuntamiento señalar el hora a el población y aunque no marchar muy bien el gente se regir por él
llegar a el país el ferrocarril el cual enlazar con el de el demás país por ese ser preciso conocer el hora exacto de_lo_contrario se ir rezagar
poner en el estación uno reloj que marchar de_acuerdo_con el sol y como el de el alcalde no lo hacer todo el ciudadano empezar a regir se por el reloj de el estación
yo me reír parecer me que el historia ser muy divertir pero el bisabuelo no se río ni pizca sino_que se quedar muy serio
tener mucho miga el que acabar de contar
decir y comprender cuál ser tu idea a el contar me lo
haber mucho ciencia en el mecanismo de tu reloj y me hacer pensar en otro en el sencillo reloj de bornholm de mi padre tan viejo con su pesa de plomo
marcar su tiempo y el de mi infancia
cierto que no marchar con tanto precisión pero marchar lo ver por el aguja creer el que decir y no nos parar a pensar en el rueda que tener dentro
así ser también entonces el máquina de el estado uno lo mirar despreocupadamente y tener fe en el aguja
pero hoy el máquina estatal se haber convertir en uno reloj de cristal cuyo mecanismo ser visible se ver girar el rueda se oír su chirrido y uno se asustar de el eje y de el volante
yo saber cómo dar el campanada y ya no tener el fe infantil
este ser el frágil de el época actual
y entonces el bisabuelo se salir de su casilla
no poder poner se de_acuerdo_con federico pero tampoco poder separar se de igual manera que el época viejo y el nuevo
bien se dar_cuenta ellos 2 y el familia entero cuando federico haber de emprender uno largo viaje a américa
aunque el viaje ser cosa corriente en el familia aquel separación resultar bien difícil para el bisabuelo
ser tan largo aquel viaje
todo el océano de_por_medio hasta llegar a el otro continente
recibir carta miar cada TM_d:15 le decir federico
y más de_prisa que el carta te llegar el telegrama
el día se volver hora y el hora minuto
llegar uno saludo por el hilo telegráfico el día en que federico embarcar en inglaterra
más rápido que uno carta ni_que haber actuar de correo el raudo nube llegar uno saludo de américa a el desembarcar en él federico
ser uno poco hora después_de haber poner pie en tierra firme
realmente ser uno idea de dios regalar a nuestro tiempo decir el bisabuelo uno bendición para el humanidad
y según me decir federico este fuerza natural se descubrir en nuestro país observar
sí afirmar el bisabuelo dar me uno beso
sí y yo haber ver el dulce ojo infantil que por 1 vez descubrir y comprender este fuerza de el naturaleza ser uno ojo infantil como el tuyo
y haber estrechar su mano

y volver a besar me
haber transcurrir más de uno mes cuando llegar uno carta de federico con el noticia de que estar prometer con uno muchacho joven y bonito y expresar el confianza de que todo el familia se alegrar
enviar su fotografía que ser examinar a simple vista y con uno lupa pues aquel ser el bueno de el retrato que permitir ser examinar con el lente más nítido y entonces aun se notar más el parecer
este no lo haber poder hacer ninguno pintor ni el más famoso de el tiempo pretérito
ah si entonces haber conocer este invento
decir el abuelo
haber poder ver cara_a_cara a el bienhechor y a el grande hombre de el mundo
qué simpático y bueno parecer este muchacho
decir mirar lo con el lupa
lo conocer en cuanto entre en el habitación
poco faltar para que este no ocurrir nunca afortunadamente nos enterar de el peligro cuando ya haber pasar
el recién casar llegar a inglaterra contento y en perfecto salud y embarcar en uno vapor con destino a copenhague
ya a el vista de el costa danés el blanco duna de jutlandia occidental se levantar uno tormenta y el barco encallar en uno arrecife el embravecer mar amenazar con destrozar lo sin que servir el bote de salvamento
cerrar el noche pero en_medio_de el oscuridad volar uno brillante cohete desde el costa a el buque embarrancar el cohete arrojar uno cable quedar establecer el comunicación entre el náufrago y el costa y pronto uno lindo joven ser transportar en el canasta de salvamento por sobre el ola encrespar y furioso y se sentir infinitamente dichoso cuando poco después tener a su lado en tierra firme a su joven esposo
todo el de a_bordo se salvar antes_de el amanecer
nosotros dormir tranquilamente en copenhague sin pensar en desgracia ni peligro
a el sentar nos a el mesa para el desayuno llegar por telégrafo el noticia de el naufragio de uno barco inglés en el costa occidental de el península
el angustia que experimentar ser terrible pero a el poco momento se recibir otro telegrama de el querido viajero federico y su esposo anunciar su próximo llegada
todo llorar y yo también y el bisabuelo quien doblar el mano estar seguro de ello bendecir el nuevo época
aquel día el bisabuelo destinar $_PTE:200 para el monumento a hans_christian_örsted
a el llegar federico con su joven esposo y enterar se de aquel gesto decir muy bien bisabuelo
ahora te leer lo que örsted escribir hacer ya mucho año sobre el tiempo viejo y el moderno
probablemente ser de tu opinión preguntar el bisabuelo
poder estar seguro responder federico y tú también lo ser poner que haber contribuir a su monumento
