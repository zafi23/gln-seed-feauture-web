en uno tortuoso callejuela entre varios mísero casucha se alzar uno de pared entramar alto y desvencijar
vivir en él gente muy pobre y el más mísero de todo ser el buhardilla en cuyo ventanuco colgar a el luz de el sol uno viejo jaula abollar que ni siquiera tener bebedero en su lugar haber uno gollete de botella poner del_revés tapar por debajo con uno tapón de corcho y lleno de agua
uno viejo solterón estar asomar a el exterior acabar de adornar con prímula el jaula donde uno diminuto pardillo saltar de uno a otro palo cantar tan alegremente que su voz resonar a gran distancia
ay bien poder tú cantar
exclamar el gollete
bueno no ser que lo decir como lo decir nosotros pues uno casco de botella no poder hablar pero lo pensar a su manera como nosotros cuando hablar para nuestro adentros
sí tú poder cantar pues no te faltar ninguno miembro
si tú saber como yo lo saber el que significar haber perder todo el parte inferior de el cuerpo sin quedar me más que cuello y boca y aun este con uno tapón meter dentro
seguro que no cantar
pero valer más así que siquiera tú poder alegrar te
yo no tener ninguno motivo para cantar aparte que no saber hacer lo antes sí saber cuando ser uno botella hacer y derecho y me frotar con uno tapón
ser entonces uno verdadero alondra me llamar el gran alondra
y luego cuando vivir en el bosque con el familia de el pellejero y celebrar el boda de su hijo
me acordar como si ser ayer
el de aventura que haber pasar y que poder contar te
haber estar en el fuego y en el agua meter en el negro tierra y haber subir a altura que muy poco haber alcanzar y ahí me tener ahora en este jaula exponer a el aire y a el sol
a el mejor te gustar oír mi historia aunque no lo ir a contar en voz alto pues no poder
y así el gollete de botella hablar para sí o por_lo_menos pensar lo para su adentros empezar a contar su historia que ser notable de verdad
entretanto el pajarillo cantar su alegre canción y abajo en el calle todo el mundo ir y venir pensar cada cual en su problema o en nada
pero el gollete de el botella recordar que recordar
ver el horno ardiente de el fábrica donde soplar le haber dar vida recordar que hacer uno calor sofocante en aquel horno estrepitoso lugar de su nacimiento que mirar a su hondura le haber entrar gana de saltar de nuevo a ellos pero que poco_a_poco a el ir se enfriar se ir sentir bien y a gusto en su nuevo sitio en hilera con uno regimiento entero de hermano y hermano nacer todo en el mismo horno aunque uno destinar a contener champaña y otro cerveza el cual no ser poco diferencia
más tarde ya en el ancho mundo caber muy bien que en uno botella de cerveza se envasar el exquisito lacrimae christi y que en uno botella de champaña echar betún de calzado pero siempre quedar el forma como ejecutoria de el nacimiento
el noble ser siempre noble aunque por dentro estar lleno de betún
después_de uno rato todo el botella ser embalar el nuestro con el demás
no pensar entonces él que acabar en simple gollete y que servir de bebedero de pájaro en aquel altura el cual no dejar de ser uno existencia honroso pues siquiera se ser algo
no volver a ver el luz de el día hasta que lo desembalar en el bodega de uno cosechero junto_con su compañero y lo enjuagar por 1 vez cosa que le producir uno sensación extraño
se quedar allí vacío y sin tapar presa de uno curioso desfallecimiento
algo le faltar no saber qué a_punto_fijo pero algo
hasta que lo llenar de venir uno vino viejo y de solera lo tapar y lacrar pegar le a continuación uno papel en que se leer primera calidad
ser como sacar sobresaliente en el examen pero ser que en realidad el vino ser bueno y el botella bueno también
cuando se ser joven todo el mundo se sentir poeta
el botella se sentir lleno de canción y verso referente a cosa de el que no tener el menor idea el verde montaña solear donde madurar el uva y donde el retozón muchacho y el bullicioso mozo cantar y se besar
ah qué bello ser el vida
todo aquel cantar y resonar en_el_interior_de el botella el mismo que ocurrir en el de el joven poeta que con frecuencia tampoco saber nada de todo aquel
uno bueno día lo vender
el aprendiz de el peletero ser enviar a comprar uno botella de vino de el mejor y así ser él a parar a el cesto junto_con jamón salchicha y queso sin que faltar tampoco uno mantequilla de magnífico aspecto y uno pan exquisito
el propio hijo de el peletero vaciar el cesto
ser joven y lindo reír su ojo azul y uno sonrisa se dibujar en su boca que hablar tan elocuentemente como su ojo
su mano ser fino y delicado y muy blanco aunque no tanto como el cuello y el pecho
se ver a el legua que ser uno de el mozo más bello de el ciudad y sin embargo no estar prometer
cuando el familia salir a el bosque el cesta de el comida quedar en el regazo de el hijo el cuello de el botella asomar por entre el extremo de el blanco pañuelo cubrir el tapón uno sello de lacre rojo que mirar a el rostro de el muchacho
pero no dejar de echar tampoco ojear a el joven marino sentar a su lado
ser uno amigo de infancia hijo de uno pintor retratista
acabar de pasar felizmente su examen de piloto y a el día siguiente se embarcar en uno nave con rumbo a lejano país
de ello haber estar hablar largamente mientras empaquetar y en el curso de el conversación no se haber reflejar mucho alegría en el ojo y en el boca de el lindo hijo de el peletero
el 2 joven se meter por el verde bosque enzarzar en uno coloquio
de qué hablar
el botella no lo oír pues se haber quedar en el cesta
pasar mucho rato antes_de que lo sacar pero cuando a el fin lo hacer haber suceder cosa muy agradable todo el ojo estar sonriente incluso el de el hijo el cual apenas abrir el boca y tener el mejilla encender como rosa encarnar
el padre coger el botella lleno y el sacacorchos
ser extraño sí el impresión que se sentir cuando a uno el descorchar por vez 1
jamás olvidar el cuello de el botella aquel momento solemne a el saltar el tapón le haber escapar de dentro uno raro sonido plump
seguir de uno gorgoteo a el caer el vino en el vaso
por el felicidad de el prometido
decir el padre y todo el vaso se vaciar hasta el último gota mientras el joven piloto besar a su hermoso novio
decir y bendición
exclamar el 2 viejo
el mozo volver a llenar el vaso
por mi regreso y por el boda de hoy en uno año
brindar y cuando el vaso volver a quedar vacío levantar el botella añadir has asistir a el día más hermoso de mi vida nunca más volver a servir
y lo arrojar a el aire
poco pensar entonces el muchacho que aun ver volar otro vez el botella y sin embargo así ser
el botella ser a caer en el espeso cañaveral de uno pequeño estanque que haber en el bosque el gollete recordar aun perfectamente cómo haber ir a parar allí y cómo haber pensar les dar vino y ellos me devolver agua cenagoso su intención ser bueno de_todos_modos
no poder ya ver a el pareja de novio ni a su regocijar padre pero durante largo rato lo estar oír cantar y charlar alegremente
llegar en este 2 chiquillo campesino que mirar por entre el caña descubrir el botella y se lo llevar a casa
volver a estar atender
en el casa de el bosque donde morar el muchacho el víspera haber llegar su hermano mayor que ser marino para despedir se pues ir a emprender uno largo viaje
correr el madre de_un_lado_para_otro empaquetar cosa y más cosa a el anochecer el padre ir a el ciudad a ver a su hijo por último vez antes_de su partida y a llevar le el último saludo de el madre
haber poner ya en el hato uno botella de aguardiente de hierba aromático cuando se presentar el muchacho con el botella encontrar que ser mayor y más resistente
su capacidad ser superior a el de el botella y el licor ser muy bueno para el dolor de estómago pues entre otro mucho hierba contener corazoncillo
este vez no llenar el botella con vino como el anterior sino con uno poción amargo aunque excelente para el estómago
el nuevo botella reemplazar a el antiguo y así reanudar aquel su correría
pasar a_bordo de el barco propiedad de peter_jensen justamente el mismo en el que servir el joven piloto el cual no ver el botella aparte que el más probable ser que no lo haber reconocer ni pensar que ser el mismo con cuyo contenido haber brindar por su noviazgo y su feliz regreso
aunque no ser vino el que lo llenar no ser menos bueno su contenido
a peter_jensen lo llamar su compañero el boticario pues a_cada_momento sacar el botella y administrar a alguien el excelente medicina excelente para el estómago entender nos y aquel durar hasta que se haber consumir el último gota
ser día feliz y el botella soler cantar cuando lo frotar con el tapón
de entonces le venir el nombre de alondra el alondra de peter_jensen
haber transcurrir uno largo tiempo y el botella haber ser dejar vaciar en uno rincón mas haber aquí que si el cosa ocurrir durante el viaje de ida o el de vuelta el botella no lo saber nunca a_punto_fijo pues jamás desembarcar se levantar uno tempestad
ola enorme negro y denso se encabritar levantar el barco hasta el nube y lo lanzar en todo dirección se quebrar el palo mayor uno golpe de mar abrir uno vía de agua y el bomba resultar inútil
ser uno noche oscuro como boca de lobo y el barco se ir a picar en el último momento el joven piloto escribir en uno hoja de papel en el nombre de dios naufragar
estampar el nombre de su prometer el suyo propio y el de el buque meter el papel en uno botella vacío que encontrar a mano y tapar lo fuertemente el arrojar a el mar tempestuoso
ignorar que ser el mismo que haber servir para llenar el vaso de el alegría y de el esperanza
ahora flotar entre el ola llevar uno mensaje de adiós y de muerte
se hundir el barco y con él el tripulación mientras el botella volar como uno pájaro llevar dentro uno corazón uno carta de amor
y salir el sol y se poner de nuevo y a el botella le parecer como si volver a el tiempo de su infancia en que ver el rojo horno ardiente
vivir período de calma y nuevo tempestad pero ni se estrellar contra uno roca ni ser tragar por uno tiburón
más de uno año estar flotar a el azar orar hacia el norte orar hacia [??:??/??/??:1200:pm] a merced de el corriente marino
por el demás ser dueño de sí pero al_cabo_de uno tiempo uno llegar a cansar se incluso de este
el hoja escribir con el último adiós de el novio a su prometido sólo duelo haber traer suponer que haber ir a parar a el mano a que ir destinar
pero dónde estar aquel mano tan blanco cuando allá en el verde bosque se extender sobre el jugoso hierba el día de el noviazgo
dónde estar el hijo de el peletero
dónde se hallar su tierra y cuál ser el más próximo
el botella lo ignorar seguir en su eterno vaivén y a el fin se sentir ya harto de aquel vida su destino ser otro
con todo continuar su viaje hasta que finalmente ser arrojar a el costa en uno país extraño
no comprender uno palabra de el que el gente hablar no ser el lengua que oír en otro tiempo y uno se sentir muy desvalido cuando no entender el idioma
alguien recoger el botella y lo examinar
ver que contener uno papel y lo sacar pero por mucho vuelta que le dar nadie saber interpretar el línea escribir
estar claro que el botella haber ser arrojar a el mar deliberadamente y que en el hoja se explicar el motivo de ello pero nadie saber leer lo por el que volver a introducir el pliego en el frasco el cual ser colocar en uno gran armario de uno espacioso habitación de uno casa grandioso
cada vez que llegar uno forastero sacar el hoja lo desdoblar y manosear con el que el escribir trazado a_lápiz ir borrar se progresivamente y volver se ilegible a el fin nadie poder reconocer que aquel ser letra
el botella permanecer todavía otro año en el armario luego lo llevar a el desván donde se cubrir de telaraña y de polvo
allí recordar él el día feliz en que en el bosque contener vino tinto y aquel otro en que vagar mecer por el ola portador de uno misterio uno carta uno suspiro de despedida
en el desván pasar 20 año y quién saber hasta cuándo haber seguir en él de no haber ser porque reconstruir el casa
a el quitar el techo salir el botella algo decir de él el presente pero cualquiera lo entender
no se aprender nada vivir en el desván aunque se estar en él 20 año
si me haber dejar en el habitación de abajo pensar de seguro que haber aprender el lengua la levantar y enjuagar y bien que lo necesitar
se sentir entonces diáfano y transparente joven de nuevo como en día pretérito pero el hoja escribir que estar encerrar en su interior se estropear completamente con él lavar
llenar el frasco de semilla no saber él de qué clase
lo tapar y envolver con el que no ver ni uno resquicio de luz y no hablar ya de sol y luna cuando se ir de viaje haber que poder ver algo pensar el botella
pero no poder ver nada aunque de_todos_modos hacer el principal viajar y llegar a destino
allí lo desenvolver
menudo trabajo se haber tomar con él en el extranjero exclamar alguien
y a_pesar_de_todo seguramente se haber rajar
pero no no se haber rajar
el botella comprender todo el palabra que se decir pues lo hacer en el lengua que oír en el horno vidriero en casa de el bodeguero en el verde bosque y luego en el barco el único viejo y bueno lengua que él poder comprender
haber llegar a su tierra natal que saludar alborozar
de puro gozo por poco saltar de el mano que lo sostener apenas se dar_cuenta de que lo descorchar y vaciar
lo llevar después a el bodega para que no estorbar y allí se quedar olvidar de el todo
en casa ser donde se estar mejor aunque ser en el bodega
jamás se le ocurrir
pensar cuánto tiempo pasar en él llevar ya allí varios año bien apoltronar cuando uno bueno día bajar uno individuo y se llevar todo el botella
el jardín ofrecer uno aspecto brillante lámpara encender colgar en guirnalda y farol de papel relucir a_modo_de grande tulipán transparente
el noche ser magnífico y el atmósfera quieto y diáfano brillar el estrella en uno cielo de luna nuevo este se ver como uno bola de color grisazulado ribetear de oro
para quien tener bueno vista resultar hermoso
el sendero lateral estar también algo iluminar lo suficiente para no andar por ellos a ciego
entre el seto haber colocar botella cada uno con uno luz y de su número formar_parte nuestro antiguo conocido destinar a terminar TM_d:1 en simple gollete bebedero de pájaro
en aquel momento le parecer todo infinitamente hermoso pues volver a estar en_medio_de el verdor tomar_parte en el fiesta y el regocijo oír el canto y el música el rumor y el zumbido de mucho voz humano especialmente el que llegar de el parte de el jardín adornar con linterna de papel de color
cierto que él estar en uno de el camino lateral pero justamente aquel dar oportunidad para entregar se a el recuerdo
el botella poner de pie y sostener el luz prestar uno utilidad y uno placer y así ser como deber ser
en hora semejante se olvidar uno hasta de el 20 año de reclusión en el desván
muy cerca de él pasar uno pareja solitario cogida de el brazo como aquel novio de el bosque el piloto y el hijo de el peletero
el botella tener el impresión de que revivir el escena
por el jardín pasear el invitado y también gente de el pueblo deseoso de admirar aquel magnificencia
entre este pasear uno viejo solterón que haber ver morir a todo su familiar aunque no le faltar amigo
por su cabeza pasar el mismo pensamiento que por el mente de el botella pensar en el verde bosque y en uno joven pareja de enamorado de todo haber gozar puesto que el novio ser él mismo
haber ser el hora más feliz de su vida hora que no se olvidar ya nunca ni cuando se llegar a ser uno viejo solterón
pero ni él reconocer el botella ni este a el exprometida y así ser como andar todo por el mundo pasar uno a el lado de otro hasta que volver a encontrar nos ese le ocurrir a ellos que venir a encontrar se en el mismo ciudad
el botella salir de el jardín para volver a el tienda de el cosechero donde otro vez lo llenar de vino para el aeronauta que el [G:??/??/??:????:??] deber elevar se en globo
uno enorme hormiguero de persona se apretujar para asistir a el espectáculo
resonar el música de el banda militar y se efectuar múltiple preparativo el botella lo ver todo desde uno cesta donde se hallar junto_con uno conejo vivo aunque medio morir de miedo porque saber que se lo llevar a el altura con el exclusivo objeto de soltar lo en paracaídas
el botella no saber de subida ni de bajada ver cómo el globo ir hinchar se gradualmente y cuando ya alcanzar el máximo de volumen comenzar a levantar se y a dar muestra de inquietud
de pronto cortar el amarra que lo sujetar y el aeróstato se elevar en el aire con el aeronauta el cesto el botella y el conejo
el música romper a tocar y todo el espectador gritar hurra
ser gracioso este de volar por el aire
pensar el botella ser otro forma de navegar
no haber peligro de choque aquí arriba
mucho millar de persona seguir el aeronave con el mirada entre ellos el viejo solterón desde el abrir ventana de su buhardilla de cuyo pared colgar el jaula con el pardillo que no tener aun bebedero y deber contentar se con uno diminuto escudilla de madera
en el mismo ventana haber uno tiesto con uno arrayán que haber apartar algo para que no caer a el calle cuando el mujer se asomar
este distinguir perfectamente a el aeronauta en su globo y poder ver cómo soltar el conejo con el paracaídas y luego arrojar el botella proyectar lo hacia lo alto
el viejo solterón poco sospechar que lo haber ver volar ya otro vez aquel día feliz en el bosque cuando ser él aun muy jovencito
a el botella no le dar tiempo de pensar ser tan inopinado aquel de encontrar se de_repente en el punto crucial de su existencia
a el fondo se vislumbrar campanario y tejado y el persona no ser mayor que hormiga
luego se precipitar a uno velocidad muy distinto de el de el conejo
voltear en el aire sentir se joven y retozón estar aun lleno de vino hasta el mitad aunque por muy poco tiempo
qué viaje
el sol le comunicar su brillo todo el gente seguir con el vista su vuelo el globo haber desaparecer ya y pronto desaparecer también el botella
ser a caer sobre uno de el tejado hacer se 1000 pedazo pero el casco llevar tal impulso que no se quedar en el lugar de el caída sino_que seguir saltar y rodar hasta dar en el patio donde acabar de desmenuzar se y desparramar se por el suelo
sólo el gollete quedar entero cortar en redondo como con uno diamante
poder servir de bebedero para uno pájaro decir el hombre que habitar en el sótano pero él no tener pájaro ni jaula y tampoco ser cosa de comprar se uno y otro sólo por el mero hecho de tener uno cuello de botella apropiar para bebedero
el viejo solterón de el buhardilla le encontrar aplicación y haber aquí cómo el gollete ser a parar arriba donde le poner uno tapón de corcho y el parte que antes mirar a el cielo ser ahora colocar hacia abajo
cambios bien frecuente en el vida
lo llenar de agua fresco y lo colgar de el reja de el jaula por el exterior y el ave se poner a cantar con tanto brío y regocijo que su trino resonar a gran distancia
ay bien poder tú cantar
ser el que decir el gollete de el botella el cual no dejar de ser uno notabilidad ya_que haber estar en el globo
ser todo el que se saber de su historia
colgar ahora en_calidad_de bebedero oír el rumor y el grito de el transeúnte y el conversación de el viejo solterón en su cuartucho
ser el caso que acabar de llegar uno visita uno amigo de su edad y ambos se poner a charlar no de el gollete de el botella sino de el mirto de el ventana
no te gastar $_PTE:2 por el corona de novio de tu hijo decir el solterón yo te dar uno que haber conservar con flor magnífico
ver aquel árbol de el ventana
ser uno esqueje de el arrayán que me regalar el día en que me prometer para que al_cabo_de uno año me tejer el corona de novio pero ese día jamás llegar
se cerrar el ojo destinar a iluminar mi gozo y mi dicha en este vida
reposar ahora dulcemente en el fondo de el mar pobre alma mío
el árbol se convertir en uno árbol viejo pero yo envejecer más aun y cuando aquel se marchitar cortar el último de su rama verde y lo plantar y aquel rama se haber volver este árbol que a el fin ser uno adorno de novio el corona de tu hijo
mientras pronunciar este palabra grueso lágrima resbalar por el mejilla de el viejo solterón hablar de el amigo de su juventud de su noviazgo en el bosque
pensar en el momento en que todo haber brindar por el prometido pensar en el 1 beso pero todo este se lo callar ahora no ser sino uno viejo solterón
en tanto cosa pensar
pero ni por uno momento le venir a el imaginación que en el ventana haber uno recuerdo de aquel día venturoso el gollete de el botella que haber decir plump
a el saltar el tapón con uno estampido
por su parte él no lo reconocer tampoco pues aunque haber poder seguir perfectamente el narración no lo hacer
para qué
estar sumir en su propio pensamiento

