erar se uno vez uno príncipe hijo de uno rey nadie poseer tanto y tan hermoso libro como él en ellos se leer cuanto suceder en el mundo y además tener bello estampa
se hablar en aquel libro de todo el pueblo y país pero ni uno palabra contener acerca_de el lugar donde se hallar el paraíso terrenal y este ser precisamente el objeto de el constante pensamiento de el príncipe
de muy niño ya antes_de ir a el escuela su abuelo le haber contar que el flor de el paraíso ser pastel el más dulce que caber imaginar y que su estambre estar henchir de el vino más delicioso
uno flor contener todo el historia otro el geografía otro el tabla de multiplicar bastar con comer se el pastel y ya se saber uno el lección y cuanto más se comer más historia se saber o más geografía o aritmética
el niño lo haber creer entonces pero a_medida_que se hacer mayor y se ir despertar su inteligencia y enriquecer se con conocimiento comprender que el belleza y magnificencia de el paraíso terrenal deber ser de otro género
ay
por qué se le ocurrir a eva comer de el árbol de el ciencia de el bien y de el mal
por qué probar adán el fruto prohibir
el que ser yo no lo haber hacer y el mundo jamás haber conocer el pecado
así decir entonces y así repetir cuando tener ya cumplir 17 año
el paraíso absorber todo su pensamiento
TM_d:1 se ser solo a el bosque pues ser aquel su mayor placer
se hacer de noche se acumular el nubarrón en el cielo y pronto descargar uno verdadero diluvio como si el cielo entero ser uno catarata por el que el agua se precipitar a torrente el oscuridad ser tan completo como poder ser lo en el pozo más profundo
caminar resbalar por el hierba empapar y tropezar con el desnudo piedra que sobresalir de el rocoso suelo
nuestro pobre príncipe chorrear agua y en todo su cuerpo no quedar uno partícula seco
tener que trepar por grande roca musgoso rezumante de agua y se sentir casi a el límite de su fuerza cuando de pronto percibir uno extraño zumbido y se encontrar delante_de uno gran cueva iluminar
en su centro arder uno hoguera tan grande como para poder asar en él uno ciervo entero y así ser realmente uno ciervo maravilloso con su altivo cornamenta aparecer ensartar en uno asador que girar lentamente entre 2 tronco entero de abeto
uno mujer anciano pero alto y robusto cual si se tratar de uno hombre disfrazar estar sentar junto_a el fuego a el que echar leña continuamente
acercar te le decir
sentar te a el lado de el fuego y secar te el ropa
qué corriente haber aquí
observar el príncipe sentar se en el suelo
más fuerte ser cuando llegar mi hijo responder el mujer
estar en el gruta de el viento mi hijo ser el 4 viento de el tierra
entender
dónde estar tu hijo
preguntar el príncipe
oh
ser difícil responder a pregunta tonto decir el mujer
mi hijo obrar a su capricho jugar a pelota con el nube allá arriba en el sala grande
y señalar el temporal de el exterior
ya comprender contestar el príncipe
pero hablar muy bruscamente no ser así el doncella de mi casa
bah
ellos no tener otro cosa que hacer
yo deber ser duro si querer mantener a mi hijo disciplinar y disciplinar lo tener aunque no ser fácil cosa manejar lo
ver aquel 4 saco que colgar de el pared
pues le tener más miedo de el que tú le tener antaño a el azote detrás_de el espejo
poder dominar a el mozo te lo asegurar y no tener más remedio que meter se en el saco aquí no andar con remilgo
y allí se estar sin poder salir y marchar se por el suyo hasta que a mí me dar el gana
ahí llegar uno
ser el viento norte que entrar con uno frío glacial esparcir granizo por el suelo y arremolinar copo de nieve
vestir calzón y chaqueta de piel de oso y traer uno gorra de piel de foca calar hasta el oreja largo carámbano le colgar de el barba y grano de pedrisco le bajar de el cuello rodar por el chaqueta
no se acercar enseguida a el fuego
le decir el príncipe
poder helar se le el cara y el mano
hielo
responder el viento con uno sonoro risotada
hielo
no haber cosa que más me gustar
pero de dónde salir ese mequetrefe
cómo haber venir a dar en el gruta de el viento
ser mi huésped intervenir el viejo y si no te gustar mi explicación ya estar meter te en el saco
me entender
bastar este palabra para hacer le entrar en razón y el viento norte se poner a contar de dónde venir y dónde haber estar aquel mes
vengar de el mar polar decir estar en el isla_de_los_osos con el ballenero ruso dormir sentar en el timón cuando zarpar de el cabo_norte de_vez_en_cuando me despertar uno poquitín y me encontrar con el petrel volar entre mi pierna
ser uno ave muy curioso pegar uno fuerte aletazo y luego se mantener inmóvil con el ala desplegar
no te perder en digresión decir el madre
llegar luego a el isla_de_los_osos
qué hermoso ser aquel
haber uno pista de baile liso como uno plato y nieve semiderretir con poco musgo esparcir por el suelo haber también agudo piedra y esqueleto de morsa y oso polar como gigantesco brazo y pierna cubrir de moho
se haber decir que nunca brillar allí el sol
soplar ligeramente por entre el niebla para que poder ver se el cobertizo
ser uno choza hacer de madero acarrear por el agua el tejado estar cubrir de piel de morsa con el parte interior vuelta hacia fuera rojo y verde sobre el techo haber uno oso blanco gruñir
me ir a el playa a ver el nido de el polluelo que chillar abrir el pico
le soplar en el gaznate para que lo cerrar
más lejos se revolcar el morsa parecer a intestino viviente o gigantesco oruga con cabeza de cerdo y diente de uno vara de largo
te explicar bien hijo observar el madre
el boca se me hacer agua oír te
luego empezar el caza
disparar uno arpón a el pecho de uno morsa y por encima_de el hielo saltar uno chorro de sangre ardiente como uno surtidor
yo me acordar entonces de mi treta me poner a soplar y mi velero el alto montaña de hielo aprisionar el bote
qué tumulto entonces
qué manera de silbar y de gritar
pero yo silbar más que ellos
haber de depositar sobre el hielo el cuerpo de el morsa capturar el caja y el aparejo yo le verter encima montón de nieve y forzar el embarcación bloquear a que derivar hacia el sur con su botín para que probar el agua salar
jamás volver a el isla_de_los_osos
cuánto mal haber hacer
le decir su madre
otro te contar el que hacer de bueno replicar el viento
pero ahí tener a mi hermano de poniente ser el que más querer saber a mar y llevar conseguir uno frío delicioso
no ser el pequeño céfiro
preguntar el príncipe
claro que ser el céfiro
responder el viejo pero no tan pequeño
antes ser uno chiquillo muy simpático pero este pasar ya
realmente tener aspecto salvaje pero se tocar con uno especie de casco para no lastimar se
empuñar uno porra de caoba cortar en el selva americano pues gastar siempre de lo mejor
de dónde venir
 le preguntar su madre
de el selva virgen responder donde el bejuco espinoso formar uno valla entre árbol y árbol donde el serpiente de agua moro entre el húmedo hierba y el hombre estar de más
y qué hacer allí
contemplar el río profundo lo ver precipitar se de el peña levantar uno húmedo polvareda y volar hasta el nube para captar el arco iris
ver nadar en el río el búfalo salvaje pero ser más fuerte que él y el corriente se lo llevar agua abajo junto_con uno bandada de pato salvaje a el llegar a el rabión el pato levantar el vuelo mientras el búfalo ser arrastrar
me gustar el espectáculo y provocar uno tempestad tal que árbol centenario se ir río_abajo y se hacer triza
ese ser cuanto se te ocurrir hacer
preguntar el viejo
dar voltereta en el sabana acariciar el caballo salvaje y sacudir el cocotero
sí tener mucho cosa que contar pero no haber que decir todo el que uno saber verdad viejo
y dar tal beso a su madre que por poco el tumba ser uno mozo muy impulsivo
se presentar luego el viento sur con su turbante y uno holgar túnica de beduino
qué frío hacer aquí dentro
exclamar echar leña a el fuego
bien se notar que el viento norte ser el 1 en llegar
hacer uno calor como para asar uno oso polar
replicar aquel
ese ser tú uno oso polar
decir el de el sur
querer ir a parar a el saco
intervenir el viejo
sentar te en aquel piedra y decir nos dónde haber estar
en africa madre responder el interpelar
estar cazar león con el hotentote en el país de el cafre
qué hierba crecer en su llanura verde como aceituna
por allí brincar el ñu uno avestruz me retar a correr pero ya comprender que yo ser mucho más ligero
llegar después a el desierto de arena amarillo que parecer el fondo de el marzo encontré uno caravana estar sacrificar el último camello para obtener agua pero le sacar muy poco
el sol arder en el cielo y el arena en el suelo y el desierto se extender hasta el infinito
me revolcar en el fino arena suelto arremolinar lo en grande columna
qué danza aquel
haber ver cómo el dromedario coger miedo y el mercader se tapar el cabeza con el caftán arrodillar se ante mí como ante alá su dios
quedar sepultar cubierto por uno pirámide de arena
cuando soplar de nuevo por aquel lugar el sol blanquear su hueso y el viajero ver que otro hombre estar allí antes que ellos
de otro modo nadie lo creer en el desierto
así sólo haber cometer tropelía decir el madre
a el saco
y en_un_abrir_y_cerrar_de_ojos agarrar a el viento de el sur por el cuerpo y lo meter en el saco
el prisionero se revolver en el suelo pero el mujer se le sentar encima y haber de quedar se quieto
qué hijo más travieso tener
observar el príncipe
y que lo decir
asentir el madre pero yo poder con ellos
ahí tener a el cuarto
ser el viento de levante y vestir como uno chino
toma venir de este lado
preguntar el mujer
creer que haber estar en el paraíso
mañana ir allí responder el levante pues hacer 100 año que lo visitar por último vez
ahora vengar de china donde danzar en_torno_a el torre_de_porcelana hacer resonar todo el campana
en el calle aporrear a el funcionario medir le el espalda con vara de bambú ser gente de el grado primero a noveno y todo gritar gracias mi paternal bienhechor
pero no lo pensar ni_mucho_menos
y yo venir sacudir el campana tsingtsangtsu
siempre hacer de el tuya decir el madre
convenir que mañana vaya a el paraíso siempre aprender algo bueno
beber de el manantial de el sabiduría y traer me uno botella de su agua
muy bien responder el levante
pero por qué meter en el saco a mi hermano de el sur
dejar lo salir
querer que me hablar de el ave_fénix pues cada vez que ir a el jardín de el edén de siglo en siglo el princesa me preguntar acerca_de él
andar abrir el saco madrecita querido y te dar 2 bolsa de té verde y fresco que yo mismo coger de el planta
bueno lo hacer por el té y porque ser mi preferir
y abrir el saco de el que salir el viento de el sur muy abatir y cabizbajo pues el príncipe haber ver todo el escena
ahí tener uno hoja de palma para el princesa decir
me lo dar el ave_fénix el único que haber en el mundo
haber escribir en él con el pico todo su biografía uno vida de 100 año
así poder leer lo él mismo
yo presenciar cómo el ave prender fuego a su nido estar él dentro y se consumir igual que hacer el mujer de uno hindú
cómo crepitar el rama seco
y qué humareda y qué olor
a el fin todo se ir en llama y el viejo ave_fénix quedar convertir en ceniza pero su huevo que yacer ardiente en_medio_de el fuego estallar con gran estrépito y el polluelo salir volar
ahora ser él el soberano de todo el ave y el único ave_fénix de el mundo
de uno picotazo hacer uno agujero en el hoja de palma ser su saludo a el princesa
ser hora de que tomar algo decir el madre de el viento y sentar se todo junto_a él comer de el ciervo asar
el príncipe se haber colocar a el lado de el levante y así no tardar en ser bueno amigo
decir me preguntar el príncipe qué princesa ser este de que hablar y dónde estar el paraíso
oh
responder el viento
si querer ir allá ver mañana conmigo pero uno cosa deber decir te que ninguno ser humano estar allí desde el tiempo de adán y eva
ya lo saber por el historia_sagrada
sí desde_luego afirmar el príncipe
cuando lo expulsar el paraíso se hundir en el tierra pero conservar su sol su aire tibio y todo su magnificencia
residir allí el reina de el hada y en él estar el isla_de_la_bienaventuranza a el que jamás llegar el muerte y donde todo ser espléndido
montar te mañana sobre mi espalda y te llevar conmigo crear que no haber inconveniente
pero ahora no me decir nada más querer dormir
de_madrugada despertar el príncipe y tener uno gran sorpresa a el encontrar se ya sobre el nube
ir sentar en el dorso de el viento de levante que lo sostener firmemente
pasar a tanto altura que el bosque y el campo el río y el lago aparecer como en uno gran mapa iluminar
bueno día
decir el viento
aun poder seguir dormir uno poco más pues no haber gran cosa que ver en el tierra llano que tener debajo
a_menos_que querer contar el iglesia destacar como puntito blanco sobre el tablero verde
llamar tablero verde a el campo y prado
ser uno gran incorrección no despedir me de tu madre y de tu hermano decir el príncipe
el que dormir estar disculpar responder el viento y echar a correr más velozmente que hasta entonces como poder comprobar se por el copa de el árbol pues a el pasar por encima_de ellos crepitar el rama y hoja y poder ver lo también en el mar y el lago pues se levantar enorme ola y el grande barco se zambullir en el agua como cisne
hacia el atardecer cuando ya oscurecer contemplar el bello espectáculo de el grande ciudad iluminar salpicar el paisaje
ser como si haber encender uno pedazo de papel y se ver el chispa de fuego extinguir se uno tras otro como otro tanto niño que salir de el escuela
el príncipe dar palmada pero el viento le advertir que deber estar se quieto pues poder caer se y quedar colgar de el punta de uno campanario
el águila de el oscuro bosque volar rauda ciertamente pero le ganar el viento de levante
el cosaco montar en su caballo correr ligero por el estepa pero más ligero correr el príncipe
ahora ver el himalaya
decir el viento
ser el cordillera más alto de asia y no tardar ya en llegar a el jardín de el paraíso
torcer más a el sur y pronto percibir el aroma de su especia y flor
higueras y granar crecer silvestre y el parra salvaje tener racimo azul y rojo
bajar allí y se tender sobre el hierba donde el flor saludar a el viento inclinar el cabeza como dar le el bienvenida
estar ya en el paraíso
preguntar el príncipe
no todavía no responder el levante pero ya faltar poco
ver aquel muro de roca y el gran hueco donde colgar el sarmiento a_modo_de cortina verde
haber de atravesar lo
envolver te en tu capa aquí el sol arder pero a uno paso de nosotros hacer uno frío gélido
el ave que volar sobre aquel abismo tener el ala de el lado de acá en el tórrido verano y el otro en el invierno riguroso
entonces este ser el camino de el paraíso
preguntar el príncipe
se hundir en el caverna uf
qué frío más horrible
pero durar poco rato el viento desplegar su ala que brillar como fuego
qué abismo
el enorme peñasco de el que se escurrir el agua se cerner sobre ellos adoptar el figura más asombroso pronto el cueva se estrechar de tal modo que se ver forzar a arrastrar se a_cuatro_patas otro vez se ensanchar y abrir como si estar a el aire libre
se haber decir cripta sepulcral con mudo órgano y bandera petrificar
ir a el paraíso por el camino de el muerte
preguntar el príncipe pero el viento no responder limitar se a señalar le hacia delante_de donde venir uno bello luz azul
el bloque de roca colgar sobre su cabeza se ir difuminar en uno especie de niebla que a el fin adquirir el luminosidad de uno blanco nube bañar por el luna
respirar entonces uno atmósfera diáfano y tibio puro como el de el montaña y aromatizar por el rosa de el valle
fluir por allí uno río límpido como el mismo aire y en su agua nadar pez que parecer de oro y plata serpentear en él anguila purpúreo que a cada movimiento lanzar chispa azul y el ancho hoja de el nenúfar reflejar todo el tono de el arco iris mientras el flor ser uno auténtico llamar ardiente de uno rojo amarillento alimentar por el agua como el lámpara por el aceite
uno sólido puente de mármol bellamente cincelar cual si ser hacer de encaje y perla de cristal conducir por encima_de el río a el isla de el bienaventuranza donde se hallar el jardín de el paraíso
el viento coger a el príncipe en brazo y lo transportar a el otro lado de el puente
allí el flor y hoja cantar el más bello canción de su infancia pero mucho más melodiosamente de el que poder hacer lo el voz humano
y aquel árbol ser palmera o gigantesco planta acuático
nunca haber ver el príncipe árbol tan alto y vigoroso en largo guirnalda pender maravilloso enredadera tal como sólo se ver figurar en color y oro en el margen de el antiguo devocionario o entrelazar en su inicial
formar el más raro combinación de ave flor y arabesco
muy cerca en el hierba se pasear uno bandada de pavo real con el fulgurante cola desplegar
ese parecer
pero a el tocar lo se dar_cuenta el príncipe de que no ser animal sino planta ser grande lampazo que brillar como el esplendoroso cola de el pavo real
el león y el tigre saltar como ágil gato por entre el verde seto cuyo aroma semejar el de el flor de el olivo y tanto el león como el tigre ser manso el paloma torcaz relucir como hermoso perla acariciar con el ala el melena de el león y el antílope siempre tan esquivo se estar quieto agitar el cabeza como deseoso de participar también en el juego

