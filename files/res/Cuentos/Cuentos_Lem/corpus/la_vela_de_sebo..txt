hervir y bullir mientras el fuego llamear bajo de el olla ser el cuna de el vela de sebo y de aquel cálido cuna brotar el vela entero esbelto de uno solo pieza y uno blanco deslumbrante con uno forma que hacer que todo quien lo ver pensar que prometer uno futuro luminoso y deslumbrante y que ese promesa que todo ver haber de mantener se y realizar se
el oveja uno precioso oveja ser el madre de el vela y el crisol ser su padre
de su madre haber heredar el cuerpo deslumbrantemente blanco y uno vago idea de el vida y de su padre haber recibir el ansia de ardiente fuego que atravesar médula y hueso  y fulgurar en el vida
sí así nacer y crecer cuando con el mayor más luminoso expectativa así se lanzar a el vida
allí encontrar a otro mucho criatura extraño a el que se juntar pues querer conocer el vida y hallar tal_vez a el mismo tiempo el lugar dónde más a gusto poder sentir se
pero su confianza en el mundo ser excesivo este solo se preocupar por sí mismo nada en absoluto por el vela de sebo pues ser incapaz de comprender para qué poder servir por ese intentar usar lo en provecho propio y coger el vela de forma equivocar el negro dedo llenar de mancha cada vez mayor el límpido color de el inocencia que a el poco desaparecer por completo y quedar totalmente cubrir por el suciedad de el mundo que lo rodear haber estar en uno contacto demasiado estrecho con él mucho más cercano de el que poder aguantar el vela que no saber distinguir el limpio de el sucio  pero en su interior seguir ser inocente y puro
ver entonces su falso amigo que no poder llegar hasta su interior y furioso tirar el vela como uno trasto inútil
y el negro cáscara externo no dejar entrar a el bueno que tener miedo de ensuciar se con el negro color temer llenar se de mancha también ellos  de_modo_que no se acercar
el vela de sebo estar ahora solo y abandonar no saber qué hacer
se ver rechazar por el bueno y descubrir también que no ser más que uno objeto destinar a hacer el mal se sentir inmensamente desdichado porque no haber dedicar su vida a nada provechoso que incluso tal_vez haber manchar de negro el mejor que haber en torno suyo y no conseguir entender por qué ni para qué haber ser crear por qué tener que vivir en el tierra quizá destruir se a sí mismo y a otro
más y más cada vez más profundamente reflexionar pero cuanto más pensar tanto mayor ser su desánimo pues a_fin_de_cuentas no conseguir encontrar nada bueno ninguno sentido auténtico en su existencia ni lograr distinguir el misión que se le haber encomendar a el nacer
ser como si su negro cubierta haber velar también su ojo
mas aparecer entonces uno llama uno mechero este conocer a el vela de sebo mejor que él mismo porque el mechero ver con todo claridad a través incluso de el cáscara externo y en el interior ver que ser bueno por ese se aproximar a él y luminoso esperanza se despertar en el vela se encender y su corazón se derretir
lo llamar relucir como uno alegre antorcha de esponsales todo estar iluminar y claro a su alrededor y iluminar a el camino para quien lo llevar su verdadero amigo  que feliz buscar ahora el verdad ayudar por el resplandor de el vela
pero también el cuerpo tener fuerza suficiente para alimentar y dar vida a el llameante fuego
gota a gota semilla de uno nuevo vida caer por todo parte descender en gota por el tronco cubrir con su miembro suciedad de el pasado
no ser solamente producto físico también espiritual de el esponsales
y el vela de sebo encontrar su lugar en el vida y saber que ser uno auténtico vela que lucir largo tiempo para alegría de él mismo y de el demás criatura
fin
