silbar el viento entre el rama de el viejo sauce
se decir que se oír uno canción el viento lo cantar el árbol lo recitar
si no lo comprender pregunta a el viejo juana el de el asilo él saber de este pues nacer en este parroquia
hacer mucho año cuando aun pasar por aquí el camino real el árbol ser ya alto y corpulento
estar donde estar todavía frente_a el blanco casa de el sastre con su pared entramar cerca de el estanque que entonces ser el bastante grande para abrevar el ganado y para que en verano se zambullir y chapotear desnudo el niño de el aldea
junto_a el árbol haber erigir uno piedra miliar hoy estar decaer y invadir por el zarzamora
el nuevo carretera ser desviar hacia el otro lado de el rico finca el viejo camino real quedar abandonar y el estanque se convertir en uno charca invadir por lenteja de agua
cuando saltar uno rana el verde se separar y aparecer el agua negro en torno crecer y seguir crecer espadaña junco y iris amarillo
el casa de el sastre envejecer y se inclinar y el tejado se convertir en uno bancal de musgo y siempreviva se derrumbar el palomar y el estornino establecer en él su nido el golondrina construir el suyo alinear bajo el tejado y en el alero como si aquel ser uno casa afortunado
antaño lo haber ser ahora estar solitario y silencioso
solo y apático vivir en él el pobre rasmus como lo llamar
haber nacer allí allí haber jugar de niño saltar por campo y seto chapotear en el estanque y trepar a el copa de el viejo sauce
este extender su grande rama como lo extender todavía pero el tempestad haber curvar ya el tronco y el tiempo haber abrir uno grieta en él que el viento y el intemperie haber cuidar de llenar de tierra
de aquel tierra haber nacer hierba y verdor incluso haber brotar uno pequeño serbal
cuando en primavera llegar el golondrina volar en_torno_a el árbol y a el tejado pegar su barro y construir su nido mientras el pobre rasmus tener el suyo completamente abandonar sin cuidar de reparar lo ni siquiera sustentar lo
qué más dar
exclamar el mismo que decir ya su padre
él se quedar en su casa mientras el golondrina se marchar y volver el fiel animal
también se marchar y volver el estornino con su canción aflautado
en otro tiempo rasmus competer con él en cantar pero ahora ya no cantar ni tocar el flauta
silbar el viento entre el viejo sauce y seguir silbar parecer como si se oír uno canción el viento lo cantar el árbol lo recitar
si no lo comprender ver a preguntar a el viejo juana el de el asilo él saber de este cosa de otro tiempo ser como uno crónica con estampa y viejo recuerdo
cuando el casa ser nuevo y estar en bueno estado se trasladar a él ivar_ulze el sastre de el pueblo y su mujer maren uno matrimonio honrar y laborioso
por aquel fecha el viejo juana ser uno niño hijo de el zuequero uno de el más pobre de el parroquia
más de uno vez haber recibir pan y mantequilla de maren a quien no faltar comida
estar en bueno relación con el propietario de el finca lo ver siempre alegre y risueño no se intimidar y si saber usar el boca no menos saber servir se de el mano el aguja correr tan ligero como el lengua sin que por ese se olvidar de el cuidado de su casa y de su hijo casi 12 pues ser 11 el 12 no llegar
el pobre tener siempre el nido lleno de cría gruñir el propietario de el casa
si se poder ahogar como se hacer con el gato dejar sólo uno o 2 de el más robusto todo salir ganar
dios misericordioso
exclamar el mujer de el sastre
el hijo ser uno bendición divino ser el alegría de el casa
cada niño ser uno padrenuestro más
si se hacer difícil saciar a tanto boca uno se esforzar más y encontrar consejo y apoyo en todo parte
nuestro señor no nos abandonar si no lo abandonar nosotros
el propietario estar de_acuerdo_con maren lo aprobar con uno gesto de el cabeza y le acariciar el mejilla lo haber hacer mucho vez y incluso lo haber besar pero entonces el señor ser uno niño y maren su niñera
el 2 se querer y seguir querer se
cada año para el navidades de el finca de el propietario enviar provisión a casa de el sastre uno barril de harina uno cerdo 2 pato otro barril de manteca queso y manzana
todo aquel ayudar a llenar el despensa
entonces ivar_ulze se mostrar satisfacer pero no tardar en volver con su estribillo qué más dar
el casa estar hacer uno primor con cortina en el ventana y también flor clavel y balsamina
uno alfabeto de bordador colgar bien enmarcar en el pared y a su lado uno dedicatoria en verso obra de el propio maren_ulze que tener maña en componer rima
no estar poco orgulloso de su apellido de ulze ser el único palabra de el lengua que rimar con « sülze » que significar gelatina
no dejar de ser uno ventaja
decir reír
estar siempre de bueno humor y nunca se le oír decir como a su marido para qué
su expresión habitual ser a_dios rogar y con el mazo dar
él lo hacer así y el cosa marchar bien
el hijo crecer dejar el nido se ir a tierra lejano y salir todo de bueno índole
rasmus ser el menor tan hermoso de niño que uno de el más renombrado pintor de el ciudad se brindar a pintar lo tal_y_como haber venir a el mundo
el retrato estar ahora en el palacio real el propietario lo haber ver allí y reconocer a el pequeño rasmus a_pesar_de ir en cuero
pero llegar malo tiempo
el sastre sufrir de artritismo en el 2 mano se le formar grueso nódulo y tanto el médico como el curandero stine se declarar impotente
no haber que desanimar se
decir maren
de nada servir agachar el cabeza
poner que el mano de el padre no poder ayudar nos procurar yo dar más ligereza a el mío
el pequeño rasmus poder también tirar de el aguja
se sentar ya a el mesa de coser cantar como uno flauta ser uno chiquillo muy alegre
pero no deber quedar se todo el día sentar allí decir el madre haber ser uno pecado contra el pequeño tener también que jugar y saltar
juana el hijo de el zuequero ser su mejor compañero de juego
su familia ser aun más pobre que el de rasmus
no ser bonito y andar descalzo llevar el vestido romper pues nadie cuidar de él y jamás se le ocurrir hacer lo él mismo no ser sino uno niño alegre como el pajarillo a el sol de nuestro_señor
rasmus y juana soler jugar junto_a el piedra miliar bajo el corpulento sauce
el tener grande idea querer ser uno bueno sastre y vivir en el ciudad donde haber maestro que tener 10 oficial en_torno_a su mesa lo saber por su padre
allí se hacer él oficial y luego maestro juana ir a visitar lo y si saber cocinar preparar el comida para el 2 y tener su propio habitación
a juana le parecer todo aquel uno tanto improbable pero rasmus no dudar de que todo suceder al_pie_de_la_letra
y así se pasar el hora bajo el viejo árbol mientras el viento silbar a_través_de su rama y hoja ser como si el viento cantar y el árbol recitar
en otoño caer el hoja y el lluvia gotear de el rama desnudo
ya reverdecer
decir el mujer
qué más dar
replicar el hombre
año_nuevo nuevo preocupación para salir de el paso
tener el despensa lleno observar él
y poder dar gracias_a el señor
yo estar sano y no me faltar energía
ser uno pecado quejar
el navidades lo pasar el propietario en su finca pero a el semana después_de año_nuevo volver a el ciudad donde residir durante el invierno contento y satisfacer asistir a baile y fiesta invitar incluso a palacio
el señor haber recibir de francia 2 precioso vestido
nunca el sastresa maren haber ver uno tela uno corte y uno costura como aquel
pedir permiso a el propietario para ir con su marido a ver el vestido pues para uno sastre de pueblo ser uno cosa jamás ver
el hombre lo examinar sin decir palabra y ya de vuelta en su casa no hacer más comentario que su habitual qué más dar
y por uno vez su palabra ser sensato
el señor regresar a el ciudad donde se reanudar el baile y el fiesta pero en_medio_de todo aquel diversión morir el anciano señor y su esposo no poder ya lucir su magnífico vestido
quedar muy apesadumbrar y se poner de riguroso luto de_pies_a_cabeza no tolerar ni uno cinta blanco
todo el criado ir de negro y incluso el coche de gala ser recubrir de paño de este color
uno noche gélido en que brillar el nieve y centellear el estrella llegar de el ciudad el carroza fúnebre conducir el cadáver que deber recibir sepultura en el panteón familiar de el cementerio de el pueblo
el administrador y el alcalde esperar a caballo sostener antorcha encender ante el puerta de el camposanto
el iglesia estar iluminar y el sacerdote recibir el cadáver en el entrada de el templo
llevar el féretro a el coro acompañar de todo el población
hablar el párroco y se cantar uno coral
el señor se hallar también presente en el iglesia haber hacer el viaje en el coche de gala cubrir de crespón en el parroquia nunca haber presenciar uno espectáculo semejante
durante todo el invierno se estar hablar en el pueblo de aquel solemnidad fúnebre el entierro de el señor
en él se ver el importante que ser comentar el gente de el pueblo
nacer en elevar cuna y ser enterrar con grande honor
qué más dar
decir el sastre
ahora no tener ni vida ni bien
a nosotros a el menos nos quedar uno de el 2 cosa
no hablar así
le reñir maren
ahora gozar de vida eterno en el cielo
cómo lo saber maren
preguntar el sastre
uno muerto ser bueno abono
pero ese ser demasiado noble para servir de algo en el tierra tener que reposar en el cripta
no decir impiedad
protestar maren
te repetir que gozar de vida eterno
quién te lo haber decir maren
repetir el sastre
maren echar su delantal sobre el pequeño rasmus no querer que oír aquel desatino
se lo llevar llorar a el choza y le decir lo que oír hijo mío no ser tu padre quien lo decir sino el demonio que estar en el habitación y imitar su voz
rezar el padrenuestro
lo rezar el 2
y juntar el mano de el niño
ahora volver a estar contento decir
confiar en ti y en dios_nuestro_señor
pasar uno año el viudo se poner de medio luto el alegría haber volver a su corazón
correr el rumor de que tener uno pretendiente y pensar volver a casar se
maren saber algo de ello y el párroco uno poco más aun
el [G:??/??/??:????:??] de ramos después_de el sermón haber de leer se el amonestación de el viudo y su prometido el cual ser algo así_como picapedrero o escultor no se saber a_ciencia_cierta por aquel fecha thorwaldsen y su arte no andar todavía en todo el boca
el nuevo propietario no ser noble aunque sí hombre de categoría
nadie entender a_punto_fijo en qué se ocupar pero se decir que tallar estatua y ser muy experto en su trabajo además_de joven y guapo
qué más dar
decir el sastre ulze
el [G:??/??/??:????:??] de ramos ser amonestar luego se cantar uno coral y se administrar el comunión
el sastre su mujer y el pequeño rasmus estar en el iglesia el padre comulgar pero el pequeño permanecer sentar en el banco pues aun no haber recibir el confirmación
en el último tiempo andar escaso de ropa en casa de el sastre el traje viejo estar usado y lleno de remiendo y pieza pero aquel día el 3 llevar vestido nuevo aunque negro como si asistir a uno entierro estar confeccionar con el tela que haber recubrir el coche fúnebre
haber salir uno chaqueta y uno pantalón para el marido uno vestido cerrar hasta el cuello para maren y para rasmus uno traje completo que le servir para el confirmación cuando llegar el hora se lo haber hacer holgar adrede
en todo aquel indumentaria se invertir el totalidad de el tela que tapizar el coche tanto por dentro como por fuera
nadie tener por qué saber de dónde proceder aquel paño y no_obstante pronto correr el voz stine el curandero y otro comadre de su mismo calaña pronosticar que aquel vestido llevar el peste y el enfermedad a el casa
sólo para bajar a el tumba haber que vestir se con ropa funerario
el juana de el zuequero llorar a el oír este comentario y como resultar que desde aquel día ser empeorar el salud de el sastre se echar de ver a quién le tocar pronto el turno de llorar
y así ser
el 1 [G:??/??/??:????:??] después_de el trinidad fallecer el sastre ulze y maren quedar solo a el cuidado de el casa
y seguir llevar lo y mantener lo unir sin perder nunca el confianza en sí mismo y en dios
a el año siguiente rasmus ser confirmar
haber sonar para él el hora de trasladar se a el ciudad como aprendiz en casa de uno sastre de renombre que si no tener 12 oficial en su mesa siquiera tener uno
el pequeño rasmus valía por medio y estar contento y alegre pero juana llorar pues lo querer más de el que él mismo creer
el mujer de el sastre se quedar en el viejo casa y continuar el negocio de su marido
suceder este por el tiempo en que se inaugurar el nuevo camino real
el antiguo que pasar por delante_de el vivienda de el sastre quedar como camino vecinal el vegetación invadir el estanque que pronto quedar convertir en uno charca lleno de lenteja de agua
se volcar el piedra miliar pues ya no servir de nada pero el árbol seguir vivir robusto y hermoso el viento silbar entre su rama y hoja
se marchar el golondrina y se marchar también el estornino para regresar a el primavera siguiente y a el 4 vez volver también con ellos rasmus
haber pasar el examen de oficial sastre y ser uno mozo guapo aunque delgaducho
su intención ser cargar se el mochila a el espalda y marchar se a ver mundo pero su madre desear retener lo conseguir
en ninguno sitio se estar tan bien como en casa
el demás hijo se haber desperdigar todo él ser el más joven y deber quedar se con su madre
trabajo no ir a faltar le ni_mucho_menos poder recorrer el comarca como sastre ambulante trabajar TM_d:15 en uno lugar y otro 15 en otro
también este ser viajar
y rasmus seguir el consejo de su madre
volver pues a dormir bajo el techo de su casa natal y sentar a el pie de el viejo sauce volver a oír el rumor de el viento soplar entre su rama
ser uno mozo de bueno presencia saber cantar como uno pájaro cantar viejo y nuevo canción
en el grande finca ser recibir con simpatía especialmente en casa de klaus_hansen el 2 entre el labrador rico de el parroquia
su hijo elsa ser como uno bello flor siempre risueño
alguno persona mal intencionado asegurar que reír sólo para exhibir su precioso diente pero el verdad ser que ser alegre por naturaleza y aficionar a travesura pero todo le estar bien
se prendar de rasmus y él de él pero el 2 se lo guardar
así ir cómo el muchacho se volver melancólico tener más de el temperamento de su padre que de el de su madre
su bueno humor se despertar solamente cuando llegar elsa entonces el 2 se reír bromear y hacer travesura pero aunque no le faltar bueno oportunidad nunca le decir uno palabra de su pasión
qué más dar
pensar
su padre querer casar lo bien y yo no tener nada
el más acertar ser marchar me de aquí
pero no poder alejar se de el finca le parecer que uno hilo lo atar a él para el muchacho ser como uno pájaro amaestrar que cantar y trinar al_gusto_de él
juana el hijo de el zuequero estar emplear como sirviente en el propiedad donde tener que hacer el trabajo más humilde ir a el prado con el carro de el leche a ordeñar el vaca junto_con otro criado y cuando ser preciso acarrear también estiércol
nunca entrar en el habitación principal y apenas ver a rasmus y a elsa pero oír que ser casi prometer
rasmus ser rico decir
me alegrar por él
y su ojo se humedecer el cual cuadrar muy mal con su palabra
TM_d:1 de mercado klaus_hansen se trasladar a el ciudad acompañar de rasmus que tanto a el ida como a el vuelta viajar a el lado de elsa
estar loco de amor pero no lo dar a entender en nada
ser hora de que hablar
pensar el muchacho y haber que convenir en que tener razón
si no se decidir tener que sacudir me lo
y pronto se hablar en el casa de que el campesino más rico de el parroquia se haber declarar a elsa
así ser en efecto pero todo el mundo ignorar el respuesta de el joven
el pensamiento dar vuelta en el cabeza de rasmus
uno atardecer elsa le poner uno anillo de oro en el dedo y le preguntar qué significar aquel
noviazgo decir él
y con quién creer tú
preguntar él
con el rico labrador
aventurar él
acertar
exclamar elsa y saludar lo con uno gesto de el cabeza se marchar
también se marchar él y volver a casa de su madre fuera_de sí
se atar el mochila y se disponer a lanzar se a el mundo a_pesar_de el lágrima de el viejo
cortar uno bastón de el viejo sauce cantar como si estar de bueno humor porque se marchar a ver el maravilla de el ancho mundo
qué pena para mí
suspirar el mujer
pero ser el mejor y más acertar que poder hacer y deber resignar me
confiar en dios y en ti que yo esperar volver te a ver alegre y contento
avanzar por el nuevo carretera cuando ver a juana que pasar guiar uno carro lleno de estiércol
él no se haber dar_cuenta de su presencia y él preferir que no lo ver por ese se ocultar detrás_de uno vallado y juana pasar a poquísimo distancia
se marchar a correr mundo nadie saber adónde
su madre pensar que regresar antes_de fin de año
ver cosa nuevo tener nuevo pensamiento ser como el viejo pliegue que no poder alisar se con el plancha
tener demasiado de su padre mejor querer que se parecer a mí pobre hijo mío
pero volver seguramente no ser posible que renunciar a su madre y a su casa
el mujer estar disponer a esperar largo tiempo
elsa esperar sólo uno mes luego se ir a encontrar secretamente a el curandero stine entender en el arte de curar echar el carta y decir el buenaventura sí saber más que friján
en_consecuencia conocer también el paradero de rasmus lo leer en el poso de el café
se encontrar en uno ciudad extranjero pero no poder descifrar su nombre
haber en aquel ciudad soldado y mujer alegre
estar vacilar entre tomar el mosquete o uno de aquel mozo
elsa no poder soportar ese noticia
gustosa dar el dinero que tener ahorrar para redimir lo a_condición_de que nadie saber que ser cosa suyo
y el viejo stine prometer hacer volver a el muchacho conocer 1/2 peligroso para el persona interesar pero infalible
hacer cocer en uno olla uno mezcla que lo forzar a marchar se de el lugar donde estar fuera el que ser y regresar junto_a el olla y a el lado de su amado
ser posible que tardar mes pero a el fin acudir a_menos_que haber morir
deber seguir sin paz ni reposo día y noche a_través_de mar y de montaña con bueno o mal tiempo y por mucho que ir su fatiga
tener que regresar a su tierra ser forzoso
el luna estar en su 1 cuadrante el mejor momento para el hechizo decir el viejo stine
el tiempo ser borrascoso crujir el viejo sauce
stine cortar uno rama y hacer uno nudo dentro aquel contribuir a atraer a rasmus a el hogar de su madre
coger musgo y siempreviva de el tejado y lo meter en el olla que haber poner ya a el fuego
elsa tener que arrancar uno hoja de el libro de cántico y casualmente arrancar el último el que contener el fe de errata
el mismo dar decir el brujo echar lo a el puchero
mucho cosa haber de ir a parar a aquel caldo que deber cocer sin interrupción hasta el vuelta de rasmus
el gallo negro de el casa de el viejo stine tener que sacrificar el rojo cresta que ser también a el olla
también ser a él el grueso sortija de oro de elsa y stine le haber advertir de_antemano que desaparecer para siempre
desde_luego ser listo el viejo
asimismo ir a parar a el puchero otro mucho cosa que no saber enumerar
y venir hervir sobre el fuego vivo o sobre ceniza ardiente
sólo él y elsa lo saber
pasar el luna nuevo y pasar el cuarto menguante todo el día se presentar elsa_aún no lo ver venir
saber mucho cosa
decir stine y ver otro mucho
el que no poder ver ser si ser muy largo el camino
ya haber trasponer el 1 montaña haber cruzar el mar tempestuoso
el camino a_través_de el grande bosque ser largo
el mozo tener ampolla en el pie y fiebre en el cuerpo pero haber de seguir sin remedio
no no
decir elsa
me dar lástima
ahora ya no poder detener se
si lo obligar a hacer lo caer morir en_medio_de el carretera
haber transcurrir mucho tiempo
brillar el luna lleno el viento silbar entre el rama de el viejo sauce y en el cielo iluminar por el luna se dibujar uno arco iris
este ser el señal
decir stine
ahora llegar rasmus
pero no llegar
largo ser el espera
decir stine
ya estar cansar responder elsa y su visita a el brujo empezar a escasear aparte que no le llevar más regalo
se serenar su espíritu y uno mañana todo el parroquia saber que elsa haber dar el sí a el rico labrador
ver el casa y el campo el ganado y el ajuar
todo estar en bueno condición no haber ninguno motivo que aconsejar retrasar el boda
el grande festejo durar TM_d:3 y se bailar a el son de clarinete y violín
todo el habitante de el parroquia ser invitado y también asistir el viejo ulze quien terminar ya el fiesta y después que el anfitrión se haber despedir de su huésped y el trompeta haber cerrar el solemnidad se marchar a su casa con el resto de el banquete
haber cerrar el puerta solamente con uno palo
lo encontrar abrir a su regreso y en el casa estar rasmus
acabar de llegar
santo_dios
no ser sino piel y hueso estar pálido y demacrar
rasmus
exclamar su madre
ser posible que ser tú
qué enfermo parecer
pero me alegrar el tener te aquí de nuevo
y le servir uno bueno comida con el vianda que traer de el boda asar y uno pedazo de torta
en el curso de el último tiempo decir el mozo haber pensar con gran frecuencia en su madre en el casa y en el viejo sauce
parecer extraño el vez que en sueño haber ver el árbol y a juana descalzo
no mencionar a elsa
estar enfermo y tener que acostar se pero nosotros no creer que ser por culpa de el olla ni_que este haber ejercer influencia alguno sobre él
sólo el viejo stine y elsa lo creer pero nunca hablar de ello
rasmus yacer enfermo de fiebre contagioso por ese nadie ir a el casa de el sastre excepto juana el hijo de el zuequero el cual romper a llorar a el ver el acabado que estar el joven
el doctor le recetar algo de el farmacia pero él se negar a tomar el medicamento
qué más dar
decir
tomar lo y te curar le insistir su madre
confiar en dios y en ti mismo
gustosa dar mi vida por ver te otro vez con carne en el cuerpo cantar y silbar como antes
rasmus salir de su enfermedad pero su madre se contagiar y dios lo llamar a su seno en_vez_de a él
el casa quedar solitaria solitario y mísero
estar agotar decir en el parroquia
pobre_rasmus
en el curso de su viaje haber llevar uno vida desordenar
aquel y no el negro olla ser el que consumir su salud y poner el inquietud en su alma
el cabello se le aclarar y volver gris no hacer nada a derecha qué más dar
decir
ir más a el taberna que a el iglesia
uno anochecer de otoño se dirigir penosamente a su casa bajo el lluvia y el viento por el fangoso camino que conducir a el taberna
hacer ya mucho tiempo que su madre reposar en el sepultura
también se haber marchar el golondrina el estornino y el fiel pájaro pero juana el hijo de el zuequero no se haber ir
ser a su encuentro y lo acompañar uno trecho
hacer uno esfuerzo rasmus
qué más dar
responder él
no deber decir ese
le reñir juana
acordar te de el palabra de tu madre confía en dios y en ti
no lo hacer rasmus y tener que hacer lo
nunca decir qué más dar
así no hacer nunca nada
no lo dejar hasta el puerta de su casa pero él en_vez_de entrar se dirigir a el viejo sauce sentar se en el hito derribar
el viento silbar entre el rama de el árbol ser como uno canción como uno discurso
rasmus responder hablar en voz alto pero nadie lo oír aparte el árbol y el viento
qué frío
ser hora de acostar me
dormir dormir
y se ir mas no a su casa sino a el estanque donde caer desfallecer
llover a torrente y el viento ser helado pero él no se dar_cuenta
cuando salir el sol y el corneja reanudar su vuelo sobre el cañaveral rasmus despertar medio morir
si se haber caer con el cabeza donde le quedar el pie no se haber volver a levantar el lenteja de agua haber ser su mortaja
a el hacer se de día juana volver a casa de el sastre él ir su amparo lo llevar a el hospital
nos conocer de niño le decir
tu madre me dar mucho vez de comer y de beber y nunca se lo agradecer bastante
tú recobrar el salud volver a ser uno hombre y a vivir
y dios disponer que seguir vivir pero el salud y el facultad se haber perder para siempre
volver el golondrina reanudar su vuelo y se marchar de nuevo uno y otro vez
rasmus envejecer antes_de tiempo
vivir solo en su casa que ir decaer visiblemente
ser pobre más aun que juana
no tener fe le decir él
si no ser por dios qué nos quedar
tener que ir a tomar el comunión
seguramente no haber volver desde que te confirmar
bah
qué más dar
replicar él
si decir el que pensar dejar lo
el señor no querer a su mesa invitado forzar
pero pensar en tu madre y en tu niñez
erar uno muchacho bueno y piadoso
querer que te cantar uno canción de infancia
qué más dar
replicar él
a mí siempre me consolar decir él
juana ser uno santo
y lo mirar con ojo cansar y apagar
juana cantar el canción pero no leer lo de uno libro pues no tener ninguno sino de memoria
qué palabra más hermoso
decir él
pero no haber poder seguir lo bien
tener el cabeza tan pesar
rasmus ser ya viejo y elsa no ser joven tampoco
nosotros mencionar su nombre aunque rasmus no lo hacer nunca
ser ya abuelo y tener uno nieto muy travieso
el chiquillo jugar con el otro niño de el pueblo y rasmus se acercar a el grupo apoyar en su bastón y se quedar parar mirar lo sonriente como si su imaginación evocar tiempo pretérito
el nietecito de elsa gritar señalar lo pobre_rasmus
y el demás niño seguir su ejemplo
pobre_rasmus
repetir y todo se poner a perseguir a el viejo con gran griterío
ser TM_d:1 gris y agobiante a el que seguir otro mucho pero después_de el día agobiante y gris venir a el fin uno de sol
uno magnífico mañana de pentecostés el iglesia aparecer adornar con verde rama de abedul que impregnar el aire con el aroma de el bosque mientras el sol brillar sobre el banco
el grande candelabro de el altar estar encender se administrar el comunión y juana figurar entre el fiel arrodillar pero rasmus no se hallar presente
aquel mismo mañana dios lo haber llamar a sí
dios ser el gracia y el misericordia
haber transcurrir mucho año desde aquel mañana
el casa de el sastre seguir en pie pero nadie lo habitar el noche menos pensar uno tormenta lo hundir
el estanque estar invadir de caña y junco
el viento silbar aun en el viejo árbol se decir que se oír uno canción el viento lo cantar el árbol lo recitar si no lo comprender ver a preguntar se lo a el viejo juana el de el asilo
en el asilo vivir y cantar su canción piadoso aquel mismo que cantar a rasmus
él pensar en él y rezar por él a dios_nuestro_señor
poder contar mucho cosa de el tiempo pasar recuerdo que murmurar en el viejo árbol
fin
