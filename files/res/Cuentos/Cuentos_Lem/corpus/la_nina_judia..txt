asistir a el escuela de pobre entre otro niño uno muchacho judío despierto y bueno el más listo de el colegio
no poder tomar_parte en uno de el lección el de religión pues el escuela ser cristiano
durante el clase de religión le permitir estudiar su libro de geografía o resolver su ejercicio de matemáticas pero el chiquillo tener terminar muy pronto su deber
tener delante uno libro abrir pero él no lo leer escuchar desde su asiento y el maestro no tardar en dar_cuenta de que seguir con más atención que el demás alumno
ocupar te de tu libro le decir con dulzura y gravedad pero él lo mirar con su brillante ojo negro y a el preguntar le comprobar que el niño estar mucho más enterar que su compañero
haber escuchar comprender y asimilar el explicación
su padre ser uno hombre de bien muy pobre
cuando llevar a el niño a el escuela poner por condición que no lo instruir en el fe cristiano
pero se temer que si salir de el escuela mientras se dar el clase de enseñanza religioso perturbar el disciplina o despertar recelo y antipatía en el demás y por ese se quedar en su banco pero el cosa no poder continuar así
el maestro llamar a el padre de el chiquillo y le decir que deber elegir entre retirar a su hijo de el escuela o dejar que se hacer cristiano
no poder soportar su mirada ardiente el fervor y anhelo de su alma por el palabra de el evangelio añadir
el padre romper a llorar yo mismo saber muy poco de nuestro religión decir pero su madre ser uno hijo de israel firme en su fe y en el lecho de muerte le prometer que nuestro hijo nunca ser bautizar
deber cumplir mi promesa ser para mí uno pacto con dios
y el niño ser retirada de el escuela de el cristiano
haber transcurrir alguno año
en uno de el ciudad más pequeño de jutlandia servir en uno modesto casa de el burguesía uno pobre muchacho de fe mosaico llamar sara tener el cabello negro como ébano el ojo oscuro pero brillante y luminoso como soler ser habitual entre el hijo de el oriente
el expresión de el rostro seguir ser el de aquel niño que desde el banco de el escuela escuchar con mirada inteligente
cada [G:??/??/??:????:??] llegar a el calle desde el iglesia el son de el órgano y el cántico de el fiel llegar a el casa donde el joven judío trabajar laborioso y fiel
guardar el [S:??/??/??:????:??] ordenar su religión pero el [S:??/??/??:????:??] ser para el cristiano día de labor y sólo poder observar el precepto en el más íntimo de su alma y este le parecer insuficiente
sin embargo qué ser para dios el día y el hora
este pensamiento se haber despertar en su alma y el [G:??/??/??:????:??] de el cristiano poder dedicar lo él en parte a su propio devoción y como a el cocina llegar el son de el órgano y el coro para él aquel lugar ser santo y apropiar para el meditación
leer entonces el antiguo_testamento tesoro y refugio de su pueblo limitar se a él pues guardar profundamente en el memoria el palabra que decir su padre y su maestro cuando ser retirar de el escuela el promesa hacer a el madre moribundo de que sara no se hacer nunca cristiano que jamás abandonar el fe de su antepasado
el nuevo_testamento deber ser para él uno libro cerrar a_pesar_de que saber mucho de el cosa que contener pues el recuerdo de niñez no se haber borrar de su memoria
uno velada se hallar sara sentar en uno rincón de el sala atender a el lectura de el jefe de el familia le estar permitir puesto que no leer el evangelio sino uno viejo libro de historia por ese se haber quedar
tratar el libro de uno caballero húngaro que prisionero de uno bajá turco ser uncir a el arado junto_con el buey y tratado a latigazo el burla y malo trato lo haber llevar a el borde de el muerte
el esposo de el cautivo vender todo su alhaja y hipotecar el castillo y el tierra a_la_vez_que su amigo aportar cuantioso suma pues el rescate exigir ser enorme ser reunir sin embargo y el caballero redimir de el oprobio y el esclavitud
enfermo y achacoso regresar el hombre a su patria
poco después sonar el llamada general a el lucha contra el enemigo de el cristiandad el enfermo a el oír lo no se dar punto de reposo hasta ver se montar en su corcel su mejilla recobrar el color parecer volver su fuerza y partir a el guerra
y ocurrir que hacer prisionero precisamente a aquel mismo bajá que lo haber uncir a el arado y lo haber hacer objeto de todo suerte de burla y malo trato
ser encerrar en uno mazmorra pero al_poco_rato acudir a visitar lo el caballero y le preguntar qué creer que te esperar
bien lo saber responder el turco
tu venganza
sí el venganza de el cristiano reponer el caballero
el doctrina de cristo nos mandar perdonar a nuestro enemigo y amar a nuestro prójimo pues dios ser amor
volver en paz a tu tierra y a tu familia y aprender a ser compasivo y humano con el que sufrir
el prisionero prorrumpir en llanto cómo poder yo esperar el que estar ver
estar seguro de que me esperar el martirio y el tortura por ese me tomar uno veneno que me matar en poco hora
ir a morir no haber salvación posible
pero antes_de que terminar mi vida explicar me el doctrina que encerrar tanto amor y tanto gracia pues ser uno doctrina grande y divino
dejar que en él morir que morir cristiano
su petición ser atender
tal ser el leyenda el historia que el dueño de el casa leer en alto voz
todo lo escuchar con fervor pero sobre todo llenar de fuego y de vida a aquel muchacho sentar en el rincón sara el joven judío
grande lágrima asomar a su brillante ojo negro en su alma infantil volver a sentir como ya lo sentir antaño en el banco de el escuela el sublimidad de el evangelio
el lágrima rodar por su mejilla
no dejar que mi hijo se hacer cristiano
haber ser el último palabra de su madre moribundo y en su corazón y en su alma resonar aquel otro palabra de el mandamiento divino honrarás a tu padre y a tu madre »
no ser cristiano
me llamar el judío aun el [G:??/??/??:????:??] último me lo llamar en ser de burla el hijo de el vecino cuando me estar frente_a el puerta abrir de el iglesia mirar el brillo de el cirio de el altar y escuchar el canto de el fiel
desde mi tiempo de el escuela hasta ahora haber venir sentir en el cristianismo uno fuerza que penetrar en mi corazón como uno rayo de sol aunque cierre el ojo
pero no te afligir en el tumba madre no ser perjuro a el voto de mi padre no leer el biblia cristiano
tener a el dios de mi antepasado ante él poder inclinar mi cabeza
y transcurrir más año
morir el cabeza de el familia y dejar a su esposo en situación apurar
haber que renunciar a el muchacho pero sara no se ir sino_que acudir en su ayuda en el momento de necesidad contribuir a sostener el peso de el casa trabajar hasta alto hora de el noche y procurar el pan de cada día con el labor de su mano
ninguno pariente querer acudir en auxilio de el familia el viudo cada día más débil haber de pasar se mes entero en el cama enfermo
sara lo cuidar el velar trabajar dulce y piadoso ser uno bendición para el casa hundir
toma_la_biblia decir TM_d:1 el enfermo
leer me uno fragmento
ser tan largo el velada y sentar tanto deseo de oír el palabra de dios
sara bajar el cabeza doblar el mano sobre el biblia y abrir lo se poner a leer lo a el enfermo
a menudo le acudir el lágrima a el ojo pero aumentar en ellos el claridad y también en su alma madre tu hijo no poder recibir el bautismo de el cristiano ni ingresar en su comunidad lo querer así y respetar tu voluntad estar unir aquí en el tierra pero más_allá_de él
estar aun más unir en dios que nos guía y llevar allende el muerte
él descender a el tierra y después_de dejar lo sufrir el hacer más rico
lo comprender
no saber yo mismo cómo ser
ser por él en él_cristo
se estremecer a el pronunciar su nombre y uno bautismo de fuego lo recorrer todo él con más fuerza de el que el cuerpo poder soportar por el que caer desplomar más rendir que el enfermo a quien velar
pobre_sara
decir no haber poder resistir tanto trabajo y tanto vela
lo llevar a el hospital donde morir
lo enterrar pero no a el cementerio de el cristiano no haber en él lugar para el joven judío sino ser junto_a el muro allí recibir sepultura
y el hijo_de_dios que resplandecer sobre el tumba de el cristiano proyectar también su gloria sobre el de aquel doncella judío que reposar fuera_de el sagrado recinto y el cántico religioso que resonar en el camposanto cristiano lo hacer también sobre su tumba a el que también llegar el revelación hay uno resurrección en cristo
en él_el_señor que decir a su discípulo juan os haber bautizar con agua pero yo os bautizar en el nombre de el espíritu_santo »

