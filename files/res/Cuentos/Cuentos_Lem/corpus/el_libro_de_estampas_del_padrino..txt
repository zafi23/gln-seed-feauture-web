el padrino saber contar historia mucho y muy largo
y saber también recortar estampa y dibujar figura
cuando se acercar el navidades coger uno cuaderno de hoja blanco y limpio y en ellos pegar ilustración recortar de libro y periódico si no bastar para su propósito lo dibujar con su propio mano
de niño yo ir obsequiar con mucho de aquel libro de estampa pero el más hermoso de todo ser uno acerca_de el año memorable en que el gas sustituir en copenhague a el viejo farol de aceite de pescado título que figurar en 1 página
haber que guardar muy bien este libro me decir mi padre sólo lo sacar en ocasión solemne
el padre haber anotar en el tapa si romper el libro no ser uno gran delito
peor haber obrar más de uno amigo
el mejor ser cuando el padrino sacar el cuaderno leer en alto voz el verso y demás cosa escribir en él y luego se poner a contar
entonces sí que el historia se volver uno verdadero historia
en el 1 página haber uno estampa recortar de el correo_volante donde aparecer copenhague con el torre_redonda y el iglesia de nuestra_señora
a el izquierda haber pegar uno dibujo que representar uno viejo linterna con el letrero aceite y a el derecha estar uno candelabro con el palabra « gas »
fijar te en el portada decir el padrino
ser el introducción a el historia que ir a oír
también poder haber servir para uno comedia que haber poder titular se aceite y gas o el vida de copenhague
ser uno título sensacional
a el pie de el página aparecer todavía otro grabado que no ser muy fácil de interpretar por ese te lo descifrar ser uno caballo infernal
deber figurar a el fin de el libro pero se haber adelantar para advertir que ni el introducción ni el cuerpo de el obra ni su desenlace valer gran cosa
él lo haber hacer mejor si haber poder hacer lo
como te decir el caballo infernal durante el día ir enganchar a el periódico estar en el columna como decir pero a el anochecer se escapar y se situar ante el puerta de el poeta y relinchar para que el hombre que estar dentro se morir en_seguida pero no morir si haber en él vida verdadero
el caballo infernal ser casi siempre uno pobre diablo que andar desorientar pero necesitar aire y alimento para correr y relinchar
el libro de el padrino no le gustar ni pizcar de ese estar seguro razón de más para creer que no ser tan malo
mirar ahí tener el 1 página el portada
ser precisamente el último noche que se encender el viejo linterna de aceite
haber instalar gas en el ciudad y dar uno luz tan vivo que aquel pobre farol quedar casi eclipsar por completo
aquel noche yo salir a el calle decir el padrino
el gente circular en todo dirección para ver el nuevo iluminación
haber uno gran gentío casi doble número de pierna que de cabeza
el vigilante estar triste pues presentir que lo despedir como a el farol de aceite
este recordar su tiempo pasar ya_que no poder pensar en el venidero
recordar tanto y tanto cosa de el velada silencioso y de el noche oscuro
me apoyar en el poste de el farol y oír chisporrotear el aceite y el pabilo oír también el que decir el linterna y te lo repetir
haber hacer cuanto haber poder decir
servir a nuestro época lo alumbrar en el hora de alegría y en el de pena
haber presenciar mucho cosa notable poder decir que haber ser el ojo nocturno de copenhague
ahora el nuevo luz venir a ocupar nuestro puesto y desempeñar nuestro función
cuánto año ir a brillar y para qué lo hacer ser cosa que aun estar por ver
ser más luminoso que nosotros haber que reconocer lo pero qué tener ese de particular cuando lo fundir a uno en forma de poste con tanto conexión
todo se ayudar entre sí
tener cañería en todo el sentido y poder procurar se fuerza dentro y fuera_de el ciudad
en cambio nosotros el linterna de aceite haber de alumbrar con el que llevar dentro sin poder contar con el pariente
nosotros y nuestro abuelo haber estar alumbrar copenhague durante uno tiempo largo inacabable
mas poner que este ser el último noche que nos encender como si ser su ayudante no querer murmurar ni mostrar nos envidioso brillante compañero por_el_contrario estar alegre y complaciente
ser el viejo centinela a quien relevar alabardero de_nuevo_cuño vestir con mejor uniforme
le contar lo que nuestro linaje haber ver y vivir remontar nos hasta el abuelo todo el historia de copenhague
ojalá usted y su descendiente poder presenciar y narrar hasta el último poste de gas acontecimiento tan memorable el día en que como hoy nosotros tener que despedir lo día que le llegar sin duda
deber estar preparar para cuando venir
el hombre inventar seguramente uno iluminación más intenso que el gas yo haber oír decir a uno estudiante que alguno día se llegar a quemar agua de el marzo la mecha chisporrotear a el decir este el linterna tener el sensación de que ya lo estar empapar de agua
el padrino escuchar con atención y pensar que el viejo linterna haber tener uno excelente idea a el aprovechar aquel noche de cambio de el aceite por el gas para pasar revista a todo el historia de copenhague
jamás haber que desperdiciar uno bueno idea decir el padrino
yo lo adoptar enseguida me ir a casa y confeccionar este libro de estampa
se remontar aun a tiempo anterior a el de el linterna
haber aquí el libro y aquí ir el historia la vida de copenhague
empezar con uno tiniebla absoluto uno hoja negro como el carbón ser el época de el oscuridad
volver ahora el página decir el padrino
ver este grabado
sólo se ver el mar embravecer y el furioso viento nordeste
bloque de hielo por doquier nadie navegar por su agua aparte el enorme piedra que allá en noruega se precipitar de el roca sobre el hielo
el viento impeler el témpano como empeñar en enseñar a el montaña germano el peñasco que haber en el norte
el flota de hielo haber llegar ya a el estrecho de el costa zelandés donde se levantar hoy copenhague ciudad que entonces no existir
bajo el agua se extender grande banco de arena el bloque de hielo cargar con el enorme piedra chocar contra uno de ellos y todo el helada flotar se detener sin que el viento poder despegar lo de el fondo
por ese henchir de cólera maldecir el banco de arena el fondo de el ladrón como lo llamar jurar que si alguno día se elevar por encima_de el superficie marino desembarcar allí ladrón y bandido
pero mientras maldecir y protestar salir el sol y en su rayo se columpiar radiante espíritu bueno hijo de el luz que bailar por encima_de el frígido bloque de hielo y lo derretir por el que el grande piedra que estar presa en ellos se precipitar a el fondo sobre el banco de arena
chusma de el sol
gritar el viento nordeste
ser este camaradería y parentesco
ya me acordar para vengar me
lo maldecir
nosotros lo bendecir responder el hijo de el luz
el banco emerger y nosotros lo proteger
sobre él se levantar el bondad_la_verdad y el belleza
estúpido
gritar el viento
ver
de todo este nada saber el linterna decir el padrino pero yo sí lo saber y ser de gran importancia en el vida de copenhague
volver ahora el página añadir
haber pasar mucho año y el banco de arena se haber elevar
uno ave marino se haber posar sobre el mayor de el piedra el que más sobresalir de el agua
poder ver lo en el estampa
correr el año
el mar arrojar pez morir a el arena brotar tenaz carrizo se marchitar y podrir y abonar el suelo
nacer otro especie de hierba y el banco de arena se transformar en uno isla verdeante
desembarcar el vikingo estallar reyerta y desafío que ser otro tanto avenida de el muerte
en el holm_de_seeland haber uno bueno fondeadero
arder el 1 linterna de aceite crear que asar pescado sobre él abundar bastante
el arenque circular en enorme bandada por el sund hasta el extremo de dificultar el maniobra de el embarcación
brillar el agua como si en su seno estallar relámpago de calor el fondo relucir como uno aurora boreal
el sund ser rico en pez por ese se ir poblar el costa de seeland
el pared de el casa ser de roble y el tejado de corteza no ser árbol el que faltar
el barco entrar en el puerto el linterna de aceite arder balancear se en el jarcia mientras el viento nordeste soplar cantar huuui
si en el holm brillar uno linterna ser de bandido
contrabandistas y bandido prosperar en el isla de el ladrón
crear que el maldad ir extender se tal_y_como yo querer decir el viento nordeste
no tardar en venir el árbol de el que poder sacudir el fruto
y aquí tener el árbol continuar el padrino
ver el horca en el isla de el ladrón
de él colgar ladrón y asesino tal_y_como se hacer entonces
el viento soplar hacer chocar entre sí el largo esqueleto y el luna brillar satisfacer sobre ellos como brillar hoy sobre uno fiesta campestre
también el sol enviar contento su rayo ayudar a que se podrir el colgante osamenta y desde su rayo cantar el hijo de el luz lo saber lo saber
en tiempo venidero este ser hermoso
ser uno tierra bello y feliz
necio palabra
refunfuñar el viento
volver otro página decir el padrino
doblar el campana en el ciudad de roeskilde residencia de el obispo absalón hombre que lo mismo leer el biblia que blandir el espada
tener poder y voluntad y se haber proponer proteger contra el pillaje a el laborioso pescador de el puerto de aquel ciudad que entretanto haber crecer y convertir en centro comercial
mandar rociar con agua bendecir aquel suelo infame se restituir el honra a el isla de el ladrón
albañiles y carpintero poner mano a el obra por iniciativa de el obispo pronto se levantar uno edificio
el rayo de el sol besar su rojo muro
así surgir el casa_de_axel
castillo con torreón firme en el tormenta muro que desafiar el siglo
huuuh
venir el viento norte con su hálito helado
soplar arremeter mas el castillo no ceder
y en el lugar se levantar copenhague el puerto de el comerciante
morada de sirena entre lago brillante construida en el verde floresta
acudir el extranjero a comprar pescado levantar tienda y casa en cuyo ventana el vejiga de cerdo hacer de cristal pues el vidrio ser muy caro surgir granero con pináculo y polea
ver
en este tienda estar el solterón el que no poder casar se comerciar con jengibre y pimienta ser el pimentero
el viento nordeste pasear su ráfaga por el calle y calla arremolinar el polvo arrancar alguno que otro tejado de paja
vaca y cerdo se meter en el arroyo
a puñada y empujón me llevar el casa en_torno_a el castillo de axel
no poder equivocar me
lo llamar steileborg_de_tyvsö
y el padrino me mostrar uno dibujo hacer por él mismo
junto_a el muro se alinear el palo de cada uno de el cual pender el cabeza de uno pirata capturar regañar el diente
este haber suceder de verdad afirmar el padrino convenir saber lo y comprender lo
el obispo absalón estar en el baño y a_través_de el delgado pared oír que se acercar uno barco corsario
salir inmediatamente subir a su barco y tocar el cuerno a cuyo ser acudir el tripulación y el flecha volar y se clavar en el espalda de el pirata
este tratar de huir remar con todo su fuerza el flecha le herir en el mano pero no haber tiempo para arrancar lo
el obispo capturar a todo el que haber quedar con vida y mandar decapitar lo y exhibir el cabeza en el muralla de el castillo
el viento nordeste soplar con todo el fuerza de su carrillo hinchar con mal tiempo en el boca como decir el marino
me estirar aquí decir el viento
echar en este lugar ver todo este negocio
se quedar encalmar varios hora soplar luego durante día y noche
transcurrir año
salir el guardián de el torre de el castillo y mirar a el este a el oeste a el norte y a el sur
ahí lo tener en este estampa decir el padrino señalar lo
ahí estar y ahora te decir el que ver
ante el muralla de steileborg se desplegar a el mar hasta el golfo_de_kjöge el canal que seguir hasta el costa de seeland ser muy ancho
frente_a serritslev_mark y solbjerg_mark donde estar el grande poblado prosperar el nuevo ciudad con su casa de pared entramar y fachado en hastial
haber callejón entero ocupar por zapatero y curtidor abacero y cervecero haber uno plazamercado uno casa gremial y junto_a el playa donde anteriormente haber uno isla se levantar el magnífico iglesia de san_nicolás
tener uno torre y uno espira alto uno y otro se reflejar bellamente en el agua límpido
no lejos_de allí se encontrar el iglesia de nuestra_señora donde rezar y cantar misa oler el incienso y arder el cirio
copenhague ser ahora el sede de el obispo el obispo de roeskilde lo regir y gobernar
otro prelado llamar erlandsen ocupar el casa de axel
en el cocina estar asar se servir cerveza y vino especiado mientras sonar violín y timbal
arder cirio y lámpara el palacio relucir como uno linterna encender para iluminar todo el país y todo el reino
el viento nordeste soplar a poniente en_torno_a el fortificación de el ciudad que no ser sino uno vallado de plancha
con_tal_que resistir
fuera estar el rey de dinamarca_cristóbal_i_los sublevar lo derrotar en skjelskör y ahora buscar refugio en el ciudad de el obispo
el viento silbar decir le como el prelado quédate fuera
quedar te fuera
el puerta estar cerrar para ti
atravesar uno época de descontento el día ser difícil
todo querer gobernar
el bandera de el holstein ondear en el torre de el castillo haber privación y sufrimiento ser el noche de el terror guerra en el país y el muerte negro uno noche tenebroso pero luego venir waldemar_atterdag
el ciudad de el obispo ser ahora el ciudad de el rey
tener casa de hastial y estrecho callejón tener guarda y uno casa consistorial en el puerta de poniente se alzar uno horca amurallar
ninguno forastero poder ser ahorcar en él
haber que ser ciudadano de el capital para tener el privilegio de colgar allí tan alto dominar kjöge y su pollo
magnífico horca
exclamar el viento nordeste
ser uno adorno para el paisaje
y venir soplar y arremeter
de alemania llegar el aflicción y el miseria
venir el hansas decir el padrino venir de rostock_lubeck y brema pretender algo más que apoderar se de el ganso de oro de el torre de waldemar
en el capital de dinamarca mandar más que el mismo rey venir en barco armar
nadie estar preparar y por otro parte el rey erich no desear pelear se con su primo alemán ser mucho y muy fuerte
el monarca y su cortesano se precipitar por el puerta de poniente dirigir se a sorö junto_a el lago tranquilo y el verde bosque entre canción de amor y chocar de copa
sin embargo se haber quedar en copenhague uno corazón real uno verdadero cabeza de rey
ver este figura este mujer joven delicado y fino de ojo azul y cabello de lino
ser el reina de dinamarca_felipa princesa de inglaterra
él se quedar en el aterrorizar ciudad en cuyo angosto callejón y calle de empinar escalera y cerrar tenduchos el ciudadano correr a el desbandada totalmente desorientar
él tener el valor y el corazón de uno hombre llamar a el ciudadano y a el campesino lo animar lo estimular
se aparejar el nave se equipar el fortín el cañón retumbar vomitar fuego y humo
volver el ánimo
dios no abandonar a dinamarca y el sol brillar en todo el corazón mientras el júbilo de el victoria iluminar el ojo
bendecir ser felipa
el bendición en el choza en el hogar en el palacio real donde ser atender el herido y enfermo
haber recortar $_DKK:1 para poner lo como marco a este estampa
bendecir ser el reina felipa
saltar ahora alguno año continuar el narrador
copenhague saltar con ellos
el rey cristián_i haber estar en roma_el_papa le haber dar su bendición y en todo el largo camino haber ser objeto de homenaje y honor
en su país levantar uno casa de piedra cocer en él prosperar el ciencia que ser difundir en latín
el hijo de el familia humilde de el terruño y de el taller poder venir también abrir se paso a_fuerza_de mendigar llevar el largo y amplio manto negro cantar frente_a el puerta de el ciudadano
junto_a el casa de el ciencia donde todo se decir en latín haber otro casita en el que reinar el lengua y el costumbre danés
para desayuno se servir sopa de cerveza y se almorzar a el [??:??/??/??:1000:am]
a_través_de el pequeño cristal brillar el sol en el alacena y en el librería en el cual se guardar tesoro literario como el rosario y « comedia piadoso » de el señor_miguel el « recetario_de_henrik_harpenstren » y el « crónica rimar danés » de el hermano niels_de_sorö
todo danés deber conocer lo decir el dueño de el casa y este ser el hombre llamar a divulgar lo
ser el 1 impresor de dinamarca el holandés godofredo_de_gehmen
practicar el bendecir arte negro el imprenta
y el libro llegar a el real palacio y a el casa de el burgués
proverbios y canción adquirir vida imperecedero
el que el hombre no saber expresar en poema y canción lo cantar el pájaro de el canción popular con palabra florido pero claro
volar libre y volar lejos a el aposento de el servicio y a el castillo señorial gorjear se posar como el halcón en el mano de el amazona se deslizar como uno ratoncillo y se poner a piar ante el siervo campesino en el perrera
charla vaciar
exclamar el acerar viento nordeste
ser primavera
replicar el rayo de el sol
mirar cómo asomar el verde hierba
seguir hojear en nuestro libro de estampa decir el padrino
cómo resplandecer copenhague
torneos y juego magnífico desfile
mirar el noble caballero en su armadura el encopetar dama vestir de seda y oro
el rey hans otorgar a el elector_de_brandeburgo el mano de su hijo isabel
qué joven ser y qué contentar estar
andar sobre terciopelo en su ojo brillar el porvenir el felicidad de el vida doméstico
a su lado avanzar su real hermano el príncipe cristián de ojo melancólico y sangre ardiente y alborotar
el burgués lo querer él conocer su cuita el futuro de el pobre vivir en su pensamiento
sólo dios conceder el felicidad
adelante con nuestro libro de estampa
proseguir el padrino
el viento soplar furioso cantar el agudo espada el tiempo difícil y sin paz
ser TM_d:1 gélido de mediar de [??:??/4/??:????:??]
por qué el multitud se apretujar frente_a el palacio frente_a el viejo aduana donde estar anclar el nave real izar el bandera y el vela extender
se ver gente en el ventana y el tejado
reinar el dolor y el aflicción el incertidumbre y el miedo
todo el mirada se concentrar en el castillo en cuyo dorada sala se bailar otrora el danza de el antorcha mientras hoy aparecer silencioso y desierto
mirar a el ventana de el torreón desde el cual el rey cristián tanto vez seguir con el vista a el otro lado de el puente_de_la_corte y de el estrecho callejón a su palomita el muchacho holandés que haber traer de el ciudad de bergen
el postigo estar cerrar el multitud mirar a el palacio haber aquí que se abrir el puerta y se bajar el puente levadizo
ahí venir el rey cristián con su fiel consorte isabel que se negar a abandonar a su real esposo en el hora de el desgracia
haber fuego en su pecho fuego en su pensamiento
querer romper con el viejo tiempo romper el yugo de el campesino favorecer a el burgués cortar el ala a el voraz cernícalo
pero ser demasiar
helo ahí abandonar su patria y su reino para ganar se en el extranjero amigo y pariente
su esposo y su leal lo acompañar todo el ojo estar húmedo a aquel hora de el separación
se mezclar el voz que entonar el canción de el tiempo en su favor en su contra uno triple coro
escuchar el palabra de el nobleza pues haber quedar escribir y imprimir maldición sobre ti cristián_el_malvado
el sangre verter en el mercado de estocolmo clamar venganza contra ti y te maldecir
también el coro de el monje expresar el mismo sentencia repudiado ser por dios y por nosotros
traer a este tierra el doctrina luterano le entregar el iglesia y el púlpito permitir que hablar el lengua de el demonio
maldición sobre ti cristián_el_malvado
pero el campesino y el burgués llorar cristián rey bondadoso
el campesino no haber de ser vender como ganar ni trocar por uno perro de caza
este ley ser tu ejecutoria
pero el palabra de el humilde ser como paja a el viento
pasar ahora el barco por delante_de el palacio y el ciudadano correr a lo alto de el muralla para decir uno último adiós a el real nave
largo ser el tiempo y tenebroso
no te fiar de el amigo no te fiar de el pariente
tío_federico de el castillo de kiel ambicionar el trono
el rey federico estar ante copenhague
ver este estampa copenhague_la_leal
se cerner sobre él negro nubarrón grabado tras grabado fijar te en cada uno
en uno estampa ruidoso resonar todavía en el leyenda y en el canción el tiempo ser duro difícil amargo
quotqué ser de el rey cristián el ave sin rumbo
lo haber cantar el pájaro que volar lejos allende el tierra y el mar
el cigüeña llegar pronto en primavera procedente de el sur a_través_de el país germano
haber ver el que ir a contar
ver a el fugitivo rey cristián cruzar el erial
lo esperar allí uno mísero carruaje tirar por uno caballo
ir en el vehículo su hermano el margravesa de brandeburgo que su marido expulsar por haber se mantener fiel a el doctrina luterano
en el oscuro páramo se encontrar el proscribir hijo de el rey
largo ser el tiempo y angustioso no confiar en tu amigo y pariente
el golondrina llegar de el castillo de sönderborg entonar uno canción plañidero el rey cristián haber ser traicionar
yacer allí encerrar en el profundo torre su grave paso dejar huella en el pavimento de piedra su dedo grabar signo en el duro mármol ah
qué dolor hallar palabra como el que oír el duro piedra
de el mar embravecer venir el quebrantahuesos
el mar ser amplio y libre y lo surcar uno barco tripular por el valeroso fionés sören_nordby
el fortuna lo acompañar pero el fortuna ser veleidoso como el viento y el tiempo
en jutlandia y en fionia gritar el cuervo y el corneja avanzamos
el cosa ir bien muy bien
yacer allá cadáver de caballo y de hombre
ser uno época de inquietud con el querella de el conde
el campesino empuñar su maza el comerciante su cuchillo y todo echar a gritar degollaremos el lobo hasta que no quedar ni uno lobezno
nubes y humo subir de el ciudad incendiar
el rey cristián estar prisionero en el castillo de sönderborg no poder escapar no ver copenhague ni su extremo miseria
en el herbazal a el norte de el ciudad estar cristián_iii allí donde estar su padre
en el capital reinar el terror el hambre y el peste
apoyar contra el pared de el iglesia yacer el cadáver de uno mujer vestir de harapo 2 criatura vivo sentar en su regazo chupar sangre de el pecho de el morir
el valor haber ceder ceder el resistencia
oh tú leal copenhague
resonar clarín
escuchar el timbal y el trompeta
en rico traje de seda y terciopelo con pluma ondeantes se acercar el noble montado en caballo guarnecer de oro cabalgar hacia el altmark
haber allí alguno torneo alguno lucha a el antiguo usanza
burgueses y campesino endomingar se encaminar también allí
a ver qué
acaso haber erigir uno pira para quemar imagen papista o estar allí el verdugo como estar en el pira de slaghoek
el rey señor de el país ser luterano haber que reconocer lo y proclamar lo en todo forma
distinguir dama y noble doncella con alto cuello y lucir perla en el cofia estar sentar detrás_de el abrir ventana contemplar aquel esplendor
sobre uno paño extender y bajo uno dosel se sentar el consejo_de_el_reino en su traje antiguo cerca de el trono real
el monarca permanecer silencioso
su voluntad y el de el consejo ser leer en alto voz y en lengua danés burgués y campesino haber de oír palabra duro duro reconvención por el resistencia que oponer a el alto nobleza
el ciudadano ser humillar el campesino se convertir en esclavo
luego se alzar voz de condenación contra el obispo de el país
su poder haber terminar
todo el bien de el iglesia y de el convento pasar a el rey y a el nobleza
reinar el soberbia y el odio reina el ostentación reina el desolación
ave pobre ir cojear cojear
ave rico rauda ir rauda ir
el tiempo de transformación traer conseguir negro nube pero también sol
haber luz ahora en el casa de el ciencia en el hogar de el estudiante y nombre de entonces brillar aun hoy
hans_tausen el pobre hijo de el herrero de fionia_fue aquel mozo de el ciudad de birken
su nombre pervivir en el memoria danés
lutero danés luchar con el espada de el verbo y vencer con el espíritu en el corazón de el pueblo
brillar allí el nombre de petrus_palladius latinizar de el danés peter_plade obispo de roeskilde hijo asimismo de uno pobre herrero de el tierra jutlandesa
y entre el apellido nobiliario destacar el de hans_friis canciller de el reino
sentar a el estudioso a su mesa cuidar de ellos y de el alumno
uno por encima_de todo ser objeto de uno hurra y de uno canción mientras mojar uno estudiante su pluma en el puerto de axel el obra de el rey cristián ser saludar con hurras
en aquel tiempo de transformación el rayo de el sol atravesar el tupir nube
ahora volver el página
qué ser el que silbar y cantar en el gran_belt junto_a el costa de samsö
emerger de el mar uno sirena de cabellera verde como el alga y predecir a el campesino nacerá uno príncipe que ser uno rey poderoso y grande
nacer en el campo bajo el oxiacanto florido
hoy su nombre brillar en leyenda y canción en_torno_a el castillo feudal y el palacio
surgir el bolsa con su torre y su espira se levantar rosenborg muy por encima_de el muralla el estudiante tener su casa propio junto_a el cual se alzar el torre_redonda señalar a el cielo uno columna de urania que dominar el isla_de_hveen donde yacer uranienborg su dorar cúpula brillar a el luz de el luna y el sirena cantar acerca_de el hombre que morar en él el genio de noble sangre tycho_brahe a quien visitar rey y hombre ilustre
a tal altura llevar el nombre de dinamarca que él y el cielo estrellar ser conocer en todo el país civilizar de el globo
mas dinamarca lo repudiar
en su dolor se consolar con uno canción no estar el cielo por doquier
qué más necesitar entonces
su canción tener el vida de el canción popular como el de el sirena de cristián_iv
venir ahora uno página que deber considerar con atención decir el padrino
el estampa seguir el estampa como el verso en el canción popular
ser uno poesía tan alegre en su comienzo como triste en el final
uno princesa danzar en el palacio real qué precioso estar
mirar lo sentada en el rodilla de cristián_iv
ser su hijo querido leonor
crecer en el virtud y cualidad que adornar a uno mujer
el hombre más ilustre de el poderoso nobleza korfitz_ulfeldt ser su prometido
él ser uno niño todavía someter a el azote de su severo ayo él se quejar a su amado y hacer bien
qué listo ser qué cortés y instruir
saber griego y latín cantar en italiano a el son de su laúd ser capaz de hablar acerca_de el papa y de lutero
el rey cristián yacer de_cuerpo_presente en el capilla de el catedral de roeskilde el hermano de leonor subir a el trono
en el palacio de copenhague todo ser esplendor y magnificencia belleza y talento y por encima_de todo destacar el reina_sofía_amalia_de_luneburgo
quién saber como él dominar el caballo
quién ser tan elegante en el baile
quién hablar con tanto erudición y ingenio como el reina de dinamarca
leonor_cristina_ulfeldt
así decir el embajador francés ésta superar a todo en belleza y inteligencia
en el suelo liso de el palacio crecer el cardo de el maldad
fuertemente agarrar propagar a su alrededor el sarcasmo y el injuria la bastardo
su coche siempre parar junto_a el puente de palacio donde ir el reina allí deber ir él
el calumnia el invención el mentira dar su fruto
y en el noche silencioso ulfeldt coger el mano de su esposo
tener el llave de el puerta de el ciudad y abrir uno de ellos
el caballo aguardar a el exterior
galopar a lo largo de el orilla camino de el tierra de suecia
volver el página de el mismo modo que el suerte volver el espalda a el 2
ser otoño con su día corto y su largo noche gris estar el cielo y húmedo
el viento soplar frío aumentar por momento su violencia
rugir entre el follaje de el bosque el hoja volar al_interior_de el mansión de peder_oxe desierto y abandonar por su dueño
y el viento silba sobre chistianshavn en_torno_a el morada de kai_lykke ahora ser uno cárcel
él haber ser proscribir infamar su escudo de arma aparecer romper y su efigie cuelga de el horca más alto
de este modo haber ser castigar su petulante y ligero palabra sobre el venerar reina de el país
aullar el viento volar por el solar abandonar donde se levantar el mansión de el mayordomo imperial hoy sólo quedar de él uno piedra
lo arrojar como uno guijarro sobre el hielo flotante decir el viento el piedra quedar varar en el lugar donde TM_d:1 surgir el isla de el ladrón maldito por mí después venir a parar a el palacio de el señor de ulfeldt donde el castellano cantar a el son de el laúd leer en griego y en latín y llevar erguir el cabeza
ahora quedar sólo el piedra con su inscripción para eterno ludibrio y vergüenza de el traidor corfitz_ulfeldt
dónde estar ahora el noble dama
huuihuui
silbar el viento con voz de nieve
llevar ya mucho año en el torre azul detrás_de el castillo donde el ola se estrellar contra el muralla cenagoso
en el recinto haber más humo que calor el ventanita quedar muy alto junto_a el techo
el niño mimar de el rey cristián el distinguir señorito el noble dama qué pobre y miserable vivir ahora
el recuerdo extender cortina y tapiz sobre el pared ennegrecer de el cárcel
el mujer pensar en el tiempo feliz de su juventud en el rasgo bondadoso y radiante de su padre pensar en su magnífico viaje de boda en el día de su encumbramiento en el de miseria en holanda_inglaterra y bornholm
nada ser demasiado gravoso para el amor verdadero
pero entonces estar él a su lado y ahora estar solo solo para siempre
no saber dónde estar su tumba nadie lo saber
lealtad a el hombre ser todo su crimen
pasar allí mucho y largo año mientras fuera bullir el vida
nunca se detener pero nosotros nos parar uno instante a pensar en aquel mujer y en el que decir el canción fui fiel a el esposo en el honor en el desgracia y en el gran dolor
ver este grabado
decir el padrino
estar en invierno el hielo tender uno puente entre laaland y fionia uno puente para carlos_gustavo que avanzar arrollador
el pillaje y el incendio el terror y el miseria reinar en todo el país

