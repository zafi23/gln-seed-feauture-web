haber llegar uno pato de portugal alguno sostener que de españa pero dar el mismo el caso ser que lo llamar el portugués
ser hembra poner huevo lo matar y lo asar
este ser su historia
todo el polluelo que salir de su huevo heredar el nombre de portugués con el cual se poner bien en claro su nobleza
ahora de todo su familia quedar sólo uno hembra en el corral confundir con el gallina entre el cual el gallo se pavonear con insoportable arrogancia
me herir el oído con su horrible canto decir el portugués
no se poder negar que ser hermoso aunque no ser de el familia de el pato
sólo con que saber moderar se uno poco
pero el moderación ser virtud propio de persona educar
fijar te en este pajarillo cantor que vivir en el tilo de el jardín vecino
ese sí que ser cantar
sólo de oír lo me conmover
a su canto lo llamar portugal como a todo el exquisito
cuánto querer tener uno pajarito así a mi lado
ser para él uno madre tierno y cariñoso
lo llevar en el sangre en mi sangre portugués
y mientras decir este llegar uno de aquel pájaro cantor caer de cabeza desde el tejado y aunque el gato estar a el acecho lograr escapar con uno ala rotar y se meter en el corral
el gato tener que ser este escoria de el sociedad
exclamar el pato
bien lo conocer de el tiempo en que tener patio
que uno ser de su ralea tener vida y poder correr por el tejado
no crear que este se permitir en portugal
y compadecer a el pajarillo y lo compadecer también el demás pato que no ser portugués
pobre animal
decir acercar se a ver lo uno tras otro es verdad que no saber cantar confesar pero sentir el música y haber algo en nosotros que vibrar a el oír lo
todo nos dar_cuenta aunque no querer hablar de ello
pues yo sí querer hablar de ello declarar el portugués y hacer algo por el pajarillo ser uno deber que tener
a el decir este se subir de uno aletazo a el abrevadero y se poner a chapotear en el agua con tal furia para remojar el ave que por poco lo ahogar
pero el intención ser bueno
ser uno bueno acción decir y el demás deber tomar ejemplo
pip
decir el pajarillo intentar sacudir se el agua de el ala romper
le ser difícil mover el ala pero comprender que el pato lo haber remojar con bueno intención
ser usted muy bueno señor
decir temblar ante el idea de recibir uno 2 ducha
nunca haber reflexionar sobre mi sentimiento decir el portugués pero saber que amar a todo mi semejante menos a el gato ese nadie poder exigir me lo devorar a 2 de mi pequeño
pero acomodar se como si estar en su casa
también yo ser oriundo de uno país lejano ya lo haber notar usted en mi porte y en mi plumaje
mi marido no ser de mi casta ser de el país
mas no creer que yo ser orgulloso
si alguien en este corral poder comparar se con usted ese ser yo se lo asegurar
se le haber meter portugal en el mollera decir uno patio ordinario que ser muy chistoso y el otro de su clase celebrar mucho su ocurrencia y se acercar atropelladamente gritar guac
enseguida trabar amistad con el pajarillo
el portugués hablar bien haber que reconocer lo decir
a nosotros el palabra nos salir con dificultad de el pico pero interés sí tener
y si nada poder hacer por usted a el menos no lo aturdir con nuestro cháchara y ese nos parecer el mejor de todo
tener usted uno voz delicioso observar uno de el más viejo
deber de ser uno gran dicha el poder hacer disfrutar a tanto
yo confesar que el canto no ser mi fuerte por ese estar con el pico cerrar el cual siempre valer más que decir tontería como tanto hacer
no lo molestar decir el portugués
necesitar descanso y cuidar
pajarillo querer que volver a remojar lo
oh no gracia dejar que me secar
suplicar el interpelar
pues para mí el hidroterapia ser el mejor observar el portugués
el distracción ser también uno bueno remedio
no tardar en venir a visitar nos el gallina de a el lado haber entre ellos 2 chino que llevar pantalón ser muy culto y distinguir y además ser importar el cual lo elevar mucho en mi concepto
llegar el gallina y con ellos el gallo el cual estar muy cortés y no decir grosería
ser usted uno excelente cantor decir iniciar el conversación y saber sacar de su voz todo el partido posible haber cuenta de el débil que ser
ahora que para revelar el virilidad mediante el potencia de el canto le hacer faltar uno fuerza de locomotora
el 2 chino a el ver a el pajarillo quedar embelesar
por efecto de el ducha recibir estar el pobrecillo tan desgreñar que se parecer mucho a uno pollito chino
ser encantador
exclamar acercar se para entrar en relación con él
hablar cuchichear y en el lengua de el p que ser el usar por el chino distinguir
nosotros pertenecer a su especie
el pato incluso el portugués ser ave acuático seguramente ya lo haber observar
usted no nos conocer todavía pero cuánto relación tener y cuánto estar impaciente por conocer nos
vivir entre el gallina aunque nacer para ocupar uno barra más alto que el mayoría de el demás
pero dejar este
convivir con el otro cuyo principio no ser el nuestro sin meter nos con nadie procurar ver sólo el lado bueno de el cosa y hablar únicamente de el acción virtuoso por difícil que ser encontrar lo donde no lo haber
mas hablar con franqueza aparte nosotros 2 y el gallo no haber nadie en el gallinero que valer nada ni ser honorable
en_cuanto_a el habitante de el corral de pato andar se con cuidado
se lo advertir pajarito
ver aquel derrabado de allá
no se fiar ser falso y insidioso
aquel de pluma de color con uno lunar en el ala ser pendenciero y siempre querer llevar el razón a_pesar_de que no lo tener nunca
aquel pato gordo de allá hablar mal de todo el mundo el cual ser contrario a nuestro temperamento
si uno no tener nada bueno que decir deber cerrar el pico
el portugués ser el único que poseer cierto cultura y con quien se poder alternar pero ser muy apasionar y hablar demasiado de portugal
ir modo de cuchichear ese chino
decir alguno pato
ser uno pesada nunca haber hablar con ellos
en este llegar el marido de el portugués quien cometer el indelicadeza de tomar a el pájaro cantor por uno gorrión
no ver el diferencia decir cuando se le sacar de su error pero me importar uno bledo
ser uno niñería qué más dar
no tomar a mal su palabra le cuchichear el portugués
en su profesión ser apreciable y este ser el principal
ahora me retirar a descansar ser nuestro obligación engordar hasta que sonar el hora de ser embalsamar con manzana y ciruela
así decir se echar a el sol guiñar el ojo
estar tan bien y tan cómodo
y dormir a_sus_anchas
el pajarillo se le acercar a salto estirar el ala herida y se instalar a el lado de su protector
el sol enviar su calor confortante ser uno lugar ideal
el gallina de el vecino gallinero que haber venir de visita todo ser corretear y escarbar al_fin_y_a_la_postre el que lo haber traer ser el esperanza de llenar se el buche
el chino ser el 1 en marchar se y poco después lo seguir el otro
el patio chistoso decir de el portugués que pronto volver a ser mamaíta a el oír el cual el demás soltar el carcajada
ser para reventar de risa
decir y aprovechar el ocasión para repetir se el chiste anterior
qué gracioso ser aquel pato
finalmente el demás se echar también a dormir
llevar uno rato descansar cuando de pronto alguien tirar a el corral uno cubo de mondadura
a el ruido que hacer todo el compañía despertar sobresaltar con uno estrepitoso batir de ala
también el portugués despertar y en su precipitación por poco aplastar a el pajarillo
pip
gritar este
no me pisar de este modo bueno señor
por qué se poner en_medio_de el camino
replicar el otro
no haber que ser tan melindroso
también yo tener nervio y sin embargo nunca haber decir pip
no se enojar
 se excusar el ave
se me escapar el pip
de el boca
el portugués sin hacer le caso se precipitar sobre el mondadura y se zampar su bueno parte
cuando ya haber comer y volver a echar se el pajarillo querer mostrar se cariñoso se le acercar y le cantar uno cancioncita tilelelit
quivit quivit
de_todo_corazón te ir a cantar cuando por ese mundo volver a volar
quivit quivit
tilelelit
después_de comer solar echar uno siesta decir el pata
convenir que se acostumbrar usted a nuestro modo de vivir
ahora dormir
el pajarillo quedar el mar de confuso pues haber obrar con bueno intención
cuando el señor se despertar le ofrecer uno granito de trigo que haber encontrar
pero el dama haber dormir mal y por_consiguiente estar de mal humor
este ofrecer se lo a uno polluelo
gruñir
no se quedar ahí parar y no me fastidiar
estar enojar conmigo se lamentar el pájaro
deber haber hacer alguno disparate
disparar te
refunfuñar el portugués
ser uno palabra de muy mal gusto y le advertir que no tolerar el grosería
ayer lucir el sol para mí decir el pajarillo pero hoy hacer TM_d:1 oscuro y gris
qué triste estar
usted no saber nada de el tiempo replicar el pato
el día aun no haber terminar y no poner ese cara de tonto
me mirar usted con uno ojo tan airar como el que me acechar cuando caer a el corral
sinvergüenza gritar el portugués
comparar me con el gato ese animal de rapiña
ni uno gota de su malo sangre correr por mi vena
me hacer_cargo de usted y pretender enseñar le bueno modales
y le dar uno picotazo en el cabeza con tal furia que lo matar
cómo
decir
ni uno picotazo poder soportar
ahora ver que nunca se haber adaptar a nuestro modo de vivir
me portar con él como uno madre ese sí pues corazón no me faltar
el gallo vecino meter el cabeza en el corral cantar con su estrépito de locomotora
usted ser causa de mi muerte con su eterno griterío
decir el pata
de todo el ocurrir tener el culpa usted
él haber perder el cabeza y haber faltar poco para que yo perder también el mío
no ocupar mucho espacio el pajarito
decir el gallo
hablar de él con más respeto
replicar el portugués
tener voz saber cantar y ser muy ilustrar
ser cariñoso y tierno y este convenir tanto a el animal como a ese que llamar persona humano
todo el pato se congregar en_torno_a el pobre pajarillo morir
el pato tener pasión violento o lo dominar el envidia o ser uno dechado de piedad y como en aquel ocasión no existir ninguno motivo de envidia se sentir compasivo y el mismo le suceder a el 2 gallina chino
jamás tener uno pájaro cantor como este
ser casi chino
y se echar a llorar de tal forma que no parecer sino_que cloquear y el demás gallina cloquear también mientras a el pato se le enrojecer el ojo
el que ser corazón tener decir nadie poder negar nos lo
corazón
replicar el portugués sí en efecto casi tanto como en portugal
bueno haber que pensar en meter se algo en el buche observar el pato marido este ser el que importar
aunque se romper uno juguete quedar mucho
fin
