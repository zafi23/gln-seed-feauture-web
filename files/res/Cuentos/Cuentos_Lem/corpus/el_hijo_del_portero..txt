el general vivir en el 1 piso y el portero en el sótano
haber uno gran distancia entre el 2 familia primero lo separar todo el planta bajo y luego el categoría social
pero el 2 morar bajo uno mismo tejado con el mismo vista a el calle y a el patio en el cual haber uno espacio plantar de césped con uno acacia florido a el menos en el época en que florecer el acacia
bajo el árbol soler sentar se el emperejilar nodriza con el pequeño emilia el hijo de el general más emperejilar todavía
delante_de ellos bailar descalzo el niño de el portero
tener grande ojo castaño y oscuro cabello y el niño le sonreír y le alargar el manita
cuando el general contemplar aquel espectáculo desde su ventana inclinar el cabeza con aire complacer decir charmant
el general tan joven que casi haber poder pasar por hijo de uno 1 matrimonio de el militar no se asomar nunca a el ventana a mirar a el patio pero tener mandar que si bien el pequeño de el gente de el sótano poder jugar con el niño no le estar permitir tocar lo y el ama cumplir al_pie_de_la_letra el orden de el señor
el sol entrar en el 1 piso y en el sótano el acacia dar flor que caer y a el año siguiente dar otro nuevo
florecer el árbol y florecer también el hijo de el portero haber decir uno tulipán recién abrir
el hijo de el general crecer delicado y paliducho con el color rosado de el flor de acacia
ahora bajar raramente a el patio salir a tomar el aire en el coche con su mamá y siempre que pasar saludar con el cabeza a el pequeño jorge de el portero
a el principio le dirigir incluso beso con el mano hasta que su madre le decir que ser demasiado mayor para hacer lo
uno mañana subir el mocito a llevar a el general el carta y el periódico que haber dejar en el portería
mientras estar en el escalera oír uno leve ruido en el 4 donde guardar el arena blanco emplear para el limpieza de el suelo
pensar que ser uno pollito allí encerrar abrir el puerta y se encontrar ante el hijo de el general vestir de gasa y encaje
no lo decir a mi papá se enfadar
pero qué pasar
qué suceder señorito
preguntar jorge
todo estar arder responder él
llama y llama
jorge abrir el puerta de el habitación de el niño
el cortina de el ventana estar casi completamente quemar y el barrote arder
el niño lo hacer caer de uno salto y pedir socorro a grito
de no haber ser por él el casa entero se haber incendiar
el general y el general interrogar a emilita
sólo coger uno cerilla decir el niño prender enseguida y el cortina también
escupir para apagar el fuego escupir cuanto poder pero no tener bastante saliva y entonces salir correr de el habitación pues pensar que mi papá se enfadar
escupir
decir el general qué palabrota ser ese
cuándo lo oír a tu papá o a tu mamá
lo aprender ahí abajo
a jorgito empero le dar uno moneda de $_ATS:4 que no ser a parar a el pastelería no sino a el hucha
y pronto haber en él el chelín suficiente para comprar uno caja de lápiz de color con el cual poder iluminar su numeroso dibujo
este fluir materialmente de el lápiz y el dedo
el 1 el regalar a emilita
charmant
exclamar el general
hasta el general admitir que se ver perfectamente el idea de el chiquillo
 tiene talento 
este palabra ser comunicar para su satisfacción a el mujer de el portero
el general y su esposo ser persona de el nobleza tener su escudo de arma cada cual el propio en el portezuela de el coche
el señor haber hacer bordar el suyo en todo su pieza de tela tanto exterior como interior así_como en su gorro de dormir y en el bolso de cama
ser $_PTE:1 precioso y su bueno florín haber costar a su padre pues no haber nacer con él ni él tampoco
haber venir a el mundo demasiado pronto 7 año antes que el blasón
el mayoría de el persona lo recordar sólo el familia lo haber olvidar
el escudo de el general ser antiguo y de gran tamaño llevar lo encima haber ser como para que rechinar el hueso y ahora se le haber añadir otro
y a el señor general parecer que se le oír rechinar el hueso cuando se dirigir en su carroza a el baile de el corte todo tieso y envarar
el general ser ya viejo y de cabello entrecano pero montar en su caballo hacer aun bueno figura
como estar convencer de ello salir todo el día a caballo con su ordenanza a el distancia conveniente
cuando entrar en uno reunión parecer también hacer lo a caballo y tener tanto condecoración que resultar casi increíble
pero qué ir a hacer le
haber entrar muy joven en el carrera militar y haber participar en mucho maniobra todo en otoño y en_tiempo_de paz
de aquel tiempo recordar uno anécdota el único que saber contar
su suboficial cortar uno vez el retirada a uno príncipe hacer lo prisionero por lo que este haber de entrar en el ciudad en_calidad_de cautivo junto_con uno grupo de soldado detrás_de el general
haber ser uno acontecimiento inolvidable que el general narrar año tras año con regularidad repetir siempre el memorable palabra que hablar pronunciar a el restituir el sable a el príncipe sólo uno suboficial poder hacer prisionero a su_alteza yo nunca
y el príncipe haber responder es usted incomparable
jamás el general haber tomar_parte en uno campaña de verdad
cuando el guerra asolar el país él entrar en el carrera diplomático y ser acreditar sucesivamente en 3 cortes extranjero
hablar el francés tan a_la_perfección que por este lengua casi haber olvidar el propio bailar bien montar bien y el condecoración se acumular en su pecho en número incontable
el centinela le presentar arma uno lindo muchacho lo hacer también y ello le valer ser elevar a el rango de general y tener uno hijo encantador que parecer caída de el cielo
y el hijo de el portero bailar ante él en el patio y le regalar todo su dibujo y pintura que él mirar complacer antes_de romper lo
ser tan delicado y tan lindo
mi pétalo de rosa
 le decir el general
nacer para uno príncipe
el príncipe estar ya en el puerta pero nadie lo saber
el persona no ver nunca más_allá_de el umbral
hacer poco nuestro pequeño partir su merienda con él decir el mujer de el portero
no tener ni queso ni carne y sin embargo le gustar como si ser buey asar
se haber armar el gordo si llegar a ver lo el general pero no se enterar
jorge haber compartir su merienda con emilita y muy a gusto haber compartir también su corazón si ello haber poder dar le gusto
ser uno bueno muchacho listo y despierto
a el sazón concurrir a el escuela nocturno de el academia para perfeccionar se en el dibujo
emilita también progresar en su conocimiento hablar francés con su ama y tener profesor de baile
jorge ir a recibir el confirmación para pascuas decir el mujer de el portero
tan mayor ser ya
convenir poner lo de aprendiz observar el padre
haber que dar le uno bueno oficio y ser uno carga menos
pero tener que venir a dormir a casa responder el madre
no ser cosa fácil encontrar uno maestro que disponer de dormitorio para aprendiz
igualmente tener que vestir lo y en_cuanto_a el comida no suponer uno gran sacrificio ya saber que se contentar con uno patata hervir
su instrucción no nos costar nada dejar lo que seguir su camino
no nos pesar ya lo ver
lo decir su profesor
el traje de confirmación estar listo
el propio madre lo haber confeccionar
se lo haber cortar uno sastre de el vecindad que tener muy bueno mano
como decir el portero si haber disponer de medio y tener uno taller con oficial haber ser sastre de el corte
el vestido estar listo y el confirmando también
el día de el ceremonia uno de el padrino de jorge el más rico de todo uno exmozo de almacén de edad ya avanzar regalar a su ahijado uno gran reloj de metal barato
ser uno reloj viejo y muy usar que siempre adelantar pero mejor ser ese que atrasar ser uno regalo espléndido
el obsequio de el familia de el general consistir en uno devocionario encuadernar en tafilete se lo enviar el señorito a quien jorge haber regalar tanto dibujo
en el portada se leer su nombre y el de él con el expresión afectuoso protector
lo haber escribir el muchacho a el dictado de el general y su marido a el leer lo lo haber encontrar charmant verdaderamente ser uno gran atención de parte de persona tan distinguir decir el mujer de el portero y jorge haber de vestir su traje de confirmación y con su devocionario subir a dar el gracia
el general estar sentar muy arropar pues padecer jaqueca siempre que se aburrir
recibir a jorge muy amablemente lo felicitar y le desear que nunca tener que sufrir aquel dolor de cabeza
el general ir en bata de noche gorra de borla y bota ruso de caña rojo
por 3 vez recorrer el habitación sumir en su pensamiento y recuerdo finalmente se detener y pronunciar el siguiente discurso así ya tener a el pequeño jorge hacer uno cristiano
saber también uno hombre bueno y respetar a tu superior
cuando ser viejo poder decir lo aprender de el general
ser sin duda el discurso más largo de cuanto el bravo militar hablar pronunciar en todo su vida luego volver a reconcentrar se y adoptar uno aire de gran dignidad
pero de todo el que jorge oír y ver en aquel casa el que más se grabar en su recuerdo ser el señorito emilia
qué encantador
qué dulce vaporoso y distinguir
si tener que pintar lo tener que hacer lo en uno pompa de jabón
uno fino perfume se exhalar de todo su vestido y de su ensortijar cabello rubio
se haber decir uno capullo de rosa recién abrir
y con aquel criatura haber partir él TM_d:1 su merienda
él se lo haber comer con verdadero voracidad con uno gesto de aprobación a cada bocado
se acordar aun de aquel
sí seguramente y en recuerdo le haber regalar el hermoso devocionario
a el 1 luna nuevo de el año siguiente seguir uno viejo tradición salir a el calle con uno trozo de pan y $_ATS:1 y abrir el libro a el azar buscar uno canción que le descubrir su porvenir
salir uno cántico de alabanza y de gracia
preguntar luego a el oráculo por el destino de emilita
proceder con extremo cuidar para no dar con uno himno mortuorio y a_pesar_de_todo el libro se abrir en uno página que hablar de el muerte y de el sepultura pero quién creer en ese tontería
y sin embargo experimentar uno angustia infinito cuando poco más tarde el encantador muchacho caer enfermo y el coche de el doctor se parar cada [??:??/??/??:1200:pm] delante_de el puerta
no conservar a el niño decir el portero
el bueno dios saber bien a quién deber llamar a su lado
no morir sin embargo y jorge seguir componer dibujo y enviar se lo
dibujar el palacio de el zar y el antiguo kremlin tal_y_como ser con su torre y cúpula que en el dibujo de el muchacho parecer enorme calabaza verde y dorar por el sol
a emilita le gustar mucho este composición y aquel mismo semana jorge le enviar otro representar también edificio para que el niño poder fantasear acerca_de el que haber detrás_de el puerta y ventana
dibujar uno pagoda chino con campanilla en cada uno de su 16 piso y 2 templo griego con esbelto columna de mármol y grande escalinata alrededor
dibujar asimismo uno iglesia noruego de madera se ver que estar construir todo él de tronco y viga muy bien tallar y modelar y encajar uno con otro con uno arte singular
pero el más bonito de el colección ser uno edificio que él titular palacio_de_emilita porque él deber habitar lo TM_d:1
ser uno invención de jorge y contener todo el elemento que le haber gustar más en el restante construcción
tener el viguería de talla como el iglesia noruego columna de mármol como el templo griego campanilla en cada piso y en el alto cúpula verde y dorar como el kremlin_de_el_zar
ser uno verdadero palacio infantil y bajo cada ventana se leer el destino de el sala correspondiente aquí dormir emilia aquí emilia bailar y jugar a quotvisitasquot
dar gusto mirar lo y causar el admiración de todo
charmant
exclamar el general
pero el anciano conde pues haber uno conde anciano más distinguir aun que el general y propietario de uno palacio propio y uno gran hacienda señorial no decir nada
se enterar de que lo haber imaginar y dibujar el hijo de el portero
ya no ser uno niño pues haber recibir el confirmación
el anciano conde examinar el dibujo y se guardar su opinión
uno mañana en que hacer uno tiempo de perro gris húmedo en uno palabra abominable significar sin embargo para jorge el principio de uno de el día más radiante y bello de su vida
el profesor de el academia_de_arte lo llamar
escuchar amigo le decir tener que hablar tú y yo
dios te haber dotar de aptitud excepcional y haber querer a el mismo tiempo que no te faltar el ayuda de persona virtuoso
el anciano conde que vivir en este calle haber hablar conmigo
haber ver tu dibujo pero ahora no hablar de ellos pues tener demasiado que corregir
desde ahora poder asistir 2 vez por semana a mi escuela de dibujo y aprender a hacer el cosa como se deber
crear que ser mayor tu disposición para arquitecto que para pintor
pero tener tiempo para pensar lo
presentar te hoy mismo a el señor conde de el esquina y dar gracias_a dios por haber poner a este hombre en tu camino
ser uno hermoso casa el de el conde allá en el esquina de el calle
el ventana estar enmarcar con relieve de piedra representar elefante y dromedario todo de el tiempo antiguo pero el anciano conde vivir de_cara_a el nuevo y a todo el bueno que nos haber traer el mismo si haber salir de el 1 piso como de el sótano o de el buhardilla
crear observar el mujer de el portero que cuanto más de vera ser noble el persona más sencillo ser
mirar el anciano conde qué llano y amable
y hablar exactamente como tú y como yo no lo hacer así el general
no estar poco entusiasmar anoche jorge después_de visitar a el conde
pues el mismo me ocurrir hoy a mí después_de haber ser recibir por este gran señor
ver el bien que hacer a el no poner a jorge de aprendiz
tener mucho talento
pero necesitar apoyo de el de fuera observar el padre
ya lo tener reponer el madre
el conde hablar con palabra muy claro y preciso
pero el cosa salir de casa de el general opinar el portero y también a él deber estar le agradecer
desde_luego responder el madre aunque no crear yo que le deber gran cosa
dar el gracias_a dios y se lo dar también por el restablecimiento de emilita
el niño salir adelante en efecto y lo mismo hacer jorge
al_cabo_de uno año ganar el 2 medalla de plata y después el 1
más nos haber valer poner lo de aprendiz
exclamar llorar el mujer de el portero así lo haber tener a nuestro lado
qué se le haber perder en roma
no volver a ver lo aunque regresar alguno día
pero nunca volver mi hijo querer
pero si ser por su bien si ser uno gran honor para él
lo consolar el padre
gracias por tu consuelo protestar el mujer pero ni tú mismo creer el que estar decir
estar tan triste como yo
el aflicción de el padre ser justificar pero no lo ser menos el viaje
para el muchacho ser uno gran suerte decir el gente
llegar el hora de despedir se incluso de el familia de el general
el señor no salir pues sufrir de fuerte jaqueca
el general le repetir su único anécdota el que haber decir a el príncipe y el respuesta de este es usted incomparable
luego le tender el blando mano
emilia se lo estrechar a su vez parecer afligir pero jorge estar aun más triste
el tiempo pasar deprisa cuando se trabajar pero también cuando no se hacer nada
el tiempo ser igual de largo pero no de útil
para jorge ser provechoso pero no largo ni_mucho_menos excepto cuando pensar en el ser querer que haber dejar en casa
qué tal ir el cosa en el 1 piso y en el sótano
se escribir naturalmente
cuánto cosa poder reflejar uno carta
día de sol y otro turbio y difícil
así llegar uno anunciar que su padre haber morir y que el madre quedar solo
emilia se haber portar como uno ángel de consuelo
haber bajar a el sótano escribir el madre añadir que le permitir continuar de portero
el general llevar su diario en el que registrar cada baile y cada tertulia a que haber concurrir así_como el visita de todo el forastero
el diario estar ilustrar con el tarjeta de el diplomático y de el alto nobleza el dama estar orgulloso de su diario
haber ir crecer a lo largo de el tiempo a costa de hora bajo fuerte jaqueca pero también como fruto de claro noche ser decir de baile cortesano
emilia haber asistir ya a el 1 baile su madre llevar uno vestido rojo brillante con encaje negro traje español
el hijo ir de blanco fino y exquisito
cinta de seda verde ondear como junco entre su dorado rizo coronar por uno guirnalda de lirio de agua
su ojo despedir uno brillo azul y límpido su boca ser rojo y delicado todo él ser comparable a uno sirena hermoso hasta el indecible
3 príncipe bailar con él uno tras otro naturalmente
el general estar luego TM_d:8 sin que le doler el cabeza
mas aquel baile no ser el único en perjuicio de el salud de emilia
por ese ser uno suerte que llegar el verano con su descanso y su vida a el aire libre
el anciano conde invitar a el familia a su palacio
este palacio tener uno parque admirable
uno parte de él se conservar como en su tiempo primitivo con espeso seto verde que no parecer sino_que uno andar entre verde mampara interrumpir por mirilla
bojes y tejo estar cortar en figura de estrella y pirámide y el agua brotar de gruta de concha en derredor haber estatua de mármol raso de bello rostro y noble ropaje
cada arriate tener uno forma distinto uno figurar uno pez otro $_PTE:1 de arma otro uno inicial
este ser el parte francés de el parque
desde él se penetrar en el bosque fresco y verde donde el árbol crecer en pleno libertad por ese ser tan grande y tan magnífico
el césped ser verde y mullir y le pasar con frecuencia el rodillo lo segar y cuidar para que se poder andar sobre él como sobre uno alfombra
ser el parte inglés de el jardín
el época antiguo y el nuevo decir el conde
aquí a el menos se armonizar y el uno valorizar a el otro
dentro_de 2 año el palacio tener su auténtico carácter
ir a embellecer lo y mejorar lo a fondo
le mostrar el dibujo y le presentar a el arquitecto a quien haber invitar a comer
charmant
responder el general
uno verdadero paraíso
exclamar el general y allí tener además uno castillo medieval
ser mi gallinero replicar el conde
el paloma vivir en el torre el pavo en el 1 piso pero abajo reina el viejo elsa
en todo lado tener habitación para huésped el clueco vivir independiente el gallina con su polluelo también y el pato tener uno salida especial a el agua
charmant
repetir el general
y todo se dirigir a ver aquel maravilla
en el centro de el habitación estar el viejo elsa y a su lado su hijo el arquitecto jorge
él y emilita se volver a encontrar al_cabo_de bastante año y el encuentro ocurrir en el gallinero
sí allí estar él y de verdad que ser uno apuesto mozo
abrir y resolver ser el expresión de su rostro brillante su negro cabello y en su labio se dibujar uno sonrisa como querer significar a mí no me lo dar os conocer a fondo
el anciano no llevar zueco se haber poner media en honor de el distinguir visitante
el gallina cloquear y el gallo cacarear y el pato anadear con su rap rap camino de el agua
pero el fino muchacho el amigo de su niñez el hijo de el general permanecer de pie con uno rubor en su mejilla de ordinario tan pálido el grande ojo abrir el boca tan elocuente a_pesar_de que no salir de él ni uno palabra
y el saludo que él recibir ser el más amable que uno joven poder esperar de uno dama que no pertenecer a uno encumbrar familia o haber bailar más de uno vez con él
pues él y el arquitecto nunca haber bailar junto
el conde tomar el mano de el joven y lo presentar no le ser de el todo desconocer nuestro joven amigo don_jorge
el general corresponder con uno inclinación el hijo estar a_punto_de ofrecer le el mano pero se retener
nuestro pequeño amigo jorge
decir el general
viejo amigo de casa
charmant
venir usted hacer uno perfecto italiano le decir el general
hablar el lengua como uno nativo verdad
mi señor no hablar el italiano pero lo cantar explicar el general
en el mesa jorge se sentar a el derecha de emilia el general haber entrar de el brazo de él mientras el conde lo dar a el general
don_jorge hablar y contar y lo hacer bien él ser quien ayudar por el anciano conde animar el mesa con su relato y su ingenio
emilia callar atento el oído el mirada brillante
pero no decir nada
él y jorge se reunir en el terraza entre el flor uno rosal lo ocultar
de nuevo jorge tener el palabra ser el 1 en hablar
gracias por su amable conducta con mi anciano madre le decir
saber que el noche en que fallecer mi padre usted bajar a su casa y permanecer a su lado hasta que se cerrar su ojo
gracias
y coger el mano de emilia lo besar bien poder hacer lo en aquel ocasión
uno vivo rubor cubrir el mejilla de el muchacho que le responder apretar le el mano y mirar le con su expresivo ojo azul
su madre ser tan bueno persona
cómo lo querer
me dejar leer todo su carta crear que lo conocer bien
qué bueno ser usted conmigo cuando yo ser niño
me dar dibujo
que usted romper interrumpir jorge
no conservar aun uno obra suyo en mi palacio
ahora ir a construir lo de verdad decir jorge entusiasmar se con su propio palabra
el general y el general discutir en su habitación acerca_de el hijo de el portero y convenir en que saber mover se y expresar se
poder ser preceptor decir el general
tener ingenio se limitar a observar el general
durante el dulce día de verano don_jorge ir con frecuencia a el palacio de el conde
lo echar de menos si no lo hacer
cuánto don le haber hacer dios con preferencia a nosotros pobre mortal le decir emilia
no le estar muy agradecer
a jorge le halagar oír aquel alabanza de labio de el hermoso muchacho en quien encontrar alto aptitud
el general estar cada vez más persuadir de el imposibilidad de que jorge haber nacer en uno sótano
por otro parte el madre ser uno excelente mujer decir
haber de reconocer lo aunque ser sobre su tumba
pasar el verano llegar el invierno y nuevamente se hablar de don_jorge
ser bien ver y se le recibir en el lugar más encumbrar el general hasta se encontrar con él en uno baile de el corte
organizar otro en casa en honor de el señorito emilia
ser correcto invitar a don_jorge
cuando el rey invitar también poder hacer lo el general decir este crecer se el menos LN_in:1
invitar a don_jorge y este acudir y acudir príncipe y conde y cada uno bailar mejor que el anterior
pero emilia sólo bailar el 1 baile le doler LN_ft:1 no ser que ser uno cosa de cuidado pero tener que ser prudente renunciar a bailar y limitar se a mirar a el demás
y se estar sentar mirar con el arquitecto a su lado
parecer usted disponer a dar le el basílica de san_pedro todo entero decir el general pasar ante ellos con uno sonrisa muy complacer de sí mismo
con el mismo sonrisa complaciente recibir a don_jorge uno día más tarde
probablemente el joven venir a dar el gracia por el invitación a el baile
qué otro cosa si no
pero no ser otro cosa
el más sorprendente el más extravagante que caber imaginar de su labio salir palabra de locura el general no poder prestar crédito a su oído
inconcebible
uno petición completamente absurdo don_jorge solicitar el mano de emilita
señor mío
exclamar el general poner se colorar como uno cangrejo
no lo comprender en absoluto
qué decir usted
qué querer
no lo conocer
cómo haber poder ocurrir se le venir a mi casa con este embajada
no saber si deber quedar me o retirar me y andar de espalda se ir a su dormitorio y lo cerrar con llave dejar solo a jorge
este aguardar uno minuto y luego se retirar
en el pasillo estar emilia
qué contestar mi padre
decir con voz tembloroso
jorge le estrechar el mano
me dejar plantar
otro día estar de mejor suerte
el lágrima asomar a el ojo de emilia
en el de el joven brillar el confianza y el ánimo el sol brillar sobre el 2 enviar le su bendición
entretanto el general seguir en su habitación fuera_de sí por el ira
su rabia le hacer desatar se en improperio qué monstruoso locura
qué desvarío de portero
menos de TM_h:1 después el general haber oír el escena de boca de su marido
llamar a emilia a_solas
pobre criatura
ofender te de este modo
ofender nos a todo
ver lágrima en tu ojo pero te favorecer
estar encantador llorar
te parecer a mí el día de mi boda
llorar llorar emilia querido
sí haber de llorar replicar el muchacho si tú y papá no decir que sí
hija
exclamar el general
tú estar enfermo estar delirar y por tu culpa ir a recaer en mi terrible jaqueca
qué desgracia haber caer sobre nuestro casa
querer el muerte de tu madre emilia
te quedar sin madre
y a el general se le humedecer el ojo no poder soportar el idea de su propio muerte

