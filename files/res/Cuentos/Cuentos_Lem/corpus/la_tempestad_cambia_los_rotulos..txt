en día remoto cuando el abuelo ser todavía uno niño y llevar pantalón encarnar y chaqueta de igual color cinturón alrededor_de el cuerpo y uno pluma en el gorra pues así vestir el pequeño cuando ir endomingar mucho cosa ser completamente distinto de como ser ahora
ser frecuente el procesión y cabalgata ceremonia que hoy haber caer en desuso pues nos parecer anticuar
pero dar gusto oír contar lo a el abuelo
realmente deber de ser uno bello espectáculo el solemne traslado de el escudo de el zapatero el día que cambiar de casa gremial
ondear su bandera de seda en el que aparecer representar uno gran bota y uno águila bicéfalo el oficial más joven llevar el gran copa y el arca cinta rojo y blanco descender flotante de el manga de su camisa
el mayor ir con el espada desenvainar con uno limón en el punta
lo dominar todo el música y el mayor de el instrumento ser el pájaro como llamar el abuelo a el alto percha con el medio luna y todo el sonajero imaginable uno verdadero música turco
sonar como 1000 demonio cuando lo levantar y sacudir y a uno le doler el ojo cuando el sol dar sobre el oro el plata o el latón
a el cabeza de el comitiva marchar el arlequín vestir de 1000 pedazo de tela de todo el color con el cara negro y cascabel en el cabeza como caballo de trineo
vapulear a el gente con su palmeta y armar gran alboroto aunque sin hacer daño a nadie y el gente se apretujar retroceder y volver a adelantar se
el niño se meter de pie en el arroyo viejo comadre se dar codazo poner cara agrio y echar peste
el uno reír el otro charlar puerta y ventana estar lleno de curioso y lo haber incluso en lo alto de el tejado
lucir el sol y caer también uno chaparrón pero el lluvia beneficiar a el campesino y aunque mucho quedar calado ser uno verdadero bendición para el campo
qué bien contar el abuelo
de niño haber ver aquel fiesta en todo su esplendor
el oficial más antiguo de el gremio pronunciar uno discurso desde el tablado donde haber ser colgar el escudo uno discurso en verso expresamente componer por 3 de el miembro que para inspirar se se haber beber uno bueno jarra de ponche
y el gente gritar hurra
dar gracia por el discurso pero aun ser más sonoro el hurras cuando el arlequín montar en el tablado imitar a el demás
el bufón hacer su payasada y beber hidromel en vas de aguardiente que luego arrojar a el multitud el cual lo pescar a el vuelo
el abuelo guardar todavía uno regalo de uno oficial albañil que lo haber coger
ser el mar de divertir
y luego colgar el escudo en el nuevo casa gremial enmarcar en flor y follaje
fiestas como aquel no se olvidar nunca por viejo que llegar uno a ser decir abuelo y en efecto él no lo olvidar con haber ver tanto y tanto espectáculo magnífico
nos hablar de todo ellos pero el más divertir ser sin duda el de el comitiva de el rótulo por el calle de el gran ciudad
de niño el abuelo haber hacer con su padre uno viaje a el ciudad
ser el 1 vez que visitar el capital
circular santísimo gente por el calle que él creer se tratar de uno de aquel procesión de el escudo
haber uno cantidad ingente de rótulo para trasladar se haber cubrir el pared de 100 salón si en_vez_de colgar lo en el exterior se haber guardar dentro
en el de el sastre aparecer pintar todo clase de traje pues coser para todo clase de gente basto o fino luego haber el rótulo de el tabaquero con lindo chiquillo fumar cigarro como si ser de verdad
se ver rótulo con mantequilla y arenque ahumar valón para sacerdote ataúd qué saber yo así_como el más variar inscripción y anuncio
uno poder andar por el calle durante TM_d:1 entero contemplar rótulo y más rótulo además se enterar enseguida de el gente que habitar en el casa poner que tener su escudo colgar en el exterior y como decir abuelo ser muy conveniente y aleccionador saber quién vivir en uno gran ciudad
pero querer el azar que cuando el abuelo ser a el ciudad ocurrir algo extraordinario con el rótulo él mismo me lo contar con aquel ojo de pícaro que poner cuando querer hacer me creer algo
lo explicar tan serio
el 1 noche que pasar en el ciudad hacer uno tiempo tan horrible que hasta salir en el periódico uno tiempo como nadie recordar otro igual
el teja volar por el aire viejo plancha se venir a el suelo hasta uno carretilla se echar a correr solo calle abajo para salvar se
el aire bramar mugir y lo sacudir todo ser uno tempestad desatar
el agua de el canal se desbordar por encima_de el muralla pues no saber ya por dónde correr
el huracán rugir sobre el ciudad llevar se el chimenea más de uno viejo y altivo remate de campanario haber de inclinar se y desde entonces no haber volver a enderezar se
junto_a el casa de el viejo jefe de bombero uno bueno hombre que llegar siempre con el último bomba haber uno garita
el tempestad se encaprichar de él lo arrancar de_cuajo y lo lanzar calle abajo rodar
y fijar te qué cosa más raro
se quedar plantar frente_a el casa de el pobre oficial carpintero que haber salvar 3 vida humano en el último incendio
pero el garita no pensar en ello
el rótulo de el barbero aquel gran bacía de latón ser arrancar y disparar contra el hueco de el ventana de el consejero judicial cosa que todo el vecindario considerar poco menos que ofensiva pues todo el mundo y hasta el amigo más íntimo llamar a el esposo de el consejero el navaja
ser listo y conocer el vida de todo el persona más que ellos mismo
uno rótulo con uno bacalao ser a dar sobre el puerta de uno individuo que escribir uno periódico
resultar uno pesar broma de el viento que no pensar que uno periodista no tolerar broma pues ser rey en su propio periódico y en su opinión personal
el veleta volar a el tejado de enfrente en el que se quedar como el más negro de el maldad decir el vecino
el tonel de el tonelero quedar colgar bajo el letrero de modas de señor
el minuta de el fonda poner en uno pesar marco a el puerta de el establecimiento ser llevar por el viento hasta el entrada de el teatro a el que el gente no acudir nunca ser uno cartel ridículo rábanos picante y repollo relleno
y entonces le dar a el gente por ir a el teatro
el piel de zorro de el peletero su honroso escudo aparecer pegar a el cordón de el campanilla de uno joven que asistir regularmente a el 1 sermón parecer uno paraguas cerrar andar en_busca_de el verdad y según su tío ser uno modelo
el letrero academia de estudio superior ser encontrar en el club de billar y recibir a_cambio otro que poner « aquí se criar niño con biberón »
no tener el menor gracia y resultar muy descortés
pero lo haber hacer el tormenta y ir usted a pedir le cuenta
ser uno noche espantoso
imaginar te que por el mañana casi todo el rótulo haber cambiar de sitio en alguno caso con tan malo idea que abuelo se negar a contar lo limitar se a reír se por dentro bien lo observar yo
y como pícaro lo ser desde_luego
el pobre gente de el gran ciudad especialmente el forastero andar de cabeza y no poder ser de otro modo si se guiar por el cartel
a lo mejor uno pensar asistir a uno grave asamblea de anciano donde haber de debatir se cuestión de el mayor trascendencia y ir a parar a uno bullicioso escuela donde el niño saltar por encima_de mesa y banco
haber quien confundir el iglesia con el teatro y este sí que ser penoso
uno tempestad como aquel no se haber ver jamás en nuestro día
aquel lo ver sólo el abuelo y aun ser uno chiquillo
tal_vez no lo ver nosotros sino nuestro nieto
lo esperar y rogar que se estar quieto en casa cuando el vendaval cambiar el rótulo

