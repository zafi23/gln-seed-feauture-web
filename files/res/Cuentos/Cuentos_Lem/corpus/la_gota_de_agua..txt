seguramente saber lo que ser uno cristal de aumento uno lente circular que hacer el cosa 100 vez mayor de el que ser
cuando se coger y se colocar delante_de el ojo y se contemplar a su través uno gota de agua de el balsa de allá fuera se ver más de 1000 animal maravilloso que de otro modo pasar inadvertir y sin embargo estar allí no caber duda
se decir casi uno plato lleno de cangrejo que saltar en revoltijo
ser muy voraz se arrancar uno a otro brazo y pata muslo y nalga y no_obstante estar alegre y satisfacer a su manera
pues haber aquí que vivir en otro tiempo uno anciano a quien todo llamar criblecrable pues tal ser su nombre
querer siempre hacer se con lo mejor de todo el cosa y si no se lo dar se lo tomar por_arte_de_magia
así peligrar cuanto estar a su alcance
el viejo estar sentar TM_d:1 con uno cristal de aumento ante el ojo examinar uno gota de agua que haber extraer de uno charco de el foso
dios miar que hormiguero
uno sinfín de animal ir de_un_lado_para_otro y venir saltar y brincar venir zamarrear se y devorar se mutuamente
qué asco
exclamar el viejo criblecrable
no haber modo de obligar lo a vivir en paz y quietud y de hacer que cada uno se cuidar de su cosa
y pensar que te pensar pero como no encontrar el solución tener que acudir a el brujería
haber que dar le color para poder ver lo más bien decir y le verter encima uno gota de uno líquido parecer a vino tinto pero que en realidad ser sangre de hechicero de el mejor clase de el de a 6 penique
y todo el animal quedar teñir de rosa parecer uno ciudad lleno de salvaje desnudo
qué tener ahí
le preguntar otro viejo brujo que no tener nombre y este ser precisamente el bueno de él
si adivinar lo que ser responder criblecrable te el regalo pero no ser tan fácil acertar lo si no se saber
el brujo innominado mirar por el lupa y ver efectivamente uno cosa comparable a uno ciudad donde todo el gente correr desnudo
ser horrible pero más horrible ser aun ver cómo todo se empujar y golpear se pellizcar y arañar morder y desgreñar
el que estar arriba querer ir se abajo y viceversa
fijar te fijar te
su pata ser más largo que el mío
paf
fuera con él
ahí ir uno que tener uno chichón detrás_de el oreja uno chichón insignificante pero le doler y todavía le ir a doler más
y se echar sobre él y lo agarrar y acabar comer se lo por culpa de el chichón
otro permanecer quieto pacífico como uno doncella sólo pedir tranquilidad y paz
pero el doncella no poder quedar se en su rincón tener que salir el agarrar y en uno momento estar descuartizar y devorar
ser muy divertir
decir el brujo
sí pero qué creer que ser
preguntar criblecrable
ser capaz de adivinar lo
tomar pues ser muy fácil responder el otro
ser copenhague o cualquiera otro gran ciudad todo ser igual
ser uno gran ciudad el que ser
ser agua de el charco
contestar criblecrable
fin
