haber uno rosal en el ventana
hasta hacer poco estar verde y lozano mas ahora tener uno aspecto enfermizo algo deber ocurrir le
el que le pasar ser que haber llegar soldado y tener que alojar lo
el recién llegar se lo comer vivo a_pesar_de tratar se de uno tropa muy respetable en uniforme verde
hablar con uno de el alojar que aunque sólo contar TM_d:3 de edad ser ya bisabuelo
saber el que me decir
pues me contar mucho cosa de él y de todo el tropa
ser el regimiento más notable entre todo el criatura de el tierra
cuando hacer calor dar a luz hijo vivo pues entonces el tiempo se prestar a ello nos casar enseguida y celebrar el boda
cuando hacer frío poner huevo así el pequeño estar caliente
el más sabio de todo el animal el hormiga a el que respetar sobremanera nos estudiar y apreciar
no se nos comer sino_que coger nuestro huevo lo poner entre el suyo y en el piso inferior de su casa lo colocar por orden numérico en hilera y en capa de manera que cada día poder salir uno de el huevo
entonces nos llevar a el establo y sujetar nos el pata posterior nos ordeñar hasta que morir ser uno sensación agradable
nos dar el nombre más hermoso imaginable dulce vaca lechero
este ser el nombre que nos dar el animal inteligente como el hormiga sólo el hombre no lo hacer el cual ser uno ofensa capaz de hacer nos perder el ecuanimidad
no poder escribir nada para arreglar este embarazoso situación y poner el cosa en su punto
nos mirar estúpidamente y además con ojo colérico total porque nos comer uno pétalo de rosa cuando ellos devorar todo el ser vivo todo el que verdear y florecer
nos dar el nombre más despectivo y más odioso que caber imaginar no me atrever a decir lo puh
me marear sólo a el pensar lo
no poder repetir lo a el menos cuando ir de uniforme y como nunca me lo quito
nacer en el hoja de el rosal
yo y todo el regimiento vivir de él pero gracias_a nosotros subsistir otro mucho ser más elevar en el escala de el creación
el hombre no nos tolerar venir a matar nos con agua jabonoso que ser uno bebida horrible
me parecer que lo estar oler
ser abominable ese de ser lavar cuando uno nacer para no ser lo
hombre
tú que me mirar con enfurruñar ojo de agua jabonoso pensar en nuestro misión en el naturaleza en nuestro sabio función de poner huevo y dar hijo vivo
también a nosotros nos alcanzar aquel mandato creced y multiplicar os
nacer en rosa y en rosa morir nuestro vida entero ser poesía
no nos ofender con el nombre más repugnante y abyecto que encontrar con el nombre de pero no no lo decir no lo repetir
llamar nos vaco lechera de el hormiga regimiento de el rosal o verde
y yo el hombre permanecer allí contemplar el rosal y el verde cuyo verdadero nombre no querer pronunciar para no ofender a uno habitante de el rosa a uno gran familia con huevo y hijo vivo
el agua jabonoso con que me disponer a lavar lo pues haber venir con él y con muy malo intención lo batir hasta que sacar espuma soplar con él burbuja de jabón y contemplar su belleza acaso encontrar uno cuento en cada 1
el ampolla se hacer muy voluminoso y brillar con todo el color mientras en su centro parecer flotar uno perla de plata
oscilar se desprender emprender el vuelo hacia el puerta y se estrellar contra él pero se abrir el puerta y se presentar el hada de el cuento en persona
qué bien
ahora él os contar pues ir a hacer lo mejor que yo el cuento de el
no decir el nombre
de el verde
el de el pulgón  me corregir el hada de el cuento
haber que llamar a todo el cosa por su verdadero nombre y si a vez no convenir a el menos en el cuento deber hacer se

