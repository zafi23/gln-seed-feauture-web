cualquiera haber decir que algo importante ocurrir en el balsa de el pueblo y sin embargo no pasar nada
todo el pato tanto el que se mecer en el agua como el que se haber poner de cabeza pues saber hacer lo de pronto se poner a nadar precipitadamente hacia el orilla en el suelo cenagoso quedar bien visible el huella de su pie y su grito poder oír se a gran distancia
el agua se agitar violentamente y ese que uno momento antes estar terso como uno espejo en el que se reflejar uno_por_uno el árbol y arbusto de el cercanía y el viejo casa de campo con el agujero de el fachada y el nido de golondrina pero muy especialmente el gran rosal cuajar de rosa que bajar desde el muro hasta muy adentro de el agua
el conjunto parecer uno cuadro poner del_revés
pero en cuanto el agua se agitar todo se revolver y el pintura se esfumar
2 pluma que haber caer de el pato a el desplegar el ala se balancear sobre el ola como si soplar el viento y sin embargo no lo haber
por fin quedar inmóvil el agua recuperar su primitivo tersura y volver a reflejar claramente el fachada con el nido de golondrina y el rosal con cada uno de su flor que ser hermoso aunque ellos lo ignorar porque nadie se lo haber decir
el sol se filtrar por entre el delicado y fragante hoja y cada rosa se sentir feliz de modo parecer a el que nos suceder a el persona cuando estar sumir en nuestro pensamiento
qué bello ser el vida
decir cada uno de el rosa
el único que desear ser poder besar a el sol por ser tan cálido y tan claro
y también querer besar el rosa de debajo_de el agua se parecer tanto a nosotros
y besar también a el dulce ave de el nido que asomar el cabeza piar levemente no tener aun pluma como su padre
ser bueno el vecino que tener tanto el de arriba como el de abajo
qué hermoso ser el vida
aquel pajarillo de arriba y de abajo el segundo no ser sino el reflejo de el 1 en el agua ser gurriato hijo de gorrión haber ocupar el nido abandonar por el golondrina el año anterior y se encontrar en él como en su propio casa
ser patio el que allí nadar
preguntar el gurriato a el ver flotar en el agua el pluma de el palmípedo
no preguntar tontería
replicar el madre
no ver que ser pluma prenda de vestir vivo como el que yo llevar y que usted llevar también sólo que el nuestro ser más fino
por el demás me gustar tener lo aquí en el nido pues ser muy caliente
querer saber de qué se espantar el pato
haber suceder algo en el agua
yo no haber ser aunque confesar que haber piar uno poco fuerte
ese cabezota de rosa deber saber lo pero no saber nada mirar se en el espejo y despedir perfume ese ser cuanto saber hacer
qué vecino tan aburrir
escuchar el pajarillo de arriba
decir el rosa hacer ensayo de_canto
no saber todavía pero ya venir
qué bonito deber ser saber cantar
ser delicioso tener vecino tan alegre
en aquel momento llegar galopar 2 caballo venir a abrevar uno zagal montar uno de ellos despojar de todo su prenda de vestir excepto el sombrero grande y de ancho ala
el mozo silbar como si ser uno pajarillo y se meter con su cabalgadura en el parte más profundo de el balsa a el pasar junto_a el rosal cortar uno de su rosa se lo prender en el sombrero para ir bien adornar y seguir adelante
el otro rosa mirar a su hermano y se preguntar mutuamente adónde ir
pero ninguno lo saber
a vez me gustar salir a correr mundo decir uno de el flor a su compañero
aunque también ser muy hermoso este rincón verde en que vivir
durante el día brillar el sol y nos calentar y por el noche el cielo ser aun más bello poder ver lo a_través_de el agujero que tener
se referir a el estrella pensar que ser agujero de el cielo
no llegar a más el ciencia de el rosa
nosotros traer vida y animación a este paraje decir el gorriona
el nido de golondrina ser de bueno agüero decir el gente por ese se alegrar de tener nos
pero aquel vecino el gran rosal que se encaramar por el pared producir humedad
esperar que se marchar pronto y en su lugar crecer trigo
el rosa sólo servir de adorno y para perfumar el ambiente a_lo_sumo para sujetar lo a el sombrero
todo el año se marchitar lo saber por mi madre
el campesino lo conservar en sal y entonces tener uno nombre francés que no saber pronunciar ni me importar luego lo esparcir por el ventana cuando querer que oler bien
y este ser todo su vida
no servir más que para alegrar el ojo y el olfato
ya lo saber pues
a el anochecer cuando el mosquito empezar a danzar en el aire tibio y el nube adquirir su tonalidad rojo se presentar el ruiseñor y cantar a el rosa que en este mundo el bello se parecer a el luz de el sol y vivir eternamente
pero el rosa creer que el ruiseñor cantar su propio loanzas y cualquiera lo haber pensar también
no se le ocurrir que ser ellos el objeto de su canto sin embargo experimentar uno gran placer y se preguntar si tal_vez el gurriato no se volver a su vez ruiseñor
haber comprender muy bien el que cantar el pájaro decir el gurriato
sólo uno palabra querer que me explicar qué significar el bello
no ser nada responder el madre ser uno simple apariencia
allá arriba en el finca de el señor donde el paloma tener su casa propio y todo el día se le repartir guisante y grano yo haber comer también con ellos y alguno día venir usted decir me con quién andar y te decir quién ser pues en aquel finca tener 2 pájaro de cuello verde y uno mechón de pluma en el cabeza
poder extender el cola como si ser uno gran rueda tener todo el color hasta el punto de que doler el ojo de mirar lo
se llamar pavo real y ser el belleza
sólo con que lo desplumar uno poquitín casi no se distinguir de nosotros
me entrar gana de emprender lo a picotazo con ellos pero ser tan grande
pues yo lo ir a picotear exclamar el benjamín de el gurriato el mocoso no tener aun pluma
en el cortijo vivir uno joven matrimonio que se querer tiernamente el 2 ser laborioso y despierto y su casa ser uno primor de bien cuidar
el domingo por el mañana salir el mujer cortar uno ramo de el rosa más bello y lo poner en uno florero en el centro de el armario
ahora me dar_cuenta de que ser [G:??/??/??:????:??]
decir el marido besar a su esposo y luego se sentar y leer uno salmo coger de el mano mientras el sol penetrar por el ventana iluminar el fresco rosa y a el enamorar pareja
este espectáculo me aburrir
decir el gorriona que lo contemplar desde su nido de enfrente y echar a volar
el mismo hacer uno semana después pues cada [G:??/??/??:????:??] poner rosa fresco en el florero y el rosal seguir florecer tan hermoso
el gorrión que ya tener pluma haber querer lanzar se a volar con su madre pero este le decir quedaos aquí
y se estar quieto
él se ir pero como soler ocurrir con harto frecuencia de pronto quedar cogida en uno lazo hacer de crin de caballo que uno muchacho haber colocar en uno rama
el crin aprisionar fuertemente el pata de el gorriona tanto que parecer que ir a partir lo
qué dolor y qué miedo
el chico coger el pájaro oprimir le terriblemente sólo ser uno gorrión
decir pero no lo soltar sino_que se lo llevar a casa golpear lo en el pico cada vez que chillar
en el casa haber uno viejo entender en el arte de fabricar jabón para el barba y para el mano jabón en bola y en pastilla
ser uno viejo alegre y trotamundos a el ver el gorrión que traer el niño de el que según ellos no saber qué hacer le preguntar quieren que lo poner guapo
uno estremecimiento de terror recorrer el cuerpo de el gorriona a el oír aquel palabra
el viejo abrir su caja que contener color bello tomar uno bueno porción de purpurina y cascar uno huevo que le proporcionar el chiquillo separar el claro y untar con él todo el cuerpo de el ave espolvorear lo luego con el oro
y de este modo quedar el gorriona dorar aunque no pensar en su belleza pues se morir de miedo
después el jabonero arrancar uno trapo rojo de el forro de su viejo chaqueta lo cortar en forma de cresta y lo pegar en el cabeza de el pájaro
ahora ver volar el pájaro de oro
decir soltar a el animal el cual presa de mortal terror emprender el vuelo por el espacio solear
dios mío y cómo relucir
todo el gorrión y también uno corneja que no estar ya en el 1 edad se asustar a el ver lo pero se lanzar en su persecución ávido de saber quién ser aquel pájaro desconocer
de dónde de dónde
gritar el corneja
esperar uno poco esperar uno poco
decir el gorrión
pero él no estar para aguardar dominar por el miedo y el angustia se dirigir en_línea recta hacia su casa
poco le faltar para desplomar se rendir pero cada vez ser mayor el número de su perseguidor grande y chico alguno se disponer incluso a atacar lo
fijar se en ese fijar se en ese
gritar todo
fijar se en ese fíjense en ese
gritar también su cría cuando a madre llegar a el nido
seguramente ser uno pavo tener todo el color y hacer daño a el ojo como decir madre
pip
ser el belleza
y arremeter contra él a picotazo impedir le posar se en el nido y estar el gorriona tan aterrorizar que no ser capaz de decir pip
y mucho menos claro estar ser su madre
el otro ave lo agredir también le arrancar todo el pluma y el pobre caer ensangrentar en_medio_de el rosal
pobre animal
decir el rosa
ver te ocultar
apoyar el cabeza sobre nosotros
el gorriona extender por último vez el ala luego lo oprimir contra el cuerpo y expirar en el seno de el familia vecino de el fresco y perfumar rosa
pip
decir el gurriato en el nido no entender dónde poder estar nuestro madre
no ser uno treta suyo para que nos despabilar por nuestro cuenta y nos buscar el comida
nos haber dejar en herencia el casa pero quién de nosotros se quedar con él cuando llegar el hora de constituir uno familia
pues ya ver cómo lo echar de aquí el día en que ampliar mi hogar con mujer y hijo decir el más pequeño
yo tener mujer y hijo antes que tú
replicar el 2
yo ser el mayor
gritar 1/3
todo empezar a increpar se a propinar se aletazo y picotazo y paf
uno tras otro ir caer de el nido pero aun en el suelo seguir pelear se
con el cabeza de lado guiñar el ojo dirigir hacia arriba ser su modo de manifestar su enfado
saber ya volar uno poquitín luego se ejercitar uno poco más y por último convenir en que para reconocer se si alguno vez se encontrar por ese mundo de dios decir 3 vez pip
y rascar otro tanto con el pie izquierdo
el más pequeño que haber quedar en el nido se instalar a_sus_anchas pues haber quedar como único propietario pero no durar mucho su satisfacción
aquel mismo noche se incendiar el casa el rojo llama estallar a_través_de el ventana prender en el paja seco de el techo y en uno momento el cortijo entero quedar reducir a ceniza
el matrimonio poder salvar se pero el gurriato morir abrasar
cuando salir el sol a el mañana siguiente y todo parecer despertar de uno sueño tranquilo y reparador de el casa no quedar más que alguno viga carbonizar que se sostener contra el chimenea el único que seguir en pie
de entre el resto salir aun uno denso humareda pero delante se alzar lozano y florido el rosal cuyo rama y flor se reflejar en el agua límpido y tranquilo
qué bello ser el rosa frente_a el casa incendiar
exclamar uno hombre que acertar a pasar por allí
ir a tomar uno apunte
sacar de el bolsillo uno lápiz y uno cuaderno de hoja blanco pues ser pintor y dibujar el escombro humeante el madero calcinar sobre el chimenea que se inclinar cada vez más y en 1 término el gran rosal florido que ser verdaderamente hermoso y constituir el motivo central de el cuadro
poco hora más tarde pasar por el lugar 2 de el gorrión que hablar nacer allí
dónde estar el casa
preguntar
dónde estar el nido
pip
todo se haber consumir y nuestro valiente hermano haber morir achicharrar
le estar bien emplear por haber se querer quedar con el nido
el rosa haber escapar con vida helas ahí con su mejilla colorar
el desgracia de el vecino lo dejar tan fresco
no querer dirigir le el palabra
este sitio se me hacer insoportable
y se echar a volar
en uno hermoso y solear día de el siguiente otoño que parecer de verano bajar el paloma a el seco y limpio suelo de el patio que se extender frente_a el gran escalera de el hacienda señorial
lo haber negro y blanco y abigarrar su pluma brillar a el sol y el viejo madre decir a el pichón agruparse chico agrupar se
pues así parecer mejor
quién ser ese pequeñín pardusco que saltar entre nosotros
preguntar uno paloma cuyo ojo despedir destello rojo y verde
pequeñín pequeñín
decir
ser gorrión pobrecillos
siempre haber tener fama de ser bondadoso dejar le que se llevar uno granito
hablar poco entre ellos y rascar tan graciosamente con el pie
rascar en efecto 3 vez lo hacer con el pie izquierdo decir a el mismo tiempo pip
y entonces se reconocer ser 3 gorrión de el nido de el casa quemar
qué bien se comer aquí
decir el gorrión
y el paloma se pasear a su alrededor pavonear se y guardar se su opinión
fijar te en aquel buchón
decir uno de el paloma a su vecino
qué manera de tragar se el arbejones
comer demasiar y se quedar con el mejor además
curr curr
mirar cómo se le henchir el buche
ir con el bicho feo y asqueroso
curr curr
y su ojo despedir rojo chispa de indignación
agrupar se agrupar se
pequeñines pequeñines
curr curr
así discurrir el cosa entre el amable paloma y el pichón y así ser de esperar que seguir discurrir dentro_de 1000 año
el gorrión se tratar a_cuerpo_de_rey se mover a_sus_anchas entre el paloma aunque no se encontrar en su elemento
harto a el fin se largar mientras intercambiar opinión acerca_de su huésped
saltar luego el valla de el jardín y como estar abrir el puerta de el habitación que dar a él uno saltar a el umbral
haber comer muy bien y se sentir animoso
pip
decir me lanzar
pip
decir el otro también yo me lanzar y más aun que tú
y se entrar en el habitación
no haber nadie en él y el 3 a el ver lo de uno volada se plantar en el centro y decir o dentro_de el todo o nada
ser curioso el nido de el hombre
tomar
qué ser ese
ser el rosa de el viejo casa que se reflejar en el agua y el viga carbonizar apoyar contra el ruinoso chimenea
cómo haber ir a parar aquel a el habitación de el hacienda señorial
el 3 gorrión se alzar para volar por encima_de el rosa y de el chimenea pero ir a chocar contra uno pared
ser uno cuadro uno grande y magnífico cuadro que el pintor haber componer a_base_de su apunte
pip
decir el gorrión
no ser nada sólo ser apariencia
pip
este ser el belleza
lo comprender
yo no
y se alejar volar pues entrar persona en el cuarto
transcurrir día y aun año el paloma arrullar mucho vez por no decir gruñir el muy enredón
el gorrión pasar el invierno helar se y el verano dar se el gran vida
todo estar ya prometer o casar como se querer
tener pequeño y como ser natural cada uno creer que el suyo ser el más listo y hermoso
uno volar por aquí otro por allá y cuando se encontrar se reconocer por su pip
y el triple rascar con el pie izquierdo
el más viejo ser uno gorriona solterón que no tener lo nido ni polluelo
deseoso de ir se a uno gran ciudad emprender el vuelo hacia copenhague
haber allí cerca de el palacio uno gran casa pintar de vivo color junto_a el canal donde amarrar barco cargar de manzana y mucho otro cosa
el ventana ser más ancho por el parte inferior que por el superior y si el gorrión mirar dentro_de el edificio cada habitación se le aparecer como uno tulipán con 1000 color y arabesco y en el centro de el flor haber personaje blanco de mármol aunque alguno ser de yeso pero este no saber distinguir lo el ojo de el gorrión
en el cima de el casa haber uno grupo de bronce figurar uno cuadriga guiar por el dios de el victoria y todo ser de metal el carro el caballo y el dios
ser el museo thorwaldsen
cómo brillar cómo brillar
decir el gorriona
seguramente este ser el belleza
pip
pero aquí ser mucho mayor que en el pavo
recordar que ser niño su madre le haber decir que el belleza más grande estar en el pavo
bajar a el patio donde todo ser magnífico con palmera y rama pintar en el pared en el centro crecer uno gran rosal lleno de rosa que se extender hasta el lado oponer de uno tumba
volar hasta allí y se encontrar con varios gorrión que agitar el ala
decir pip
y rascar 3 vez con el pie izquierdo aquel saludo tan querer que tanto vez dirigir a uno y otro en el curso de su vida sin que nadie lo comprender pues el que uno vez se separar no soler volver a encontrar se todo el día
pero aquel forma de saludar se haber convertir en hábito en él y haber aquí que ahora se topar con 2 viejo gorrión y uno joven que decir pip
y rascar con el pie izquierdo
ah hola bueno día bueno día
ser 3 gorrión de el viejo nido con otro más joven que formar_parte de el familia
aquí nos encontrar
decir
ser uno lugar muy distinguir pero lo que ser comida no sobrar
este ser el belleza
pip
entrar mucho persona que venir de el sala lateral donde se hallar el magnífico estatua de mármol y se dirigir a el tumba que guardar el resto de el gran maestro autor de todo aquel escultura
cuanto se acercar contemplar con rostro radiante el sepultura de thorwaldsen alguno recoger el pétalo de rosa caer y lo guardar
alguno venir de muy lejos_de inglaterra_alemania y francia y el más hermoso de el señor coger uno rosa y se lo prender en el pecho
pensar entonces el gorrión que allí reinar el rosa que el casa haber ser construir para ellos y le parecer uno tanto exagerar pero ver que el humano mostrar tanto amor por el flor no querer ellos ser menos
pip
decir poner se a barrer el suelo con el rabo y guiñar el ojo a el rosa
no bien lo haber ver quedar persuadir de que ser su antiguo vecino y en efecto lo ser
el pintor que dibujar el rosal junto_a el viejo casa de campo incendiar haber obtener permiso ya avanzar el año para trasplantar lo y lo haber regalar a el arquitecto pues en ninguno sitio crecer rosa tan hermoso
el arquitecto haber plantar el rosal sobre el tumba de thorwaldsen donde florecer como símbolo de el belleza dar rosa encarnar y fragante que el turista se llevar como recordar a su lejano país
haber encontrar acomodo en el ciudad
preguntar el gorrión
el rosa contestar con uno gesto afirmativo y reconocer a su pardo vecino de el estanque campesino se alegrar de volver a ver lo
qué bello ser vivir y florecer encontrar se con antiguo amigo y conocido y ver siempre cara amable
aquí ser como si todo el día ser uno gran fiesta
pip
decir el gorrión
sí ser nuestro antiguo vecino su descendiente de el balsa de el pueblo se acordar de nosotros
pip
qué suerte haber tener
lo haber que hasta dormir hacer fortuna
y el verdad ser que no comprender qué belleza poder haber en uno cabeza rojo como el suyo
allí haber uno hoja seco lo ver muy bien
se poner a picotear hasta que caer pero el rosal quedar aun más lozano y más verde y el rosa seguir enviar su perfume a el tumba de thorwaldsen a cuyo nombre inmortal se haber asociar su belleza

