hacer de este más de 100 año
detrás_de el bosque a orilla de uno gran lago se levantar uno viejo palacio rodear por uno profundo foso en el que crecer cañaveral juncal y carrizo
junto_a el puente en el puerta principal hablar uno viejo sauce cuyo rama se inclinar sobre el caña
desde el valle llegar son de cuerno y trote de caballo por ese el zagala se dar prisa en sacar el ganso de el puente antes_de que llegar el partida de cazador
venir este a todo galope y el muchacho haber de subir se de uno brinco a uno de el alto piedra que sobresalir junto_a el puente para no ser atropellar
ser casi uno niño delgado y flacucho pero en su rostro brillar 2 ojo maravillosamente límpido
mas el noble caballero no reparar en ellos a pleno galope blandir el látigo por puro capricho dar con él en el pecho de el pastor con tanto fuerza que lo derribar
cada cosa en su sitio
exclamar
el tuyo ser el estercolero
y soltar uno carcajada pues el chiste le parecer gracioso y el demás le hacer coro
todo el grupo de cazador prorrumpir en uno estruendoso griterío a el que se sumar el ladrido de el perro
ser el que decir el canción borrachas llegar el rico ave
dios saber el rico que ser
el pobre muchacho a el caer se agarrar a uno de el rama colgante de el sauce y gracias_a él poder quedar suspender sobre el barrizal
en cuanto el señor y el jauría haber desaparecer por el puerta él tratar de salir de su atolladero pero el rama se quebrar y el muchacho caer en_medio_de el cañaveral sentir en el mismo momento que lo sujetar uno mano robusto
ser uno buhonero que haber presenciar todo el escena desde alguno distancia correr en su auxilio
cada cosa en su sitio
decir remedar a el noble en tono de burla y poner a el muchacho en uno lugar seco
luego intentar volver a adherir el rama quebrar a el árbol pero ese de cada cosa en su sitio no siempre tener aplicación y así lo clavar en el tierra reblandecer
crecer si poder crecer hasta convertir te en uno bueno flauta para el gente de el castillo
con ello querer augurar a el noble y el suyo uno bien merecer castigo
subir después a el palacio aunque no pasar a el salón de fiesta no ser bastante distinguir para ello
sólo le permitir entrar en el habitación de el servidumbre donde ser examinar su mercancía y discutir el precio
pero de el salón donde se celebrar el banquete llegar el griterío y alboroto de el que querer ser canción no saber hacer lo mejor
resonar el carcajada y el ladrido de el perro
se comer y beber con el mayor desenfreno
el vino y el cerveza espumeaban en copa y jarro y el can favorito participar en el festín el señorito lo besar después_de secar le el hocico con el largo oreja colgante
el buhonero ser a el fin introducir en el salón con su mercancía sólo querer divertir se con él
el vino se le haber subir a el cabeza expulsar de él a el razón
le servir cerveza en uno calcetín para que beber con ellos pero deprisa
uno ocurrencia por demás gracioso como se ver
rebaños entero de ganar cortijo con su campesino ser jugar y perder a uno solo carta
cada cosa en su sitio
decir el buhonero cuando haber poder escapar sano y salvo de aquel sodoma y gomorra como él lo llamar
mi sitio ser el camino bajo el cielo y no allá arriba
y desde el vallado se despedir de el zagala con uno gesto de el mano
pasar día y semana y aquel rama quebrar de sauce que el buhonero plantar junto_a el foso seguir verde y lozano incluso salir de él nuevo vástago
el doncella ver que haber echar raíz el cual le producir gran contento pues le parecer que ser su propio árbol
y así ser prosperar el joven sauce mientras en el propiedad todo decaer y marchar del_revés a_fuerza_de francachela y de juego 2 rueda muy poco apropiar para hacer avanzar el carro
no haber transcurrir aun 6 año cuando el noble haber de abandonar su propiedad convertir en pordiosero sin más haber que uno saco y uno bastón
lo comprar uno rico buhonero el mismo que TM_d:1 ser objeto de el burla de su antiguo propietario cuando le servir cerveza en uno calcetín
pero el honradez y el laboriosidad llamar a el viento favorable y ahora el comerciante ser dueño de el noble mansión
desde aquel momento quedar desterrar de él el naipe
malo cosa
decir el nuevo dueño
venir de que el diablo después que haber leer el biblia querer fabricar uno caricatura de él y idear el juego de carta
el nuevo señor contraer matrimonio con quién decir
pues con el zagala que se haber conservar honesto piadoso y bueno
y en su nuevo vestido aparecer tan pulcro y distinguir como si haber nacer en noble cuna
cómo ocurrir el cosa
bueno para nuestro tiempo tan ajetrear ser este uno historia demasiado largo pero el caso ser que suceder y ahora venir el más importante
en el antiguo propiedad todo marchar a_las_mil_maravillas el madre cuidar de el gobierno doméstico y el padre de el faena agrícola
llover sobre ellos el bendición el prosperidad llamar a el prosperidad
el viejo casa señorial ser reparar y embellecer se limpiar el foso y se plantar en ellos árbol frutal el casa ser cómoda acogedor y el suelo brillante y limpio
en el velada de invierno el ama y su criado hilar lana y lino en el gran salón y el domingo se leer el biblia en alto voz encargar se de ello el consejero comercial pues a este dignidad haber ser elevar el exbuhonero en el último año de su vida
crecer el hijo pues haber venir hijo y todo recibir bueno instrucción aunque no todo ser inteligente en el mismo grado como soler suceder en el familia
el rama de sauce se haber convertir en uno árbol exuberante y crecer en pleno libertad sin ser podar
 es nuestro árbol familiar
decir el anciano matrimonio y no se cansar de recomendar a su hijo incluso a el más ligero de casco que lo honrar y respetar siempre
y ahora dejar transcurrir 100 año
estar en el tiempo presente
el lago se haber transformar en uno cenagal y de el antiguo mansión nobiliario apenas quedar vestigio uno largo charca con uno ruina de piedra en uno de su borde ser cuanto subsistir de el profundo foso en el que se levantar uno espléndido árbol centenario de rama colgante ser el árbol familiar
allí seguir mostrar el hermoso que poder ser uno sauce cuando se lo dejar crecer en libertad
cierto que tener hender el tronco desde el raíz hasta el copa y que el tempestad lo haber torcer uno poco pero vivir y de todo su grieta y desgarradura en el que el viento y el intemperie haber depositar tierra fecundo brotar flor y hierba principalmente en lo alto allí donde se separar el grande rama se haber formar uno especie de jardín colgante de frambuesa y otro planta que suministrar alimento a el pajarillo hasta uno gracioso acerolo haber echar allí raíz y se levantar esbelto y distinguir en_medio_de el viejo sauce que se mirar en el agua negro cada vez que el viento barrer el lenteja acuático y lo arrinconar en uno ángulo de el charca
uno estrecho sendero pasar a_través_de el campo señorial como uno trazo hacer en uno superficie sólido
en el cima de el colina lindante con el bosque desde el cual se dominar uno soberbio panorama se alzar el nuevo palacio inmenso y suntuoso con cristal tan transparente que se haber decir que no lo haber
el gran escalinata frente_a el puerta principal parecer uno galería de follaje uno tejido de rosa y planta de amplio hoja
el césped ser tan limpio y verde como si cada mañana y cada tarde alguien se entretener en quitar hasta el más ínfimo brizna de hierba seco
en_el_interior_de el palacio valioso cuadro colgar de el pared y haber silla y diván tapizar de terciopelo y seda que parecer capaz de mover se por su propio pie mesa con tablero de blanco mármol y libro encuadernar en tafilete con canto de oro
ser gente muy rico el que allí residir gente noble ser barón
reinar allí uno gran orden y todo estar en relación con el demás
cada cosa en su sitio decir el dueño y por ese el cuadro que antaño haber adornar el pared de el viejo casa colgar ahora en el habitación de el servicio
ser trasto viejo en particular aquel 2 antiguo retrato uno de el cual representar uno hombre en casaca rosa y con enorme peluca y el otro uno dama de cabello empolvar y alto peinado que sostener uno rosa en el mano rodear uno y otro de uno gran guirnalda de rama de sauce
el 2 cuadro presentar numeroso agujero producir por el baronesitos que lo haber tomar por blanco de su flecha
ser el consejero comercial y el señor consejera el fundador de el linaje
sin embargo no pertenecer de el todo a nuestro familia decir uno de el baronesitos
él haber ser buhonero y él pastor
no ser como papá y mamá
aquel retrato ser trasto viejo y cada cosa en su sitio
se decir por ese el bisabuelo y el bisabuelo haber ir a parar a el cuarto de el servidumbre
el hijo de el párroco estar de preceptor en el palacio
TM_d:1 salir con el señorito y el mayor de el hermano que acabar de recibir su confirmación
ir por el sendero que conducir a el viejo sauce y por el camino el jovencito hacer uno ramo de flor silvestre
cada cosa en su sitio y de su mano salir uno obra artístico de raro belleza
mientras disponer el ramo escuchar atentamente cuanto decir el otro y sentir uno gran placer oír a el hijo de el párroco hablar de el fuerza de el naturaleza y de el vida de grande hombre y mujer
ser uno muchacho de alma sano y elevar de noble sentimiento y dotar de uno corazón capaz de recoger amorosamente cuanto de bueno haber crear dios
se detener junto_a el viejo sauce
el menor de el niño pedir que le fabricar uno flauta como lo haber tener ya de otro sauce y el preceptor romper uno rama de el árbol
oh no lo hacer
decir el baronesa pero ya ser tarde es nuestro viejo árbol famoso
lo querer mucho
en casa se me reír por ese pero me dar el mismo
haber uno leyenda acerca_de ese árbol
y contar cuanto haber oír de el sauce de el viejo castillo de el zagala y el buhonero que se haber conocer en aquel lugar y ser el fundador de el noble familia de el baronesa
no querer ser elevar a el nobleza ser probo y íntegro decir
tener por lema cada cosa en su sitio y temer sentir se fuera_de su sitio si se dejar ennoblecer por dinero
su hijo mi abuelo ser el 1 barón tener entender que ser uno hombre sabio de gran prestigio y muy querer de príncipe y princesa que lo invitar a todo su fiesta
a él ir el admiración de mi familia pero yo no saber por qué el viejo bisabuelo me inspirar más simpatía
qué vida tan recoger y patriarcal deber de llevar se en el viejo palacio donde el ama hilar en_compañía_de su criado y el anciano señor leer el biblia en voz alto
ser gente sensato y de gran corazón asentir el hijo de el párroco y de pronto se encontrar enzarzar en uno conversación sobre el nobleza y el burguesía y casi parecer que el preceptor no formar_parte de este último clase tal ser el calor con qué encomiar a el 1
ser uno suerte pertenecer a uno familia que se haber distinguir y por ello llevar uno impulso en el sangre uno anhelo de avanzar en todo el bueno
ser magnífico llevar uno apellido que abrir el acceso a el familia más encumbrar
nobleza ser palabra que se definir a sí mismo ser el moneda de oro que llevar su valor en su cuño
el espíritu de el época afirmar y mucho escritor estar de_acuerdo_con él naturalmente que todo el que ser noble haber de ser malo y disparatar mientras en el pobre todo ser brillante tanto más cuanto más se bajar en el escala social
pero yo no compartir este criterio que ser completamente erróneo y disparatar
en el clase superior encontrar mucho rasgo de conmovedor grandeza mi padre me contar uno a el que yo poder añadir otro mucho
TM_d:1 se encontrar de visita en uno casa distinguir de el ciudad en el que según tener entender mi abuelo haber criar a el señor
estar mi madre en el habitación a el lado de el noble y anciano señor cuando este se dar_cuenta de uno mujer de avanzar edad que caminar penosamente por el patio apoyar en 2 muleta
todo el domingo venir a recoger uno moneda
ser el pobre viejo decir el señor
le costar tanto andar
y antes_de que mi madre poder adivinar su intención haber cruzar el umbral y correr escalera abajo él su_excelencia en persona a el encuentro de el mendigo para ahorrar le el costoso esfuerzo de subir a recoger su limosna
ser sólo uno pequeño rasgo pero como el óbolo de el viudo resonar en el más hondo de el corazón y manifestar el bondad de el naturaleza humano y este ser el rasgo que deber destacar el poeta y más que nunca en nuestro tiempo pues reconfortar y contribuir a suavizar diferencia y a reconciliar a el gente
pero cuando uno persona por ser de sangre noble y poseer uno árbol genealógico como el caballo árabe se levantar como este sobre su pata trasero y relinchar en el calle y decir en su casa aquí haber estar gente de el calle
porque haber entrar alguien que no ser de el nobleza entonces el nobleza haber degenerar haber descender a el condición de uno máscara como aquel de tespis todo el mundo se burlar de el individuo y el sátira se ensañar con él
tal ser el discurso de el hijo de el párroco uno poco largo y entretanto haber quedar tallar el flauta
haber recepción en el palacio
asistir mucho invitado de el alrededor y de el capital y dama vestir con mayor o menor gusto
el gran salón pulular de visitante
reunir en uno grupo se ver a el clérigo de el comarca retirar respetuosamente en uno ángulo de el estancia como si se preparar para uno entierro cuando en realidad aquel ser uno fiesta sólo que aun no haber empezar de verdad
haber de dar se uno gran concierto para ello el baronesito haber traer su flauta de sauce pero todo su intento y el de su padre por arrancar uno nota a el instrumento haber ser vano y así lo haber arrinconar por inútil
se oír música y cantar de el clase que más divertir a el ejecutante aunque por el demás muy agradable
también usted ser uno virtuoso
preguntar uno caballero uno auténtico hijo de familia
tocar el flauta y se lo fabricar usted mismo
ser el genio que todo lo dominar y a quien corresponder el lugar de honor
dios nos guardar
yo marchar al_compás_de el época y este ser el que proceder
verdad que ir a deleitar nos con su pequeño instrumento
y alargar a el hijo de el párroco el flauta tallar de el sauce de el charca con voz claro y sonoro anunciar a el concurrencia que el preceptor de el casa lo obsequiar con uno solo de flauta fácil ser comprender que se proponer burlar se de él por el que el joven se resistir a_pesar_de ser uno bueno flautista
pero tanto insistir y lo importunar que coger el instrumento se lo llevar a su labio
ser uno flauta maravilloso
salir de él uno nota prolongar como el silbido de uno locomotora y más fuerte aun que resonar por todo el finca y más_allá_de el parque y el bosque por todo el país en uno extensión de milla y milla y a el mismo tiempo se levantar uno viento tempestuoso que bramar cada cosa en su sitio
y ya tener a papá volar como llevar por el viento hasta el casa de el pastor y a este volar a el palacio aunque no a el salón pues en él no poder entrar pero sí en el cuarto de el criado donde quedar en_medio_de todo el servidumbre y aquel orgulloso lacayo en librea y media de seda quedar como paralizar de espanto a el ver a uno individuo de tan humilde categoría sentar a el mesa entre ellos
en el salón el baronesa ser trasladar a el cabecera de el mesa el puesto principal y a su lado venir a parar el hijo de el párroco como si ser uno pareja de novio
uno anciano conde de el más rancio nobleza de el país permanecer donde estar en su lugar de honor pues el flauta ser justo como se deber ser
el caballero chistoso aquel hijo de familia que haber provocar el catástrofe volar de cabeza a el gallinero y no ser él solo
el son de el flauta se oír a varios legua a el redonda y en todo parte ocurrir cosa extraño
uno rico familia de comerciante que usar carroza de 4 caballo se ver arrojar de el carruaje ni siquiera le dejar uno puesto detrás
2 campesino acaudalar que en nuestro tiempo haber adquirir mucho bien además_de su campo propio ser a dar con su hueso en uno barrizal
ser uno flauta peligroso
afortunadamente reventar a el 1 nota y suerte haber de ello
entonces volver a el bolsillo
cada cosa en su sitio
a el día siguiente no se hablar ya de el sucedido de ahí venir el expresión guardarse el flauta
todo volver a quedar como antes excepto_que el 2 viejo retrato el de el buhonero y el de el pastor ser colgar en el gran salón a el que haber ser llevar por el ventolera y como uno entender en cosa de arte afirmar que se tratar realmente de obra maestro quedar definitivamente en_el_puesto_de honor
antes se ignorar su mérito cómo ir a saber se
pero desde aquel día presidir el salón cada cosa en su sitio y ahí lo tener
largo ser el eternidad más largo que este historia
