cada vez que morir uno niño bueno bajo de el cielo uno ángel de dios_nuestro_señor tomar en brazo el cuerpo morir y extender su grande ala blanco emprender el vuelo por encima_de todo el lugar que el pequeño amar recoger a_la_vez uno ramo de flor para ofrecer lo a dios con_objeto_de que lucir allá arriba más hermoso aun que en el suelo
nuestro señor se apretar contra el corazón todo aquel flor pero a el que más le gustar le dar uno beso con el cual él adquirir voz y poder ya cantar en el coro de el bienaventurado
haber aquí el que contar uno ángel de dios_nuestro_señor mientras se llevar a el cielo a uno niño morir y el niño lo escuchar como en sueño
volar por encima_de el diferente lugar donde el pequeño haber jugar y pasar por jardín de flor espléndido
cuál nos llevar para plantar lo en el cielo
preguntar el ángel
crecer allí uno magnífico y esbelto rosal pero uno mano perverso haber tronchar el tronco por el que todo el rama cuajar de grande capullo semiabierto colgar seco en todo dirección
pobre rosal
exclamar el niño
llevar te lo junto_a dios florecer
y el ángel lo coger dar uno beso a el niño por su palabra y el pequeño entreabrir el ojo
recoger luego mucho flor magnífico pero también humilde ranúnculo y violeta silvestre
ya tener uno bueno ramillete decir el niño y el ángel asentir con el cabeza pero no emprender enseguida el vuelo hacia dios
ser de noche y reinar uno silencio absoluto ambos se quedar en el gran ciudad flotar en el aire por uno de su angosto callejón donde yacer montón de paja y ceniza haber haber mudanza se ver casco de loza pedazo de yeso trapo y viejo sombrero todo ello de aspecto muy poco atractivo
entre todo aquel desperdicio el ángel señalar el trozo de uno tiesto romper de este se haber desprender uno terrón con el raíz de uno gran flor silvestre ya seco que por ese alguien haber arrojar a el calla
ir a llevar nos lo decir el ángel
mientras volar te contar por qué
remontar el vuelo y el ángel dar principio a su relato en aquel angosto callejón en uno bajo bodega vivir uno pobre niño enfermo
desde el día de su nacimiento estar en el mayor miseria todo el que poder hacer en su vida ser cruzar su diminuto cuartucho sostener en 2 muleta su felicidad no pasar de aquí
alguno día de verano uno rayo de sol entrar hasta el bodega nada más que medio hora y entonces el pequeño se calentar a el sol y mirar cómo se transparentar el sangre en su flaco dedo que mantener levantar delante el rostro decir sí hoy haber poder salir
saber de el bosque y de su bello verdor primaveral sólo porque el hijo de el vecino le traer el 1 rama de haber
se lo poner sobre el cabeza y soñar que se encontrar debajo_de el árbol en cuyo copa brillar el sol y cantar el pájaro
TM_d:1 de primavera su vecino le traer también flor de el campo y entre ellos venir casualmente uno con el raíz por ese lo plantar en uno maceta que colocar junto_a el cama a el lado de el ventana
haber plantar aquel flor uno mano afortunado pues crecer sacar nuevo rama y florecer cada año para el muchacho enfermo ser el jardín más espléndido su pequeño tesoro aquí en el tierra
lo regar y cuidar preocupar se de que recibir hasta el último de el rayo de sol que penetrar por el ventana el propio flor formar_parte de su sueño pues para él florecer para él esparcir su aroma y alegrar el vista a él se volver en el momento de el muerte cuando el señor lo llamar a su seno
llevar ya uno año junto_a dios y durante todo el año el planta haber seguir en el ventana olvidar y seco por ese cuando el mudanza lo arrojar a el basura de el calle
y este ser el flor el pobre flor marchito que haber poner en nuestro ramillete pues haber proporcionar más alegría que el más bello de el jardín de uno reina
pero cómo saber todo este
preguntar el niño que el ángel llevar a el cielo
lo saber responder el ángel porque yo ir aquel pobre niño enfermo que se sostener sobre muleta
y bien conocer mi flor
el pequeño abrir de par en par el ojo y clavar el mirada en el rostro esplendoroso de el ángel y en el mismo momento se encontrar en el cielo_de_nuestro_señor donde reinar el alegría y el bienaventuranza
dios apretar a el niño morir contra su corazón y a el instante le salir a este ala como a el demás ángel y con ellos se echar a volar coger de el mano
nuestro señor apretar también contra su pecho todo el flor pero a el marchito silvestre lo besar infundir le voz y él romper a cantar con el coro de angelito que rodear a el altísimo alguno muy de cerca otro formar círculo en_torno_a el 1 círculo que se extender hasta el infinito pero todo rebosante de felicidad
y todo cantar grande y chico junto_con el bueno chiquillo bienaventurado y el pobre flor silvestre que haber estar abandonar entre el basura de el calla estrecho y oscuro el día de el mudanza
fin
