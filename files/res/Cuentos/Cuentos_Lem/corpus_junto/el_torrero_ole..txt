en el mundo todo ser subir y bajar y bajar y subir
yo no poder subir ya más arriba decir el torrero ole
arriba y abajo abajo y arriba el mayoría haber de pasar por ello
a_fin_de_cuentas todo acabar ser torrero para ver desde lo alto el vida y el cosa
así hablar ole en su torre mi amigo el viejo vigía uno hombre jovial que parecer decir todo el que llevar dentro pero que sin embargo se guardar mucho cosa y muy serio en el fondo de el corazón
ser hijo de_buena_familia afirmar alguno
según ellos ser hijo de uno consejero diplomático o poder haber lo ser
haber estudiar haber llegar a profesor auxiliar y a ayudante de sacristán pero de qué servir todo ese
cuando vivir en casa de el sacristán todo lo tener gratis
ser joven y guapo según decir
querer limpiar se el bota con crema brillante pero el sacristán sólo le dar betún ordinario por ese estallar el desavenencia entre ellos
uno hablar de avaricia el otro de vanidad el betún ser el negro motivo de el enemistad y así se separar
pero el que haber exigir a el sacristán lo exigir a todo el mundo crema brillante y le dar siempre vulgar betún
por ese huir de el hombre y se hacer ermitaño pero en uno ciudad uno puesto de ermitaño que a el mismo tiempo permitir ganar se el vida sólo se encontrar en uno campanario
a él se subir pues y se instalar fumar su pipa en su solitario morada mirar arriba y abajo reflexionar sobre el que ver y contar a su manera el que haber ver y el que no lo que haber leer en el libro y dentro_de sí mismo
yo le prestar con frecuencia algo que leer libro recomendable dime con quién andar y te decir quién ser
no dar uno maravedí por el novela para institutriz inglés ni por el francés componer de uno mezcla de aire y tallo de rosa el que querer ser relato vivir libro sobre el maravilla de el naturaleza
yo lo visitar por_lo_menos uno vez a el año generalmente el 1 día de [??:??/1/??:????:??] el cambio de año siempre soler sugerir le alguno pensamiento nuevo y interesante
le relatar 2 de mi visita y me atener a su palabra lo más fielmente que poder
1 visita entre el libro que últimamente haber prestar a ole haber uno sobre el sílice que le haber interesar y divertir de uno manera especial
ser uno verdadero matusalén ese sílice decir y pasar junto_a ellos sin prestar le el menor atención
también yo lo haber hacer en el campo y en el playa donde estar a montón
caminar sobre el adoquín sin pensar en que ser vestigio de el más remoto antigüedad
yo mismo lo haber hacer
pero desde ahora cada losa poder contar con todo mi respeto
gracia por el libro que me haber enriquecer me haber librar de mi viejo idea y costumbre y me haber hacer venir gana de enterar me de más cosa
el novela de el tierra ser el más notable de todo no caber duda
lástima que no poder leer el 1 capítulo por no conocer el lenguaje
haber que leer en todo el estrato de el tierra en el guijarro en el diverso período geológico y sólo en el 6 parte aparecer el personaje humano el señor adán y el señor eva
mucho lector encontrar que venir algo tarde preferir que salir desde el principio pero a mí me dar igual
ser uno novela lleno de aventura en el que todo desempeñar uno papel
nos mover y ajetrear y sin embargo estar siempre en el mismo sitio pero el esfera girar sin abocar nos encima el océano
el corteza que pisar se aguantar firme no nos hundir en él y todo este en uno proceso que venir durar desde hacer millón de año
gracia por el libro sobre el guijarro
el que nos contar si poder hablar
no ser uno satisfacción convertir me por uno momento en uno cero aunque se estar tan alto como yo estar y que de_repente os recordar que todo incluso el más lustroso no ser en este tierra más que hormiga efímero incluso el hormiga lleno de condecoración el hormiga de_primera_clase
se sentir uno tan ridículamente joven frente_a ese piedra venerable que contar millón de año
el víspera de año_nuevo estar leer este libro y me enfrascar tanto en él que me olvidar de ir a ver mi espectáculo habitual en este fecha la salvaje tropa de amager
claro usted no saber lo que ser ese
todo el mundo haber oír hablar de el cabalgata de el brujo sobre su palo de escoba
se celebrar en el blocksberg el noche de san_juan
pero tener otro cabalgata no menos salvaje aunque más nacional y moderno que acudir a amager el noche de año_nuevo
todo el malo poeta poetisa actor periodista y artista de el publicidad verdadero hueste de gente inútil se congregar en amager en decir día montar a_horcajadas sobre su pincel o pluma de ganso el de acero no poder llevar lo ser demasiado rígido
como ya decir presenciar este espectáculo cada nochevieja
poder dar el nombre de el mayoría de el concurrente pero ser gente con el que no interesar entablar relación
además tampoco a ellos le gustar mucho que el público se enterar de su viaje a amager montar en su pluma de ganso
tener uno especie de prima uno vendedor de pescado que según él decir suministrar 3 hoja de palabra malévolo muy acreditar por el demás estar allí como invitado pero lo echar pues ni manejar el pluma de ganso ni saber montar
él lo haber contar
el mitad de el que decir ser mentira pero nos bastar con el resto
el ceremonia empezar con canto cada invitado haber componer su canción y cada uno cantar el suyo que a su juicio ser el mejor
pero todo venir a ser lo mismo
luego desfilar en corrillo el que se imponer por su mucho labia ser el que dar el grande campanada
le seguir el tamborilero menor que lo pregonar todo en el familia
allí se dar_a_conocer el que escribir sin dar su nombre ser decir el que hacer pasar betún ordinario por crema brillante
allí estar el verdugo y su asistente y este ser el más entusiasta pues de otro modo no le haber hacer caso
y también estar el bueno basurero que verter el cubo y lo calificar de bueno muy bueno excelente
en_medio_de tanto diversión pues todo el mundo deber divertir se salir de el pozo uno tallo uno árbol uno flor monstruoso uno gran hongo tan ancho como uno tejado ser el cucaña de el respetable asamblea de el que colgar todo el que haber dar a el mundo en el curso de el año que acabar de transcurrir
de él saltar chispa como llamarada ser todo el pensamiento y idea ajeno que ellos se haber apropiar y que ahora se desprender y salir despedir como uno castillo de fuego artificial
se representar uno mascarada y el poetastro recitar su producción
el más gracioso hacer juego de palabra pues no se tolerar cosa de menor categoría
el chiste resonar como si ser golpe de olla vacío contra el puerta
según mi prima ser divertido
en realidad decir mucho cosa más tan malicioso como entretenida pero me el callo pues haber que ser bueno persona pero no charlatán
por el decir se haber hacer_cargo de que saber el que allí ocurrir ser más que natural que cada noche de año_nuevo uno estar atento para presenciar el desfile de el tropa salvaje
si uno año echar de menos alguno otro ocupar su puesto
pero este vez no ver a ninguno de el invitado el guijarro me transportar a mucho legua de ellos a millón de año de distancia contemplar cómo el piedra se soltar con estrépito y marchar a el deriva arrastrar por el hielo mucho antes_de que se haber construir el arca de noé
lo ver caer a el fondo y emerger de nuevo sobre uno banco de arena que sobresalir de el agua decir esto ser zelanda
lo ver convertir se en refugio de ave de especie desconocer y de caudillo salvaje que aun conocer menos hasta que el hacha imprimir su runa en alguno piedra que luego poder servir para el cómputo de el tiempo
pero yo me haber esfumar por completo convertir en nada
caer entonces 3 4 estrella fugaz magnífico y brillante y el pensamiento tomar otro dirección
usted saber seguramente lo que ser uno estrella fugaz
pues el sabio no lo saber
yo tener mi idea acerca_de ellos y de mi idea parto
cuánto vez se pronunciar con íntimo sentimiento de gratitud el nombre de el que haber crear cosa tan bueno y admirable
con frecuencia el gratitud ser silencioso pero no se perder por ello
yo imaginar que lo recoger el sol y uno de su rayo llevar el sentimiento hasta el bienhechor
si ser uno pueblo entero el que enviar su agradecimiento a el largo de el año entonces este llegar como uno ramillete que se depositar sobre el tumba de el bienhechor
para mí resultar uno verdadero placer el contemplar el paso de uno estrella fugaz especialmente en el noche de año_nuevo conjeturar a quién ir dirigir aquel ramillete de gratitud
hacer poco caer uno brillante hacia el sudoeste uno acción de gracia de mucho y mucho persona a quién ir destinar
sin duda caer en el ladera de el fiordo de flensburg donde el darebrog acariciar con su hálito el tumba de schleppegrell_lässöe y su compañero
uno caer en el centro de el país cerca de sorö uno ramo sobre el tumba de holberg expresión de gratitud de tanto y tanto por su bello obra teatral
ser uno magnífico pensamiento y reconfortante el de saber que uno estrella fugaz caer sobre nuestro sepultura
no ser sobre el mío ser cierto ninguno rayo de sol me traer palabra de gratitud pues no haber motivo
yo no dar lustre a nada terminar ole mi sino en el mundo haber ser el servir de betún ordinario
2 visita era_año_nuevo cuando me presentar en el torre ole me hablar de el copa que se vaciar con_ocasión_de el trasiego de el viejo goteo a el nuevo goteo como él llamar a el año
luego me contar su historia de el copa que no dejar de tener su miga
cuando el reloj dar el 12 campanada en el último noche de el año el gente reunir en_torno_a el mesa levantar el copa y brindar por el año que empezar
se entrar en él con el vaso en el mano bueno principio para el bebedor
si se iniciar ir se a el cama entonces ser bueno principio para el holgazán
en el transcurso de el año el sueño desempeñar indudablemente uno importante papel pero el copa también
saber usted quién habitar en el copa
me preguntar
pues morar en ellos el salud el alegría y el desenfreno y también el enojo y el amargo desventura
cuando contar el copa contar naturalmente el brindis que se hacer para el distinto persona
ver
el 1 copa ser el de el salud
en él crecer el hierba salutífera
si el fijo en el viga a el término de el año poder estar en el glorieta de el salud
tomar ahora el 2 copa
de él volar uno pajarito piar ingenuo y alegremente por el que el hombre aguzar el oído y tal_vez cantar con él la vida ser bello
no agachar el cabeza
valor y adelante
de el 3 copa salir uno mocito alado no se le poder llamar uno ángel pues tener sangre y mentalidad de duende no por malicia sino por puro travesura
si se colocar detrás_de el oreja nos inspirar uno alegre ocurrencia
si se instalar en nuestro corazón este se calentar tanto que uno se sentir retozón se volver uno bueno cabeza a juicio de el demás cabeza
en el 4 copa no haber hierba ni pájaro ni chiquillo en él se encontrar el norma de el entendimiento y nunca haber que salir se de el norma
si tomar el 5 copa llorar sobre ti mismo sentir uno alegría interior o te desahogar de uno manera o otro
saltar de el copa con uno chasquido el príncipe carnaval locuaz y travieso te arrastrar y te olvidar de tu dignidad suponer que lo tener
olvidar más cosa de el que deber
todo ser baile cantar y bullicio el máscara te llevar con ellos el hijo de el diablo vestir de seda y terciopelo venir con el pelo suelto y el hermoso miembro huir de ellos si poder
el 6 copa
oh
en él estar satán en persona uno hombrecillo bien vestido elocuente agradable amable que te comprender perfectamente te dar siempre el razón ser todo tu yo
acudir con uno linterna y te guiar a casa
existir uno viejo leyenda acerca_de aquel santo que deber elegir uno entre el 7 pecado capital y parecer le que ser el menor escoger el embriaguez y de este modo se quedar con el 6 restante
el hombre y el diablo mezclar su sangre este ser el 6 copa y entonces proliferar todo el germen de el mal cada uno de el cual se alzar con uno fuerza semejante a el de el semilla de mostaza de el biblia que crecer hasta convertir se en uno árbol y se extender por el mundo entero y a el mayoría no le quedar entonces más remedio que ir a parar a el crisol para ser refundir
este ser el historia de el copa decir el torrero ole
y poder contar se junto_con el de el crema brillante y el betún
yo le poner el 2 a su disposición
tal ser el 2 visita a ole
si te apetecer saber más de él haber que menudear ese visita

