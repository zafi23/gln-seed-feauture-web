haber uno vez 25 soldado de plomo hermano todo ya_que lo haber fundir en el mismo viejo cuchara
fusil a el hombro y el mirada a el frente así ser como estar con su espléndido guerrera rojo y su pantalón azul
el 1 que oír en su vida cuando se levantar el tapa de el caja en que venir ser quotsoldaditos de plomo
quot había ser uno niño pequeño quien gritar este batir palma pues ser su regalo de cumpleaños
enseguida lo poner en fila sobre el mesa
cada soldado ser el vivo imagen de el otro con_excepción_de uno que mostrar uno pequeño diferencia
tener uno solo pierna pues a el fundir lo haber ser el último y el plomo no alcanzar para terminar lo
así_y_todo allí estar él tan firme sobre su único pierna como el otro sobre el 2
y ser de este soldado de quien ir a contar el historia
en el mesa donde el niño lo acabar de alinear haber otro mucho juguete pero el que más interés despertar ser uno espléndido castillo de papel
por su diminuto ventana poder ver se el salón que tener en su interior
a el frente haber uno árbol que rodear uno pequeño espejo
este espejo hacer el vez de lago en el que se reflejar nadar uno blanco cisne de cera
el conjunto resultar muy hermoso pero el más bonito de todo ser uno damisela que estar de pie a el puerta de el castillo
él también estar hacer de papel vestir con uno vestido de claro y vaporoso muselina con uno estrecho cinta azul anudar sobre el hombro a_manera_de banda en el que lucir uno brillante lentejuela tan grande como su cara
el damisela tener el 2 brazo en alto pues haber de saber usted que ser bailarín y haber alzar tanto uno de su pierna que el soldado de plomo no poder ver dónde estar y creer que como él sólo tener uno
 ésta ser el mujer que me convenir para esposo  se decir
 pero qué fino ser si hasta vivir en uno castillo
yo en cambio sólo tener uno caja de cartón en el que ya habitar 25 no ser uno lugar propio para él
de_todos_modos pasar el que pasar tratar de conocer lo  y se acostar cuan largo ser detrás_de uno caja de tabaco que estar sobre el mesa
desde allí poder mirar a el elegante damisela que seguir parada sobre uno solo pierna sin perder el equilibrio
ya avanzar el noche a el otro soldado de plomo lo recoger en su caja y todo el gente de el casa se ir a dormir
a ese hora el juguete comenzar su juego recibir visita pelear se y bailar
el soldado de plomo que también querer participar de aquel alboroto se esforzar ruidosamente dentro_de su caja pero no conseguir levantar el tapa
el cascanueces dar salto mortal y el tiza se divertir escribir broma en el pizarra
tanto ruido hacer el juguete que el canario se despertar y contribuir a el escándalo con uno trino en verso
el único que ni pestañear siquiera ser el soldado de plomo y el bailarín
él permanecer erguir sobre el punta de el pie con el 2 brazo a el aire él no estar menos firme sobre su único pierna y sin apartar uno solo instante de él su ojo
de pronto el reloj dar el 12 campanada de el [??:??/??/??:0000:am] y crac
se abrir el tapa de el caja de rapé
mas creer usted que contener tabaco
no lo que allí haber ser uno duende negro algo así_como uno muñeco de resorte
soldado de plomo
gritar el duende
querer hacer me el favor de no mirar más a el bailarín
pero el soldado se hacer el sordo
estar bien esperar a mañana y ver decir el duende negro
a el otro día cuando el niño se levantar alguien poner a el soldado de plomo en el ventana y ya ser obra de el duende o de el corriente de aire el ventana se abrir de_repente y el soldado se precipitar de cabeza desde el 3 piso
ser uno caída terrible
quedar con su único pierna en alto descansar sobre el casco y con el bayoneta clavar entre 2 adoquín de el calle
el sirviente y el niño bajar apresuradamente a buscar lo pero aun cuando faltar poco para que lo aplastar no poder encontrar lo
si el soldado haber gritar quotaquí estar
quot lo haber ver
pero él creer que no estar bien dar grito porque vestir uniforme militar
luego empezar a llover cada vez más y más fuerte hasta que el lluvia se convertir en uno aguacero torrencial
cuando escampar pasar 2 muchacho por el calle
qué suerte
exclamar uno
aquí haber uno soldado de plomo
ir a hacer lo navegar
y construir uno barco con uno periódico colocar a el soldado en el centro y allá se ir por el agua de el cuneta abajo mientras el 2 muchacho correr a su lado dar palmada
santo cielo cómo se arremolinar el ola en el cuneta y qué corriente tan fuerte haber
bueno después_de todo ya le haber caer uno bueno remojón
el barco de papel saltar arriba y abajo y a vez girar con tanto rapidez que el soldado sentir vértigo
pero continuar firme y sin mover uno músculo mirar hacia adelante siempre con el fusil a el hombro
de_buenas_a_primeras el barquichuelo se adentrar por uno ancho alcantarilla tan oscuro como su propio caja de cartón
quotme gustar saber adónde ir a parar  pensar
 apostaría a que el duende tener el culpa
si a el menos el pequeño bailarín estar aquí en el bote conmigo no me importar que este ser 2 vez más oscuroquot precisamente en ese momento aparecer uno enorme rata que vivir en el túnel de el alcantarilla
dónde estar tu pasaporte
preguntar el rata
a ver enseñar me tu pasaporte
pero el soldado de plomo no responder uno palabra sino_que apretar su fusil con más fuerza que nunca
el barco se precipitar adelante perseguir de cerca por el rata
ah
haber que ver cómo rechinar el diente y cómo le gritar a el estaca y paja que pasar por allí
detener lo
detener lo
no haber pagar el peaje
no haber enseñar el pasaporte
el corriente se hacer más fuerte y más fuerte y el soldado de plomo poder ya percibir el luz de el día allá en el sitio donde acabar el túnel
pero a_la_vez escuchar uno sonido atronador capaz de desanimar a el más valiente de el hombre
imaginar se usted
justamente donde terminar el alcantarilla el agua se precipitar en uno inmenso canal
aquel ser tan peligroso para el soldado de plomo como para nosotros el arriesgar nos en uno bote por uno gigantesco catarata
por_entonces estar ya tan cerca que no lograr detener se y el barco se abalanzar a el canal
el pobre soldado de plomo se mantener tan derecho como poder nadie decir nunca de él que haber pestañear siquiera
el barco dar 2 o 3 vuelta y se llenar de agua hasta el borde se hallar a_punto_de zozobrar
el soldado tener ya el agua a el cuello el barco se hundir más y más el papel de tan empapar comenzar a deshacer se
el agua se ir cerrar sobre el cabeza de el soldado de plomo  y este pensar en el lindo bailarín a el que no ver más y uno antiguo canción resonar en su oído adelante guerrero valiente
adelante te aguardar el muerte
en ese momento el papel acabar de deshacer se en pedazo y el soldado se hundir sólo para que a el instante uno gran pez se lo tragar
oh y qué oscuridad haber allí dentro
ser peor aun que el túnel y terriblemente incómodo por el estrecho
pero el soldado de plomo se mantener firme siempre con su fusil a el hombro aunque estar tender cuan largo ser
súbitamente el pez se agitar hacer el más extraño contorsión y dar uno vuelta terrible
por fin quedar inmóvil
al_poco_rato uno haz de luz que parecer uno relámpago lo atravesar todo brillar de nuevo el luz de el día y se oír que alguien gritar uno soldado de plomo
el pez haber ser pescado llevar a el mercado y vender y se encontrar ahora en el cocina donde el sirviente lo haber abrir con uno cuchillo
coger con 2 dedo a el soldado por el cintura y lo conducir a el sala donde todo el mundo querer ver a aquel hombre extraordinario que se dedicar a viajar dentro_de uno pez
pero el soldado no le dar el menor importancia a todo aquel
lo colocar sobre el mesa y allí  en fin cuánto cosa maravilloso poder ocurrir en este vida
el soldado de plomo se encontrar en el mismo salón donde haber estar antes
allí estar todo el mismo niño el mismo juguete sobre el mesa y el mismo hermoso castillo con el lindo y pequeño bailarín que permanecer aun sobre uno solo pierna y mantener el otro extender muy alto en el aire pues él haber ser tan firme como él
este conmover tanto a el soldado que estar a_punto_de llorar lágrima de plomo pero no lo hacer porque no haber estar bien que uno soldado llorar
lo contemplar y él le devolver el mirada pero ninguno decir uno palabra
de pronto uno de el niño agarrar a el soldado de plomo y lo arrojar de cabeza a el chimenea
no tener motivo alguno para hacer lo ser por_supuesto aquel muñeco de resorte el que lo haber mover a ello
el soldado se hallar en_medio_de intenso resplandor
sentir uno calor terrible aunque no saber si ser a_causa_de el fuego o de el amor
haber perder todo su brillante color sin que nadie poder afirmar si a_consecuencia_de el viaje o de su sufrimiento
mirar a el bailarín lo mirar él y el soldado sentir que se derretir pero continuar impávido con su fusil a el hombro
se abrir uno puerta y el corriente de aire se apoderar de el bailarín que volar como uno sílfide hasta el chimenea y ser a caer junto_a el soldado de plomo donde arder en uno repentino llamarada y desaparecer
poco después el soldado se acabar de derretir
cuando a el mañana siguiente el sirviente remover el ceniza lo encontrar en forma de uno pequeño corazón de plomo pero de el bailarín no haber quedar sino su lentejuela y este ser ahora negro como el carbón
fin
