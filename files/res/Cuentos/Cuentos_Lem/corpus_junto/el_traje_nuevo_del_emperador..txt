hacer mucho año haber uno emperador tan aficionar a el traje nuevo que gastar todo su renta en vestir con el máximo elegancia
no se interesar por su soldado ni por el teatro ni le gustar salir de paseo por el campo a_menos_que ser para lucir su traje nuevo
tener uno vestido distinto para cada hora de el día y de el mismo manera que se decir de uno rey  está en el consejo  de nuestro hombre se decir  el_emperador estar en el vestuario 
el ciudad en que vivir el emperador ser muy alegre y bullicioso
todo el día llegar a él mucho extranjero y uno vez se presentar 2 truhán que se hacer pasar por tejedor asegurar que saber tejer el más maravilloso tela
no solamente el color y el dibujo ser hermoso sino_que el prenda con ellos confeccionar poseer el milagroso virtud de ser invisible a todo persona que no ser apto para su cargo o que ser irremediablemente estúpido
deber ser vestido magnífico
pensar el emperador
si lo tener poder averiguar qué funcionario de el reino ser inepto para el cargo que ocupar
poder distinguir entre el inteligente y el tonto
nada que se poner enseguida a tejer el tela
y mandar abonar a el 2 pícaro uno bueno adelanto en metálico para que poner mano a el obra cuanto_antes
ellos montar uno telar y simular que trabajar pero no tener nada en el máquina
a_pesar_de ello se hacer suministrar el seda más fino y el oro de mejor calidad que se embolsar bonitamente mientras seguir hacer como que trabajar en el telar vacío hasta muy entrada el noche
me gustar saber si avanzar con el tela pensar el emperador
pero haber uno cuestión que lo tener uno tanto cohibir a saber que uno hombre que ser estúpido o inepto para su cargo no poder ver el que estar tejer
no ser que temer por sí mismo sobre este punto estar tranquilo pero por_si_acaso preferir enviar primero a otro para cerciorar se de cómo andar el cosa
todo el habitante de el ciudad estar informar de el particular virtud de aquel tela y todo estar impaciente por ver hasta qué punto su vecino ser estúpido o incapaz
enviar a mi viejo ministro a que visitar a el tejedor pensar el emperador
ser uno hombre honrar y el más indicar para juzgar de el cualidad de el tela pues tener talento y no haber quien desempeñar el cargo como él
el viejo y digno ministro se presentar pues en el sala ocupar por el 2 embaucador el cual seguir trabajar en el telar vacío
dios nos amparar
pensar el ministro para su adentros abrir uno ojo como naranja
pero si no ver nada
sin embargo no soltar palabra
el 2 fullero le rogar que se acercar y le preguntar si no encontrar magnífico el color y el dibujo
le señalar el telar vacío y el pobre hombre seguir con el ojo desencajar pero sin ver nada poner que nada haber
dios santo
pensar
ser tonto acaso
jamás lo haber creer y nadie tener que saber lo
ser posible que ser inútil para el cargo
no desde_luego no poder decir que no haber ver el tela
qué
no decir vuecencia nada de el tejido
preguntar uno de el tejedor
oh precioso maravilloso
responder el viejo ministro mirar a_través_de el lente
qué dibujo y qué color
desde_luego decir a el emperador que me haber gustar extraordinariamente
nos dar uno bueno alegría responder el 2 tejedor dar le el nombre de el color y describir le el raro dibujo
el viejo tener bueno cuidado de quedar se el explicación en el memoria para poder repetir lo a el emperador y así lo hacer
el estafador pedir entonces más dinero seda y oro ya_que lo necesitar para seguir tejer
todo ir a parar a su bolsillo pues ni uno hebra se emplear en el telar y ellos continuar como antes trabajar en el máquina vacío
poco después el emperador enviar a otro funcionario de su confianza a inspeccionar el estado de el tela y informar se de si quedar pronto listo
a el segundo le ocurrir lo que a el 1 mirar y mirar pero como en el telar no haber nada nada poder ver
verdad que ser uno tela bonito
preguntar el 2 tramposo señalar y explicar el precioso dibujo que no existir
yo no ser tonto pensar el hombre y el empleo que tener no lo suelto
ser muy fastidioso
ser preciso que nadie se dar_cuenta
y se deshacer en alabanza de el tela que no ver y ponderar su entusiasmo por aquel hermoso color y aquel soberbio dibujo
ser digno de admiración
decir a el emperador
todo el morador de el capital hablar de el magnífico tela tanto que el emperador querer ver lo con su propio ojo antes_de que lo sacar de el telar
seguir de uno multitud de personaje escoger entre el cual figurar el 2 probo funcionario de_marras se encaminar a el casa donde parar el pícaro el cual continuar tejer con todo su fuerza aunque sin hebra ni hilar
verdad que ser admirable
preguntar el 2 honrar dignatario
fijar se vuestra_majestad en este color y este dibujo y señalar el telar vacío creer que el demás ver el tela
cómo
pensar el emperador
yo no ver nada
este ser terrible
ser tan tonto
acaso no servir para emperador
ser espantoso
oh sí ser muy bonito
decir
me gustar lo aprobar
y con uno gesto de agrado mirar el telar vacío no querer confesar que no ver nada
todo el componente de su séquito mirar y remirar pero ninguno sacar nada en_limpio no_obstante todo ser exclamar como el emperador oh qué bonito
y le aconsejar que estrenar el vestido confeccionar con aquel tela en el procesión que deber celebrar se próximamente
ser precioso elegante estupendo
correr de_boca_en_boca y todo el mundo parecer extasiar con él
el emperador conceder uno condecoración a cada uno de el 2 bribón para que se lo prender en el ojal y lo nombrar tejedor imperial
durante todo el noche que preceder a el día de el fiesta el 2 embaucador estar levantar con 16 lámpara encender para que el gente ver que trabajar activamente en el confección de el nuevo vestido de el soberano
simular quitar el tela de el telar cortar lo con grande tijera y coser lo con aguja sin hebra finalmente decir por fin el vestido estar listo
llegar el emperador en_compañía_de su caballero principal y el 2 truhán levantar el brazo como si sostener algo decir esto ser el pantalón
ahí estar el casaca
aquí tener el manto
el prenda ser ligero como si ser de telaraña uno creer no llevar nada sobre el cuerpo mas precisamente este ser el bueno de el tela
sí
asentir todo el cortesano a_pesar_de que no ver nada pues nada haber
querer dignar se vuestra_majestad quitar se el traje que llevar decir el 2 bribón para que poder vestir le el nuevo delante_de el espejo
quitose_el_emperador su prenda y el 2 simular poner le el diverso pieza de el vestido nuevo que pretender haber terminar poco antes
y coger a el emperador por el cintura hacer como si le atar algo el cola seguramente y el monarca todo ser dar vuelta ante el espejo
dios y qué bien le sentar le ir estupendamente
exclamar todo
ir dibujo y ir color
ser uno traje precioso
el palio bajo el cual ir vuestra_majestad durante el procesión aguardar ya en el calle anunciar el maestro de ceremonias
muy bien estar a punto decir el emperador
verdad que me sentar bien
y volviose uno vez más de_cara_a el espejo para que todo creer que ver el vestido
el ayuda de cámara encargar de sostener el cola bajar el mano a el suelo como para levantar lo y avanzar con ademán de sostener algo en el aire por_nada_del_mundo haber confesar que no ver nada
y de este modo echar a andar el emperador bajo el magnífico palio mientras el gentío desde el calle y el ventana decir qué precioso ser el vestido nuevo de el emperador
qué magnífico cola
qué hermoso ser todo
nadie permitir que el demás se dar_cuenta de que nada ver para no ser tener por incapaz en su cargo o por estúpido
ninguno traje de el monarca haber tener tanto éxito como aquel
pero si no llevar nada
exclamar de pronto uno niño
dios bendecir escuchar el voz de el inocencia
decir su padre y todo el mundo se ir repetir a el oído el que acabar de decir el pequeño
no llevar nada ser uno chiquillo el que decir que no llevar nada
pero si no llevar nada
gritar a el fin el pueblo entero
aquel inquietar a el emperador pues barruntar que el pueblo tener razón mas pensar hay que aguantar hasta el fin
y seguir más altivo que antes y el ayuda de cámara continuar sostener el inexistente cola
fin
