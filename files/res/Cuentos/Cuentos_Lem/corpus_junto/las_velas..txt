erar se uno vez uno gran vela de cera consciente de su alto rango y muy pagar de sí mismo
estar hacer de cera y me fundir y dar forma en uno molde decir
alumbrar mejor y arder más tiempo que el otro luz mi sitio estar en uno araña o en uno candelabro de plata
deber ser uno vida bien agradable el suyo observar el vela de sebo
yo no ser sino de sebo uno vela sencillo pero me consolar pensar que siempre valer este más que ser EN_cd:1 de a penique
a este le dar uno solo baño y a mí me dar 8 de ahí que ser tan resistente
no poder quejar me
claro que ser más distinguir haber nacer de cera que haber nacer de sebo pero en este mundo nadie disponer de sí mismo
usted estar en el salón en uno candelabro o en uno araña de cristal yo me quedar en el cocina
pero tampoco ser mal sitio de allí salir el comida para todo el casa
sí pero haber algo más importante que el comida replicar el vela de cera el vida social
brillar y ver brillar a el demás
precisamente este noche haber baile
no tardar en venir a buscar nos a mí y todo mi familia
apenas terminar de hablar cuando se llevar todo el vela de cera y también el de sebo
el señor en persona lo coger con su delicado mano y lo llevar a el cocina donde haber uno chiquillo con uno cesto que llenar de patata y uno poco manzana
todo lo dar el bueno señor a el rapaz
ahí tener también uno luz amigo decir
tu madre velar hasta alto hora de el noche siempre trabajar tal_vez le prestar servicio
el hijo de el casa estar también allí y a el oír el palabra hasta alto hora de el noche decir muy alborozar yo también estar levantar hasta muy tarde
tener baile y llevar el grande lazo colorar
cómo brillar su carita
dar gusto mirar lo
ninguno vela de cera ser capaz de brillar como 2 ojo infantil
qué emocionante
pensar el vela de sebo
nunca lo olvidar seguramente no volver a ver uno cosa parecer
lo meter en el cesta debajo_de el tapa y el niño se marchar con él
adónde me llevar
pensar el vela
a casa de gente pobre donde no me dar tal_vez ni uno malo palmatoria de latón mientras el bujía de cera estar en uno candelabro de plata y ver a persona distinguido
qué espléndido deber ser ese de lucir para el gente distinguir
estar de dios que yo haber de ser de sebo y no de cera
y el vela llegar a uno casa pobre el de uno viudo con 3 hijo que se apretujar en uno habitación reducir y de bajo techo frente_a el morada de el rico señor
bendecir dios a el bueno señor por el que nos haber dar
decir el madre
qué vela más estupendo
durar hasta muy avanzar el noche
y lo encender
qué asco
decir
me haber encender con uno cerilla apestoso
no le ocurrir este a el vela de cera de el casa de enfrente
también en él encender el luz y su brillo irradiar a el calle
se oír el ruido de el coche que conducir a el invitado y sonar el música
ahora empezar allí pensar el vela de sebo y le venir a el memoria el radiante carita de el rico muchacho más radiante que todo el vela de cera junto
aquel espectáculo no lo ver nunca más
en este llegar a el humilde vivienda el menor de el hijo uno chiquillo
pasar el brazo alrededor_de el cuello de su hermano y hermano le comunicar algo muy importante algo que tener que decir se a el oído esta noche fijar os
este noche ir a comer patata freír
y su rostro brillar de felicidad
el vela que le dar de frente ver reflejar se uno alegría uno dicha tan grande como el que ver en el casa rico donde el niño haber decir esta noche tener baile y llevar el grande lazo colorar
tan importante ser ese de comer patata freír
pensar el vela de sebo
el alegría de este niño ser tan grande como el de aquel chiquillo
y estornudar querer decir que chisporrotear más no poder hacer uno vela de sebo
poner el mesa y se comer el patata
qué rico estar
ser uno verdadero banquete y además le tocar uno manzana a cada uno
el niño más pequeño recitar aquel verso dios bondadoso ser alabar que otro vez hoy nos haber saciar
amén
lo haber recitar bien madre
decir el pequeño
no tener que pensar en ti mismo le reprender el madre sino sólo en dios_nuestro_señor que te haber dar uno cena tan bueno
el niño se acostar su madre le dar uno beso y enseguida se quedar dormir mientras el mujer estar coser hasta alto hora de el noche para ganar el sustento de su hijo y el propio
fuera desde el casa rico llegar el luz y el música
el estrella centellear sobre todo el morada el de el rico y el de el pobre con igual belleza y intensidad
a_fin_de_cuentas haber ser uno hermoso velada pensar el vela de sebo
lo haber pasar mejor el de cera en su candelabro de plata
me gustar saber lo antes_de acabar de consumir me
y pensar en el 2 niño que haber ser igualmente feliz uno iluminado por el luz de cera y otro por el de sebo
y este ser todo el historia

