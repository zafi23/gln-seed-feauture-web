erar se uno vez uno caballero muy elegante que por todo equipaje poseer uno calzador y uno peine pero tener uno cuello de camisa que ser el más notable de el mundo entero y el historia de este cuello ser el que ir a relatar
el cuello tener ya el edad suficiente para pensar en casar se y haber aquí que en el cesto de el ropa coincidir con uno liga
decir el cuello jamás ver a nadie tan esbelto distinguir y lindo
me permitir que le preguntar su nombre
no se lo decir
responder el liga
dónde vivir pues
insistir el cuello
pero el liga ser muy tímido y pensar que el pregunta ser algo extraño y que no deber contestar lo
ser usted uno cinturón verdad
decir el cuello uno especie de cinturón interior
bien ver mi simpático señorito que ser uno prenda tanto de utilidad como de adorno
hacer el favor de no dirigir me el palabra
decir el liga
no crear que le haber dar_pie para hacer lo
sí me lo haber dar
cuando se ser tan bonito replicar el cuello no hacer falta más motivo
no se acercar tanto
exclamar el liga
parecer usted tan varonil
ser también uno caballero fino decir el cuello tener uno calzador y uno peine
el cual no ser verdad pues quien el tener ser su dueño pero le gustar vanagloriar se
no se acercar tanto
repetir el liga
no estar acostumbrar
qué remilgar
decir el cuello con tono burlón pero en este lo sacar de el cesto lo almidonar y después_de haber lo colgar a el sol sobre el respaldo de uno silla ser colocar en el tabla de planchar y llegar el plancha caliente
mi querido señor exclamar el cuello mi querido señor
qué calor sentar
si no ser yo mismo
si cambio totalmente de forma
me ir a quemar ir a hacer me uno agujero
huy
querer casar se conmigo
harapo
replicar el plancha correr orgullosamente por encima_de el cuello se imaginar ser uno caldera de vapor uno locomotora que arrastrar el vagón de uno tren
harapo
repetir
el cuello quedar uno poco deshilachar de el borde por ese acudir el tijera a cortar el hilo
oh
exclamar el cuello usted deber de ser 1 bailarín verdad
cómo saber estirar el pierna
ser el más encantador que haber ver
nadie ser capaz de imitar lo
ya lo saber responder el tijera
merecer ser condesa
decir el cuello
todo lo que poseer ser uno señor distinguir uno calzador y uno peine
si tener también uno condado
se me estar declarar el asqueroso
exclamar el tijera y enfadar le propinar uno corte que lo dejar inservible
a el fin tener que solicitar el mano de el peine
ser admirable cómo conservar usted todo el diente mi querido señorito
decir el cuello
no haber pensar nunca en casar se
claro ya poder figurar se lo
contestar el peine
seguramente haber oír que estar prometer con el calzador
prometida
suspirar el cuello y como no haber nadie más a quien declarar se se lo dar en decir mal de el matrimonio
pasar mucho tiempo y el cuello ser a parar a el almacén de uno fabricante de papel
haber allí uno nutrir compañía de harapo el fino ir por su lado el tosco por el suyo como exigir el corrección
todo tener mucho cosa que explicar pero el cuello lo superar a todo pues ser uno gran fanfarrón
el de novio que haber tener
decir
no me dejar uno momento de reposo
andar yo hacer uno petimetre en aquel tiempo siempre muy tieso y almidonar
tener además uno calzador y uno peine que jamás utilizar
tener que haber me ver entonces cuando me acicalar para uno fiesta
nunca me olvidar de mi 1 novio ser uno cinturilla delicado elegante y muy lindo por mí se tirar a uno bañera
luego haber uno plancha que arder por mi persona pero no le hacer caso y se volver negro
tener también relación con uno 1 bailarín él me producir el herida cuyo cicatriz conservar ser terriblemente celoso
mi propio peine se enamorar de mí perder todo el diente de mal de amor
uf
el de aventura que haber correr
pero el que más me doler ser el liga decir el cinturilla que se tirar a el bañera
cuánto pecado llevar sobre el conciencia
ya ser tiempo de que me convertir en papel blanco
y ser convertir en papel blanco con todo el demás trapo y el cuello ser precisamente el hoja que aquí ver en el cual se imprimir su historia
y le estar bien emplear por haber se jactar de cosa que no ser verdad
tener_en_cuenta para no comportar nos como él pues en verdad no poder saber si también nosotros ir a dar alguno día a el saco de el trapo viejo y ser convertir en papel y todo nuestro historia aun el más íntimo y secreto de él ser imprimir y andar por ese mundo tener que contar lo
fin
