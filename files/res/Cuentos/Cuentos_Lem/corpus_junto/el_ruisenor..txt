en china como saber muy bien el emperador ser chino y chino ser todo el que lo rodear
hacer ya mucho año de el que ir a contar mas por ese precisamente valer el pena que lo oír antes_de que el historia se haber olvidar
el palacio de el emperador ser el más espléndido de el mundo entero todo él de el más delicado porcelana
todo en él ser tan precioso y frágil que haber que ir con mucho cuidado antes_de tocar nada
el jardín estar lleno de flor maravilloso y de el más bello colgar campanilla de plata que sonar para que nadie poder pasar de largo sin fijar se en ellos
sí en el jardín imperial todo estar muy bien pensar y ser tan extenso que el propio jardinero no tener idea de dónde terminar
si seguir andar te encontrar en el bosque más espléndido que caber imaginar lleno de alto árbol y profundo lago
aquel bosque llegar hasta el mar hondo y azul grande embarcación poder navegar por debajo_de el rama y allí vivir uno ruiseñor que cantar tan primorosamente que incluso el pobre pescador a_pesar_de su mucho ocupación cuando por el noche salir a retirar el red se detener a escuchar su trino
dios santo y qué hermoso
exclamar pero luego tener que atender a su red y olvidar se de el pájaro hasta el noche siguiente en que a el llegar de nuevo a el lugar repetir dios santo y qué hermoso
de todo el país llegar viajero a el ciudad imperial y admirar el palacio y el jardín pero en cuanto oír a el ruiseñor exclamar esto ser el mejor de todo
de regreso a su tierra el viajero hablar de él y el sabio escribir libro y más libro acerca_de el ciudad de el palacio y de el jardín pero sin olvidar se nunca de el ruiseñor a el que poner por el nube y el poeta componer inspirado poema sobre el pájaro que cantar en el bosque junto_a el profundo lago
aquel libro se difundir por el mundo y alguno llegar a mano de el emperador
se hallar sentar en su sillón de oro leer y leer de_vez_en_cuando hacer con el cabeza uno gesto de aprobación pues le satisfacer leer aquel magnífico descripción de el ciudad de el palacio y de el jardín
pero el mejor de todo ser el ruiseñor decir el libro
qué ser este
pensar el emperador
el ruiseñor
jamás haber oír hablar de él
ser posible que haber uno pájaro así en mi imperio y precisamente en mi jardín
nadie me haber informar
estar bueno que uno tener que enterar se de semejante cosa por el libro
y mandar llamar a el mayordomo de palacio uno personaje tan importante que cuando uno persona de rango inferior se atrever a dirigir le el palabra o hacer le uno pregunta se limitar a contestar le p_y este no significar nada
según parecer haber aquí uno pájaro de el más notable llamar ruiseñor decir el emperador
se decir que ser el mejor que existir en mi imperio por qué no se me haber informar de este hecho
ser el 1 vez que oír hablar de él se justificar el mayordomo
nunca haber ser presentar en el corte
pues ordenar que acudir este noche a cantar en mi presencia decir el emperador
el mundo entero saber el que tener menos yo
ser el 1 vez que oír hablar de él repetir el mayordomo
lo buscar y lo encontrar
encontrar lo
dónde
el dignatario se cansar de subir y bajar escalera y de recorrer sala y pasillo
nadie de cuanto preguntar haber oír hablar de el ruiseñor
y el mayordomo volver a el emperador le decir que se tratar de uno de ese fábula que soler imprimir se en el libro
vuestro majestad_imperial no deber creer todo el que se escribir ser fantasía y uno cosa que llamar magia negro
pero el libro en que lo haber leer me lo haber enviar el poderoso emperador_de_el_japón replicar el soberano por tanto no poder ser mentiroso
querer oír a el ruiseñor
que acudir este noche a mi presencia para cantar bajo mi especial protección
si no se presentar mandar que todo el cortesano ser patear en el estómago después_de cenar
tsingpe
decir el mayordomo y vuelta a subir y bajar escalera y a recorrer sala y pasillo y medio corte con él pues a nadie le hacer gracia que le patear el estómago
y todo ser preguntar por el notable ruiseñor conocer por todo el mundo menos por el corte
finalmente dar en el cocina con uno pobre muchacho que exclamar dios mío
el ruiseñor
claro que lo conocer
qué bien cantar
todo el noche me dar permiso para que llevar alguno sobra de comida a mi pobre madre que estar enfermo
vivir allá en el playa y cuando estar de regreso me parar a descansar en el bosque y oír cantar a el ruiseñor
y oír lo se me venir el lágrima a el ojo como si mi madre me besar
ser uno recuerdo que me estremecer de emoción y dulzura
pequeño fregaplatos decir el mayordomo te dar uno empleo fijo en el cocina y permiso para presenciar el comida de el emperador si poder traer nos a el ruiseñor estar citar para este noche
todo se dirigir a el bosque a el lugar donde el pájaro soler situar se medio corte tomar_parte en el expedición
avanzar a_toda_prisa cuando uno vaca se poner a mugir
oh
exclamar el cortesano
ya lo tener
qué fuerza para uno animal tan pequeño
ahora que caer en ello no ser el 1 vez que lo oír
no ese ser uno vaca que mugir decir el fregona aún tener que andar mucho
luego oír el rana croar en uno charca
magnífico
exclamar uno cortesano
ya lo oír sonar como el campanilla de el iglesia
no ese ser rana contestar el muchacho
pero crear que no tardar en oír lo
y en_seguida el ruiseñor se poner a cantar
ser él
decir el niño
escuchar escuchar
allí estar
y señalar uno ave gris posar en uno rama
ser posible
decir el mayordomo
jamás lo haber imaginar así
qué vulgar
seguramente haber perder el color intimidar por uno visitante tan distinguir
mi pequeño ruiseñor decir en voz alto el muchacho nuestro gracioso soberano querer que cante en su presencia
con_mucho_gusto
responder el pájaro y reanudar su canto que dar gloria oír lo
parecer campana de cristal
observar el mayordomo
mirar cómo se mover su garganta
ser raro que nunca lo haber ver
causar sensación en el corte
querer que volver a cantar para el emperador
preguntar el pájaro pues creer que el emperador estar allí
mi pequeño y excelente ruiseñor decir el mayordomo tener el honor de invitar lo a uno gran fiesta en palacio este noche donde poder deleitar con su magnífico canto a su_imperial_majestad
sonar mejor en el bosque objetar el ruiseñor pero cuando le decir que ser uno deseo de el soberano lo acompañar gustoso
en palacio todo haber ser pulir y fregar
el pared y el suelo que ser de porcelana brillar a el luz de millar de lámpara de oro el flor más exquisito con su campanilla haber ser colocar en el corredor el ida y venida de el cortesano producir tal corriente de aire que el campanilla no cesar de sonar y uno no oír ni su propio voz
en_medio_de el gran salón donde el emperador estar haber poner uno percha de oro para el ruiseñor
todo el corte estar presente y el pequeño fregona haber recibir autorización para situar se detrás_de el puerta pues tener ya el título de cocinero de el corte
todo el mundo llevar su vestido de gala y todo el ojo estar fijo en el ave gris a el que el emperador hacer signo de que poder empezar
el ruiseñor cantar tan deliciosamente que el lágrima acudir a el ojo de el soberano y cuando el pájaro lo ver rodar por su mejilla volver a cantar mejor aun hasta llegar le a el alma
el emperador quedar tan complacer que decir que regalar su chinela de oro a el ruiseñor para que se lo colgar a el cuello
mas el pájaro le dar el gracia decir le que ya se considerar suficientemente recompensar
haber ver lágrima en el ojo de el emperador este ser para mí el mejor premio
el lágrima de uno rey poseer uno virtud especial
dios saber que haber quedar bien recompensar y reanudar su canto con su dulce y melodioso voz
ser el lisonja más amable y gracioso que haber escuchar en mi vida
exclamar el dama presente y todo se ir a llenar se el boca de agua para gargarizar cuando alguien hablar con ellos pues creer que también ellos poder ser ruiseñor
sí hasta el lacayo y el camarero expresar su aprobación y este ser decir mucho pues ser siempre más difícil de contentar
realmente el ruiseñor causar sensación
se quedar en el corte en uno jaula particular con libertad para salir 2 vez durante el día y uno durante el noche
poner a su servicio 10 criado a cada uno de el cual estar sujeto por_medio_de uno cinta de seda que le atar alrededor_de el pierna
el verdad ser que no ser precisamente de placer aquel excursión
el ciudad entero hablar de el notable pájaro y cuando 2 se encontrar se saludar decir el uno rui y responder el otro « señor » luego exhalar uno suspiro indicar que se haber comprender
haber incluso 11 verdulero que poner su nombre a su hijo pero ni uno de ellos resultar capaz de dar uno nota
uno bueno día el emperador recibir uno gran paquete rotular el ruiseñor
haber aquí uno nuevo libro acerca_de nuestro famoso pájaro exclamar el emperador
pero resultar que no ser uno libro sino uno pequeño ingenio poner en uno jaula uno ruiseñor artificial imitación de el vivo pero cubrir materialmente de diamante rubí y zafiro
sólo haber que dar le cuerda y se poner a cantar uno de el melodía que cantar el de verdad levantar y bajar el cola todo él uno ascua de plata y oro
llevar uno cinta atar a el cuello y en él estar escribir el ruiseñor de el emperador_de_el_japón ser pobre en comparación con el de el emperador_de_la_china
soberbio
exclamar todo y el emisario que haber traer el ave artificial recibir inmediatamente el título de gran_portador_imperial_de_ruiseñores
ahora ir a cantar junto
qué dúo hacer
y lo hacer cantar a_dúo pero el cosa no marchar pues el ruiseñor auténtico lo hacer a su manera y el artificial ir con cuerda
no se le poder reprochar decir el director_de_la_orquesta_imperial mantener el compás exactamente y seguir mi método al_pie_de_la_letra
en adelante el pájaro artificial tener que cantar solo
obtener tanto éxito como el otro además ser mucho más bonito pues brillar como uno puñado de pulsera y broche
repetir 33 vez el mismo melodía sin cansar se y el cortesano querer volver a oír lo de nuevo pero el emperador opinar que también el ruiseñor verdadero deber cantar algo
pero dónde se haber meter
nadie se haber dar_cuenta de que salir por el ventana abrir haber volver a su verde bosque
qué significar este
preguntar el emperador
y todo el cortesano se deshacer en reproche y improperio tachar a el pájaro de desagradecido
por suerte nos quedar el mejor decir y el ave mecánico haber de cantar de nuevo repetir por 30 cuarta vez el mismo canción pero como ser muy difícil no haber modo de que el oyente se lo aprender
el director_de_la_orquesta_imperial se hacer lengua de el arte de el pájaro asegurar que ser muy superior a el verdadero no_sólo en el relativo a el plumaje y el cantidad de diamante sino también interiormente
pues fijar se vuestras_señorías y especialmente su_majestad que con el ruiseñor de carne y hueso nunca se poder saber qué ser el que ir a cantar
en cambio en el artificial todo estar determinar de_antemano
se oír tal cosa y tal otro y nada más
en él todo tener su explicación se poder abrir y poner_de_manifiesto cómo obrar el inteligencia humano ver cómo estar disponer el rueda cómo se mover cómo uno se engranar con el otro
ese pensar todo decir el cortesano y el director_de_la_orquesta_imperial ser autorizar para que el [G:??/??/??:????:??] mostrar el pájaro a el pueblo
todo deber oír lo cantar decir el emperador y así se hacer y quedar el gente tan satisfacer como si se haber emborrachar con té pues así ser como lo hacer el chino y todo gritar oh
y levantar el dedo índice se inclinar profundamente
mas el pobre pescador que haber oír a el ruiseñor auténtico decir no estar mal el melodía se parecer pero le faltar algo no saber qué
el ruiseñor de verdad ser desterrar de el país
el pájaro mecánico estar en adelante junto_a el cama de el emperador sobre uno almohada de seda todo el regalo con que haber ser obsequiar oro y piedra precioso estar disponer a su alrededor y se le haber conferir el título de primer_cantor_de_cabecera_imperial con categoría de número uno a el lado izquierdo
pues el emperador considerar que este lado ser el más noble por ser el de el corazón que hasta el emperador tener a el izquierda
y el director_de_la_orquesta_imperial escribir uno obra de 25 tomo sobre el pájaro mecánico tan largo y erudito tan lleno de el más difícil palabra chino que todo el mundo afirmar haber lo leer y entender pues de otro modo haber pasar por tonto y recibir patada en el estómago
así transcurrir el cosa durante uno año el emperador_la_corte y todo el demás chino se saber de memoria el trino de_canto de el ave mecánico y precisamente por ese le gustar más que nunca poder imitar lo y lo hacer
el golfo de el calle cantar tsitsii cluclucluk
y hasta el emperador hacer coro
ser de vera divertir
pero haber aquí que uno noche estar el pájaro en pleno cantar el emperador que estar ya acostar oír de pronto uno crac
en_el_interior_de el mecanismo algo haber saltar
schnurrrr
se escapar el cuerda y el música cesar
el emperador saltar de el cama y mandar llamar a su médico de cabecera pero qué poder hacer el hombre
entonces ser llamar el relojero quien tras largo discurso y manipulación arreglar uno poco el ave pero manifestar que deber andar se con mucho cuidado con él y no hacer lo trabajar demasiado pues el perno estar gastar y no ser posible sustituir lo por otro nuevo que asegurar el funcionamiento de el música
qué desolación
desde entonces sólo se poder hacer cantar a el pájaro uno vez a el año y aun este ser uno imprudencia pero en tal ocasión el director_de_la_orquesta_imperial pronunciar uno breve discurso emplear aquel palabra tan intrincar decir que el ave cantar tan bien como antes y no haber que decir que todo el mundo se manifestar de acuerdo
pasar 5 año cuando haber aquí que uno gran desgracia caer sobre el país
el chino querer mucho a su emperador el cual estar ahora enfermo de muerte
ya haber ser elegir su sucesor y el pueblo en el calle no cesar de preguntar a el mayordomo de palacio por el estado de el anciano monarca
p responder este sacudir el cabeza
frío y pálido yacer el emperador en su grande y suntuoso lecho
todo el corte lo creer ya morir y cada cual se apresurar a ofrecer su respeto a el nuevo soberano
el camarero de palacio salir precipitadamente para hablar de el suceso y el camarero se reunir en uno té muy concurrir
en todo el salón y corredor haber tender paño para que no se oír el paso de nadie y así reinar uno gran silencio
pero el emperador no haber expirar aun permanecer rígido y pálido en el lujoso cama con su largo cortina de terciopelo y macizo borla de oro
por uno ventana que se abrir en lo alto de el pared el luna enviar su rayo que iluminar a el emperador y a el pájaro mecánico
el pobre emperador jadear con gran dificultad ser como si alguien se le haber sentar sobre el pecho
abrir el ojo y ver que ser el muerte que se haber poner su corona de oro en el cabeza y sostener en uno mano el dorar sable imperial y en el otro su magnífico estandarte
en torno por el pliegue de el cortinaje asomar extraviar cabeza alguno horriblemente feo otro de expresión dulce y apacible ser el obra bueno y malo de el emperador que lo mirar en aquel momento en que el muerte se haber sentar sobre su corazón
te acordar de tal cosa
murmurar uno tras otro
y de tal otro
y le recordar tanto que a el pobre le manar el sudor de el frente
yo no lo saber
se excusar el emperador
música música
que sonar el gran tambor chino gritar para no oír todo ese que decir
pero el cabeza seguir hablar y el muerte asentir con el cabeza a el modo chino a todo el que decir
música música
gritar el emperador
oh tú pajarillo de oro cantar cantar
te dar oro y objeto precioso con mi mano te colgar de el cuello mi chinela dorar
cantar cantar ya
mas el pájaro seguir mudo pues no haber nadie para dar le cuerda y el muerte seguir mirar a el emperador con su grande órbita vacío y el silencio ser lúgubre
de pronto resonar procedente de el ventana uno canto maravilloso
ser el pequeño ruiseñor vivo posar en uno rama
enterar de el desesperar situación de el emperador haber acudir a traer le consuelo y esperanza y cuanto más cantar más palidecer y se esfumar aquel fantasma el sangre afluir con más fuerza a el debilitar miembro de el enfermo y incluso el muerte prestar oído y decir sigue lindar ruiseñor seguir
sí pero me dar el magnífico sable de oro
me dar el rico bandera
me dar el corona imperial
y el muerte le ir dar aquel tesoro a_cambio_de otro tanto canción y el ruiseñor seguir cantar cantar de el silencioso camposanto donde crecer el rosa blanco donde el lila exhalar su aroma y donde el hierba lozano ser humedecer por el lágrima de el superviviente
el muerte sentir entonces nostalgia de su jardín y salir por el ventana flotar como uno niebla blanco y frío
gracias gracia
decir el emperador
bien te conocer ave celestial
te desterrar de mi reino sin embargo con tu canto haber alejar de mi lecho el malo espíritu haber ahuyentar de mi corazón el muerte
cómo poder recompensar te
ya me haber recompensar decir el ruiseñor
arrancar lágrima a tu ojo el 1 vez que cantar para ti este no lo olvidar nunca pues ser el joya que contentar a el corazón de uno cantor
pero ahora dormir y recuperar el fuerza que yo seguir cantar
así lo hacer y el soberano quedar sumir en uno dulce sueño qué sueño tan dulce y tan reparador
el sol entrar por el ventana cuando el emperador se despertar sano y fuerte
ninguno de su criado haber volver aun pues todo lo creer morir
sólo el ruiseñor seguir cantar en el rama
nunca te separar de mi lado
le decir el emperador
cantar cuando te apetecer y en_cuanto_a el pájaro mecánico lo romper en 1000 pedazo
no lo hacer suplicar el ruiseñor
él cumplir su misión mientras poder guardar lo como hasta ahora
yo no poder anidar ni vivir en palacio pero permitir me que venir cuando se me ocurrir entonces me posar junto_a el ventana y te cantar para que estar contento y reflexión
te cantar de el feliz y también de el que sufrir y de el mal y de el bien que se hacer a tu alrededor sin tú saber lo
tu pajarillo cantor deber volar a_lo_lejos hasta el cabaña de el pobre pescador hasta el tejado de el campesino hacia todo el que residir apartar de ti y de tu corte
preferir tu corazón a tu corona
aunque el corona exhalar cierto olor a cosa santo
volver a cantar para ti
pero deber prometer me uno cosa
el que querer
decir el emperador incorporar se en su ropaje imperial que ya se haber poner y oprimir contra su corazón el pesar sable de oro
uno cosa te pedir que no decir a nadie que tener uno pajarito que te contar todo el cosa
salir ganar
y se echar a volar
entrar el criado a ver a su difunto emperador
entrar sí y el emperador le decir buenos día

