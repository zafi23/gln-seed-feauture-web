erar se uno vez uno anciano poeta muy bueno y muy viejo
uno atardecer cuando estar en casa el tiempo se poner muy malo afuera llover a_cántaros pero el anciano se encontrar muy a gusto en su cuarto sentar junto_a el estufa en el que arder uno bueno fuego y se asar manzana
ni uno pelo de el ropa le quedar seco a el infeliz que este temporal haber pillar fuera_de casa decir pues ser uno poeta de muy bueno sentimiento
abrir me
tener frío y estar empapar
gritar uno niño desde fuera
y llamar a el puerta llorar mientras el lluvia caer furioso y el viento hacer temblar todo el ventana
pobrecillo
decir el viejo abrir el puerta
estar ante él uno rapaz completamente desnudo el agua le chorrear de el largo rizo rubio
tiritar de frío de no hallar refugio seguramente haber sucumbir víctima de el inclemencia de el tiempo
pobre pequeño
exclamar el compasivo poeta coger lo de el mano
ver conmigo que te calentar
ir a dar te vino y uno manzana porque ser tan precioso
y lo ser en efecto
su ojo parecer 2 límpido estrella y su largo y ensortijar bucle ser como de oro puro aun estar empapar
ser uno verdadero angelito pero estar pálido de frío y tiritar con todo su cuerpo
sostener en el mano uno arco magnificar pero estropear por el lluvia con el humedad el color de su flecha se haber borrar y mezclar uno con otro
el poeta se sentar junto_a el estufa poner a el chiquillo en su regazo le escurrir el agua de el cabello le calentar el manita en el suyo y le preparar vino dulce
el pequeño no tardar en rehacer se el color volver a su mejilla y saltar a el suelo se poner a bailar alrededor_de el anciano poeta
ser uno chico alegre
decir el viejo
cómo te llamar
me llamar amor responder el pequeño
no me conocer
ahí estar mi arco con el que disparo poder creer me
mirar ya haber volver el bueno tiempo y el luna brillar
pero tener el arco estropear observar el anciano
malo cosa ser
exclamar el chiquillo y recoger lo de el suelo lo examinar con atención
bah
ya se haber secar no le haber pasar nada el cuerda estar bien tenso
ir a probar lo
tensar el arco le poner uno flecha y apuntar disparar certero atravesar el corazón de el bueno poeta
ya ver que mi arco no estar estropear
decir y con uno carcajada se marchar
se haber ver uno chiquillo más malo
disparar así contra el viejo poeta que lo haber acoger en el caliente habitación se haber mostrar tan bueno con él y le haber dar tan exquisito vino y su mejor manzana
el bueno señor yacer en el suelo llorar realmente lo haber herir en el corazón
oh qué niño tan pérfido ser ese amor
se lo contar a todo el chiquillo bueno para que estar precaver y no jugar con él pues procurar causar le alguno daño
todo el niño y niño bueno a quien contar el sucedido se poner en guardia contra el treta de amor pero este continuar hacer de el suyo pues realmente ser de el piel de el diablo
cuando el estudiante salir de su clase él marcha a su lado con uno libro debajo_de el brazo y vestido con levita negro
no lo reconocer y lo coger de el brazo creer que ser también uno estudiante y entonces él le clavar uno flecha en el pecho
cuando el muchacho venir de escuchar a el señor cura y haber recibir ya el confirmación él lo seguir también
sí siempre ir detrás_de el gente
en el teatro se sentar en el gran araña y echar llama para que el persona crear que ser uno lámpara pero quizá
demasiado tarde descubrir ellos su error
correr por el jardín y en_torno_a el muralla
sí TM_d:1 herir en el corazón a tu padre y a tu madre
preguntar se lo ver lo que te decir
creer me ser uno chiquillo muy travieso este amor nunca querer trato con él acechar a todo el mundo
pensar que TM_d:1 disparar uno flecha hasta a tu anciano abuelo pero de ese hacer mucho tiempo
ya pasar pero él no lo olvidar
caramba con este diablillo de amor
pero ahora ya lo conocer y saber el malo que ser
