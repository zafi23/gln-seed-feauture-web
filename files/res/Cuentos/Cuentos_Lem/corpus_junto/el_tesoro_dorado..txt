el mujer de el tambor ser a el iglesia
ver el nuevo altar con el cuadro pintar y el ángel de talla
todo ser precioso tanto el de el tela con su color y aureola como el esculpir en madera pintar y dorar además
su cabellera resplandecer como el oro como el luz de el sol ser uno maravilla
pero el sol de dios ser aun más bello lucir por entre el árbol oscuro con tonalidad rojo claro dorar a el hora de el puesta
qué hermoso ser mirar el cara de nuestro_señor
y el mujer contemplar el sol ardiente mientras otro pensamiento más íntimo se agitar en su alma
pensar en el hijo que pronto le traer el cigüeña y este solo idea lo alborozar
con el ojo fijo en el horizonte de oro desear que su niño tener algo de aquel brillo de el sol que se parecer siquiera a uno de aquel ángel radiante de el nuevo altar
cuando por fin tener en su brazo a su hijo y lo mostrar a el padre ser realmente como uno de aquel ángel de el iglesia su cabello dorar brillar como el sol poniente
tesoro dorar mi riqueza mi sol
exclamar el madre besar el dorado rico y parecer como si en el habitación resonar música y cantar
cuánto alegría cuánto vida cuánto bullicio
el padre tocar uno redoble en el tambor uno redoble de entusiasmo
decir pelirrojo
el chico ser pelirrojo
atender a el tambor y no a el que decir su madre
ran ran ranpataplán
y todo el ciudad decir el mismo que el tambor
llevar el niño a el iglesia para bautizar lo
nada haber que objetar a el nombre que le poner pedro
el ciudad entero y con él el tambor lo llamar pedro el pelirrojo hijo de el tambor
pero su madre le besar el rojo cabello y lo llamar su tesoro dorar
en el hondonada haber uno ladera arcilloso en el que mucho haber grabar su nombre como recuerdo
el fama decir el padre de pedro no haber que despreciar lo
y así grabar el nombre propio junto_a el de su hijo
venir el golondrina en el curso de su largo viaje haber ver antiguo inscripción en el pared rocoso de el indostán y en el muro de su templo grande gesta de rey poderoso nombre inmortal tan antiguo que nadie ser capaz de leer lo ni pronunciar lo siquiera
gran nombre
fama
el golondrina construir su nido en el cañada
abrir agujero en el pared de arcilla
el viento y el lluvia descomponer el nombre y lo borrar incluso el de el tambor y su hijo
pero el nombre de pedro se conservar durante año y medio decir el padre
tonto
pensar el instrumento pero se limitar a decir ran ran ranpataplán
el rapaz pelirrojo ser uno chiquillo rebosante de vida y alegría
tener uno hermoso voz saber cantar y lo hacer como el pájaro de el bosque
ser melodía y sin embargo no lo ser
tener que ser monaguillo decir el madre
cantar en el iglesia debajo_de aquel hermoso ángel dorar a el que se parecer
gato color de fuego decir el malicioso de el ciudad
el tambor se lo oír a el comadre de el vecindad
no vaya a casa pedro
gritar el golfo callejero si dormir en el buhardilla se pegar fuego en el piso alto y tu padre tener que batir el tambor
pero antes me dejar el baqueta
replicar pedro y a_pesar_de ser pequeño arremeter valientemente contra ellos y tumbar a el 1 de uno puñetazo en el estómago mientras el otro poner pie en polvoroso
el músico de el ciudad ser uno hombre fino y distinguir hijo de uno tesorero real
le gustar el aspecto de pedro y alguno vez que otro se lo llevar a su casa le regalar uno violín y le enseñar a tocar lo
el niño tener gran disposición el habilidad de su dedo parecer indicar que ir a ser algo más que tambor que ser músico municipal
querer ser soldar decir sin embargo
ser todavía uno chiquillo y creer que el mejor de el mundo ser llevar fusil marcar el paso uno 2 uno 2
y lucir uniforme y sable
pues tener que aprender a obedecer a mi llamada decir el tambor
plan plan rataplán
ese estar bien si poder ascender hasta general decir el padre
mas para ese hacer falta que haber guerra
dios nos guardar
exclamar el madre
nada tener que perder replicar el hombre
cómo que no
y nuestro hijo
mas pensar que poder volver convertir en general
sin brazo ni pierna
responder el madre
no yo querer guardar mi tesoro dorar
ran ran ran
se poner a redoblar el tambor
haber estallar el guerra
el soldado partir y el pequeño con ellos
mi cabeza de oro
tesoro dorar
llorar el madre
en su imaginación el padre se lo ver famoso
en_cuanto_a el músico opinar que en_vez_de ir a el guerra deber haber se quedar con el músico municipal
pelirrojo
lo llamar el soldado y pedro se reír pero si a alguno se le ocurrir llamar le piel de zorro el chico apretar el diente y poner cara de enfado
el 1 mote no le molestar
despierto ser el mozuelo de genio resolver y humor alegre
este ser el mejor cantimplora decir el veterano
más de uno noche haber de dormir al_raso bajo el lluvia y el mal tiempo calar hasta el hueso pero nunca perder el bueno humor
aporrear el tambor tocar diana ran ran tan pataplán
a levantar se
realmente haber nacer para tambor
amanecer el día de el batalla
el sol no haber salir aun pero ya despuntar el alba
el aire ser frío el combate ardiente
el atmósfera estar empañar por el niebla pero más aun por el vapor de el pólvora
el bala y granada pasar volar por encima_de el cabeza o se meter en ellos o en el tronco y miembro pero el avance seguir
alguno_que_otro caer de rodilla el sien ensangrentar el cara lívido
el tambor conservar todavía su color sano hasta entonces estar sin uno rasguño
mirar siempre con el mismo cara alegre el perro de el regimiento que saltar contento delante_de él como si todo aquel ser puro broma como si el bala caer sólo para jugar con ellos
marchar
de frente
decir el consigna de el tambor
tal ser el orden que le dar
sin embargo poder suceder que el orden ser de retirada y a vez este ser el más prudente y en efecto le ordenar retirada
pero el tambor no comprender el orden y tocar adelante a el ataque
así lo haber entender y el soldado obedecer a el llamar de el parche
ser uno famoso redoble uno redoble que dar el victoria a quien estar a_punto_de ceder
ser uno batalla encarnizar y que costar muy cara
el granada desgarrar el carne en sangrante pedazo incendiar el pajar en el que haber buscar refugio el herir donde permanecer hora y hora sin auxilio abandonar tal_vez hasta el muerte
de nada servir pensar en todo ello y no_obstante uno lo pensar incluso cuando se hallar lejos en el pequeño ciudad apacible
en él cavilar el viejo tambor y su esposo
pedro estar en el guerra
ya estar harto de gemido
decir el hombre
se trabar uno nuevo batalla el sol no haber salir aun pero amanecer
el tambor y su mujer dormir se haber pasar casi todo el noche en vela hablar de el hijo que estar allí en_manos_de dios
y el padre soñar que el guerra haber terminar el soldado regresar y pedro ostentar en el pecho el cruz de plata
en cambio el madre soñar que ir a el iglesia y contemplar el cuadro y el ángel de talla con su cabello dorar y haber aquí que su hijo querer el tesoro de su corazón estar entre el ángel vestir de blanco cantar tan maravillosamente como sólo el ángel poder hacer lo mientras se elevar a el cielo con ellos y envolver en el resplandor de el sol enviar uno dulce saludo a su madre
tesoro dorar
exclamar el mujer despertar
dios se lo haber llevar conseguir
doblar el mano hundir el cabeza en el cortina estampar y prorrumpir a llorar
dónde estar entre el montón de caído en el gran fosa que cavar para el muerto
tal_vez estar en el fondo de el pantano
nadie conocer su tumba no haber rezar ninguno oración sobre él
su labio balbucear uno padrenuestro agachar el cabeza y se quedar medio dormir
se sentir tan cansar
ser pasar el día entre el vida y el sueño
ser a el anochecer uno arco iris se dibujar encima_de el bosque desde este a el profundo pantano
entre el pueblo circular uno superstición que pasar por verdad incontrovertible
existir uno gran tesoro en el lugar donde el arco iris tocar el tierra
también allí deber de haber uno pero nadie pensar en el pequeño tambor aparte su madre que de continuo soñar en él
y el día ser pasar entre el vida y el sueño
no haber sufrir el más mínimo rasguño no haber perder uno solo de su dorar cabello
plan plan rataplán
ser él ser él
haber decir el tambor y cantar el madre si lo haber ver o soñar
entre canto y hurras y con el laurel de el victoria regresar el soldado a casa uno vez terminar el guerra y concertar el paz
describir grande círculo marchar a el cabeza el perro de el regimiento como deseoso de hacer el camino 3 vez más largo
y pasar semana y día y pedro se presentar en el casa de su padre
venir moreno como uno gitano el ojo brillante radiante el rostro como el luz de el sol
su madre lo estrechar entre su brazo y lo besar en el boca en el ojo en el dorar cabello
volver a tener a el lado a su hijo
no lucir el cruz de plata como haber soñar su padre pero venir con el miembro entero como su madre no haber soñar
qué alegría
llorar y reír y pedro abrazar el viejo instrumento
todavía estar aquí ese trasto viejo
decir y el padre tocar uno redoble en él
se decir que acabar de estallar uno gran incendio exclamar el parche
fuego en el tejar fuego en el corazón tesoro mío
ran ran rataplán
y después
sí y después
preguntar lo a el músico
pedro se emancipar aun de el tambor decir
pedro ser más grande que yo
y ese que ser hijo de uno criado de el palacio real
pero el que haber aprender en todo uno vida pedro lo aprender en medio año
haber tanto franqueza en él dar uno tal impresión de bondad
su ojo brillar y brillar su cabello nadie poder negar lo
deber teñir se el pelo decir el vecino
a el hijo de el policía le quedar muy bien y pescar novio
pero al_cabo_de muy poco lo tener de el color de lenteja de agua y ahora tener que estar se lo teñir continuamente
no le faltar dinero para hacer lo replicar el vecino y tampoco le faltar a pedro
lo recibir en el casa más distinguir incluso en el de el alcalde y dar lección de piano a el señorito lotte
sí saber tocar el piano y interpretar melodía delicioso no escribir aun en ninguno pentagrama
tocar en el noche claro y tocar también en el oscuro
ser inaguantable decir el vecino y el viejo tambor de alarma también creer que aquel ser demasiado
tocar hasta que su pensamiento levantar el vuelo y grande proyecto para el futuro se arremolinar en su cabeza gloria
y lotte el hijo de el alcalde estar sentar a el piano su fino dedo danzar sobre el tecla y su nota percutir en el corazón de pedro
le parecer como si aquel ser demasiado estrecho y el impresión lo tener no uno vez sino varios
por ese TM_d:1 coger le el fino dedo y el delicado mano lo mirar en el grande ojo castaño
dios sólo saber el que decir nosotros poder conjeturar lo
lotte se sonrojar hasta el cuello y el hombro no le responder uno palabra
en aquel momento entrar uno forastero en el habitación uno hijo de el consejero_de_estado con uno reluciente calva que le llegar hasta el pescuezo
pedro permanecer mucho rato con ellos y el dulce mirada de lotte no se apartar de él
aquel noche hablar a su padre de el grande que ser el mundo y de el riqueza que se encerrar para él en el violín
gloria
ran ran rataplán
decir el tambor de alarma
este pedro nos ir a volver loco
me parecer que estar chiflar
a el mañana siguiente el madre se ir a el compra
saber el último noticia pedro
decir a el volver
lotte el hijo de el alcalde se haber prometer con el hijo de el consejero_de_estado
anoche mismo se cerrar el compromiso
no
exclamar pedro saltar de el silla
pero su madre insistir en que sí lo saber por el mujer de el barbero a el cual se lo haber comunicar el propio alcalde
pedro se volver pálido y caer desplomar en el silla
dios santo
qué te pasar
gritar el mujer
nada
nada
dejar me marchar responder él y el lágrima le rodar por el mejilla
hijo miar querer
tesoro dorar
exclamar el madre llorar
pero el tambor de alarma se poner a tocar lotte morir lotte morir
se terminar el canción
pero el canción no haber terminar todavía quedar aun mucho estrofa y muy largo el más bello uno tesoro para todo el vida
pues_sí_que lo haber coger fuerte
decir el vecino
todo tener que leer el carta que le enviar su tesoro y escuchar el que el diario contar de él y de su violín
le mandar mucho dinero y bien que lo necesitar el mujer desde que enviudar
tocar en presencia de rey y emperador decir el músico a mí el suerte no me sonreír
pero él ir mi discípulo y recordar a su viejo maestro
su padre soñar decir el mujer que pedro regresar de el guerra con uno cruz de plata en el pecho
en campaña no lo ganar allí deber de ser más difícil obtener lo
pero ahora lucir el cruz de caballero
si su padre poder ver lo
famoso
gruñir el tambor de alarma y todo su ciudad natal lo repetir
aquel tamborcillo pedro el pelirrojo que de niño calzar zueco y a quien de mayor haber ver tocar el tambor y en el baile ser ya famoso
tocar ante nosotros antes_de hacer lo ante el rey decir el alcaldesa
entonces estar loco por lotte
querer subir y siempre subir
ser presumir y extraño
mi marido se echar a reír cuando se enterar de aquel desatino
hoy_lotte ser el señor consejero
se esconder uno tesoro en el corazón de aquel pobre niño que de tambor haber tocar el adelante marchar
llevar a el victoria a el que estar a_punto_de ceder
en su corazón haber uno tesoro uno manantial de nota divino que se escapar de su violín como si en él estar encerrar todo uno órgano y como si todo el elfo bailar en su cuerda en uno noche de verano
se oír el canto de el tordo y el claro voz humano por ese hechizar a todo el corazón y hacer que su nombre correr de_boca_en_boca
arder uno gran fuego el fuego de el entusiasmo
y además ser tan guapo
decir el dama y el viejo le dar el razón
el más viejo de todo abrir uno álbum de rizo famoso sólo para poder procurar se uno de el rico y hermoso cabello de el joven violinista uno tesoro uno tesoro dorar
y uno bueno día entrar en el pobre morada de el tambor aquel hijo bello como uno príncipe más feliz que uno rey lleno de luz el ojo resplandeciente el rostro como el sol
y estrechar entre su brazo a su madre y él lo besar en el boca llorar tan feliz como sólo de gozo se poder llorar
dirigir uno saludo a cada uno de el viejo mueble a el cómoda con el taza de té y el florero a el lecho donde dormir de pequeño
sacar el viejo tambor de alarma y lo poner en el centro de el habitación padre haber tocar ahora uno redoble decir a su madre
lo hacer yo por él
y se poner a aporrear lo con todo su fuerza armar uno estrépito de 1000 demonio y el instrumento se sentir tan honrar que reventar de orgullo
tener bueno puño
decir el tambor
ahora guardar de él uno recuerdo para todo el vida
me temer que el viejo estallar también de alegría con su tesoro
y ahí tener el historia de el tesoro dorar

