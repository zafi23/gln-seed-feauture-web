haber en dinamarca uno viejo castillo llamar kronborg
estar junto_a el öresund estrecho que cruzar diariamente centenar de grande barco el mismo inglés que ruso y prusiano saludar a el viejo castillo con salva de artillería bum
y él contestar con su cañón bum
pues de este forma el cañón decir buenos día
y muchas gracia
en invierno no pasar por allí ninguno buque ya_que entonces estar todo cubrir de hielo hasta muy arriba_de el costa sueco pero en el bueno estación ser uno verdadero carretera
ondear el bandera danés y sueco y el población de ambos país se decir buenos día
y muchas gracia
pero no a cañonazo sino con uno amistoso apretón de mano y uno llevar pan blanco y rosquilla a el otro pues el comida forastero siempre saber mejor
pero el más estupendo de todo ser el castillo de kronborg en cuyo cueva profundo y tenebroso a el que nadie bajo residir holger_el_danés
ir vestido de hierro y acero y apoyar el cabeza en su robusto brazo su largo barba cuelga por sobre el mesa de mármol a el que estar pegar
dormir y soñar pero en sueño ver todo el que ocurrir allá arriba en dinamarca
por nochebuena bajar siempre uno ángel de dios y le decir que ser cierto el que haber soñar y que poder seguir dormir tranquilamente pues dinamarca no se encontrar aun en verdadero peligro
si este peligro se presentar holger el viejo danés se levantar y romper el mesa a el retirar el barba
volver a el mundo y pegar tan fuerte que su golpe se oír en todo el ámbito de el tierra
uno anciano explicar a su nietecito todo este cosa acerca_de holger y el pequeño saber que todo el que decir su abuelo ser el puro verdad
mientras contar el viejo se entretener tallar uno gran figura de madera que representar a holger destinar a adornar el proa de uno barco pues el abuelo ser escultor de madera o_sea uno hombre que tallar figura para espolón de barco figura que ir de_acuerdo_con el nombre de el navío
y en aquel ocasión haber representar a holger erguir y altivo con su largo barba el ancho espada de combate en uno mano mientras el otro se apoyar en el escudo adornar con el arma danés
el abuelo contar tanto y tanto cosa de hombre y mujer notable de dinamarca que el nieto creer a el fin que saber tanto como el propio holger el cual además se limitar a soñar lo y cuando se ir a acostar se poner a pensar tanto en aquel que aplicar el barbilla contra el colcha y se dar a creer que tener uno luengo barba pegar a él
el abuelo se haber quedar para proseguir su trabajo y realizar el último parte de el mismo que ser el escudo danés
cuando ya estar listo contemplar su obra pensar en todo lo que leer y oír y en el que aquel noche haber explicar a el muchacho
hacer uno gesto con el cabeza se limpiar el gafas y volver a sentar se decir durante el tiempo que me quedar de vida seguramente no volver holger pero ese pequeño que dormir ahí tal_vez lo ver y estar a su lado el día que ser necesario
y el viejo abuelo repetir su gesto y cuanto más examinar su holger más se convencer de que haber hacer uno bueno talla le parecer que cobrar color y que el armadura brillar como hierro y acero en el escudo de arma el corazón se enrojecer gradualmente y el león coronar saltar
ser el escudo más hermoso de cuanto existir en el mundo entero decir el viejo
el león ser el fuerza y el corazón el piedad y el amor
contemplar el 1 león y pensar en el rey knud que incorporar el gran inglaterra a el trono de dinamarca y a el considerar el 2 recordar a waldemar unificador de dinamarca y conquistador de el país vendo el 3 león le traer a el memoria a margarita que unir dinamarca_suecia y noruega
y cuando se fijar en el rojo corazón le parecer que brillar aun más que antes ser llama que se mover y su pensamiento ser en_pos_de cada uno de ellos
el 1 llamar lo conducir a uno estrecho y oscuro cárcel ocupar por uno prisionero uno hermoso mujer hijo de cristián_iv_leonora_ulfeldt y lo llamar se posar cual uno rosa en su pecho florecer y brillar con el corazón de el mejor y más noble de todo el mujer danés
sí ser uno de el corazón de el escudo de dinamarca decir el abuelo
y luego su mente se dirigir a el llama 2 que lo llevar a alto mar donde el cañón tronar y el barco aparecer envolver en humo y lo llamar se fijar como uno condecoración en el pecho de hvitfeldt cuando para salvar el flota volar su propio barco con él a_bordo
el 3 llamar lo transportar a el mísero cabaña de groenlandia donde el párroco hans_egede realizar su apostolado de amor con palabra y obra el llamar ser uno estrella en su pecho uno corazón en el arma danés
y el pensamiento de el abuelo se anticipar a el llama flotante pues saber adónde ir este
en el pobre vivienda de el campesino federico_vi de pie escribir con tiza su nombre en el viga
el llama temblar sobre su pecho y en su corazón en aquel humilde estancia su corazón pasar a forzar parte de el escudo danés
y el viejo se secar el ojo pues haber conocer a el rey federico con su cabello de plata y su noble ojo azul y por él haber vivir
y juntar el mano se quedar inmóvil con el mirada fijo
entrar entonces su nuera a decir a el anciano que ser ya muy tarde y hora de descansar y que el mesa estar poner
pero qué hermoso estatua haber hacer abuelo
exclamar el joven
holger y nuestro escudo completo
decir que este cara lo haber ver ya antes
no tú no lo haber ver decir el abuelo pero yo sí y haber procurar tallar lo en el madera tal_y_como lo tener en el memoria
cuando el inglés estar en el rada el [??:2/4/??:????:??] saber demostrar que ser el antiguo danés
a_bordo de el dinamarca donde yo servir en el escuadra de steen_bille haber a mi lado uno hombre se haber decir que el bala le tener miedo
cantar alegremente viejo canción mientras disparar y combatir como si ser uno ser sobrehumano
me acordar todavía de su rostro pero no saber ni lo saber nadie de dónde venir ni adónde ser
mucho vez haber pensar si ser holger el viejo danés en persona que haber salir de kronborg para acudir en nuestro ayuda a el hora de el peligro
este ser el que pensar y ahí estar su efigie
y el figura proyectar uno gran sombra en el pared y incluso sobre parte de el techo parecer como si allí estar el propio holger pues el sombra se mover claro que poder también ser debido_a que lo llamar de el lámpara arder de manera irregular
el nuera dar uno beso a el abuelo y lo acompañar hasta el gran sillón colocar delante_de el mesa y él y su marido hijo de el viejo y padre de el chiquillo que dormir en el cama se sentar a cenar
el anciano hablar de el león y de el danés de el fuerza y el clemencia y explicar de modo bien claro que existir otro fuerza además_de el espada y señalar el armario que guardar viejo libro allí estar el comedia completo de holberg tan leer y releer que uno creer conocer desde hacer mucho tiempo a todo su personaje
ver
este también saber zurrar decir el abuelo
hacer cuanto poder por acabar con todo el disparatar y torpe que haber en el gente
y señalar el espejo sobre el cual estar el calendario con el torre_redonda decir también_tico_brahe manejar el espada pero no con el propósito de cortar carne y quebrar hueso sino para trazar uno camino más preciso entre el estrella de el cielo
y luego aquel cuyo padre ser de mi profesión el hijo de el viejo escultor aquel a quien yo mismo haber ver con su blanco cabello y ancho hombro aquel cuyo nombre ser famoso en todo el país de el tierra
sí él saber esculpir yo sólo saber tallar
sí_holger poder aparecer se nos en figura muy diverso para que en todo el pueblo se hablar de el fuerza de dinamarca
brindar a el salud de bertel
pero el pequeño en su cama ver claramente el viejo kronborg y el öresund y ver a el verdadero holger allá abajo con su barba pegar a el mesa de mármol soñar con todo el que suceder acá arriba
y holger soñar también en el reducir y pobre vivienda de el imaginero oír cuanto en él se hablar y con uno movimiento de el cabeza sin despertar de su sueño decir sí se acordar de mí danés retener me en su memoria
no lo abandonar en el hora de el necesidad
allá ante el kronborg brillar el luz de el día y el viento llevar el nota de el cuerno de caza a el tierra vecino el barco a el pasar enviar su salva bum
bum
y desde el castillo contestar bum
bum
pero holger no se despertar por ruidoso que ser el cañonazo pues sólo decir buenos día
mucho gracia
de uno modo muy distinto tener que disparar para despertar lo pero un_día_u_otro despertar pues holger el danés ser de recio madera

