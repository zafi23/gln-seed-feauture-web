erar se uno vez uno chiquillo que se haber resfriar
cuando estar fuera_de casa se haber mojar el pie nadie saber cómo pues el tiempo ser completamente seco
su madre lo desnudar y acostar y pedir el tetera se disponer a preparar le uno taza de té de saúco pues este calentar
en este venir aquel viejo señor tan divertir que vivir solo en el último piso de el casa
no tener mujer ni hijo pero querer a el niño y saber tanto cuento y historia que dar gusto oír lo
ahora ir a tomar te el té decir el madre a el pequeño y a el mejor te contar uno cuento además
lo hacer si saber alguno nuevo decir el viejo con uno gesto amistoso
pero cómo se haber mojar el pie este rapaz
preguntar
ese decir yo
contestar el madre
cualquiera lo entender
me contar uno cuento
pedir el niño
poder decir me exactamente pues deber saber lo qué profundidad tener el arroyo de el callejón por donde ir a el escuela
me llegar justo a el caña de el bota responder el pequeño pero sólo si me meter en el agujero hondo
conque así te mojar el pie eh
decir el viejo
bueno ahora tener que contar te uno cuento pero el caso ser que ya no saber más
pues inventar se uno nuevo replicar el chiquillo
decir mi madre que de todo el que observar sacar usted uno cuento y de todo el que tocar uno historia
sí pero ese cuento y historia no servir
el de verdad venir por sí solo llamar a el frente y decir aquí estar
llamar pronto
preguntar el pequeño
el madre se echar a reír poner té de saúco en el tetera y le verter agua hervir
contar contar
lo hacer si el cuento querer venir por sí solo pero ser muy remilgar
sólo se presentar cuando le venir en gana
esperar
añadir
ya lo tener
escucha haber uno en el tetera
el pequeño dirigir el mirada a el tetera el tapa se levantar y el flor de saúco salir de el cacharro tierno y blanco proyectar grande rama largo y hasta de el pitorro salir esparcir se en todo dirección y crecer sin cesar
ser uno espléndido saúco uno verdadero árbol que llegar hasta el cama apartar el cortina
ser todo él uno cuajo de flor oloroso y en el centro haber uno anciano de bondadoso aspecto extrañamente vestir
todo su ropaje ser verde como el hoja de el saúco lleno de grande flor blanco
a 1 vista no se distinguir si aquel ser tela o verdor y flor vivo
cómo se llamar este mujer
preguntar el niño
ver el romano y griego responder el viejo lo llamar dríada pero este palabra no lo entender nosotros
allá en nyboder le dar otro nombre mejor lo llamar quotmamita saúcoquot y haber de fijar te en este
escuchar y contemplar el espléndido saúco
haber uno como él florido también allá abajo crecer en uno ángulo de uno era pequeño y humilde
uno [??:??/??/??:1200:pm] 2 anciano se haber sentar a el sol bajo aquel árbol
ser uno marino muy viejo y su mujer que no lo ser menos
tener ya bisnieto y pronto celebrar el boda de oro aunque apenas se acordar ya de el día de su boda el hada desde el árbol parecer tan satisfacer como este de aquí
yo saber cuándo ser su boda de oro decir pero el viejo no lo oír hablar de tiempo pasar
te acordar
decir el viejo marino
te acordar de cuando ser niño y correr y jugar en este mismo era
plantar talle en el suelo y hacer uno jardín
sí replicar el anciano lo recordar bien
regar el tallo uno y ellos ser uno rama de saúco que echar raíz y sacar verde brote y se convertir en uno árbol grande y espléndido este mismo bajo el cual estar
sí este ser decir él y allí en el esquina haber uno gran barreño en él flotar mi barca
yo mismo me lo haber tallar
qué bien navegar
pero pronto lo hacer yo por otro mar
sí pero antes ir a el escuela y aprender uno cuanto cosa proseguir él y luego nos prometer
el 2 llorar pero aquel tarde ir coger de el mano a el torre_redonda para ver el ancho mundo que se extender más_allá_de copenhague y de el océano
después nos ir a frederiksberg donde el rey y el reina pasear por el canal en su embarcación de gala
pero pronto me tocar a mí navegar por otro lugar durante mucho año
ser lejos muy lejos en el curso de largo viaje
sí cuánto lágrima me costar
decir él
creer que haber morir te ver en el fondo de el mar sepultar en el fango
cuánto noche me levantar para ver si el veleta girar
sí girar pero tú no volver
me acuerdo de TM_d:1 que estar llover a_cántaros el basurero se parar frente_a el puerta de el casa donde yo servir
ser uno tiempo espantoso
yo salir con el cubo de basura y me quedar en el puerta y mientras aguardar allí se me acercar el cartero y me dar uno carta uno carta tuyo
dios miar el que haber viajar aquel sobre
lo abrir y leer el carta llorar y reír a_la_vez
estar tan contento
decir el papel que te hallar en tierra cálido donde crecer el café
qué país más maravilloso deber ser
me contar tanto cosa
y yo lo estar ver mientras el lluvia caer sin cesar de pie yo con mi cubo de basura
alguien me coger por el talle
pero tú le propinar uno bueno bofetón muy sonoro por cierto
no saber que ser tú
haber llegar junto_con el carta y estar tan guapo
y todavía lo ser
llevar en el bolsillo uno largo pañuelo de seda amarillo y uno sombrero nuevo
qué elegante ir
dios mío y qué tiempo hacer y cómo estar el calle
entonces nos casar decir él te acordar
y de cuándo venir el 1 hijo y después maría y niels y pedro y juan y cristián
sí y todo crecer y se hacer persona como dios mandar a quien todo el mundo apreciar
y su hijo haber tener ya hijo a su vez decir el viejo
nuestro bisnieto haber bueno semilla
no ser en este tiempo de el año cuando nos casar
sí justamente ser hoy el día de su boda de oro intervenir el hada de el sabucal meter el cabeza entre el 2 viejo el cual pensar que ser el vecino que le hacer seña
se mirar a el ojo y se coger de el mano
al_poco_rato se presentar el hijo y el nieto todo saber muy bien que ser el boda de oro ya lo haber felicitar pero el viejo se haber olvidar mientras se acordar muy bien de el ocurrir tanto año antes
el saúco exhalar uno intenso aroma y el sol cerca ya de el puerta dar a el cara de el abuelo
el 2 tener rojo el cara y el más pequeño de su nieto bailar a su alrededor gritar alegre que haber cena de fiesta comer patata caliente
y el hada asentir desde el árbol y se sumar a el hurras de el demás
pero este no ser uno cuento observar el chiquillo que escuchar el narración
tú lo saber mejor replicar el viejo señor que contar
lo preguntar a el hada de el saúco
no ser uno cuento decir este el cuento venir ahora
el más bello leyenda surgir de el realidad de otro modo mi hermoso saúco no poder haber salir de el tetera
y sacar de el cama a el chiquillo lo estrechar contra su pecho y el rama cuajar de flor se cerrar en_torno_a el 2
quedar ellos rodear de espeso follaje y el hada se echar a volar por el aire
qué indecible hermosura
el hada se haber transformar en uno lindo muchacho pero su vestido seguir ser de el mismo tela verde salpicar de flor blanco que llevar en el saúco
en el pecho lucir uno flor de saúco de verdad y alrededor_de su rubio cabellera ensortijar uno guirnalda de el mismo flor
su ojo ser grande y azul y ser maravilloso mirar lo
él y el chiquillo se besar y entonces quedar de igual edad sentir el mismo alegría
coger de el mano salir de entre el follaje y de pronto se encontrar en el espléndido jardín de el casa paterno en_medio_de el verde césped el bastón de el padre aparecer atar a uno estaquilla
para el pequeño haber vida en aquel bastón no bien se haber montar en él el reluciente pomo se convertir en uno magnífico cabeza de caballo con largo y negro melena ondulante y de el caña salir 4 pata esbelto y vigoroso el animal ser robusto y valiente
se echar a cabalgar a galope por el césped
olé
correr mucho milla decir el muchacho ir a el finca donde estar el año pasar
y venir cabalgar alrededor_de el césped mientras el muchacho que como saber ser el hada de el saúco gritar ya estar llegar
ver el casa de campo con el gran horno que parecer uno gigantesco huevo que salir de el pared y dar a el camino
el saúco extender su rama por encima y el gallo ir de_un_lado_a_otro escarbar el suelo para su gallina
mirar cómo se pavonear
ahora estar cerca de el iglesia en el cumbre de el colina entre corpulento roble uno de el cual estar medio morir
y ahora llegar a el herrería donde arder el fuego y el hombre medio desnudo golpear con su martillo esparcir uno lluvia de chispa
adelante camino de el casa de el señor
y todo el que ir nombrar el chiquillo montar en el bastón lo ver el niño a_pesar_de que no se mover de el prado
jugar luego en el camino lateral y plantar uno jardín en el tierra él se sacar uno flor de saúco de el cabello y lo plantar y crecer como hacer aquel que haber plantar el viejo cuando niño ya
ir coger de el mano como el abuelo hacer de pequeño pero no se encaminar a el torre_redonda ni a el jardín de frederiksberg sino_que el muchacho sujetar a el niño por el cintura y se echar a volar por todo dinamarca y llegar el primavera y luego el verano el tiempo de el cosecha y finalmente el invierno y mil de imagen se pintar en el ojo y el corazón de el niño mientras el muchacho cantar jamás olvidar este
en todo el curso de el vuelo el saúco estar exhalar su aroma suave y delicioso
bien observar el niño el rosa y el haya verde pero el sabucal oler con mayor intensidad aun pues su hoja pender de el corazón de el niño y sobre él reclinar el pequeño a menudo el cabeza durante el vuelo
qué hermoso ser este en primavera
exclamar el muchacho y se encontrar en el bosque de haya en pleno reverdecer con oloroso asperilla a el pie de el árbol y rosado anemone entre el hierba
ah
por qué no ser siempre primavera en el perfumar hayal de dinamarca
qué espléndido ser aquí el verano
exclamar él mientras pasar por delante_de viejo castillo de el tiempo de el caballero cuyo rojo muro y recortar frontón se reflejar en el canal donde nadar cisne y a el largo de el cual se extender antiguo y fresco avenida
en el campo el mies ondear como el mar en el ribazo crecer flor rojo y amarillo y en el seto prosperar el lúpulo silvestre y el florido enredadera
a el anochecer se remontar el luna grande y redondo el montón de heno de el prado esparcir su agradable fragancia
este no se olvidar nunca
ser magnífico aquí el otoño volver a exclamar el muchacho
el aire ser aun más alto y más azul y el bosque presentar uno bello combinación de tono rojo amarillo y verde
pasar correr perro de caza grande bandada de ave salvaje volar gritar por encima_de el sepulcro megalítico recubrir de zarzamora que proyectar su sarmiento en_torno_a el vetusto piedra
el mar ser de uno azul negruzco y aparecer salpicar de barco de vela y en el ser mujer maduro doncella y niño recoger lúpulo y lo meter en uno gran tonel el joven cantar canción mientras el viejo narrar cuento de duende y gnomo
dónde poder estar se mejor
qué hermoso ser aquí el invierno
repetir el niño
todo el árbol estar cubrir de escarcha como blanco coral el nieve crepitar bajo el pie como si se llevar siempre zapato nuevo y en el cielo se suceder el lluvia de estrella
en el sala estar encender el árbol de navidad haber regalo y bueno humor en el casa de labranza resonar el violín y rebanada de manzana caer a el sartén
hasta el niño más pobre decir qué hermoso ser el invierno
y sí ser hermoso y el muchacho enseñar a el niño todo el cosa el saúco seguir exhalar su fragancia y el bandera rojo con el cruz blanco seguir ondear aquel bandera bajo el cual haber navegar el viejo marino de nyboder
el niño se hacer uno mozo y tener que salir a el ancho mundo lejos a el tierra cálido donde crecer el café
pero a el despedir se el muchacho se desprender de el pecho uno flor de saúco y se lo dar como recuerdo
él lo poner cuidadosamente en su libro de cántico y siempre que lo abrir en tierra extraño lo hacer en el página donde guardar el flor y cuanto más lo contemplar más verde se poner él
le parecer a el mozo respirar el aroma de el bosque patrio y ver claramente a el muchacho que lo mirar por entre el pétalo con aquel ojo suyo azul y límpido y susurrar qué hermoso ser aquí el primavera el verano el otoño y el invierno
y centenar de imagen cruzar su mente
así transcurrir mucho año el muchacho ser ya uno anciano y estar sentar con su anciano esposo bajo uno árbol en flor
se haber coger de el mano como el bisabuelo y el bisabuelo de nyboder y el mismo que ellos hablar de el tiempo pretérito y de el boda de oro
el muchacho de ojo azul y de el flor de saúco en el pelo desde el alto de el árbol inclinar el cabeza con gesto de aprobación y decir hoy celebrar su boda de oro
sacar se luego 2 flor de su corona lo besar y ellos relucir primero como plata y después como oro y cuando lo poner en el cabeza de el anciano cada flor se transformar en uno áureo corona
y allí seguir el 2 semejante a uno rey y uno reina bajo el árbol fragante y él contar a su anciano esposo el historia de el hada de el sabucal igual que se lo haber contar antes a él cuando ser uno chiquillo y el 2 convenir en que en aquel historia haber mucho cosa que correr pareja con el propio y el que más se parecer ser el que más le gustar
así ser decir el muchacho de el árbol
alguno me llamar hada otro dríada pero en realidad mi nombre ser recuerdo
yo ser el que vivir en el árbol que crecer y crecer continuamente
poder pensar en el pasado y contar lo
dejar me ver si conservar aun tu flor
el viejo abrir su libro de cántico y allí estar el flor de saúco fresco y lozano como si acabar de coger lo y el recuerdo hacer uno gesto de aprobación y el 2 anciano
con el corona de oro en el cabeza seguir sentar a el sol poniente
cerrar el ojo y
bueno el cuento se haber terminar
el chiquillo yacer en su cama haber ser aquel uno sueño o realmente le haber contar uno cuento
sobre el mesa se ver el tetera pero de él no salir ninguno saúco y el anciano señor de el piso alto se dirigir a el puerta para marchar se
qué bonito haber ser
decir el pequeño
madre haber estar en el tierra cálido
no me extrañar responder el madre
cuando uno se haber tomar uno par de taza de infusión de flor de saúco no haber duda de que se encontrar en el tierra cálido
y lo arropar bien para que no se enfriar
estar dormir mientras yo y él discutir sobre si ser uno cuento o uno historia
y dónde estar el hada de el saúco
preguntar el niño
en el tetera replicar el mujer y poder seguir en él

