ser otoño
estar en el alto de el baluarte contemplar el mar surcar por numeroso barco y a_lo_lejos el costa sueco que se destacar altivo a el luz de el sol poniente
a nuestro espalda descender abrupto el bosque y nos rodear árbol magnífico cuyo amarillo follaje ir desprender se de el rama
a el fondo haber casa lóbrego con empalizada y en el interior donde el centinela efectuar su monótono paseo todo ser angosto y tétrico pero más tenebroso ser todavía del_otro_lado de el enrejar cárcel donde se hallar el presidiario el delincuente peor
uno rayo de el sol poniente entrar en el desnudo celda pues el sol brillar sobre el bueno y el malo
el preso hosco y rudo dirigir uno mirada de odio a el tibio rayo
uno pajarillo volar hasta el reja
el pájaro cantar para el bueno y el malo
su canto ser uno breve trino pero el pájaro se quedar allí agitar el ala
se arrancar uno pluma y se esponjar el de el cuello y el mal hombre encadenar lo mirar
uno expresión más dulce se dibujar en su hosco cara uno pensamiento que él mismo no comprender claramente brotar en su pecho uno pensamiento que tener algo de común con el rayo de sol que entrar por el reja y con el violeta que tan abundante crecer allá ser en primavera
luego resonar el cuerno de el cazador melódico y vigoroso
el pájaro se asustar y se echar a volar alejar se de el reja de el preso el rayo de sol desaparecer y volver a reinar el oscuridad en el celda el oscuridad en el corazón de aquel hombre malo pero el sol haber brillar y el pájaro haber cantar
segan resonar hermoso toque de el cuerno de caza
el atardecer ser apacible el mar estar en calma terso como uno espejo
el aguja de zurcir érase uno vez uno aguja de zurcir tan fino y puntiagudo que se creer ser uno aguja de coser
fijar se en el que hacer y manejar me con cuidado decir a el dedo que lo manejar
no me dejar caer que si ir a el suelo lo pasar negro para encontrar me
ser tan fino
ir ir que no haber para tanto
decir el dedo sujetar lo por el cuerpo
miren aquí llegar yo con mi séquito proseguir el aguja arrastrar tras sí uno largo hebra pero sin nudo
el dedo apuntar el aguja a el zapatilla de el cocinero el cuero de el parte superior haber reventar y se disponer a coser lo
qué trabajo más ordinario
exclamar el aguja
no ser para mí
me romper me romper
y se romper no os lo decir
suspirar el víctima
ser demasiado fino
ya no servir para nada pensar el dedo pero haber de seguir sujetar lo mientras el cocinero le aplicar uno gota de lacre y luego ser clavar en el pechera de el blusa
tomar
ahora ser uno prendedor
decir el vanidoso
bien saber yo que con el tiempo hacer carrera
cuando uno valer un_día_u_otro se lo reconocer
y se reír para su adentros pues por fuera ser muy difícil ver cuándo se reír uno aguja de zurcir
y se quedar allí tan orgulloso cómo si ser en coche y pasear el mirada a su alrededor
poder tomar me el libertad de preguntar le con el deber respeto si_acaso ser usted de oro
inquirir el alfiler vecino suyo
tener usted uno porte majestuoso y cabeza propio aunque pequeño
deber procurar crecer pues no siempre se poder poner gota de lacre en el cabo
a el oír este el aguja se erguir con tanto orgullo que se soltar de el tela y caer en el vertedero en el que el cocinero estar lavar
ahora me ir de viaje decir el aguja
con_tal_que no me perder
pero ser el caso que se perder
este mundo no estar hacer para mí pensar ya en el arroyo de el calle
ser demasiado fino
pero tener conciencia de mi valer y este siempre ser uno pequeño satisfacción
y mantener su actitud sin perder el bueno humor
por encima_de él pasar flotar todo clase de objeto viruta paja y pedazo de periódico
cómo navegar
decir el aguja
poco se imaginar el que haber en el fondo
yo estar en el fondo y aquí seguir clavar
tomar
ahora pasar uno viruta que no pensar en nada de el mundo como no ser en uno quotvirutaquot o_sea en él mismo y ahora venir uno paja qué manera de revolcar se y de girar
no pensar tanto en ti que dar contra uno piedra
y ahora uno trozo de periódico
nadie se acordar de el que poner y no_obstante cómo se ahuecar
yo en cambio me estar aquí paciente y quieto saber lo que ser y seguir ser lo
TM_d:1 ser a parar a su lado uno objeto que brillar tanto que el aguja pensar que tal_vez ser uno diamante pero en realidad ser uno casco de botella
y como brillar el aguja se dirigir a él presentar se como alfiler de pecho
usted deber ser uno diamante verdad
bueno
sí algo por el estilo
y el 2 quedar convencer de que ser joya excepcional y se enzarzar en uno conversación acerca_de el presuntuoso que ser el gente
saber
yo vivir en el estuche de uno señorito decir el aguja de zurcir ser cocinero tener 5 dedo en cada mano pero nunca haber ver nada tan engreír como aquel 5 dedo y sin embargo todo su misión consistir en sostener me sacar me de el estuche y volver me a meter en él
brillar acaso
preguntar el casco de botella
brillar
exclamar el aguja
no pero a orgulloso nadie lo ganar
ser 5 hermano todo dedo de nacimiento
ir siempre junto el mar de tieso uno a el lado de el otro a_pesar_de que ninguno ser de el mismo longitud
el de más afuera se llamar pulgar ser corto y gordo estar separar de el mano y como sólo tener uno articulación en el dorso sólo poder hacer uno inclinación pero afirmar que si a uno hombre se lo cortar quedar inútil para el servicio militar
luego venir el lameollas que se meter en el dulce y en lo amargo señalar el sol y el luna y ser el que apretar el pluma cuando escribir
el larguirucho se mirar a el demás desde lo alto el « borde dorar » se pasear con uno aro de oro alrededor_de el cuerpo y el menudo « meñique » no hacer nada de el cual estar muy ufano
todo ser jactar se y vanagloriar se
por ese ser yo a dar en el vertedero
ahora estar aquí brillar decir el casco de botella
en el mismo momento llegar más agua a el arroyo lo desbordar y se llevar el casco
vamos
a este lo haber despachar decir el aguja
yo me quedar ser demasiado fino pero este ser mi orgullo y valer el pena
y permanecer altivo sumir en su pensamiento
de tan fino que ser casi creer que nacer de uno rayo de sol
tener el impresión de que el sol me buscar siempre debajo_de el agua
ser tan sutil que ni mi padre me encontrar
si no se me haber romper el ojo crear que llorar pero no no ser distinguir llorar
TM_d:1 se presentar varios pilluelo y se poner a rebuscar en el arroyo en_pos_de clavo viejo perro chico y otro cosa por el estilo
ser uno ocupación muy sucio pero ellos se divertir de_lo_lindo
ay
exclamar uno se haber pinchar con el aguja de zurcir
este marrana
yo no ser ninguno marrana sino uno señorito
protestar el aguja pero nadie lo oír
el lacre se haber desprender y el metal estar ennegrecer pero el negro hacer más esbelto por el que el aguja se creer aun más fino que antes
ahí venir flotar uno cáscara de huevo
gritar el chiquillo y clavar en él el aguja
negro sobre fondo blanco observar este
qué bien me sentar
ser bien visible
con_tal_que no me marear ni vomitar
pero no se marear ni vomitar
ser uno gran cosa contra el mareo tener estómago de acero
en este sí que estar por encima_de el vulgo
me sentar como si nada
cuánto más fino ser uno más resistir
crac
exclamar el cáscara a el sentir se aplastar por el rueda de uno carro
uf cómo pesar
añadir el aguja
ahora sí que me marear
me romper me romper
pero no se romper pese_a haber ser atropellar por uno carro
quedar en el suelo y el que ser por mí poder seguir allí mucho año

