 IN THE NURSERY FATHER and mother and brothers and sisters were gone to the play only little Anna and her grandpapa were left at home
Well have a play too he said and it may begin immediately But we have no theatre cried little Anna and we have no one to act for us my old doll cannot for she is a fright and my new one cannot for she must not rumple her new clothes One can always get actors if one makes use of what one has observed grandpapa
Now well go into the theatre
Here we will put up a book there another and there a third in a sloping row
Now three on the other side so now we have the side scenes
The old box that lies yonder may be the back stairs and well lay the flooring on top of it
The stage represents a room as every one may see
Now we want the actors
Let us see what we can find in the playthingbox
First the personages and then we will get the play ready
One after the other that will be capital
 Heres a pipehead and yonder an odd glove they will do very well for father and daughter But those are only two characters said little Anna
Heres my brothers old waistcoat could not that play in our piece too
 Its big enough certainly replied grandpapa
It shall be the lover
Theres nothing in the pockets and thats very interesting for thats half of an unfortunate attachment
And here we have the nutcrackers boots with spurs to them
Row dow dow
 how they can stamp and strut
 They shall represent the unwelcome wooer whom the lady does not like
What kind of a play will you have now
 Shall it be a tragedy or a domestic drama
 A domestic drama please said little Anna for the others are so fond of that
Do you know one
 I know a hundred said grandpapa
Those that are most in favor are from the French but they are not good for little girls
In the meantime we may take one of the prettiest for inside theyre all very much alike
Now I shake the pen
 Cockalorum
 So now heres the play brinbranspan new
 Now listen to the playbill And grandpapa took a newspaper and read as if he were reading from it THE PIPEHEAD AND THE GOOD HEAD A Family Drama in One Act CHARACTERS MR
PIPEHEAD a father
MR
WAISTCOAT a lover
MISS GLOVE a daughter
MR
DE BOOTS a suitor
And now were going to begin
The curtain rises
We have no curtain so it has risen already
All the characters are there and so we have them at hand
Now I speak as Papa Pipehead
 Hes angry today
One can see that hes a colored meerschaum
Snik snak snurre bassellurre
 Im master of this house
 Im the father of my daughter
 Will you hear what I have to say
 Mr
de Boots is a person in whom one may see ones face his upper part is of morocco and he has spurs into the bargain
Snikke snakke snak
 He shall have my daughter
 Now listen to what the Waistcoat says little Anna said grandpapa
Now the Waistcoats speaking
The Waistcoat has a laydown collar and is very modest but he knows his own value and has quite a right to say what he says I havent a spot on me
 Goodness of material ought to be appreciated
I am of real silk and have strings to me  On the wedding day but no longer you dont keep your color in the wash This is Mr
Pipehead who is speaking
Mr
de Boots is watertight of strong leather and yet very delicate he can creak and clank with his spurs and has an Italian physiognomy But they ought to speak in verses said Anna for Ive heard thats the most charming way of all They can do that too replied grandpapa and if the public demands it they will talk in that way
Just look at little Miss Glove how shes pointing her fingers
 Could I but have my love Who then so happy as Glove
 Ah
 If I from him must part Im sure twill break my heart
 Bah
 The last word was spoken by Mr
Pipehead and now its Mr
Waistcoats turn O Glove my own dear Though it cost thee a tear Thou must be mine For Holger Danske has sworn it
 Mr
de Boots hearing this kicks up jingles his spurs and knocks down three of the sidescenes Thats exceedingly charming
 cried little Anna
Silence
 silence
 said grandpapa
Silent approbation will show that you are the educated public in the stalls
Now Miss Glove sings her great song with startling effects I cant see heigho
 And therefore Ill crow
 Kikkeriki in the lofty hall
 Now comes the exciting part little Anna
This is the most important in all the play
Mr
Waistcoat undoes himself and addresses his speech to you that you may applaud but leave it alone thats considered more genteel
I am driven to extremities
 Take care of yourself
 Now comes the plot
 You are the Pipehead and I am the good head snap
 there you go
 Do you notice this little Anna
 asked grandpapa
Thats a most charming comedy
Mr
Waistcoat seized the old Pipehead and put him in his pocket there he lies and the Waistcoat says You are in my pocket you cant come out till you promise to unite me to your daughter Glove on the left
I hold out my right hand Thats awfully pretty said little Anna
And now the old Pipehead replies Though Im all ear Very stupid I appear Wheres my humor
 Gone I fear And I feel my hollow sticks not here Ah
 never my dear Did I feel so queer
Oh
 pray let me out And like a lamb led to slaughter Ill betroth you no doubt To my daughter Is the play over already
 asked little Anna
By no means replied grandpapa
Its only all over with Mr
de Boots
Now the lovers kneel down and one of them sings Father
 and the other Come do as you ought to do Bless your son and daughter And they receive his blessing and celebrate their wedding and all the pieces of furniture sing in chorus Klink
 clanks
 A thousand thanks And now the play is over
 And now well applaud said grandpapa
Well call them all out and the pieces of furniture too for they are of mahogany And is not our play just as good as those which the others have in the real theatre
 Our play is much better said grandpapa
It is shorter the performers are natural and it has passed away the interval before teatime THE END 