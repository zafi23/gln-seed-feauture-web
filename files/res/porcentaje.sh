#! /bin/bash

nlines=$(cat $1 | wc -l)
nexist=0


while read line
do 
	
	if [ -z "$(cat Cuentos_Infantiles/corpus_junto/* | tr '[:upper:]' '[:lower:]' | grep -e "$line")" ]; 	then
		nexist=$((nexist+1))
		echo $line
	fi

done < $1

echo $nexist
echo $(echo "$nexist/$nlines*100" | bc -l)
