On Noah_'s_Ark things were getting a_bit boring .
Noah and his animals had spent so many days secluded there that they started organising games and activities to amuse themselves .
But , with all that pent up energy , the games got rather rowdy , and a woodpecker ended up drilling a hole in the bottom of the ark. As water began entering the boat , the hole got bigger .
So , more water came in , and things got a_bit worrying .
One by one , different animals tried to fix the hole .
They even got competitive about it because everyone wanted to be the animal that had saved the ark. The beaver built a dam over the hole , but not even that worked .
Everyone was scared , worried that the boat would sink .
That was , until the bee started talking .
The bee explained to everyone how it was that bees always worked together , as a team , each one doing the job they were best at .
On hearing this , all the animals set about working together , each one playing their part by contributing their own special talent .
The birds grabbed onto parts of the ark with their beaks , and flapped their wings furiously , lifting the boat up a little .
The elephants sucked up the water in their trunks and shot it back into the sea .
The fastest animals ran here and there , collecting materials .
Those used to making nests took this material and stuffed it quickly into the hole .
And so , working together , the animals managed to reduce the amount of water coming into the ark , but they still had not stopped it completely .
Desperate , they kept asking each other if there were any other animals that could help .
They searched and searched , but there were no other animals left in the ark. Then , suddenly , a little fish swam in through the hole .
The animals realised that they still had not asked for help from all the sea creatures .
They asked the little fish to go and summon help to save their boat .
He swam off and soon fish after fish arrived at the ark. Even a big whale came , and the whale pressed its great belly against the hole in the ship .
This stopped any more water entering , and it gave the animals on the ark time to close up the hole
