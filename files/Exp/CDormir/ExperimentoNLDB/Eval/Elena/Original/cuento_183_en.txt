There was once a tiny , tiny star about the same size as a mosquito .
The star lived in space , next to its parents , which were two absolutely enormous stars .
The tiny star was very curious , and always wanted to know what was going on .
She wanted to travel around to see for herself , but her parents told her that she was still too little to go off on her own , and that she would have to wait .
One day , the star saw a little blue planet .
The planet looked so lovely that the tiny star forgot her parents ´ rules , and off she went to get a better look at it .
But she flew so fast , so incredibly fast , that she soon got lost , and did not know where home was .
When she arrived on the blue planet , which was the Earth , the people and animals thought she was a very bright firefly , and everyone wanted to catch her .
She fled as fast she could , frightened , and she hid herself under a sheet .
Seeing the sheet moving , everyone thought that she was a ghost , and they ran off terrified .
The little star used her new disguise to have fun , scaring the living daylights out of everyone she met .
As she went on she arrived at a mountain , where a great dragon lived .
The little star tried to scare the dragon too , but she did not know that it was a ghost-eating dragon .
She only got a whiff of this when she found herself surrounded by the flames that the dragon had spit at her .
Luckily , she was a very hot little star , so she managed to escape the dragon and its flames .
However , she had been frightened to death , and this combined with the sadness of missing her parents .
She cried for a while , but then , as night fell , she got an idea to help her find her parents .
She went to a very high mountain and found a big boulder .
Then , looking up at the sky , she hid behind the boulder , then reappeared , then hid again . . and so_on .
Her parents were very worried about her , and were searching everywhere to find her .
They saw her light appearing and disappearing off in the distance .
Instantly , off they flew to find her .
So the little star had tasted adventure and learnt many things , but she no_longer wanted to go off on her own until she was much older
