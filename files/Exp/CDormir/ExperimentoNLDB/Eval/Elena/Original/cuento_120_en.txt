Once upon a time , a boy went on his holidays to a great castle .
He ran through all the rooms and corridors , apart_from one wing , which he never entered for fear of the darkness there .
In that part of the castle lived a frightened multicoloured ghost .
He had never left that area because he was afraid of the light .
Both the boy and the ghost tried to overcome their fear several times , but without success .
That was , until one day when the boy summoned up all his courage , and started crossing the dark corridor .
He managed to do this by imagining that his friends had organised a surprise party for him , and were waiting in the darkness .
The boy and the ghost met there , and soon became great friends .
So friendly did they become that the boy helped the ghost overcome his fear of the light
