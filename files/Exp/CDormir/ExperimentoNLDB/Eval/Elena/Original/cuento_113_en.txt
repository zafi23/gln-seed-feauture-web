Once upon a time there was a Wicked_Wizard .
One night the wizard visited a city and stole a_thousand tongues from its sleeping inhabitants .
He took these tongues and cast a spell on them .
The spell meant that these tongues could only say bad things about people .
Then the wizard returned the tongues to their owners , who suspected nothing .
In very little time , that city was filled with the sound of people saying bad things about each other , " Yes , he did that , she did the other , boy , was that guy a bore , and the other guy was really clumsy ... " Soon everyone was angry with everyone else , and this brought the Wicked_Wizard no end of satisfaction .
On seeing all this , the Good_Wizard decided to intervene with his own powers .
He cast a spell on the ears of the city dwellers .
Under this spell , whenever the ears heard people criticising others , they would close up tightly , so_that nothing could be heard .
And so started the great and terrible battle between tongues and ears .
The one endlessly criticising , the other blocking all this out .
Who won the battle ?
Well , with the passing of time , the tongues started to feel completely useless .
Why talk if no_one was listening ?
Being tongues , they liked to be heard , so they gradually started to change the kind of things they would say .
When the tongues realised that saying good things about people meant they would be listened to once again , they were filled with joy , and forgot forever the spell they had been under .
Even to this day , the Wicked_Wizard continues casting spells on tongues all over the world .
But thanks to the Good_Wizard now everyone knows that to put an end to gossiping , all one has to do is pay no attention to it
