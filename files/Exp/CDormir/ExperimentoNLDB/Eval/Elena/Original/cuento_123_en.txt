There was once a clown named Lemon .
He was a_lot_of fun , but also very careless .
Whenever he did anything , he almost always ended up tearing his jacket , getting a hole in his sock , or ripping the knee of his trousers .
Everyone said he should take more care , but Lemon found that really boring .
So , one day , he had the happy idea of buying himself a sewing machine .
This machine was so fantastic that it sewed everything up within a moment .
It seemed like Lemon would not have to worry about his things anymore .
Soon , the most important day in Lemon 's life arrived .
It was the day when everyone in town prepared a party for him , in celebration of Lemon 's many years of service in making the citizens chuckle .
On that day he would not have to wear his colourful clown suit .
That day he would be dressed like anyone else ; very elegantly in his suit , and everyone commenting on his smart appearance .
However , the night before , he had a look in his closet , and there was not a single suit in good condition .
They were all ripped and torn , with dozens of stitching marks .
Lemon could not possibly go to the party dressed in those .
Well , Lemon was sharp and quick-witted , and he solved this problem by attending the gala celebration in his usual clown suit .
That definitely amused the audience , but Lemon was not as keen on it .
He had dreamed of being , just for one day in the year , the hero of the show , and not just the clown .
Very early the next day , Lemon replaced all those damaged old suits , and since then , he has looked after his clothes with great care .
He realised that using short-term remedies ends up being no remedy at all
