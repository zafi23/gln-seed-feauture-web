Gail the Whale lived in a small salty lake .
She was the only whale in that territory , and she led a very comfortable life .
In fact , this easy life made her a_bit fussy .
But , one year , there was such an incredibly hot summer that the lake 's water really warmed up .
Gail , used to such an ideal existence , could hardly stand the hot water .
A little fish , which had spent some time in a child 's goldfish bowl , told Gail that humans used fans to cool themselves down in summer .
From then on , Gail_the_Whale could not think of anything else apart_from how to build her very own fan .
Everyone told her that she was overreacting , and that the hot weather would soon pass , but Gail got to work , constructing her enormous fan .
When it was finally finished , she started fanning away at herself .
Unlucky for the fish !
The giant fan beat the little lake 's waters so strongly that huge waves rolled right across it .
The waves crashed onto the lakeshore , leaving the lake half empty , and Gail stranded in only a few inches of water . " You could not just hang on for while . You had to empty the lake , " some unhappy-looking fish berated her . " So impatient ! So selfish ! " others shouted .
But the worst of it for Gail was not the insults , but the fact that with so little water around her , the heat was becoming unbearable .
Preparing herself to die of heatstroke , she said her goodbyes to all her friends , and they asked for her forgiveness .
She assured them all that if she were to live again she would be stronger and learn to put up with life 's discomforts .
Yet , once again , Gail_the_Whale was overreacting .
She managed to survive those hot days without dying , although , of course , she suffered .
When the next rains arrived , the lake filled up again , and the weather improved .
Naturally , Gail had to keep her promise , and show everyone that she had learned not to be so dependent on comfort , so impatient , and so fussy
