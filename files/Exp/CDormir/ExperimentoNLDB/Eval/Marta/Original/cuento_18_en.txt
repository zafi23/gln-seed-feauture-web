The forest wolf spent his nights howling at the moon .
He was making fun of her , of how old she was , how slowly she moved , and how little light she had .
In the same forest , when the howling had stopped , the little hedgehog would come out to console the moon .
One day , both the wolf and the hedgehog were far from home and were caught unawares by a great storm .
When the storm subsided , both animals were lost .
As the moon came out , the wolf began his usual howling , while the hedgehog was feeling sad and frightened at being lost .
Before long the hedgehog heard a voice calling him , but he could not see anyone around .
It was the moon , who was so grateful for the hedgehog 's constant help and advice , that she wanted to help him find his way home .
So the moon gathered up all her light into one single ray , to help show the hedgehog how to get back safely .
The hedgehog arrived home in the early hours , while the wolf remained lost , out in the darkness , and scared to death .
Only then did he realise that all his rudeness to the moon had been pointless and cruel .
The moon did not shine for him until the wolf asked for forgiveness for his bad attitude , and promised not to bother anyone again like that
