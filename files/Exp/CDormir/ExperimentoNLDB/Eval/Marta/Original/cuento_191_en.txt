There was once a boy who had two naughty little toy men .
One day , one of the little men saw a lovely box of matches in the kitchen , and took them - despite knowing that it was not a toy and could be dangerous .
The two toy men took advantage of the boy 's absent-mindedness , and quickly took a match and hid it in the car in which the boy usually put the men in to play .
Later , the boy went to the playground with his car , but when he was there the end of the match stuck out of the car window and scraped against a stone .
The match ignited and the car began to burn .
Fortunately , the boy 's mother was nearby and she managed to quickly put the fire out .
However , they could not save the part of the car in which the two little men were sitting .
They were burnt and partly melted onto the car seat .
Now they could never leave that car again .
The boy got a real fright , and understood why his mother never let him play with matches and other dangerous things .
And there they remained , trapped forever , those two little toy men .
And now , whenever they see the boy about to do something dangerous they try to get his attention , to warn him of what happens when we play with fire
