Once upon a time , a juggling clown came to a village .
The clown went from town to town , earning a little money from his show .
In that village he began his act in the square .
While everyone was enjoying the show , a naughty boy started to make fun of the clown , telling him to leave the village .
The shouts and insults made the clown nervous , and he dropped one of his juggling balls .
Some others in the crowd started booing because_of this mistake , and in the end the clown had to leave quickly .
He ran off , leaving four of the juggling balls .
But neither the clown nor his juggling balls were in any way ordinary .
During that night , each one of the balls magically turned into a naughty boy , just like the one who had shouted the insults .
All except one ball , which turned into another clown .
For the whole of the next day , the copies of the naughty boy walked round the village , making trouble for everyone .
In the afternoon , the copy of the clown started his juggling show , and the same thing happened as the previous day .
But , this time , there were four naughty boys shouting , instead_of one .
Again , the clown had to run off , leaving another four balls behind .
Once more , during the night , three of those balls turned into copies of the naughty boy , and one turned into a clown .
And so the same story repeated itself for several days , until the village was filled with naughty boys who would leave no_one in peace .
The village elders decided to put an end to all this .
They made sure that none of the naughty boys would disrespect or insult anyone .
When the clown 's show began , the elders prevented the boys even making a squeak .
So the clown managed to finish his show , and could spend that night in the village .
That night , three of the copies of the naughty boy disappeared , and the same happened until only the clown and the original naughty boy remained .
The boy , and everyone in the village , had been shown just how far they could go .
From then on , instead_of running visitors away , that village made every effort to make sure that visitors would spend a nice day there .
The villagers had discovered just how_much a humble travelling clown can teach with his show
