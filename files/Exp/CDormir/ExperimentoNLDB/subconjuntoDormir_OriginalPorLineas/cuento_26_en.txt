Penguin , Reindeer and Fox were great friends .
One day , Penguin and Reindeer found a load of fruit , and decided to keep it a secret .
On the way , they met Fox , who seeing them so happy , asked them why .
They told him they could not say , because it was a secret , but Fox asked them to trust him , so they told him about the fruit .
When they arrived at the village , Fox forgot about his promise , and told everyone .
When Penguin and Reindeer returned to the place where they had found the fruit , the animals of the village had already been there and eaten it all .
That same day , Penguin and Reindeer found another place full of food , and the same thing happened again with Fox .
Angered by these betrayals , they decided to teach Fox a lesson .
The next day they told Fox that they had found a lake so full of fish that no effort was needed to catch them .
Fox again told everyone in the village about this .
The next day , Fox came by , covered in cuts and bruises .
After telling all the animals about the lake full of fish , everyone , including even the polar bears , had gone there .
But , not finding anything , they felt deceived , and had given Fox a good beating .
Fox learned that keeping people 's trust is very important , and that to get it in the first place you have to earn it with loyalty and always keeping your word .
Penguin and Reindeer devised another trick for Fox but , as he was no_longer a bigmouth , he did not betray them , and Penguin and Reindeer regained their faith in Fox , thus forgiving him
