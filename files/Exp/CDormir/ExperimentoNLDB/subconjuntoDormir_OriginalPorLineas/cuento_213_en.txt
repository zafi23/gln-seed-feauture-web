A young Queen was given a special present from a great wizard .
It was a magic chest which would bring happiness to the whole kingdom whenever it was opened in a place where there was a spirit of generosity .
The Queen travelled all over her kingdom , looking for the most generous people .
When she had collected them all , she opened the magic chest .
However , nothing whatsoever happened .
That was , until one day when , returning to her castle , the Queen saw a poor little boy begging .
The Queen would have given the boy some money , but she did not have any with her .
So the boy asked her if she could give him the old chest she had , so he could sell it for a little money , in town .
At first the Queen hesitated , because she had been told the chest was magic .
But on seeing how poor the boy was , she gave it to him .
The boy took the chest and opened it .
Immediately , all the most wonderful things one could imagine started flying out of the chest , accompanied by the sound of singing : " Why look for it in others ? Goodness always starts in yourself " , went the song .
And as_well as enjoying all the wonders of the magic chest , the Queen learned to set an example in virtue , and she became the best Queen ever to reign over that kingdom
