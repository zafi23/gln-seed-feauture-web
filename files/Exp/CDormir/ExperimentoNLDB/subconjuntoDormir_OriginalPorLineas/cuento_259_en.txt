Pogo the Ogre , Big_Giant , and The_Abominable One had spent hundreds of years locked up in Monster_Prison .
They had entered voluntarily , when they realised that frightening children was not a good way to make a living .
Since then , the three of them had been sad and lonely figures .
Frightening children was the only thing they knew how to do well .
They lost all hope , and felt useless .
They had already served their full jail sentences several times , but when they were told they were free to go , they answered by asking where they could go , and what could they do , if all they knew in life was scaring people ... However , all that changed on the day they locked up Nightmare .
Nightmare was a tiny little monster , who devoted very little time to frightening others and spent most of the day sleeping .
But when awake , he was great fun .
He told hundreds of stories of how he had changed people 's dreams to make them more amusing , and of how these changes almost always turned out so badly that they would have scared anyone .
Pogo the Ogre , and his friends , loved Nightmare 's stories , but they had to wait for the sleepyhead to get up so he could tell them .
Now , that was by no means easy .
It seemed not even an earthquake could wake the guy .
Until one day , when the three monsters combined their terrific cries .
Nightmare woke up with a start .
He looked at them , wide-eyed , but he did not seem frightened or angry .
Rather , he seemed happy . - " Great ! " he said , " I have always wanted to get up early . That way you enjoy the day much better . You know what ? You guys should work as alarm clocks . I know a_lot_of sleepyheads who would really thank you for it . " The three monsters were so happy to hear those words .
Finally they could be of some use !
After so many years , it turned out they could do much more than they had believed .
And they could do it without frightening or bothering children .
That very day they left the prison , ready to make their first alarm call .
And so it was that the three monsters became famous for their special service for sleepyheads , and were happy to have understood that there is always something nice we can do , just waiting to be discovered
