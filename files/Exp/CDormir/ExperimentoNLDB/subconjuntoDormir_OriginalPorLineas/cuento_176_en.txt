The Great_Wizard was a hunter of lies .
He invented magic stones to help find the child who told most of them .
The magic stones were beautiful , and with every lie they would grow bigger .
The stones moved from person to person until they reached the worst of liars .
A little boy , who was a terrible liar , started collecting these magic stones , and when he had a great many of them , he decided to leave on a little boat .
When the boy and his boat were out at sea , the wizard appeared and started asking him questions about the stones .
Because the boy only ever answered with lies , the stones started growing , and under their weight the boat began to sink .
The boy was frightened and started crying .
He regretted telling so many lies , and he asked the wizard to forgive him .
However , the wizard said that he would only save the boy if he would agree to become his apprentice .
The boy agreed , and spent many years as the wizard 's assistant .
Until one day the wizard retired , and the boy , who had been such a terrible liar , ended up being the new Great_Wizard , hunter of lies
