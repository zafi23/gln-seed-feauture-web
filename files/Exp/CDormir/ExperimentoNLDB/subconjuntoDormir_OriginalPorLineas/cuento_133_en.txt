One night , a hedgehog was scanning the sky with his telescope , when he saw what seemed to be a spaceship flying to the moon .
When he managed to properly focus on it , he found out that it was a craft belonging to an unfortunate Martian who seemed to have had an accident , and had to make an emergency landing on the moon .
The hedgehog realised that surely only he himself had seen this , so he decided to try to save the Martian .
He called together a few animals to help .
They could not think what to do , so they called for more and more animals to join in .
In the end , practically everyone in the forest was involved .
It occurred to them that if they stood on top of each other , they could build a big tower and perhaps reach the moon .
That proved somewhat difficult , and most animals ended up having had a finger in their eye , someone 's foot in their ear , and numerous bumps on the head .
However , after much perseverance , they finally reached the moon and rescued the Martian .
Most unfortunately , while he was coming down the tower of animals , the bear could not help sneezing .
He happened to be allergic to moon dust .
The whole tower crashed to Earth with a great din comprising howls , roars , and other assorted animal cries .
Seeing all this , the Martian thought that the animals would very angry with him because they would blame it all on him .
But it was just the opposite .
As they recovered from the fall , the animals jumped and clapped with joy , happy to have achieved something so difficult together .
The whole day was spent partying .
The Martian observed everything , and when he returned to his planet the other Martians were astonished at what had happened .
And so it was that those simple and helpful animals taught the Martians the importance of working together , joyfully , in a team .
Since then , Martians no_longer travel alone during their journeys through space .
Now they go in groups , always willing to help each other , and make sacrifices whenever necessary
