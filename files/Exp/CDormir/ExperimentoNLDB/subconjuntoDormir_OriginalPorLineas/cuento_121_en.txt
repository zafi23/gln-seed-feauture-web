Once upon a time , there was a tortoise on a ship , and the ship sank .
Some time later the tortoise made it to a desert land surrounded by water on all sides except_for one .
The landward side led up to a big , steep , craggy mountain .
To avoid starving to death , the tortoise decided to climb to the top of the mountain , hoping that he would be able to cross to the other side .
When he got to the snow-covered summit he was freezing cold , and then a blizzard started .
He just managed to make out a small pathway leading down the other side of the mountain .
But the path was guarded by a big monster that would not stop shouting . " Uuh uuh uuh ! " Such a sight and sound almost killed the tortoise with fright , and all he wanted to do was hide his head inside his shell .
But , looking around him , he saw that many other animals were lying frozen to death , and with looks of horror on their faces .
So the tortoise did not go into his shell .
He summoned up all his courage to move down the path towards the monster .
The closer the tortoise got , the more the monster changed its shape .
Then , when he was almost upon it , the tortoise realised that what he had thought was a monster , was only a great pile of rocks , which formed a shape just like a monster .
As for the " Uuh uuh uuh " , the tortoise realised this was just the sound of the wind blowing through a small cave .
The tortoise carried on , and eventually descended into a beautiful valley , filled with woods , and plenty_of food .
The tortoise lived very happily here , and became known everywhere as the Brave_Little_Tortoise
