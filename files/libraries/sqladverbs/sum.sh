#!/bin/bash

synset=''
sent=''

while read line
do 
	
	arr=($line)
	syn=${arr[0]}
	word=${arr[1]}
	if [[ $synset == $syn ]]; then
		sent="$sent $word"
	else
		echo $sent
		synset=$syn
		sent="$syn $word"
	fi	

done < $1

