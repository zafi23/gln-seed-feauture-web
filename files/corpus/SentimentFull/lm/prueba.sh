#!/bin/bash

for f in $1*
do
  if [ -d "$f" ]; then
	cd $f
	echo "$f"
	cd ../..
  fi
done
