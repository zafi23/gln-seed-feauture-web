
export PATH=$PATH:/home/cristina/Escritorio/Software/srilm-1.7.1/bin/i686-m64://home/cristina/Escritorio/Software/srilm-1.7.1/bin

cd "entrenamiento2gram"

# Train lms
echo "training PP1...."
fngram-count -factor-file 2gramCuentosPP1.flm -text corpus_lem.tar.gz -lm -tolower
echo "training PL1...."
fngram-count -factor-file 2gramCuentosPL1.flm -text corpus_lem.tar.gz -lm -tolower
echo "training LP1...."
fngram-count -factor-file 2gramCuentosLP1.flm -text corpus_lem.tar.gz -lm -tolower
echo "training LL1...."
fngram-count -factor-file 2gramCuentosLL1.flm -text corpus_lem.tar.gz -lm -tolower

cd ..

cd "entrenamiento3gram"
echo "training PP1P2...."
fngram-count -factor-file 3gramCuentosPP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training PL1L2...."
fngram-count -factor-file 3gramCuentosPL1L2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training LL1L2...."
fngram-count -factor-file 3gramCuentosLL1L2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training LP1P2...."
fngram-count -factor-file 3gramCuentosLP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk

echo "training PW1W2...."
fngram-count -factor-file 3gramCuentosPW1W2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training WW1W2...."
fngram-count -factor-file 3gramCuentosWW1W2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training WP1P2...."
fngram-count -factor-file 3gramCuentosWP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk
