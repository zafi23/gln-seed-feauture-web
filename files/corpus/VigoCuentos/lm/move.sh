#!/bin/bash

for f in $1*
do
  if [ -d "$f" ]; then
	cp $2*.flm $f/
  fi
done
