P-adverb:W-Once:L-once P-preposition:W-upon:L-upon P-determiner:W-a:L-a P-noun:W-time:L-time P-determiner:W-a:L-a P-noun:W-boy:L-boy P-verb:W-was:L-be P-verb:W-walking:L-walk P-preposition:W-through:L-through P-determiner:W-the:L-the P-noun:W-countryside:L-countryside P-adverb:W-when:L-when P-pronoun:W-he:L-he P-verb:W-saw:L-saw P-punctuation:W-,:L-, P-preposition:W-between:L-between P-determiner:W-some:L-some P-noun:W-clouds:L-cloud P-punctuation:W-,:L-, P-determiner:W-an:L-a P-noun:W-angel:L-angel P-verb:W-singing:L-sing P-determiner:W-a:L-a P-adjective:W-beautiful:L-beautiful P-noun:W-song:L-song P-punctuation:W-.:L-. 
P-determiner:W-The:L-the P-noun:W-angel:L-angel P-adverb:W-soon:L-soon P-verb:W-disappeared:L-disappear P-punctuation:W-.:L-. 
P-determiner:W-The:L-the P-noun:W-boy:L-boy P-verb:W-thought:L-think P-preposition:W-that:L-that P-pronoun:W-it:L-it P-verb:W-must:L-must P-verb:W-be:L-be P-determiner:W-the:L-the P-noun:W-doorway:L-doorway P-particle:W-to:L-to P-noun:W-heaven:L-heaven P-punctuation:W-,:L-, P-conjunction:W-and:L-and P-preposition:W-that:L-that P-pronoun:W-it:L-it P-verb:W-would:L-would P-verb:W-be:L-be P-noun:W-fun:L-fun P-particle:W-to:L-to P-verb:W-see:L-see P-pronoun:W-what:L-what P-verb:W-was:L-be P-particle:W-up:L-up P-adverb:W-there:L-there P-punctuation:W-.:L-. 
P-adverb:W-So:L-so P-pronoun:W-he:L-he P-verb:W-started:L-start P-verb:W-building:L-build P-determiner:W-a:L-a P-adjective:W-great:L-great P-adjective:W-big:L-big P-noun:W-tower:L-tower P-preposition:W-of:L-of P-noun:W-wood:L-wood P-punctuation:W-,:L-, P-verb:W-reaching:L-reach P-particle:W-up:L-up P-particle:W-to:L-to P-determiner:W-the:L-the P-noun:W-clouds:L-cloud P-punctuation:W-.:L-. 
P-adverb:W-However:L-however P-punctuation:W-,:L-, P-adverb:W-when:L-when P-determiner:W-the:L-the P-noun:W-tower:L-tower P-verb:W-was:L-be P-adverb:W-very:L-very P-adjective:W-tall:L-tall P-punctuation:W-,:L-, P-pronoun:W-it:L-it P-verb:W-collapsed:L-collapse P-punctuation:W-.:L-. 
P-pronoun:W-He:L-he P-verb:W-tried:L-try P-verb:W-building:L-build P-pronoun:W-it:L-it P-preposition:W-with:L-with P-noun:W-adobe:L-adobe P-punctuation:W-,:L-, P-preposition:W-with:L-with P-noun:W-bricks:L-brick P-punctuation:W-,:L-, P-conjunction:W-and:L-and P-adverb:W-then:L-then P-preposition:W-with:L-with P-noun:W-steel:L-steel P-punctuation:W-,:L-, P-conjunction:W-but:L-but P-pronoun:W-his:L-his P-noun:W-tower:L-tower P-adverb:W-always:L-always P-verb:W-collapsed:L-collapse P-punctuation:W-.:L-. 
P-adverb:W-Just:L-just P-adverb:W-when:L-when P-pronoun:W-he:L-he P-verb:W-was:L-be P-adverb:W-about:L-about P-particle:W-to:L-to P-verb:W-give:L-give P-particle:W-up:L-up P-punctuation:W-,:L-, P-determiner:W-the:L-the P-noun:W-angel:L-angel P-verb:W-returned:L-return P-punctuation:W-,:L-, P-determiner:W-this:L-this P-noun:W-time:L-time P-verb:W-surrounded:L-surround P-preposition:W-by:L-by P-adjective:W-other:L-other P-noun:W-angels:L-angel P-punctuation:W-.:L-. 
P-determiner:W-The:L-the P-noun:W-angel:L-angel P-verb:W-sang:L-sing P-adverb:W-again:L-again P-punctuation:W-,:L-, P-conjunction:W-and:L-and P-determiner:W-the:L-the P-noun:W-boy:L-boy P-verb:W-listened:L-listen P-adverb:W-closely:L-closely P-punctuation:W-.:L-. 
P-determiner:W-The:L-the P-noun:W-message:L-message P-preposition:W-of:L-of P-determiner:W-the:L-the P-noun:W-song:L-song P-verb:W-was:L-be P-preposition:W-that:L-that P-number:W-one:L-1 P-verb:W-could:L-can P-adverb:W-only:L-only P-verb:W-reach:L-reach P-determiner:W-that:L-that P-adjective:W-heavenly:L-heavenly P-noun:W-place:L-place P-preposition:W-if:L-if P-pronoun:W-you:L-you P-adverb:W-really:L-really P-verb:W-wanted:L-want P-particle:W-to:L-to P-punctuation:W-,:L-, P-preposition:W-with:L-with P-determiner:W-all:L-all P-pronoun:W-your:L-your P-noun:W-heart:L-heart P-punctuation:W-.:L-. 
P-adverb:W-No_longer:L-no_longer P-verb:W-was:L-be P-determiner:W-the:L-the P-noun:W-boy:L-boy P-adverb:W-just:L-just P-adjective:W-curious:L-curious P-punctuation:W-,:L-, P-adverb:W-now:L-now P-pronoun:W-he:L-he P-verb:W-desired:L-desire P-preposition:W-with:L-with P-determiner:W-all:L-all P-pronoun:W-his:L-his P-noun:W-might:L-might P-particle:W-to:L-to P-verb:W-go:L-go P-particle:W-up:L-up P-conjunction:W-and:L-and P-verb:W-visit:L-visit P-noun:W-heaven:L-heaven P-preposition:W-with:L-with P-determiner:W-the:L-the P-noun:W-angels:L-angel P-punctuation:W-.:L-. 
P-conjunction:W-But:L-but P-pronoun:W-he:L-he P-adverb:W-just:L-just P-verb:W-could:L-can P-adverb:W-not:L-not P-verb:W-manage:L-manage P-pronoun:W-it:L-it P-punctuation:W-,:L-, P-conjunction:W-and:L-and P-verb:W-overcome:L-overcome P-preposition:W-by:L-by P-pronoun:W-his:L-his P-noun:W-sense:L-sense P-preposition:W-of:L-of P-noun:W-powerlessness:L-powerlessness P-conjunction:W-and:L-and P-noun:W-sorrow:L-sorrow P-punctuation:W-,:L-, P-pronoun:W-he:L-he P-verb:W-sat:L-sit P-particle:W-down:L-down P-conjunction:W-and:L-and P-verb:W-began:L-begin P-particle:W-to:L-to P-verb:W-cry:L-cry P-punctuation:W-.:L-. 
P-pronoun:W-He:L-he P-verb:W-cried:L-cry P-conjunction:W-and:L-and P-verb:W-cried:L-cry P-conjunction:W-and:L-and P-verb:W-cried:L-cry P-punctuation:W-,:L-, P-adverb:W-so:L-so P-adverb:W-much:L-much P-adverb:W-so:L-so P-punctuation:W-,:L-, P-preposition:W-that:L-that P-adverb:W-when:L-when P-determiner:W-the:L-the P-noun:W-sun:L-sun P-verb:W-appeared:L-appear P-preposition:W-from:L-from P-preposition:W-behind:L-behind P-determiner:W-the:L-the P-noun:W-clouds:L-cloud P-pronoun:W-it:L-it P-verb:W-created:L-create P-determiner:W-a:L-a P-adjective:W-magnificent:L-magnificent P-noun:W-rainbow:L-rainbow P-punctuation:W-.:L-. 
P-determiner:W-The:L-the P-noun:W-rainbow:L-rainbow P-verb:W-led:L-lead P-adverb:W-directly:L-directly P-adverb:W-up:L-up P-particle:W-to:L-to P-determiner:W-the:L-the P-noun:W-doors:L-door P-preposition:W-of:L-of P-noun:W-heaven:L-heaven P-punctuation:W-.:L-. 
P-determiner:W-The:L-the P-noun:W-boy:L-boy P-verb:W-traveled:L-travel P-particle:W-up:L-up P-determiner:W-the:L-the P-noun:W-rainbow:L-rainbow P-punctuation:W-,:L-, P-verb:W-filled:L-fill P-preposition:W-with:L-with P-noun:W-joy:L-joy P-punctuation:W-,:L-, P-conjunction:W-and:L-and P-verb:W-knowing:L-know P-preposition:W-that:L-that P-adverb:W-only:L-only P-preposition:W-with:L-with P-adjective:W-sincere:L-sincere P-punctuation:W-,:L-, P-adjective:W-heartfelt:L-heartfelt P-noun:W-desire:L-desire P-verb:W-could:L-can P-number:W-one:L-1 P-adverb:W-ever:L-ever P-adjective:W-open:L-open P-determiner:W-the:L-the P-noun:W-doors:L-door P-particle:W-to:L-to P-noun:W-heaven:L-heaven P-punctuation:W-.:L-. 
