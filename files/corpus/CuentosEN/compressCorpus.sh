#!/bin/bash

cd $1
mkdir $2
cd ..

for f in $1*
do
  if [ -f "$f" ]; then
	cd $1
	filename=$(basename "$f")
	fname="${filename%.*}"
	name=$fname.tar.gz
	direct="$2/$fname" 
  	tar -zcf $name $filename
	mkdir $direct
	mv $name $direct
	cd ..
	
  fi
done
