P-adjetivo:W-gordy:L-gordy P-verbo:W-is:L-be P-adverbio:W-not:L-not P-Z:W-a:L-1 P-nombre:W-movie:L-movie P-pronombre:W-it:L-it P-verbo:W-is:L-be P-Z:W-a:L-1 P-Z:W-90-minute-long:L-90-minute-long P-nombre:W-sesame:L-sesame P-nombre:W-street:L-street P-nombre:W-skit:L-skit P-conjuncion:W-and:L-and P-Z:W-a:L-1 P-adverbio:W-very:L-very P-adjetivo:W-bad:L-bad P-nombre:W-one:L-one P-preposicion:W-at:L-at P-determinante:W-that:L-that P-puntuacion:W-.:L-. 
P-determinante:W-this:L-this P-nombre:W-movie:L-movie P-verbo:W-is:L-be P-adverbio:W-so:L-so P-adjetivo:W-stupid:L-stupid P-conjuncion:W-and:L-and P-adjetivo:W-dumb:L-dumb P-preposicion:W-that:L-that P-pronombre:W-it:L-it P-verbo:W-'s:L-be P-verbo:W-depressing:L-depress P-to:W-to:L-to P-verbo:W-think:L-think P-preposicion:W-that:L-that P-determinante:W-some:L-some P-nombre:W-hollywood:L-hollywood P-nombre:W-executives:L-executive P-adverbio:W-actually:L-actually P-verbo:W-gave:L-give P-determinante:W-this:L-this P-determinante:W-the:L-the P-adjetivo:W-green:L-green P-nombre:W-light:L-light P-conjuncion:W-and:L-and P-adverbio:W-even:L-even P-adverbio:W-more:L-many P-verbo:W-surprising:L-surprise P-verbo:W-is:L-be P-determinante:W-the:L-the P-nombre:W-fact:L-fact P-preposicion:W-that:L-that P-determinante:W-this:L-this P-verbo:W-is:L-be P-Z:W-a:L-1 P-nombre:W-disney:L-disney P-nombre:W-movie:L-movie P-puntuacion:W-.:L-. 
P-pronombre:W-i:L-i P-verbo:W-am:L-be P-adjetivo:W-sure:L-sure P-nombre:W-children:L-child P-verbo:W-are:L-be P-determinante:W-the:L-the P-nombre:W-target:L-target P-nombre:W-audience:L-audience P-preposicion:W-of:L-of P-determinante:W-this:L-this P-nombre:W-movie:L-movie P-conjuncion:W-but:L-but P-adjetivo:W-only:L-only P-nombre:W-kids:L-kid P-preposicion:W-under:L-under P-determinante:W-the:L-the P-nombre:W-age:L-age P-preposicion:W-of:L-of P-wh_objeto:W-five_may:L-[??:5/5/??:??.??:??] P-verbo:W-be:L-be P-adjetivo:W-able:L-able P-to:W-to:L-to P-verbo:W-tolerate:L-tolerate P-pronombre:W-it:L-it P-puntuacion:W-.:L-. 
P-pronombre:W-it:L-it P-verbo:W-is:L-be P-determinante:W-the:L-the P-nombre:W-story:L-story P-preposicion:W-of:L-of P-Z:W-a:L-1 P-nombre:W-farm:L-farm P-Z:W-a:L-1 P-nombre:W-piglet:L-piglet P-verbo:W-named:L-name P-adjetivo:W-gordy:L-gordy P-verbo:W-voiced:L-voice P-preposicion:W-by:L-by P-nombre:W-garms:L-garms P-wh_objeto:W-whose:L-whose P-nombre:W-family:L-family P-verbo:W-has:L-have P-verbo:W-been:L-be P-verbo:W-taken:L-take P-adverbio:W-away:L-away P-to:W-to:L-to P-adverbio:W-up:L-up P-adverbio:W-north:L-north P-wh_objeto:W-which:L-which P-pronombre:W-we:L-we P-verbo:W-know:L-know P-verbo:W-means:L-mean P-nombre:W-death:L-death P-puntuacion:W-.:L-. 
P-preposicion:W-of:L-of P-nombre:W-course:L-course P-pronombre:W-we:L-we P-verbo:W-can:L-can P-verbo:W-hear:L-hear P-determinante:W-the:L-the P-nombre:W-animals:L-animal P-verbo:W-talk:L-talk P-to:W-to:L-to P-determinante:W-each:L-each P-nombre:W-other:L-other P-conjuncion:W-and:L-and P-pronombre:W-they:L-they P-adverbio:W-actually:L-actually P-verbo:W-went:L-go P-to:W-to:L-to P-determinante:W-the:L-the P-nombre:W-trouble:L-trouble P-preposicion:W-of:L-of P-verbo:W-attempting:L-attempt P-to:W-to:L-to P-verbo:W-sync:L-sync P-determinante:W-the:L-the P-nombre:W-voices:L-voice P-preposicion:W-with:L-with P-pronombre:W-their:L-their P-nombre:W-mouths:L-mouth P-conjuncion:W-but:L-but P-pronombre:W-it:L-it P-verbo:W-comes:L-come P-adverbio:W-out:L-out P-adjetivo:W-terrible:L-terrible P-puntuacion:W-.:L-. 
P-adverbio:W-actually:L-actually P-pronombre:W-it:L-it P-verbo:W-'s:L-be P-adverbio:W-almost:L-almost P-adjetivo:W-funny:L-funny P-preposicion:W-in:L-in P-Z:W-a:L-1 P-nombre:W-way:L-way P-puntuacion:W-.:L-. 
P-determinante:W-the:L-the P-adjetivo:W-only:L-only P-adverbio:W-remotely:L-remotely P-verbo:W-interesting:L-interest P-conjuncion:W-and:L-and P-adjetivo:W-likable:L-likable P-nombre:W-character:L-character P-adverbio:W-soon:L-soon P-verbo:W-appears:L-appear P-Z:W-a:L-1 P-adjetivo:W-little:L-little P-nombre:W-girl:L-girl P-verbo:W-named:L-name P-nombre:W-jinnie:L-jinnie P-verbo:W-sue:L-sue P-nombre:W-macallister:L-macallister P-adjetivo:W-young:L-young P-wh_objeto:W-who:L-who P-verbo:W-sees:L-see P-adjetivo:W-gordy:L-gordy P-preposicion:W-on:L-on P-determinante:W-the:L-the P-nombre:W-back:L-back P-preposicion:W-of:L-of P-Z:W-a:L-1 P-nombre:W-truck:L-truck P-conjuncion:W-and:L-and P-adverbio:W-essentially:L-essentially P-verbo:W-steals:L-steal P-pronombre:W-him:L-him P-puntuacion:W-.:L-. 
P-nombre:W-jinnie:L-jinnie P-verbo:W-is:L-be P-Z:W-a:L-1 P-nombre:W-country:L-country P-nombre:W-singer:L-singer P-conjuncion:W-and:L-and P-determinante:W-the:L-the P-nombre:W-film:L-film P-verbo:W-goes:L-go P-adverbio:W-off:L-off P-preposicion:W-on:L-on P-Z:W-a:L-1 P-adjetivo:W-huge:L-huge P-nombre:W-tangent:L-tangent P-to:W-to:L-to P-verbo:W-show:L-show P-pronombre:W-her:L-her P-adjetivo:W-little:L-little P-nombre:W-concert:L-concert P-conjuncion:W-and:L-and P-determinante:W-the:L-the P-nombre:W-people:L-people P-verbo:W-dancing:L-dance P-to:W-to:L-to P-pronombre:W-it:L-it P-puntuacion:W-.:L-. 
P-wh_objeto:W-what:L-what P-verbo:W-is:L-be P-determinante:W-the:L-the P-nombre:W-point:L-point P-preposicion:W-of:L-of P-determinante:W-this:L-this 
P-adverbio:W-maybe:L-maybe P-pronombre:W-she:L-she P-verbo:W-is:L-be P-nombre:W-one:L-one P-preposicion:W-of:L-of P-determinante:W-the:L-the P-nombre:W-producer:L-producer P-pronombre:W-'s:L-'s P-nombre:W-relatives:L-relative P-conjuncion:W-and:L-and P-pronombre:W-they:L-they P-verbo:W-wanted:L-want P-to:W-to:L-to P-verbo:W-show:L-show P-pronombre:W-her:L-her P-preposicion:W-on:L-on P-nombre:W-camera:L-camera P-to:W-to:L-to P-verbo:W-promote:L-promote P-pronombre:W-her:L-her P-conjuncion:W-or:L-or P-pronombre:W-something:L-something P-puntuacion:W-.:L-. 
P-pronombre:W-we:L-we P-adverbio:W-then:L-then P-verbo:W-cut:L-cut P-to:W-to:L-to P-Z:W-a:L-1 P-adjetivo:W-huge:L-huge P-adjetivo:W-social:L-social P-nombre:W-gathering:L-gathering P-conjuncion:W-and:L-and P-nombre:W-drop:L-drop P-preposicion:W-in:L-in P-preposicion:W-on:L-on P-determinante:W-another:L-another P-adjetivo:W-young:L-young P-nombre:W-kid:L-kid P-verbo:W-named:L-name P-verbo:W-hank:L-hank P-nombre:W-royce:L-royce P-nombre:W-roescher:L-roescher P-wh_objeto:W-who:L-who P-verbo:W-is:L-be P-adjetivo:W-sad:L-sad P-preposicion:W-because:L-because P-pronombre:W-his:L-his P-verbo:W-divorced:L-divorce P-nombre:W-mother:L-mother P-verbo:W-is:L-be P-verbo:W-dating:L-date P-puntuacion:W-.:L-. 
P-pronombre:W-he:L-he P-verbo:W-leaves:L-leave P-determinante:W-the:L-the P-nombre:W-party:L-party P-conjuncion:W-and:L-and P-verbo:W-meets:L-meet P-nombre:W-jinnie:L-jinnie P-verbo:W-sue:L-sue P-conjuncion:W-but:L-but P-pronombre:W-he:L-he P-adverbio:W-accidentally:L-accidentally P-verbo:W-falls:L-fall P-preposicion:W-in:L-in P-Z:W-a:L-1 P-nombre:W-pool:L-pool P-adverbio:W-probably:L-probably P-preposicion:W-because:L-because P-pronombre:W-he:L-he P-verbo:W-was:L-be P-verbo:W-sitting:L-sit P-preposicion:W-on:L-on P-determinante:W-the:L-the P-verbo:W-diving:L-dive P-nombre:W-board:L-board P-preposicion:W-with:L-with P-Z:W-a:L-1 P-Z:W-$200:L-$200 P-nombre:W-suit:L-suit P-preposicion:W-on:L-on P-nombre:W-nah:L-nah P-verbo:W-did:L-do P-adverbio:W-not:L-not P-verbo:W-see:L-see P-preposicion:W-that:L-that P-determinante:W-one:L-one P-nombre:W-coming:L-coming P-verbo:W-starts:L-start P-to:W-to:L-to P-verbo:W-drown:L-drown P-conjuncion:W-and:L-and P-verbo:W-is:L-be P-adverbio:W-miraculously:L-miraculously P-verbo:W-saved:L-save P-preposicion:W-as:L-as P-nombre:W-gordy:L-gordy P-verbo:W-pushes:L-push P-Z:W-an:L-1 P-nombre:W-inflatable:L-inflatable P-verbo:W-float:L-float P-adverbio:W-over:L-over P-to:W-to:L-to P-pronombre:W-him:L-him P-conjuncion:W-and:L-and P-verbo:W-saves:L-save P-pronombre:W-him:L-him P-puntuacion:W-.:L-. 
P-preposicion:W-if:L-if P-determinante:W-this:L-this P-verbo:W-had:L-have P-adverbio:W-not:L-not P-verbo:W-been:L-be P-adverbio:W-insanely:L-insanely P-adjetivo:W-stupid:L-stupid P-adverbio:W-already:L-already P-determinante:W-the:L-the P-nombre:W-story:L-story P-adverbio:W-quickly:L-quickly P-nombre:W-changes:L-change P-wh_objeto:W-when:L-when P-nombre:W-jinnie:L-jinnie P-verbo:W-gives:L-give P-adjetivo:W-gordy:L-gordy P-to:W-to:L-to P-verbo:W-hank:L-hank P-wh_objeto:W-who:L-who P-adverbio:W-then:L-then P-verbo:W-ends:L-end P-adverbio:W-up:L-up P-verbo:W-becoming:L-become P-determinante:W-the:L-the P-nombre:W-ceo:L-ceo P-preposicion:W-of:L-of P-Z:W-a:L-1 P-nombre:W-food:L-food P-nombre:W-processing:L-processing P-nombre:W-corporation:L-corporation P-wh_objeto:W-when:L-when P-verbo:W-hank:L-hank P-pronombre:W-'s:L-'s P-nombre:W-grandfather:L-grandfather P-determinante:W-the:L-the P-adjetivo:W-original:L-original P-nombre:W-ceo:L-ceo P-verbo:W-dies:L-die P-conjuncion:W-and:L-and P-verbo:W-leaves:L-leave P-pronombre:W-his:L-his P-nombre:W-fortune:L-fortune P-to:W-to:L-to P-verbo:W-hank:L-hank P-puntuacion:W-.:L-. P-puntuacion:W-.:L-. P-puntuacion:W-.:L-. 
P-conjuncion:W-and:L-and P-adjetivo:W-gordy:L-gordy 
P-preposicion:W-of:L-of P-nombre:W-course:L-course P-exthere:W-there:L-there P-verbo:W-must:L-must P-verbo:W-be:L-be P-Z:W-a:L-1 P-nombre:W-villain:L-villain P-conjuncion:W-but:L-but P-adverbio:W-even:L-even P-determinante:W-this:L-this P-nombre:W-villain:L-villain P-nombre:W-donadio:L-donadio P-preposicion:W-as:L-as P-nombre:W-sipes:L-sipes P-verbo:W-is:L-be P-adverbio:W-not:L-not P-preposicion:W-that:L-that P-adjetivo:W-evil:L-evil P-puntuacion:W-.:L-. 
P-pronombre:W-he:L-he P-adverbio:W-never:L-never P-verbo:W-raises:L-raise P-pronombre:W-his:L-his P-nombre:W-voice:L-voice P-conjuncion:W-or:L-or P-verbo:W-becomes:L-become P-adjetivo:W-angry:L-angry P-conjuncion:W-and:L-and P-preposicion:W-of:L-of P-nombre:W-course:L-course P-pronombre:W-he:L-he P-verbo:W-has:L-have P-determinante:W-the:L-the P-adjetivo:W-typical:L-typical P-nombre:W-idiot:L-idiot P-nombre:W-goons:L-goon P-verbo:W-kidnap:L-kidnap P-adjetivo:W-gordy:L-gordy P-conjuncion:W-but:L-but P-determinante:W-this:L-this P-verbo:W-is:L-be P-adverbio:W-just:L-just P-adverbio:W-so:L-so P-preposicion:W-beyond:L-beyond P-adjetivo:W-stupid:L-stupid P-conjuncion:W-and:L-and P-adjetivo:W-cartoony:L-cartoony P-pronombre:W-we:L-we P-verbo:W-are:L-be P-adverbio:W-constantly:L-constantly P-Z:W-two:L-2 P-nombre:W-steps:L-step P-adverbio:W-ahead:L-ahead P-preposicion:W-of:L-of P-determinante:W-the:L-the P-nombre:W-story:L-story P-puntuacion:W-.:L-. 
P-pronombre:W-it:L-it P-verbo:W-'s:L-be P-adjetivo:W-hard:L-hard P-to:W-to:L-to P-verbo:W-tell:L-tell P-preposicion:W-whether:L-whether P-determinante:W-the:L-the P-adjetivo:W-overall:L-overall P-nombre:W-corniness:L-corniness P-conjuncion:W-and:L-and P-nombre:W-cheesiness:L-cheesiness P-to:W-to:L-to P-determinante:W-the:L-the P-nombre:W-movie:L-movie P-verbo:W-is:L-be P-adjetivo:W-intentional:L-intentional P-preposicion:W-because:L-because P-pronombre:W-it:L-it P-verbo:W-is:L-be P-Z:W-a:L-1 P-nombre:W-family:L-family P-nombre:W-film:L-film P-conjuncion:W-or:L-or P-preposicion:W-if:L-if P-determinante:W-the:L-the P-nombre:W-filmmaker:L-filmmaker P-pronombre:W-'s:L-'s P-verbo:W-are:L-be P-adverbio:W-just:L-just P-determinante:W-this:L-this P-adjetivo:W-untalented:L-untalented P-conjuncion:W-and:L-and P-adjetivo:W-stupid:L-stupid P-puntuacion:W-.:L-. 
P-preposicion:W-at:L-at P-nombre:W-times:L-time P-nombre:W-gordy:L-gordy P-verbo:W-is:L-be P-adjetivo:W-tolerable:L-tolerable P-to:W-to:L-to P-verbo:W-watch:L-watch P-adverbio:W-thus:L-thus P-verbo:W-earning:L-earn P-pronombre:W-it:L-it P-determinante:W-one:L-one P-nombre:W-star:L-star P-conjuncion:W-and:L-and P-adverbio:W-not:L-not P-determinante:W-the:L-the P-verbo:W-dreaded:L-dread P-nombre:W-z:L-z P-puntuacion:W-.:L-. P-conjuncion:W-but:L-but P-pronombre:W-it:L-it P-verbo:W-'s:L-be P-adverbio:W-just:L-just P-adverbio:W-so:L-so P-adverbio:W-unbelievably:L-unbelievably P-verbo:W-boring:L-bore P-nombre:W-cliche:L-cliche P-adjetivo:W-dumb:L-dumb P-adjetivo:W-unfunny:L-unfunny P-adjetivo:W-corny:L-corny P-conjuncion:W-and:L-and P-adverbio:W-just:L-just P-adjetivo:W-plain:L-plain P-adjetivo:W-bad:L-bad P-pronombre:W-it:L-it P-verbo:W-may:L-may P-verbo:W-scare:L-scare P-nombre:W-children:L-child P-pronombre:W-it:L-it P-adverbio:W-certainly:L-certainly P-verbo:W-disturbed:L-disturb P-pronombre:W-me:L-me P-puntuacion:W-.:L-. 
P-Z:W-4/21/96:L-4/21/96 P-Z:W-1/29/97:L-1/29/97 P-wh_objeto:W-6/13/97:L-[??:6/13/1997:??.??:??] P-verbo:W-see:L-see P-adverbio:W-also:L-also P-nombre:W-babe:L-babe 
