P-adverb:W-gere:L-gere:S-neutral P-punctuation:W-,:L-,:S-neutral P-verb:W-willis:L-willis:S-neutral P-punctuation:W-,:L-,:S-neutral P-adjective:W-poitier:L-poitier:S-neutral P-noun:W-chase:L-chase:S-neutral P-determiner:W-each:L-each:S-neutral P-noun:W-other:L-other:S-neutral P-preposition:W-around:L-around:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-world:L-world:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-film:L-film:S-neutral P-noun:W-review:L-review:S-neutral P-preposition:W-by:L-by:S-neutral P-noun:W-michael:L-michael:S-neutral P-noun:W-redman:L-redman:S-neutral P-noun:W-copyright:L-copyright:S-neutral P-number:W-1997:L-1997:S-neutral P-preposition:W-by:L-by:S-neutral P-noun:W-michael:L-michael:S-neutral P-noun:W-redman:L-redman:S-neutral P-adverb:W-when:L-when:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-soviet:L-soviet:S-neutral P-noun:W-union:L-union:S-neutral P-verb:W-imploded:L-implode:S-neutral P-punctuation:W-,:L-,:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-western:L-western:S-neutral P-noun:W-countries:L-country:S-neutral P-verb:W-lost:L-lose:S-neutral P-pronoun:W-their:L-their:S-neutral P-noun:W-shadow:L-shadow:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-preposition:W-with:L-with:S-neutral P-determiner:W-the:L-the:S-neutral P-verb:W-united:L-unite:S-neutral P-noun:W-states:L-state:S-neutral P-adjective:W-friendly:L-friendly:S-neutral P-preposition:W-with:L-with:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-russians:L-russians:S-neutral P-punctuation:W-,:L-,:S-neutral P-pronoun:W-we:L-we:S-neutral P-adverb:W-no_longer:L-no_longer:S-neutral P-verb:W-had:L-have:S-neutral P-number:W-an:L-1:S-neutral P-noun:W-entity:L-entity:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-blame:L-blame:S-neutral P-preposition:W-for:L-for:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-world:L-world:S-neutral P-adposition:W-'s:L-'s:S-neutral P-noun:W-problems:L-problem:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-determiner:W-this:L-this:S-neutral P-verb:W-showed:L-show:S-neutral P-particle:W-up:L-up:S-neutral P-preposition:W-in:L-in:S-positive P-noun:W-hollywood:L-hollywood:S-neutral P-noun:W-films:L-film:S-neutral P-preposition:W-as:L-as:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-communist:L-communist:S-neutral P-noun:W-government:L-government:S-neutral P-verb:W-was:L-be:S-neutral P-adverb:W-no_longer:L-no_longer:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-easy:L-easy:S-neutral P-adjective:W-bad:L-bad:S-neutral P-noun:W-guy:L-guy:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-pronoun:W-it:L-it:S-neutral P-verb:W-'s:L-be:S-neutral P-noun:W-time:L-time:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-rejoice:L-rejoice:S-neutral P-preposition:W-because:L-because:S-neutral P-pronoun:W-we:L-we:S-neutral P-verb:W-have:L-have:S-neutral P-verb:W-found:L-find:S-neutral P-pronoun:W-our:L-our:S-neutral P-adjective:W-new:L-new:S-neutral P-noun:W-villain:L-villain:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-adverb:W-now:L-now:S-neutral P-pronoun:W-it:L-it:S-neutral P-verb:W-'s:L-be:S-neutral P-adverb:W-no_longer:L-no_longer:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-russian:L-russian:S-neutral P-noun:W-government:L-government:S-neutral P-pronoun:W-who:L-who:S-neutral P-verb:W-sends:L-send:S-neutral P-noun:W-killers:L-killer:S-neutral P-preposition:W-out:L-out:S-negative P-preposition:W-into:L-into:S-neutral P-adjective:W-foreign:L-foreign:S-neutral P-noun:W-lands:L-land:S-neutral P-punctuation:W-,:L-,:S-neutral P-pronoun:W-it:L-it:S-neutral P-verb:W-'s:L-be:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-russian:L-russian:S-neutral P-punctuation:W-_:L-_:S-neutral P-noun:W-mafia:L-mafia:S-neutral P-punctuation:W-_:L-_:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-number:W-a:L-1:S-neutral P-adjective:W-perfect:L-perfect:S-neutral P-noun:W-solution:L-solution:S-neutral P-punctuation:W-,:L-,:S-neutral P-pronoun:W-it:L-it:S-neutral P-verb:W-combines:L-combine:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-dread:L-dread:S-neutral P-preposition:W-of:L-of:S-neutral P-verb:W-organized:L-organize:S-neutral P-noun:W-crime:L-crime:S-neutral P-conjunction:W-and:L-and:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-still_present:L-still_present:S-neutral P-noun:W-uneasiness:L-uneasiness:S-neutral P-preposition:W-with:L-with:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-former:L-former:S-neutral P-adjective:W-eastern:L-eastern:S-neutral P-noun:W-block:L-block:S-neutral P-noun:W-countries:L-country:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-adjective:W-best:L-good:S-neutral P-preposition:W-of:L-of:S-neutral P-determiner:W-all:L-all:S-neutral P-punctuation:W-,:L-,:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-villains:L-villain:S-neutral P-verb:W-are:L-be:S-neutral P-adverb:W-still:L-still:S-neutral P-noun:W-foreigners:L-foreigner:S-neutral P-punctuation:W-::L-::S-neutral P-noun:W-fear:L-fear:S-neutral P-preposition:W-of:L-of:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-other:L-other:S-neutral P-adverb:W-always:L-always:S-neutral P-verb:W-plays:L-play:S-neutral P-adjective:W-best:L-good:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-adverb:W-so:L-so:S-neutral P-pronoun:W-it:L-it:S-neutral P-verb:W-is:L-be:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-crime:L-crime:S-neutral P-noun:W-lord:L-lord:S-neutral P-preposition:W-in:L-in:S-positive P-noun:W-moscow:L-moscow:S-neutral P-determiner:W-that:L-that:S-neutral P-verb:W-sends:L-send:S-neutral P-adjective:W-legendary:L-legendary:S-neutral P-noun:W-hitman:L-hitman:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-punctuation:W-(:L-(:S-neutral P-noun:W-bruce:L-bruce:S-neutral P-verb:W-willis:L-willis:S-neutral P-punctuation:W-):L-):S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-assassinate:L-assassinate:S-neutral P-number:W-a:L-1:S-neutral P-adverb:W-highly:L-highly:S-neutral P-verb:W-placed:L-place:S-neutral P-pronoun:W-us:L-us:S-neutral P-noun:W-government:L-government:S-neutral P-noun:W-figure:L-figure:S-neutral P-preposition:W-in:L-in:S-positive P-noun:W-retaliation:L-retaliation:S-neutral P-preposition:W-for:L-for:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-death:L-death:S-neutral P-preposition:W-of:L-of:S-neutral P-pronoun:W-his:L-his:S-neutral P-noun:W-brother:L-brother:S-neutral P-preposition:W-during:L-during:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-nightclub:L-nightclub:S-neutral P-noun:W-raid:L-raid:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-determiner:W-the:L-the:S-neutral P-noun:W-fbi:L-fbi:S-neutral P-verb:W-is:L-be:S-neutral P-preposition:W-at:L-at:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-loss:L-loss:S-neutral P-preposition:W-as:L-as:S-neutral P-particle:W-to:L-to:S-neutral P-adverb:W-how:L-how:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-protect:L-protect:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-target:L-target:S-neutral P-preposition:W-from:L-from:S-neutral P-noun:W-someone:L-someone:S-neutral P-pronoun:W-they:L-they:S-neutral P-verb:W-are:L-be:S-neutral P-adverb:W-not:L-not:S-neutral P-adverb:W-sure:L-sure:S-neutral P-adverb:W-even:L-even:S-neutral P-verb:W-exists:L-exist:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-verb:W-coming:L-come:S-neutral P-particle:W-to:L-to:S-neutral P-pronoun:W-their:L-their:S-neutral P-noun:W-rescue:L-rescue:S-neutral P-verb:W-is:L-be:S-neutral P-adjective:W-former:L-former:S-neutral P-noun:W-ira:L-ira:S-neutral P-noun:W-operative:L-operative:S-neutral P-noun:W-declan:L-declan:S-neutral P-noun:W-mulqueen:L-mulqueen:S-neutral P-punctuation:W-(:L-(:S-neutral P-adverb:W-richard:L-richard:S-neutral P-verb:W-gere:L-gere:S-neutral P-punctuation:W-):L-):S-neutral P-pronoun:W-who:L-who:S-neutral P-verb:W-is:L-be:S-neutral P-adverb:W-temporarily:L-temporarily:S-neutral P-verb:W-released:L-release:S-neutral P-preposition:W-from:L-from:S-neutral P-noun:W-prison:L-prison:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-assist:L-assist:S-neutral P-adjective:W-fbi:L-fbi:S-neutral P-noun:W-agent:L-agent:S-neutral P-noun:W-carter:L-carter:S-neutral P-noun:W-preston:L-preston:S-neutral P-punctuation:W-(:L-(:S-neutral P-noun:W-sidney:L-sidney:S-neutral P-noun:W-poitier:L-poitier:S-neutral P-punctuation:W-):L-):S-neutral P-conjunction:W-and:L-and:S-neutral P-adjective:W-russian:L-russian:S-neutral P-adjective:W-major:L-major:S-neutral P-noun:W-valentina:L-valentina:S-neutral P-noun:W-koslova:L-koslova:S-neutral P-punctuation:W-(:L-(:S-neutral P-noun:W-diane:L-diane:S-neutral P-noun:W-venora:L-venora:S-neutral P-punctuation:W-):L-):S-neutral P-punctuation:W-.:L-.:S-neutral 
P-noun:W-mulqueen:L-mulqueen:S-neutral P-adposition:W-'s:L-'s:S-neutral P-noun:W-ex_girlfriend:L-ex_girlfriend:S-neutral P-adjective:W-basque:L-basque:S-neutral P-adjective:W-terrorist:L-terrorist:S-neutral P-noun:W-isabella:L-isabella:S-neutral P-punctuation:W-(:L-(:S-neutral P-noun:W-mathilda:L-mathilda:S-neutral P-verb:W-may:L-may:S-neutral P-punctuation:W-):L-):S-neutral P-verb:W-is:L-be:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-only:L-only:S-neutral P-noun:W-person:L-person:S-neutral P-pronoun:W-who:L-who:S-neutral P-verb:W-has:L-have:S-neutral P-verb:W-seen:L-see:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-elusive:L-elusive:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-punctuation:W-(:L-(:S-neutral P-adverb:W-presumably:L-presumably:S-neutral P-pronoun:W-there:L-there:S-neutral P-verb:W-is:L-be:S-neutral P-number:W-an:L-1:S-neutral P-adjective:W-exclusive:L-exclusive:S-neutral P-adjective:W-international:L-international:S-neutral P-adjective:W-terrorist:L-terrorist:S-neutral P-noun:W-club:L-club:S-neutral P-adverb:W-somewhere:L-somewhere:S-neutral P-adverb:W-where:L-where:S-neutral P-determiner:W-the:L-the:S-neutral P-number:W-three:L-3:S-neutral P-verb:W-met:L-meet:S-neutral P-punctuation:W-.:L-.:S-neutral P-punctuation:W-):L-):S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-film:L-film:S-neutral P-verb:W-follows:L-follow:S-neutral P-number:W-two:L-2:S-neutral P-adjective:W-parallel:L-parallel:S-neutral P-noun:W-tracks:L-track:S-neutral P-preposition:W-as:L-as:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-verb:W-prepares:L-prepare:S-neutral P-preposition:W-for:L-for:S-neutral P-pronoun:W-his:L-his:S-neutral P-number:W-$70:L-$70:S-neutral P-noun:W-million:L-million:S-neutral P-noun:W-hit:L-hit:S-neutral P-conjunction:W-and:L-and:S-neutral P-noun:W-mulqueen:L-mulqueen:S-neutral P-noun:W-attempts:L-attempt:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-locate:L-locate:S-neutral P-pronoun:W-him:L-him:S-neutral P-preposition:W-while:L-while:S-neutral P-noun:W-preston:L-preston:S-neutral P-verb:W-makes:L-make:S-neutral P-adjective:W-sure:L-sure:S-neutral P-preposition:W-that:L-that:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-irishman:L-irishman:S-neutral P-verb:W-does:L-do:S-neutral P-adverb:W-not:L-not:S-neutral P-verb:W-slip:L-slip:S-neutral P-particle:W-away:L-away:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-verb:W-crossing:L-cross:S-neutral P-adjective:W-numerous:L-numerous:S-neutral P-noun:W-borders:L-border:S-neutral P-conjunction:W-and:L-and:S-neutral P-verb:W-donning:L-don:S-neutral P-adjective:W-various:L-various:S-neutral P-noun:W-disguises:L-disguise:S-neutral P-preposition:W-for:L-for:S-neutral P-determiner:W-both:L-both:S-neutral P-pronoun:W-himself:L-himself:S-neutral P-conjunction:W-and:L-and:S-neutral P-pronoun:W-his:L-his:S-neutral P-noun:W-mini_van:L-mini_van:S-neutral P-punctuation:W-,:L-,:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-killer:L-killer:S-neutral P-verb:W-is:L-be:S-neutral P-adverb:W-always:L-always:S-neutral P-number:W-one:L-1:S-neutral P-noun:W-step:L-step:S-neutral P-adverb:W-ahead:L-ahead:S-neutral P-preposition:W-of:L-of:S-neutral P-pronoun:W-his:L-his:S-neutral P-noun:W-pursuers:L-pursuer:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-verb:W-being:L-be:S-neutral P-adverb:W-very:L-very:S-neutral P-adverb:W-loosely:L-loosely:S-neutral P-verb:W-based:L-base:S-neutral P-preposition:W-on:L-on:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-same:L-same:S-neutral P-noun:W-book:L-book:S-neutral P-determiner:W-the:L-the:S-neutral P-number:W-1973:L-1973:S-neutral P-noun:W-thriller:L-thriller:S-negative P-punctuation:W-":L-":S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-day:L-day:S-neutral P-preposition:W-of:L-of:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-punctuation:W-":L-":S-neutral P-punctuation:W-,:L-,:S-neutral P-noun:W-comparison:L-comparison:S-neutral P-preposition:W-between:L-between:S-neutral P-determiner:W-the:L-the:S-neutral P-number:W-two:L-2:S-neutral P-noun:W-films:L-film:S-neutral P-verb:W-is:L-be:S-neutral P-adjective:W-inevitable:L-inevitable:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-pronoun:W-there:L-there:S-neutral P-verb:W-is:L-be:S-neutral P-adverb:W-no_doubt:L-no_doubt:S-neutral P-preposition:W-that:L-that:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-original:L-original:S-neutral P-verb:W-is:L-be:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-better:L-good:S-neutral P-noun:W-movie:L-movie:S-neutral P-punctuation:W-,:L-,:S-neutral P-verb:W-playing:L-play:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-story:L-story:S-neutral P-preposition:W-for:L-for:S-neutral P-noun:W-suspense:L-suspense:S-neutral P-adverb:W-rather:L-rather:S-neutral P-preposition:W-than:L-than:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-current:L-current:S-neutral P-noun:W-action:L-action:S-neutral P-punctuation:W-/:L-/:S-neutral P-noun:W-adventure:L-adventure:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-preposition:W-as:L-as:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-mystery:L-mystery:S-neutral P-punctuation:W-,:L-,:S-neutral P-punctuation:W-":L-":S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-punctuation:W-":L-":S-neutral P-verb:W-has:L-have:S-neutral P-adjective:W-enough:L-enough:S-neutral P-noun:W-holes:L-hole:S-neutral P-preposition:W-in:L-in:S-positive P-pronoun:W-it:L-it:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-ruin:L-ruin:S-negative P-determiner:W-the:L-the:S-neutral P-noun:W-tale:L-tale:S-neutral P-punctuation:W-,:L-,:S-neutral P-conjunction:W-but:L-but:S-neutral P-preposition:W-if:L-if:S-neutral P-pronoun:W-you:L-you:S-neutral P-verb:W-can:L-can:S-neutral P-verb:W-accept:L-accept:S-neutral P-pronoun:W-it:L-it:S-neutral P-preposition:W-for:L-for:S-neutral P-pronoun:W-what:L-what:S-neutral P-pronoun:W-it:L-it:S-neutral P-verb:W-is:L-be:S-neutral P-punctuation:W-,:L-,:S-neutral P-adverb:W-there:L-there:S-neutral P-verb:W-is:L-be:S-neutral P-noun:W-entertainment:L-entertainment:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-be:L-be:S-neutral P-verb:W-had:L-have:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-noun:W-holes:L-hole:S-neutral P-punctuation:W-?:L-?:S-neutral P-verb:W-let:L-let:S-neutral P-pronoun:W-us:L-us:S-neutral P-verb:W-see:L-see:S-neutral P-punctuation:W-?:L-?:S-neutral P-number:W-a:L-1:S-neutral P-adjective:W-pivotal:L-pivotal:S-neutral P-noun:W-clue:L-clue:S-neutral P-preposition:W-for:L-for:S-neutral P-noun:W-mulqueen:L-mulqueen:S-neutral P-verb:W-is:L-be:S-neutral P-adverb:W-so:L-so:S-neutral P-adjective:W-obscure:L-obscure:S-neutral P-preposition:W-that:L-that:S-neutral P-pronoun:W-he:L-he:S-neutral P-verb:W-must:L-must:S-neutral P-verb:W-possess:L-possess:S-neutral P-adjective:W-psychic:L-psychic:S-neutral P-noun:W-powers:L-power:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-pick:L-pick:S-neutral P-pronoun:W-it:L-it:S-neutral P-particle:W-up:L-up:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-preposition:W-for:L-for:S-neutral P-number:W-a:L-1:S-neutral P-number:W-20_year:L-20_year:S-neutral P-noun:W-veteran:L-veteran:S-neutral P-determiner:W-that:L-that:S-neutral P-verb:W-can:L-can:S-neutral P-verb:W-command:L-command:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-big:L-big:S-neutral P-noun:W-bucks:L-buck:S-neutral P-punctuation:W-,:L-,:S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-jackal:L-jackal:S-neutral P-verb:W-is:L-be:S-neutral P-number:W-an:L-1:S-neutral P-adverb:W-incredibly:L-incredibly:S-neutral P-adjective:W-poor:L-poor:S-neutral P-noun:W-shot:L-shot:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-determiner:W-the:L-the:S-neutral P-adjective:W-final:L-final:S-neutral P-noun:W-scene:L-scene:S-neutral P-preposition:W-between:L-between:S-neutral P-adverb:W-gere:L-gere:S-neutral P-conjunction:W-and:L-and:S-neutral P-verb:W-willis:L-willis:S-neutral P-verb:W-occurs:L-occur:S-neutral P-preposition:W-in:L-in:S-positive P-number:W-a:L-1:S-neutral P-noun:W-location:L-location:S-neutral P-determiner:W-that:L-that:S-neutral P-verb:W-should:L-should:S-neutral P-verb:W-be:L-be:S-neutral P-verb:W-mobbed:L-mob:S-neutral P-preposition:W-with:L-with:S-neutral P-noun:W-police:L-police:S-neutral P-punctuation:W-,:L-,:S-neutral P-conjunction:W-but:L-but:S-neutral P-pronoun:W-it:L-it:S-neutral P-verb:W-'s:L-be:S-neutral P-adverb:W-just:L-just:S-neutral P-determiner:W-the:L-the:S-neutral P-number:W-two:L-2:S-neutral P-preposition:W-of:L-of:S-neutral P-pronoun:W-them:L-them:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-verb:W-willis:L-willis:S-neutral P-punctuation:W-':L-':S-neutral P-noun:W-disguises:L-disguise:S-neutral P-adverb:W-usually:L-usually:S-neutral P-verb:W-look:L-look:S-neutral P-preposition:W-like:L-like:S-neutral P-noun:W-bruce:L-bruce:S-neutral P-verb:W-willis:L-willis:S-neutral P-conjunction:W-and:L-and:S-neutral P-verb:W-are:L-be:S-neutral P-adverb:W-just:L-just:S-neutral P-preposition:W-as:L-as:S-neutral P-verb:W-interesting:L-interest:S-neutral P-preposition:W-as:L-as:S-neutral P-noun:W-val:L-val:S-neutral P-noun:W-kilmer:L-kilmer:S-neutral P-adposition:W-'s:L-'s:S-neutral P-preposition:W-in:L-in:S-positive P-punctuation:W-":L-":S-neutral P-determiner:W-the:L-the:S-neutral P-noun:W-saint:L-saint:S-neutral P-punctuation:W-":L-":S-neutral P-punctuation:W-.:L-.:S-neutral 
P-punctuation:W-(:L-(:S-neutral P-conjunction:W-and:L-and:S-neutral P-preposition:W-lest:L-lest:S-neutral P-pronoun:W-you:L-you:S-neutral P-verb:W-misunderstand:L-misunderstand:S-neutral P-punctuation:W-,:L-,:S-neutral P-pronoun:W-that:L-that:S-neutral P-verb:W-'s:L-be:S-neutral P-adverb:W-not:L-not:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-compliment:L-compliment:S-neutral P-punctuation:W-.:L-.:S-neutral P-punctuation:W-):L-):S-neutral P-conjunction:W-but:L-but:S-neutral P-determiner:W-the:L-the:S-neutral P-number:W-three:L-3:S-neutral P-noun:W-stars:L-star:S-neutral P-verb:W-are:L-be:S-neutral P-noun:W-fun:L-fun:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-watch:L-watch:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-pronoun:W-it:L-it:S-neutral P-verb:W-'s:L-be:S-neutral P-adjective:W-good:L-good:S-neutral P-particle:W-to:L-to:S-neutral P-verb:W-see:L-see:S-neutral P-adverb:W-gere:L-gere:S-neutral P-preposition:W-in:L-in:S-positive P-pronoun:W-something:L-something:S-neutral P-adjective:W-other:L-other:S-neutral P-preposition:W-than:L-than:S-neutral P-number:W-a:L-1:S-neutral P-noun:W-business:L-business:S-neutral P-noun:W-suit:L-suit:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-noun:W-willis:L-willis:S-neutral P-verb:W-has:L-have:S-neutral P-number:W-a:L-1:S-neutral P-verb:W-mixed:L-mix:S-neutral P-noun:W-history:L-history:S-neutral P-preposition:W-in:L-in:S-positive P-verb:W-picking:L-pick:S-neutral P-noun:W-projects:L-project:S-neutral P-punctuation:W-,:L-,:S-neutral P-conjunction:W-but:L-but:S-neutral P-pronoun:W-his:L-his:S-neutral P-noun:W-characters:L-character:S-neutral P-verb:W-are:L-be:S-neutral P-adverb:W-always:L-always:S-neutral P-adjective:W-watchable:L-watchable:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-noun:W-poitier:L-poitier:S-neutral P-verb:W-is:L-be:S-neutral P-preposition:W-by:L-by:S-neutral P-adverb:W-far:L-far:S-neutral P-determiner:W-the:L-the:S-neutral P-adjective:W-superior:L-superior:S-positive P-noun:W-actor:L-actor:S-neutral P-punctuation:W-,:L-,:S-neutral P-conjunction:W-but:L-but:S-neutral P-verb:W-has:L-have:S-neutral P-verb:W-limited:L-limit:S-neutral P-noun:W-screen:L-screen:S-neutral P-noun:W-time:L-time:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-determiner:W-the:L-the:S-neutral P-noun:W-problems:L-problem:S-neutral P-preposition:W-in:L-in:S-positive P-noun:W-logic:L-logic:S-neutral P-verb:W-are:L-be:S-neutral P-noun:W-flaws:L-flaw:S-neutral P-punctuation:W-,:L-,:S-neutral P-conjunction:W-but:L-but:S-neutral P-verb:W-do:L-do:S-neutral P-adverb:W-not:L-not:S-neutral P-verb:W-ruin:L-ruin:S-negative P-determiner:W-the:L-the:S-neutral P-noun:W-experience:L-experience:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-adverb:W-occasionally:L-occasionally:S-neutral P-pronoun:W-there:L-there:S-neutral P-verb:W-are:L-be:S-neutral P-noun:W-movies:L-movie:S-neutral P-determiner:W-that:L-that:S-neutral P-verb:W-transcend:L-transcend:S-neutral P-pronoun:W-their:L-their:S-neutral P-noun:W-blemishes:L-blemish:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-determiner:W-this:L-this:S-neutral P-verb:W-is:L-be:S-neutral P-noun:W-one:L-one:S-neutral P-preposition:W-of:L-of:S-neutral P-pronoun:W-them:L-them:S-neutral P-punctuation:W-.:L-.:S-neutral 
P-punctuation:W-[:L-[:S-neutral P-determiner:W-the:L-the:S-neutral P-verb:W-appeared:L-appear:S-neutral P-preposition:W-in:L-in:S-positive P-determiner:W-the:L-the:S-neutral P-number:W-11/20/97:L-11/20/97:S-neutral P-punctuation:W-":L-":S-neutral P-noun:W-bloomington:L-bloomington:S-neutral P-noun:W-voice:L-voice:S-neutral P-punctuation:W-":L-":S-neutral P-punctuation:W-,:L-,:S-neutral P-noun:W-bloomington:L-bloomington:S-neutral P-punctuation:W-,:L-,:S-neutral P-noun:W-indiana:L-indiana:S-neutral P-punctuation:W-]:L-]:S-neutral 
