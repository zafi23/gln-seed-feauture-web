#!/bin/bash


mkdir $2


for f in $1*
do
  if [ -f "$f" ]; then
	cd $1
	filename=$(basename "$f")
	fname="${filename%.*}"
	name=$fname.tar.gz 
  	tar -zcf $name $filename
	mv $name $2
	cd ..
	
  fi
done
