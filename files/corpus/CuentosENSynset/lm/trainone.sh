#Portatil
export PATH=$PATH:/home/cristina/Escritorio/Software/srilm-1.7.1/bin/i686-m64://home/cristina/Escritorio/Software/srilm-1.7.1/bin

#PC Lab
#export PATH=$PATH:/home/cristina/Escritorio/SOFTWARE/SRILM/bin/i686-m64://home/cristina/Escritorio/SOFTWARE/SRILM/bin

cd "entrenamiento2gram"

# Train lms
echo "training PP1...."
fngram-count -factor-file 2gramCuentosPP1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training PL1...."
fngram-count -factor-file 2gramCuentosPL1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training LP1...."
fngram-count -factor-file 2gramCuentosLP1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training LL1...."
fngram-count -factor-file 2gramCuentosLL1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training PS1...."
fngram-count -factor-file 2gramCuentosPS1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training SP1...."
fngram-count -factor-file 2gramCuentosSP1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training SS1...."
fngram-count -factor-file 2gramCuentosSS1.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training WW1...."
fngram-count -factor-file 2gramCuentosWW1.flm -text corpus_lem.tar.gz -lm -tolower -unk

7z x 2gramLL1.lm.gz
7z x 2gramLP1.lm.gz
7z x 2gramPL1.lm.gz
7z x 2gramPP1.lm.gz
7z x 2gramSS1.lm.gz
7z x 2gramSP1.lm.gz
7z x 2gramPS1.lm.gz
7z x 2gramWW1.lm.gz
cp *.lm ../

cd ..

cd "entrenamiento3gram"
echo "training PP1P2...."
fngram-count -factor-file 3gramCuentosPP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training PL1L2...."
fngram-count -factor-file 3gramCuentosPL1L2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training LL1L2...."
fngram-count -factor-file 3gramCuentosLL1L2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training LP1P2...."
fngram-count -factor-file 3gramCuentosLP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk

echo "training PW1W2...."
fngram-count -factor-file 3gramCuentosPW1W2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training WW1W2...."
fngram-count -factor-file 3gramCuentosWW1W2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training WP1P2...."
fngram-count -factor-file 3gramCuentosWP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk

echo "training PS1S2...."
fngram-count -factor-file 3gramCuentosPS1S2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training SS1S2...."
fngram-count -factor-file 3gramCuentosSS1S2.flm -text corpus_lem.tar.gz -lm -tolower -unk
echo "training SP1S2...."
fngram-count -factor-file 3gramCuentosSP1P2.flm -text corpus_lem.tar.gz -lm -tolower -unk

7z x 3gramLL1.lm.gz
7z x 3gramLP1.lm.gz
7z x 3gramPL1.lm.gz
7z x 3gramPP1.lm.gz
7z x 3gramPW1.lm.gz
7z x 3gramWP1.lm.gz
7z x 3gramWW1.lm.gz
7z x 3gramSS1.lm.gz
7z x 3gramSP1.lm.gz
7z x 3gramPS1.lm.gz
cp *.lm ../
