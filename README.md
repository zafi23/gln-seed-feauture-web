El sistema genera una frase que contenga el mayor número de palabras con un fonema concreto.

Entrada al sistema:

- Nombre del corpus
- Tipo de Seed Feature
- Fichero
- Seed Feature
- Fichero de preposiciones
- Fichero de stopwords
- Factor de Generación
- Fichero de lexicon

Ejemplo de entrada al sistema para fonemas en español:

"Cuentos_LEM phoneme fesp k prepesp stopesp l freeling"

Ejemplo de entrada para fonemas en inglés:

"Cuentos_ENG_Lem phoneme feng a prepeng stopeng l freeling"




-------------------------------------------------------------------------------------------------

Ejemplo de Entrada para polaridad

"CriticasCine polarity senticon.es.xml positive-negative prepesp stopesp l freeling"
"CriticasCine polarity senticon.es.xml negative-positive prepesp stopesp l freeling"

"review_polarity polarity senticon.en.xml positive-negative prepeng stopeng l freeling"

"review_polarity polarity positive-words.txt;negative-words.txt positive-negative prepeng stopeng l freeling"

"review_polarity polarity negative-words.txt;positive-words.txt negative-positive prepeng stopeng l freeling"

-------------------------------------------------------------------------------------------------

Ejemplo de Entrada para VImpW

Entrada al sistema:

- Nombre del corpus
- Tipo de Seed Feature
- Fichero
- Seed Feature
- Fichero de preposiciones
- Fichero de stopwords
- Factor de Generación
- Fichero de lexicon
- Fichero de gramática

"CuentosEN importantW A_CHEERFUL_TEMPER_short.5.smp_out a prepeng stopeng l freeling gramen"

-------------------------------------------------------------------------------------------------



Ejemplo de Entrada para Sentiment

Entrada al sistema:

- Nombre del corpus
- Tipo de Seed Feature
- Fichero
- Seed Feature
- Fichero de preposiciones
- Fichero de stopwords
- Factor de Generación
- Fichero de lexicon
- Fichero de gramática

"review_polarity sentiment emosenticnet.csv 3 prepeng stopeng l freeling gramen"

