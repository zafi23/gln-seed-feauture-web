/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

/**
 *
 * @author cristina
 */

import edu.mit.jverbnet.data.FrameType;
import edu.mit.jverbnet.data.IFrame;
import edu.mit.jverbnet.data.IMember;
import edu.mit.jverbnet.data.IVerbClass;
import edu.mit.jverbnet.data.IWordnetKey;
import edu.mit.jverbnet.data.WordnetKey;
import edu.mit.jverbnet.index.IVerbIndex;
import edu.mit.jverbnet.index.VerbIndex;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISenseKey;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.SynsetID;
//import it.uniroma1.lcl.babelnet.BabelNet;
//import it.uniroma1.lcl.babelnet.BabelSense;
//import it.uniroma1.lcl.babelnet.BabelSynset;
//import it.uniroma1.lcl.babelnet.BabelSynsetID;
//import it.uniroma1.lcl.babelnet.InvalidBabelSynsetIDException;
//import it.uniroma1.lcl.babelnet.WordNetSynsetID;
//import it.uniroma1.lcl.babelnet.data.BabelAudio;
//import it.uniroma1.lcl.babelnet.data.BabelPOS;
//import it.uniroma1.lcl.babelnet.data.BabelSensePhonetics;
//import it.uniroma1.lcl.babelnet.data.BabelSenseSource;
//import it.uniroma1.lcl.babelnet.resources.ResourceID;
//import it.uniroma1.lcl.jlt.util.Language;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;






public class Libraries {
    
    public void testWN() throws IOException {
        // construct the URL to the Wordnet dictionary directory

//        String path = "files/libraries/spanish_wordnet_3.0-1.0";
        String pathWN30 = "files/libraries/WNdict30";
        URL url30 = new URL("file", null, pathWN30);
        String pathWN20 = "files/libraries/WNdict20";
        URL url20 = new URL("file", null, pathWN20);
//        URL url2 = new URL("file", null , path2 ) ;

        // construct the dictionary object and open it
        IDictionary dict30 = new Dictionary(url30);
        IDictionary dict20 = new Dictionary(url20);
//        IDictionary dict2 = new Dictionary( url2 ); 
        dict30.open();
        dict20.open();
//        dict2.open();

        // look up first sense of the word "dog "
        IIndexWord idxWord = dict30.getIndexWord("dog", POS.NOUN);
        IWordID wordID = idxWord.getWordIDs().get(0);
        IWord word = dict30.getWord(wordID);

        ISynsetID s = SynsetID.parseSynsetID("SID-04202417-N");
        ISynset syn = dict30.getSynset(s);

//        ISynsetID s20 = SynsetID.parseSynsetID("SID-02604760‑V");
        ISynset syn20 = dict30.getSynset(s);
        
        IWord w = syn.getWords().get(0);

//        ISenseKey isen = syn.getWord(5).getSenseKey();
        ISenseKey isen = syn.getWord(1).getSenseKey();

        System.out.println(w.getVerbFrames().get(0).getTemplate());
        System.out.println(w.getID().getSynsetID().toString());

        System.out.println("Id30 = " + isen);
        System.out.println(" Id20 = " + syn20.getWords().get(0).getSenseKey());
        System.out.println(" Gloss = " + word.getSynset().getGloss());
        dict30.close();
        dict20.close();

        // make a url pointing to the Verbnet data
        String pathToVerbnet = "files/libraries/new_vn";
        URL url2 = new URL("file", null, pathToVerbnet);
        // construct the index and open it
        IVerbIndex index = new VerbIndex(url2);
        index.open();
        // look up a verb class and print out some info

        IVerbClass verb = index.getVerb("hit-18.1");
        IMember member = verb.getMembers().get(0);
        Set<IWordnetKey> keys = member.getWordnetTypes().keySet();
        IFrame frame = verb.getFrames().get(0);
        FrameType type = frame.getPrimaryType();
        String example = frame.getExamples().get(0);
        Iterator<IWordnetKey> it = keys.iterator();
        
        System.out.println(isen.toString().substring(0, isen.toString().length()-2));
        
        WordnetKey wk = WordnetKey.parseKey(isen.toString().substring(0, isen.toString().length()-2));
        
        if(wk == null)
            System.out.println("NULL");
        
        
        IWordnetKey iwk = wk;
        Set<IMember> m = index.getMembers(iwk);

        System.out.println(" id : " + verb.getID());
        System.out.println(" id : " + m.iterator().next().getVerbClass().getFrames().get(0).getPrimaryType().getID());
        System.out.println(" first wordnet keys : " + keys);
        System.out.println(" first frame type : " + type.getID());
        System.out.println(" first example : " + example);

    }
    
    public static void main(String[] args) throws IOException{
//        BabelNet bn = BabelNet.getInstance();
//        BabelSynset by = bn.getSynset(Arrays.asList(Language.EN),new WordNetSynsetID("wn:01079480v"));
//        for (BabelSense sense : by) {
//            System.out.println("Sense: " + sense.getSimpleLemma()
//                            + "\tLanguage: " + sense.getLanguage().toString()); 
//            System.out.println("DBPEDIA URI: " +sense.getDBPediaURI());
//            System.out.println("VERBNET URI: " +sense.getVerbNetURI());
//            System.out.println("SYNSET KEY: " +sense.getSensekey());
//            System.out.println("WORDNET OFFSET: " +sense.getWordNetOffset());
//            System.out.println("YAGOURI: " +sense.getYAGOURI());
//
//        }
        
        Libraries l = new Libraries();
        l.testWN();
        
    }
}
