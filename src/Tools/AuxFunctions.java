/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apertium.lttoolbox.process.FSTProcessor;
import seedfeaturesv.pkg2.BagW;
import seedfeaturesv.pkg2.Features.SeedFeature;
import seedfeaturesv.pkg2.Features.Sentiment;

/**
 *
 * @author cristina
 */
public class AuxFunctions {
    
    public static final FSTProcessor fstpSp = new FSTProcessor();
    public static final FSTProcessor fstpEn = new FSTProcessor();

//.load("files/apAnalyser/en-es.automorf.bin");
    
    
    public static String extractSubjectInfo(ArrayList<String> sInfo, String corpus, String language){
        String wordInfo = "";

        if(!sInfo.isEmpty()){
            for(String s: sInfo){
                if(!s.isEmpty()){
                    String[] cad = s.split("\\s+");
                    if((cad[10].equals("suj") || cad[10].equals("SBJ"))){
                        wordInfo = s;
                        break;
                    }
                    else if((cad[5].contains("pos=noun|") || cad[5].contains("pos=pronoun|")) && (cad[10].equals("cd") || cad[10].equals("OBJ"))){
                        wordInfo = s;
                    }
                    else if(cad[5].contains("pos=noun|") || cad[5].contains("pos=pronoun|")){
                        wordInfo = s;
                    }
                }
            }
        }
        return wordInfo;
    }
    
    public static String extractObjectInfo(ArrayList<String> sInfo, String corpus, String language){
        String wordInfo = "";
        if(!sInfo.isEmpty()){
            for(String s: sInfo){
                if(!s.isEmpty()){
                    String[] cad = s.split("\\s+");
                    if((cad[10].equals("atr") || cad[10].equals("cd") || cad[10].equals("OBJ"))){
                        wordInfo = s;
                        break;
                    }
                    else if(cad[5].contains("pos=noun|") || cad[5].contains("pos=pronoun|")){
                        wordInfo = s;
                    }
                }
            }
        }
        return wordInfo;
    }
    
    public static String extractNounInfo(ArrayList<String> sInfo, String corpus, String language,BagW bag){
        String wordInfo = "";

        if(!sInfo.isEmpty()){
            for(String s: sInfo){
                if(!s.isEmpty()){
                    String[] cad = s.split("\\s+");
                    if((cad[10].equals("suj") || cad[10].equals("SBJ")) && bag.containWord(cad[1])){
                        wordInfo = s;
                        break;
                    }
                    else if((cad[5].contains("pos=noun|") || cad[5].contains("pos=pronoun|")) && (cad[10].equals("cd") || cad[10].equals("OBJ")) && bag.containWord(cad[1])){
                        wordInfo = s;
                    }
                    else if(cad[5].contains("pos=noun|") || cad[5].contains("pos=pronoun|") && bag.containWord(cad[1])){
                        wordInfo = s;
                    }
                }
            }
        }
        return wordInfo;
    }
    
    public static ArrayList<String> getSentenceInfoFreeling(String sentence, String corpus, String language){
        ArrayList<String> sentInfo = new ArrayList<>();
        File archivo = null;
        FileReader fr =  null;
        BufferedReader br = null;
         try{	
            Random r = new Random();
            archivo = File.createTempFile("temp", null, new File("files/tempFiles/"));
            archivo.deleteOnExit();
            String configF=language+".cfg";
//            String command = "echo "+ sentence +" |analyze -f "+configF+" --outlv dep -d treeler --output conll --ner --nec >"+tempF;
            String command = "echo "+ sentence +" |analyze -f "+configF+" --outlv dep -d treeler --output conll >"+archivo.getAbsolutePath();
            String[] cd = {"/bin/sh", "-c", command};
            Process child = Runtime.getRuntime().exec(cd, new String[0]);
            child.waitFor();

//            archivo = new File(tempF);
            fr =  new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea = "";
            while ((linea = br.readLine()) != null) {
                if(!linea.isEmpty()){
                    sentInfo.add(linea);
                }
            }
             child.getOutputStream().close();
            child.getInputStream().close();
            child.getErrorStream().close();
            child.destroy();

        }catch(Exception e){
             e.printStackTrace();
        }finally {
                try {
                    if (null != br) {
                        br.close();
                    }
                
                    if (null != fr) {
                        fr.close();
                    }
//                    archivo.delete();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
         }
         
        
        
        return sentInfo;
    }
    
    public static Boolean isPlural(String w,String language,String pos){
        Boolean pl = false;
        try{
            FSTProcessor fstp = (language.equals("en"))?fstpEn:fstpSp;
            fstp.initAnalysis();
            StringWriter output = new StringWriter();
            fstp.analysis(new StringReader(w+"\n"), output);
            Pattern p = Pattern.compile("<"+pos+"><[a-z]+><pl>");
            Matcher m = p.matcher(output.toString());
            if (m.find())
                pl = true;
        }catch(Exception e){
             e.printStackTrace();
        }
        
        return pl;
    }
    
    public static Boolean isPersonX(String w, String person ,String language){
        Boolean pl = false;
        try{
            FSTProcessor fstp = (language.equals("en"))?fstpEn:fstpSp;
            fstp.initAnalysis();
            StringWriter output = new StringWriter();
            fstp.analysis(new StringReader(w+"\n"), output);
            Pattern p = Pattern.compile("><p"+person+"><");
            Matcher m = p.matcher(output.toString());
            if (m.find())
                pl = true;
        }catch(Exception e){
             e.printStackTrace();
        }
        
        return pl;
    }
    
    
    public static String obtainLemma(String w,String language){
        String lemma = ""; 
        try{
            FSTProcessor fstp = (language.equals("en"))?fstpEn:fstpSp;
            fstp.initAnalysis();
            StringWriter output = new StringWriter();
            fstp.analysis(new StringReader(w+"\n"), output);
            String[] saux = output.toString().split("/");
            String[] saux2 = saux[1].toString().split("<");
            lemma = saux2[0];

        }catch(Exception e){
             e.printStackTrace();
        }
        
        return lemma;
    }
    
    public static String obtainPos(String cad, String corpus, String lang){
        String pos ="";
        ArrayList<String> info = getSentenceInfoFreeling(cad, corpus, lang);
        
        for(String s: info){
            String[] saux = s.split("\\s+");
            String s2 = saux[5];
            if(s2.contains("|")){
                String[] saux2 = s2.split("\\|");
                s2 = saux2[0];
            }
            String[] saux3 = s2.split("=");
            pos+=" "+saux3[1];
        }
        
        return pos;
    }
    
    public static ArrayList<String> getPreps(String inf, ArrayList<String> words){
        ArrayList<String> prep = new ArrayList<>();
        
        for(String s: words){
            String[] saux = s.split("\\s+");
            if(saux[0].equals(inf)){
                prep.add(s.substring(s.indexOf(" ")+1,s.length()));
            }
        }
        
        return prep;
    }
    
    public static String[] randGrammar(ArrayList<String> gram){
        Random rnd = new Random();
        return gram.get((int)(rnd.nextDouble() * (gram.size()-1))).split(" ");
    }
    
    public static Boolean isNumber(String s){
        try {
		Integer.parseInt(s);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
    }
    
    public static int[] obtainSentimentSentence(String s, ArrayList<SeedFeature> features, SeedFeature f){
        int[] sent = new int[6];
        
        if(f.getFeatureID().equals("sentiment")){
            String[] aux = s.split("\\s+");
            for(String cad: aux){
                for(SeedFeature sf: features){
                    Sentiment saux = (Sentiment)sf;
                    if(sf.hasFeature(cad))
                        sent[saux.getId()]++;
                }
            }
        }
       
        return sent;
    }
    
        public static ArrayList<String> getWordPosCorpus(String posTAG,String corpus, String featG) {
        ArrayList<String> pal = new ArrayList<>();
        try {

            String comando = " cat files/corpus/" + corpus + "/corpus/* | tr -s \" \" \"\\n\" | grep -e \"P-" + posTAG + "\" | sort | uniq -i";

            String[] cmd = {
                "/bin/sh",
                "-c",
                comando
            };

            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String s;

            while ((s = stdInput.readLine()) != null) {
                String[] cadAux = s.split(":");
                for(String aux: cadAux){
                    if(aux.contains(featG.toUpperCase()+"-")){
                        String[] cadAux2 = aux.split("-");
                        pal.add(cadAux2[1].toLowerCase());
                        break;
                    }
                }
                
            }
            stdInput.close();
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        return pal;
    }
        
    public static boolean filterCond(String featG,ArrayList<String> sentWords, String[] cads, ArrayList<String> wtagCorpus, SeedFeature feature, String[] poslw, int i,String corpus,ArrayList<String> genSentences,boolean noFLM){
        boolean filter = false;
        
        switch(featG){
            case "l":
                
                if (!noFLM && !sentWords.contains(cads[(cads.length>1)?1:0]) && wtagCorpus.contains(cads[(cads.length>1)?1:0]) && !feature.wordNotDesirable(cads[(cads.length>1)?1:0]) && poslw[i].equals(getPosOfWord(cads[(cads.length>1)?1:0],corpus,featG)))
                    filter = true;
                
                if(noFLM && !sentWords.contains(cads[(cads.length>1)?1:0]))
                    filter = true;
                
                break;
            case "w":
                
                if (!noFLM && !sentWords.contains(cads[(cads.length>1)?1:0]) && wtagCorpus.contains(cads[(cads.length>1)?1:0]) && !feature.wordNotDesirable(cads[(cads.length>1)?1:0]) && poslw[i].equals(getPosOfWord(cads[(cads.length>1)?1:0],corpus,featG)))
                    filter = true;
                
                if(noFLM && !sentWords.contains(cads[(cads.length>1)?1:0]))
                    filter = true;
                
                break;
            case "s":
                if (wtagCorpus.contains(cads[(cads.length>1)?1:0]) && !feature.wordNotDesirable(cads[(cads.length>1)?1:0])){
                    if((cads[(cads.length>1)?1:0].contains("noun")&& !cads[(cads.length>1)?1:0].contains("pronoun"))||cads[(cads.length>1)?1:0].contains("verb")||cads[(cads.length>1)?1:0].contains("adjective"))
                    {
                        if(cads[(cads.length>1)?1:0].matches(".*\\d.*") && !sentWords.contains(cads[(cads.length>1)?1:0]) && notInSentence(genSentences,cads[(cads.length>1)?1:0]))
                            filter = true;
                    }
                    else
                        filter = true;  
                }
                
                if(noFLM){
                    if((cads[(cads.length>1)?1:0].contains("noun")&& !cads[(cads.length>1)?1:0].contains("pronoun"))||cads[(cads.length>1)?1:0].contains("verb")||cads[(cads.length>1)?1:0].contains("adjective"))
                    {
                        if(cads[(cads.length>1)?1:0].matches(".*\\d.*") && notInSentence(genSentences,cads[(cads.length>1)?1:0]))
                            filter = true;
                    }else
                        filter = true;
                }
                break;
        }
        
        
        return filter;
    }
    
    public static boolean notInSentence(ArrayList<String> sentences, String cad)
    {
        boolean inS = true;
        
//        for(String s : sentences){
//            if(s.contains(cad)){
//                inS = false;
//                break;
//            }
//        }
        
        return inS;
    }    
    
        public static String getPosOfWord(String word,String corpus, String featureGen) {
        String pos = "";

        try {
            
            if(word.equals("."))
                word = "\\.";
            
            String comand = " cat files/corpus/" + corpus + "/test/* files/corpus/" + corpus + "/corpus/* | tr -s \" \" \"\\n\" | grep -e \"" + featureGen.toUpperCase() + "-" + word + ":\" | sort | uniq -c | sort -nr";

            String[] cmd = {
                "/bin/sh",
                "-c",
                comand
            };

            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String s = stdInput.readLine();

            if (s != null) {
                String[] aux = s.split("\\s+");
                String[] aux2 = aux[2].split(":");
                String[] aux3 = aux2[0].split("-");

                pos = aux3[1];
            }else{
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();
                comand = "cat files/corpus/" + corpus + "/corpus/* | tr -s \" \" \"\\n\" | grep -e \"" + featureGen.toUpperCase() + "-" + word + "$\" | sort | uniq -c | sort -nr";
                cmd[2] =comand;
                process = Runtime.getRuntime().exec(cmd);
                stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                s = stdInput.readLine();

                if (s != null) {
                    String[] aux = s.split("\\s+");
                    String[] aux2 = aux[2].split(":");
                    String[] aux3 = aux2[0].split("-");

                    pos = aux3[1];
                }
            }
            stdInput.close();
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }

        return pos.toLowerCase();
    }
    
}