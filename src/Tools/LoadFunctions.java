/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import seedfeaturesv.pkg2.Features.Word;

/**
 *
 * @author cristina
 */
public class LoadFunctions {
    
    public static Lexicon loadLex(String path, String type, String language){
        Lexicon lex = new Lexicon();
        File fileIn = new File(path);
        String cad = "";
        lex.setLanguage(language);
        for(File f:fileIn.listFiles()){
            FileReader fr = null;
            BufferedReader br = null;
            try{
                fr = new FileReader(f);
                br = new BufferedReader(fr);
                
                while ((cad = br.readLine()) != null) {
                    Word w = new Word();
                    if(type.equals("eagles"))
                        w = disEAGLES(cad,2);
                    else if (type.equals("penn"))
                        w = disPENN(cad,language,2);
                   lex.addWord(w);
                }
            
            }catch (Exception e) {
            e.printStackTrace();
            } finally {
                try {
                    if (null != br) {
                        br.close();
                    }
                
                    if (null != fr) {
                        fr.close();
                    }
                    
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            
        }
        return lex;
    }
    
    public static Word disEAGLES(String cad,int pos){
        Word w = new Word();
        HashMap<String,String> features = new HashMap<>();
        String[] s = cad.split("\\s+");
        w.setWord(s[0]);
        w.getInfo().setLemma(s[1]);
        String posFull = s[pos];
        
        switch(posFull.charAt(0)){
            case 'A':
                w.setPosTAG("adjective");
                features.put("type",String.valueOf(posFull.charAt(1)));
                features.put("degree",String.valueOf(posFull.charAt(2)));
                features.put("gen",String.valueOf(posFull.charAt(3)));
                features.put("num",String.valueOf(posFull.charAt(4)));
                break;
            case 'C':
                w.setPosTAG("conjunction");
                features.put("type",String.valueOf(posFull.charAt(1)));
                break;
            case 'D':
                w.setPosTAG("determiner");
                features.put("type",String.valueOf(posFull.charAt(1)));
                features.put("person",String.valueOf(posFull.charAt(2)));
                features.put("gen",String.valueOf(posFull.charAt(3)));
                features.put("num",String.valueOf(posFull.charAt(4)));
                break;
            case 'N':
                w.setPosTAG("noun");
                features.put("type",String.valueOf(posFull.charAt(1)));
                features.put("gen",String.valueOf(posFull.charAt(2)));
                features.put("num",String.valueOf(posFull.charAt(3)));
                features.put("neclass",String.valueOf(posFull.charAt(4)));
                break;
            case 'P':
                w.setPosTAG("pronoun");
                features.put("type",String.valueOf(posFull.charAt(1)));
                features.put("person",String.valueOf(posFull.charAt(2)));
                features.put("gen",String.valueOf(posFull.charAt(3)));
                features.put("num",String.valueOf(posFull.charAt(4)));
                break;
            case 'R':
                w.setPosTAG("adverb");
                features.put("type",String.valueOf(posFull.charAt(1)));
                break;
            case 'S':
                w.setPosTAG("adposition");
                features.put("type",String.valueOf(posFull.charAt(1)));
                break;
            case 'V':
                w.setPosTAG("verb");
                features.put("type",String.valueOf(posFull.charAt(1)));
                features.put("mood",String.valueOf(posFull.charAt(2)));
                features.put("tense",String.valueOf(posFull.charAt(3)));
                features.put("person",String.valueOf(posFull.charAt(4)));
                features.put("num",String.valueOf(posFull.charAt(5)));
                features.put("gen",String.valueOf(posFull.charAt(6)));
                break;
            case 'Z':
                w.setPosTAG("number");
                features.put("type",String.valueOf(posFull.charAt(1)));
                break;
            case 'W':
                w.setPosTAG("date");
                break;
            case 'I':
                w.setPosTAG("interjection");
                break;
            case 'F':
                w.setPosTAG("punctuation");
                break;
        }
        
        w.getInfo().setMorphFeatures(features);
        
        return w;
    }
    
    public static Word disPENN(String cad,String language,int pos){
        Word w = new Word();
        HashMap<String,String> features = new HashMap<>();
        String[] s = cad.split("\\s+");
        w.setWord(s[0]);
        w.getInfo().setLemma(s[1]);
        String posFull = s[pos];
        
        if(s[0].contains("_")){
            
            String[] aux = s[0].split("_");
            
            switch(aux[1]){
                case "a": w.setPosTAG("adjective");
                    break;
                case "n": w.setPosTAG("noun");
                    break;
                case "v": w.setPosTAG("verb");
                    break;
                case "r": w.setPosTAG("adverb");
                    break;
            }
            
        }else{
     
        
            switch(posFull){
                case "JJ":
                case "JJR":
                case "JJS":
                    w.setPosTAG("adjective");
                    if(posFull.equals("JJR"))
                        features.put("degree","C");
                    if(posFull.equals("JJS"))
                        features.put("degree","S");
                    break;
                case "POS":
                    w.setPosTAG("adposition");
                    features.put("type","P");
                    break;
                case "RB":
                case "RBR":
                case "RBS":
                case "WRB":
                    w.setPosTAG("adverb");
                    if(posFull.equals("WRB"))
                         features.put("type","I");
                    else{
                        features.put("type","G");
                        if(posFull.equals("RBR"))
                            features.put("degree","C");
                        if(posFull.equals("RBS"))
                            features.put("degree","S");
                    }
                    break;
                case "CC":
                    w.setPosTAG("conjunction");
                    features.put("type","C");
                    break;
                case "DT":
                case "WDT":
                case "PDT":
                    w.setPosTAG("determiner");
                    if(posFull.equals("WDT"))
                        features.put("type","I");
                    if(posFull.equals("PDT"))
                        features.put("type","P");
                    if(AuxFunctions.isPlural(w.getWord(), language,"det")){
                        features.put("num","P");
                        w.getInfo().setLemma(AuxFunctions.obtainLemma(w.getWord(), language));
                    }
    //                else
    //                    features.put("num","S");

                    break;
                case "NNS":
                case "NN":
                case "NNP":
                case "NP00000":
                case "NP":
                case "NP00G00":
                case "NP00O00":
                case "NP00V00":
                case "NP00SP0":
                case "NNPS":
                    w.setPosTAG("noun");
                    if(posFull.equals("NNS")|| posFull.equals("NN")){
                        features.put("type","C");
                        if(posFull.equals("NNS"))
                            features.put("num","P");
                        else
                            features.put("num","S");
                    }
                    else{
                        features.put("type","P");
                        if(posFull.equals("NP00G00"))
                            features.put("neclass","L");
                        if(posFull.equals("NP00O00"))
                            features.put("neclass","Or");
                        if(posFull.equals("NP00V00"))
                            features.put("neclass","Ot");
                        if(posFull.equals("NP00SP0"))
                            features.put("neclass","P");
                        if(posFull.equals("NNPS"))
                            features.put("num","P");
                    }
                    break;
                case "RP":
                case "TO":
                    w.setPosTAG("particle");
                    if(posFull.equals("TO"))
                         features.put("type",String.valueOf(posFull.charAt(1)));

                    break;
                case "EX":
                case "WP":
                case "PRP":
                case "PRP$":
                case "WP$":
                    w.setPosTAG("pronoun");
                    if(posFull.equals("WP"))
                            features.put("type","I");
                        if(posFull.equals("PRP"))
                            features.put("type","Pe");
                        if(posFull.equals("PRP$") || posFull.equals("WP$"))
                            features.put("type","Po");
                    break;
                case "MD":
                case "VBG":
                case "VB":
                case "VBN":
                case "VBD":
                case "VBP":
                case "VBZ":
                    w.setPosTAG("verb");

                    if(posFull.equals("MD"))
                        features.put("type","M");
                    if(posFull.equals("VBG"))
                        features.put("vform","G");
                    if(posFull.equals("VB"))
                        features.put("vform","I");
                    if(posFull.equals("VBN"))
                        features.put("vform","Pa");
                    if(posFull.equals("VBD")){
                        features.put("vform","Ps");
                        if(AuxFunctions.isPersonX(w.getWord(), "3", language)){
                                features.put("person","3");
                        }
                        if(AuxFunctions.isPersonX(w.getWord(), "1", language)){
                                features.put("person2","1");
                        }
                    }
                    if(posFull.equals("VBP")){
                        features.put("vform","Pe");
                        if(AuxFunctions.isPersonX(w.getWord(), "1", language)){
                            features.put("person","1");
                        }
                    }
                    if(posFull.equals("VBZ")){
                        features.put("vform","Pe");
                        features.put("person","3");
                    }
                    break;
                case "Z":
                    w.setPosTAG("number");
                    features.put("type",String.valueOf(posFull.charAt(0)));
                    break;
                case "W":
                    w.setPosTAG("date");
                    break;
                case "IN":
                    w.setPosTAG("preposition");
                    break;
                case "I":
                    w.setPosTAG("interjection");
                    break;
                default:
                    w.setPosTAG("punctuation");
                    break;
            }
        }
        
        w.getInfo().setMorphFeatures(features);
        
        return w;
    }
    
    public static ArrayList<String> loadList(String path){
        ArrayList<String> lw = new ArrayList<>();
        
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String s = "";
        
        try{
            archivo = new File(path);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            while ((s = br.readLine()) != null) {
                if(s.contains("/")){
                    String[] saux = s.split(" ");
                    String[] saux2 = saux[1].split("/");
                    for(String s2: saux2){
                        lw.add(saux[0]+" "+s2);
                    }
                }
                else
                    lw.add(s);
            }
            
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
                try {
                    if (null != br) {
                        br.close();
                    }
                
                    if (null != fr) {
                        fr.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
         }
        
        return lw;
    }
    
    
}
