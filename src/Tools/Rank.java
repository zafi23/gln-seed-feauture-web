/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import seedfeaturesv.pkg2.Features.Phoneme;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Stack;
import seedfeaturesv.pkg2.Features.SeedFeature;
import seedfeaturesv.pkg2.Features.Sentence;
import seedfeaturesv.pkg2.SeedFeaturesV2;

/**
 *
 * @author cristina
 */
public class Rank {

    private ArrayList<String> sents;
    private Sentence selsent;
    private double maxpercent;
    public SeedFeature feature;
        
    public void init(SeedFeature p) {
        selsent = new Sentence();
        sents = new ArrayList<>();
        maxpercent = -Float.MAX_VALUE;
        feature = p;
    }
    
    public Sentence combinedRankSentence(ArrayList<Sentence> sentences,String corpus, String lm, SeedFeature p, String featureGen, ArrayList<String> lastSen,String nextS,boolean noFLM){
        init(p);
        ArrayList<Sentence> ended = extractEndingSentences(sentences,lastSen,nextS);
        double[] probended = new double[ended.size()];
        double mprima = 0D;
        
        if(ended.size()>0){
            for(int i = 0; i < ended.size(); i++){
                double prob = sentenceCombinedProbabilityBack(ended.get(i), corpus,lm, featureGen, 0.25D, 0.25D, 0.5D,noFLM);
                probended[i] = prob;
                if(prob > maxpercent){
                    selsent = ended.get(i);
                    maxpercent = prob;
                }
                mprima+=prob;
            }
            
            mprima= mprima/ended.size();
            
            ArrayList<Sentence> mostFreqEnded = new ArrayList<>();

            for(int i = 0; i < ended.size() ; i++){
                if(probended[i]>=mprima)
                    mostFreqEnded.add(ended.get(i));
            }

            float maxseedsent = 0f;

            for(Sentence s: mostFreqEnded){
                String[] aux = s.getSentence().split("\\s+");
                int nseed = 0;
                for(int i = 0; i< aux.length; i++){
                    if(feature.hasFeature(aux[i]))
                        nseed++;
                }
                float nseedper = nseed/(aux.length-1.0f); 
                if(nseedper>maxseedsent)
                {
                    maxseedsent = nseedper;
                    selsent = s;
                }

            }
            
            
        }
        String s = selsent.getSentence();
        s = s.replace(" </s>","");
        selsent.setSentence(s);
        return selsent;
    }
     
    public ArrayList<Sentence> extractEndingSentences(ArrayList<Sentence> sentences, ArrayList<String> lastSen, String nextS) {
        ArrayList<Sentence> ended = new ArrayList<>();
        ArrayList<Sentence> auxend = new ArrayList<>();
        
        
        
        for (Sentence s : sentences) {
            if(!lastSen.contains(s.getSentence())){ // && s.contains(nextS)){
                if (s.getSentence().contains(".")){
                    s.setSentence(s.getSentence()+" </s>");
                    ended.add(s);
                }
                else if(s.getSentence().contains("</s>"))
                    ended.add(s);
            }
//            else if (!lastSen.contains(s) && s.contains("."))
//                    auxend.add(s+" </s>");
//                else if(!lastSen.contains(s) && s.contains("</s>"))
//                    auxend.add(s);
        }
        
        if(ended.isEmpty())
            return auxend;
        else
            return ended;
    }

    public double sentenceCombinedProbability(String sentence, String corpus,String featureGen,double a1, double a2, double a3){
        double probEnd = 1.0;
        
        String[] words = sentence.split("\\s+");
        String[] posWords = new String[words.length];
        String w_1LL,w_1LP;
        String w_2LL,w_2LP;
        String wLL,wPL;
        
        w_1LL = w_1LP = w_2LL = w_2LP = "<s>";
        wLL = wPL = "";

        
        for(int i = 0 ; i < words.length; i++){
            posWords[i] = AuxFunctions.getPosOfWord(words[i],corpus,featureGen);
        }
        
        try{
            for(int i = 0 ; i < words.length; i++){
            
       
                float probLL,probLP,probPL;
                probLL=probLP=probPL= 0.0f;
                //Get posTag with the highest probability of being with word 

                if(!words[i].equals("</s>")){
                    wLL = featureGen+"-"+words[i];
                    wPL = "p-"+posWords[i];
                }else{
                    wLL = wPL = "</s>";
                    
                }

                //Obtener probabilidad de 3gramLL1
                String command = " grep -e [$'\t']\"" + w_2LL + " "+w_1LL+" "+wLL+"\" files/corpus/" + corpus + "/lm/3gram"+featureGen.toUpperCase()+featureGen.toUpperCase()+"1.lm | sort";

                String[] cmd = {
                "/bin/sh",
                "-c",
                command
                };

                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                String s = stdInput.readLine();

                if(s != null){
                    String[] aux = s.split("\\t+");
                    String[] aux2 = aux[1].split("\\s+");
                    if(aux2[0].equals(w_2LL) && aux2[1].equals(w_1LL) && aux2[2].equals(wLL)){
                        probLL = Float.parseFloat(aux[0]);
                    }   
                }
                else{
                    command = " grep -e [$'\t']\""+w_1LL+" "+wLL+"\" files/corpus/" + corpus + "/lm/2gram"+featureGen.toUpperCase()+featureGen.toUpperCase()+"1.lm | sort";
                    cmd[2] = command;

                    process = Runtime.getRuntime().exec(cmd);
                    stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                    s = stdInput.readLine();

                        if(s != null){
                            String[] aux = s.split("\\t+");
                            String[] aux2 = aux[1].split("\\s+");
                            if(aux2[0].equals(w_1LL) && aux2[1].equals(wLL)){
                                probLL = Float.parseFloat(aux[0]);
                            }   
                        }
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();

                //Obtener probabilidad de 3gramLP1
                command = " grep -e [$'\t']\"" + w_2LP + " "+w_1LP+" "+wLL+"\" files/corpus/" + corpus + "/lm/3gram"+featureGen.toUpperCase()+"P1.lm | sort";

                cmd[2] = command;

                process = Runtime.getRuntime().exec(cmd);
                stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                s = stdInput.readLine();

                if(s != null){
                    String[] aux = s.split("\\t+");
                    String[] aux2 = aux[1].split("\\s+");
                    if(aux2[0].equals(w_2LP) && aux2[1].equals(w_1LP) && aux2[2].equals(wLL)){
                        probLP = Float.parseFloat(aux[0]);
                    }   
                }
                else{
                    command = " grep -e [$'\t']\""+w_1LP+" "+wLL+"\" files/corpus/" + corpus + "/lm/2gram"+featureGen.toUpperCase()+"P1.lm | sort";
                    cmd[2] = command;

                    process = Runtime.getRuntime().exec(cmd);
                    stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                    s = stdInput.readLine();

                        if(s != null){
                            String[] aux = s.split("\\t+");
                            String[] aux2 = aux[1].split("\\s+");
                            if(aux2[0].equals(w_1LP) && aux2[1].equals(wLL)){
                                probLP = Float.parseFloat(aux[0]);
                            }   
                        }
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();
                 //Obtener probabilidad de 3gramPL1
                command = " grep -e [$'\t']\"" + w_2LL + " "+w_1LL+" "+wPL+"\" files/corpus/" + corpus + "/lm/3gramP"+featureGen.toUpperCase()+"1.lm | sort";

                cmd[2] = command;

                process = Runtime.getRuntime().exec(cmd);
                stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                s = stdInput.readLine();

                if(s != null){
                    String[] aux = s.split("\\t+");
                    String[] aux2 = aux[1].split("\\s+");
                    if(aux2[0].equals(w_2LL) && aux2[1].equals(w_1LL) && aux2[2].equals(wPL)){
                        probPL = Float.parseFloat(aux[0]);
                    }   
                }
                else{
                    command = " grep -e [$'\t']\""+w_1LL+" "+wPL+"\" files/corpus/" + corpus + "/lm/2gramP"+featureGen.toUpperCase()+"1.lm | sort";
                    cmd[2] = command;

                    process = Runtime.getRuntime().exec(cmd);
                    stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

                    s = stdInput.readLine();

                        if(s != null){
                            String[] aux = s.split("\\t+");
                            String[] aux2 = aux[1].split("\\s+");
                            if(aux2[0].equals(w_1LL) && aux2[1].equals(wPL)){
                                probPL = Float.parseFloat(aux[0]);
                            }   
                        }
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();
                
                w_2LL = w_1LL;
                w_1LL = featureGen+"-"+words[i];
                
                w_2LP = w_1LP;
                w_1LP = "p-"+posWords[i];

//                double decprob = ((a1*Math.pow(10,probLL))+ (a2*Math.pow(10,probLP))+ (a3*Math.pow(10,probPL)))/3;
                double decprob = (a1*Math.pow(10,probLL))+ (a2*Math.pow(10,probLP))+ (a3*Math.pow(10,probPL));
                double exp= 1D/(double)words.length;
                double problength = Math.pow(decprob,exp);

                probEnd*=problength;
            }
        } catch (IOException ioe) {
            System.out.println(ioe);
        }

    
        
        
        
        return probEnd;
    }

    public double sentenceCombinedProbabilityBack(Sentence sentence, String corpus,String lm,String featureGen,double a1, double a2, double a3,boolean noFLM){
        double probEnd = 1.0;
        
        SeedFeaturesV2 seed = new SeedFeaturesV2();
        String[] words = sentence.getSentence().split("\\s+");
        String spos = sentence.getSentencePOS() + " </s>";
        String[] posWords = spos.split("\\s+");
        String command,s,w_1LL,w_1LP,w_2LL,w_2LP,wLL,wPL;
        ArrayList<String> sLL,sLP,sPL;
        sLL = new ArrayList<>();
        sLP = new ArrayList<>();
        sPL = new ArrayList<>();
        
        w_1LL = w_1LP = w_2LL = w_2LP = "<s>";
        command = wLL = wPL = "";
        String[] cmd = {
                    "/bin/sh",
                    "-c",
                    command
                    };
        
        seed.corpus = corpus;
        seed.featureGen = featureGen;
        


        for(int i = 0 ; i < words.length; i++){


            float probLL,probLP,probPL;
            probLL=probLP=probPL= 0.0f;
            //Get posTag with the highest probability of being with word 

            if(!words[i].equals("</s>")){
                wLL = featureGen+"-"+words[i];
                if(!noFLM)
                    wPL = "p-"+posWords[i];
            }else{
                wLL = wPL = "</s>";

            }


            //Obtener probabilidad de 3gramLL1
//                sLL.add(wLL);
            sLL.add(w_2LL);
            sLL.add(w_1LL);   

            probLL = getProbabilityWord(wLL, corpus, lm+"3gram"+featureGen.toUpperCase()+featureGen.toUpperCase()+"1.lm", featureGen, sLL, 0, -1);

            
            if(!noFLM){
                //Obtener probabilidad de 3gramLP1
    //                sLP.add(wLL);
                sLP.add(w_2LP);
                sLP.add(w_1LP);

                probLP = getProbabilityWord(wLL, corpus, lm+"3gram"+featureGen.toUpperCase()+"P1.lm", featureGen, sLP, 0, -1);

                 //Obtener probabilidad de 3gramPL1  
    //                sPL.add(wPL);
                sPL.add(w_2LL);
                sPL.add(w_1LL);

                probPL = getProbabilityWord(wPL, corpus, lm+"3gramP"+featureGen.toUpperCase()+"1.lm", featureGen, sPL, 0, -1);


                w_2LL = w_1LL;
                w_1LL = featureGen+"-"+words[i];

                w_2LP = w_1LP;
                w_1LP = "p-"+posWords[i];

    //                double decprob = ((a1*Math.pow(10,probLL))+ (a2*Math.pow(10,probLP))+ (a3*Math.pow(10,probPL)))/3;
    //            double decprob = (a1*Math.pow(10,probLL))+ (a2*Math.pow(10,probLP))+ (a3*Math.pow(10,probPL));
    //            double exp= 1D/(double)words.length;
    //            double problength = Math.pow(decprob,exp);

                  double exp= 1D/(double)words.length;
                  double problength = (a1*Math.pow(Math.pow(10,probLL),exp))+ (a2*Math.pow(Math.pow(10,probLP),exp))+ (a3*Math.pow(Math.pow(10,probPL),exp));


                probEnd*=problength;
            }
            else
                probEnd*=probLL;
            sLL.clear();
            sLP.clear();
            sPL.clear();
        }

        return probEnd;
    }
    
     public float getProbabilityWord(String word, String corpus,String lm, String featureGen, ArrayList<String> lastWords,int posF, int posB){
        float prob = 0;
        try{
            
            if(posF == lastWords.size()){
                //Probabilidad palabra
                String comando = " grep -e [$'\t']\""+ word+"$\" files/corpus/" + corpus + "/lm/" + lm;
                String[] cmd = {
                    "/bin/sh",
                    "-c",
                    comando
                };
                
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String s = stdInput.readLine();
                if(s != null){
                    String[] aux = s.split("\\t+");
                    String[] aux1 = aux[1].split("\\s+");
//                    String[] aux2 = aux1[0].split("-");
                    if(word.equals(aux1[lastWords.size()-posF]))
                        prob += Float.parseFloat(aux[0]);
                }
                else{
                    stdInput.close();
                    process.getOutputStream().close();
                    process.getInputStream().close();
                    process.getErrorStream().close();
                    process.destroy();
                    comando = " grep -e [$'\t']\"<unk>$\" files/corpus/" + corpus + "/lm/" + lm;
                    cmd[2] = comando;
                    process = Runtime.getRuntime().exec(cmd);
                    stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    s = stdInput.readLine();
                    if(s != null){
                        String[] aux = s.split("\\t+");
                        if(aux.length==3)
                            prob += Float.parseFloat(aux[0]);
                    }
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();
                
                //Probabilidad del back
                comando = " grep -e [$'\t']\"";
                for(int i=posB;i < lastWords.size(); i++){
                    comando+=lastWords.get(i)+" ";
                }
                comando+= "\" files/corpus/" + corpus + "/lm/" + lm;
                cmd[2] = comando;
                process = Runtime.getRuntime().exec(cmd);
                stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                s = stdInput.readLine();
                if(s != null){
                    String[] aux = s.split("\\t+");
                    if(aux.length==3)
                        prob += Float.parseFloat(aux[2]);
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();     
            }
            else{
                if(posB == -1){
                    String comando = " grep -e [$'\t']\"";
                    
                    for(int i=posF;i < lastWords.size(); i++){
                           comando+=lastWords.get(i)+" ";
                    }
                    
                    comando+= word + "\" files/corpus/" + corpus + "/lm/" + lm;
                    String[] cmd = {
                        "/bin/sh",
                        "-c",
                        comando
                    };
                    Process process = Runtime.getRuntime().exec(cmd);
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                     String s = stdInput.readLine();

                    if(s != null){
                        boolean found = false;
                        while(s!=null && !found){
                            String[] aux = s.split("\\t+");
                            String[] aux1 = aux[1].split("\\s+");
//                            String[] aux2 = aux1[lastWords.size()-posF].split("-");
                            if(word.equals(aux1[lastWords.size()-posF])){
                                prob += Float.parseFloat(aux[0]);
                                found = true;
                            }
                            else
                                s = stdInput.readLine();
                        }
                        if(!found){
                            prob += getProbabilityWord(word, corpus, lm, featureGen, lastWords, posF+1, posB+1);
                        }
                    }
                    else
                        prob += getProbabilityWord(word, corpus, lm, featureGen, lastWords, posF+1, posB+1);
                    
                    stdInput.close();
                    process.getOutputStream().close();
                    process.getInputStream().close();
                    process.getErrorStream().close();
                    process.destroy();
                }
                else{
                    //Probabilidad palabra-factores
                    String comando = " grep -e [$'\t']\"";
                    for(int i=posF;i < lastWords.size(); i++){
                        comando+=lastWords.get(i)+" ";
                    }
                    comando+= word + "\" files/corpus/" + corpus + "/lm/" + lm;
                    String[] cmd = {
                        "/bin/sh",
                        "-c",
                        comando
                    };

                    Process process = Runtime.getRuntime().exec(cmd);
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    String s = stdInput.readLine();
                    if(s != null){
                        boolean found = false;
                        while(s!=null && !found){
                            String[] aux = s.split("\\t+");
                            String[] aux1 = aux[1].split("\\s+");
//                            String[] aux2 = aux1[lastWords.size()-posF].split("-");
                            if(word.equals(aux1[lastWords.size()-posF])){
                                prob += Float.parseFloat(aux[0]);
                                found = true;
                            }
                            else
                                s = stdInput.readLine();
                        }
                        if(!found)
                            prob += getProbabilityWord(word, corpus, lm, featureGen, lastWords, posF+1, posB+1);
                    }
                    else
                        prob += getProbabilityWord(word, corpus, lm, featureGen, lastWords, posF+1, posB+1);
                    
                    stdInput.close();
                    process.getOutputStream().close();
                    process.getInputStream().close();
                    process.getErrorStream().close();
                    process.destroy();
                    //Probabilidad del back
                    comando = " grep -e [$'\t']\"";
                    for(int i=posB;i < lastWords.size(); i++){
                        comando+=lastWords.get(i)+" ";
                    }
                    comando+= "\" files/corpus/" + corpus + "/lm/" + lm;
                    cmd[2] = comando;
                    process = Runtime.getRuntime().exec(cmd);
                    stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    s = stdInput.readLine();
                    if(s != null){
                        String[] aux = s.split("\\t+");
                        if(aux.length==3)
                            prob += Float.parseFloat(aux[2]);
                    }
                    stdInput.close();
                    process.getOutputStream().close();
                    process.getInputStream().close();
                    process.getErrorStream().close();
                    process.destroy();
                }
            }
 
        } catch (Exception ioe) {
            System.out.println(ioe);
        }   
        return prob;
    }
    
    
    
}
