/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import edu.mit.jverbnet.data.FrameType;
import edu.mit.jverbnet.data.IFrame;
import edu.mit.jverbnet.data.IMember;
import edu.mit.jverbnet.data.IVerbClass;
import edu.mit.jverbnet.data.IWordnetKey;
import edu.mit.jverbnet.data.VerbClass;
import edu.mit.jverbnet.data.WordnetKey;
import edu.mit.jverbnet.index.IVerbIndex;
import edu.mit.jverbnet.index.VerbIndex;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISenseKey;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Synset;
import edu.mit.jwi.item.SynsetID;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import seedfeaturesv.pkg2.Features.Phoneme;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import seedfeaturesv.pkg2.BagW;
import seedfeaturesv.pkg2.Features.SeedFeature;
import seedfeaturesv.pkg2.Features.Sentence;
import seedfeaturesv.pkg2.Features.Sentiment;
import seedfeaturesv.pkg2.Features.VImpW;
import seedfeaturesv.pkg2.Features.Word;
import seedfeaturesv.pkg2.SeedFeaturesV2;

/**
 *
 * @author cristina
 */
public class TestFunctions {


    public void testSpaPhonemes(String feature) {
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos phoneme fesp b prepesp stopesp "+feature;
        s.init(datos);
        
        for (SeedFeature p : s.seedFeatures) {
            s.feature = p;
            s.bag = new BagW();
            s.fillBOW();
            System.out.println("Phoneme --------- " + p.getFeatureID() + " --------");
            ArrayList<Sentence> sentences = s.generateTri(16, "3gram", true,"",new ArrayList<String>(),false);
             Rank r = new Rank();
            System.out.println("Total generated sentences: "+sentences.size());
            Sentence sentence = r.combinedRankSentence(sentences, "Cuentos", "gramLP1.lm",s.feature,feature,new ArrayList<String>(),"",false);
           
            System.out.println(sentence.getSentence());
        
        }
    }
    
        public void testSpaPhonemesSeveral(String feature) {
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos_LEM phoneme fesp a prepesp stopesp "+feature + " freeling";
        s.init(datos);
        ArrayList<String> prepsS = LoadFunctions.loadList("files/verbPrepEs");

        for (SeedFeature p : s.seedFeatures) {
            s.feature = p;
            s.bag = new BagW();
            s.fillBOW();
            System.out.println("Phoneme --------- " + p.getFeatureID() + " --------");
            Sentence sent = new Sentence();
            String nextS = "";
            Rank r = new Rank();
            ArrayList<String> generated = new ArrayList<>();
            for(int i = 0; i<3; i++){
                ArrayList<Sentence> sentences = s.generateTri(6, "3gram", true,nextS,generated,false);
                sent = r.combinedRankSentence(sentences, "Cuentos_LEM", "gramLP1.lm", s.feature,feature,generated,nextS,false);
                generated.add(sent.getSentence());
                nextS = s.nextSub(sent.getSentence());
                String pos = "determiner noun verb determiner noun punctuation";
                sent.setSentencePOS(pos);
                String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,prepsS,"");
                System.out.println(sent.getSentence() + " ----> " + sInflected);
//                System.out.println(nextS);
            } 
        
        }
    }
    
    public void testEngPhonemesSeveral(String feature) {
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos_ENG_Lem phoneme feng a prepeng stopeng "+feature + " freeling";
        s.init(datos);
        ArrayList<String> prepsE = LoadFunctions.loadList("files/verbPrepEng");
        ArrayList<String> prashal = LoadFunctions.loadList("files/phrasalEng");
        
        for (SeedFeature p : s.seedFeatures) {
            s.feature = p;
            s.bag = new BagW();
            s.fillBOW();
            System.out.println("Phoneme --------- " + p.getFeatureID() + " --------");
            Sentence sent = new Sentence();
            String nextS = "";
            Rank r = new Rank();
            ArrayList<String> generated = new ArrayList<>();
            for(int i = 0; i<3; i++){
                ArrayList<Sentence> sentences = s.generateTri(6, "3gram", true,nextS,generated,false);
                sent = r.combinedRankSentence(sentences, "Cuentos_ENG_Lem", "gramLP1.lm", s.feature,feature,generated,nextS,false);
                generated.add(sent.getSentence());
                nextS = s.nextSub(sent.getSentence());
                String pos = "determiner noun verb determiner noun punctuation";
                sent.setSentencePOS(pos);
                String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,prepsE,"");
                System.out.println(sent.getSentence() + " ----> " + sInflected);
//                System.out.println(nextS);
            } 
        
        }
    }    
        
     public void testEngPhonemes(String feature) {
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos_ENG phoneme feng a prepeng stopeng "+feature;
        s.init(datos);

        for (SeedFeature p : s.seedFeatures) {
                s.feature = p;
                s.bag = new BagW();
                s.fillBOW();
                System.out.println("Phoneme --------- " + p.getFeatureID() + " --------");
                ArrayList<Sentence> sentences = s.generateTri(16, "3gram", true,"",new ArrayList<String>(),false);
                Rank r = new Rank();
                System.out.println("Total generated sentences: "+sentences.size());
                Sentence sentence = r.combinedRankSentence(sentences, "Cuentos", "gramLP1.lm",s.feature,feature,new ArrayList<String>(),"",false);

                System.out.println(sentence.getSentence());
        
        }
    }

    public void testOver() {
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos fesp b prepesp stopesp l";
        s.init(datos);
        ArrayList<String> sentences = s.generateBi(10, "2gram", true,"");

        for (String sen : sentences) {
            System.out.println(sen);
        }

    }


    public void testRank(){
        Rank rank = new Rank();
        ArrayList<Sentence> sentences = new ArrayList<>();
        
        sentences.add(new Sentence("everybody in the book of fairy tale .",""));
        sentences.add(new Sentence("her father house .",""));
        sentences.add(new Sentence("my mother be asleep .",""));
        sentences.add(new Sentence("you have do so .",""));
        sentences.add(new Sentence("it will shine in the air like 1 bird of passage .",""));
        
        Phoneme p = new Phoneme();
        
//        String s = rank.factoredRankSentences(sentences, "Cuentos_ENG_Lem", "gramLP1.lm",p,"l");
        Sentence s = rank.combinedRankSentence(sentences, "Cuentos_ENG_Lem", "gramLL1.lm",p,"l",new ArrayList<String>(),"",false);
        System.out.println(s.getSentence());
    }
    
    
    public void testRankSpa(String feature,String file){
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos phoneme fesp b prepesp stopesp "+feature;
        s.init(datos);
        
        for (SeedFeature p : s.seedFeatures) {
            s.feature = p;
            s.bag = new BagW();
            s.fillBOW();
            System.out.println("Phoneme --------- " + p.getFeatureID() + " --------");
            ArrayList<Sentence> sentences = loadSentencesPhoneme(p.getFeatureID(), file);
            Rank r = new Rank();
            Sentence sentence = r.combinedRankSentence(sentences, "Cuentos", "gramWP1.lm",s.feature,feature,new ArrayList<String>(),"",false);
           
            System.out.println(sentence.getSentence());
        
        }
    }
    
    public void testRankEng(String feature,String file){
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos_ENG phoneme feng a prepeng stopeng "+feature;
        s.init(datos);

        for (SeedFeature p : s.seedFeatures) {
                s.feature = p;
                s.bag = new BagW();
                s.fillBOW();
                System.out.println("Phoneme --------- " + p.getFeatureID() + " --------");
                ArrayList<Sentence> sentences = loadSentencesPhoneme(p.getFeatureID(), file);
                Rank r = new Rank();
                Sentence sentence = r.combinedRankSentence(sentences, "Cuentos_ENG", "gramWP1.lm",s.feature,feature,new ArrayList<String>(),"",false);
                System.out.println(sentence.getSentence());
        
        }
    }
    
    public ArrayList<Sentence> loadSentencesPhoneme(String phon,String fich){
        ArrayList<Sentence> sentences = new ArrayList<>();
        
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String linea;
        
        try{
            archivo = new File(fich);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            
            while ((linea = br.readLine()) != null) {
                if(linea.contains("Phoneme -") && linea.contains(" "+phon+" ")){
                    while ((linea = br.readLine()) != null && !linea.contains("Phoneme -")){
                        Sentence sent = new Sentence(linea, "");
                        sentences.add(sent);
                    }
                    break;
                }
            }
            
            
        }
        catch(Exception e){
             e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        
        return sentences;
    }
    
    public void getNumberPhonemeFich(String fich, String fichphon){
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String datos = "Cuentos_LEM phoneme "+ fichphon+" a prepesp stopesp l";
        s.init(datos);
        int countWords,countWpS,numSent;
        countWords = countWpS =numSent = 0;
        
        for (SeedFeature p : s.seedFeatures) {
            s.feature = p;
//            numberPhonemes(fich, p,countWords, countWpS, numSent);
            
            File archivo = null;
            FileReader fr = null;
            BufferedReader br = null;
            String linea;
            int countW = 0;

            try{
                archivo = new File(fich);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);

                System.out.println("Phoneme ---------"+p.getFeatureID()+"----");

                while ((linea = br.readLine()) != null) {
                    if(linea.contains("Phoneme -") && linea.contains(" "+p.getFeatureID()+" ")){
                        while ((linea = br.readLine()) != null && !linea.contains("Phoneme -")){
                            countW = 0;
                            String[] words = linea.split("\\s+");
                            for(String sent: words){
                                if(p.hasFeature(sent))
                                    countW++;
                            }
                            System.out.println(linea+" ---> NumWPhoneme="+countW+"/"+words.length);
                            countWords+=countW;
                            countWpS+= words.length;
                            numSent++;
                        }
                        break;
                    }
                }


            }
            catch(Exception e){
                 e.printStackTrace();
            } finally {
                try {
                    if (null != fr) {
                        fr.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            
        } 
        
        System.out.println("Media palabras por frase="+(float)(countWpS/numSent));
        System.out.println("Media palabras con fonema por frase="+(float)(countWords/numSent));
        
    }
    
    public void numberPhonemes(String fich,SeedFeature p,int countWords,int countWpS, int numSent){
        
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String linea;
        int countW = 0;
        
        try{
            archivo = new File(fich);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            
            System.out.println("Phoneme ---------"+p.getFeatureID()+"----");
            
            while ((linea = br.readLine()) != null) {
                if(linea.contains("Phoneme -") && linea.contains(" "+p.getFeatureID()+" ")){
                    while ((linea = br.readLine()) != null && !linea.contains("Phoneme -")){
                        countW = 0;
                        String[] words = linea.split("\\s+");
                        for(String s: words){
                            if(p.hasFeature(s))
                                countW++;
                        }
                        System.out.println(linea+" ---> NumWPhoneme="+countW+"/"+words.length);
                        countWords+=countW;
                        countWpS+= words.length;
                        numSent++;
                    }
                    break;
                }
            }
            
            
        }
        catch(Exception e){
             e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void testLoadLex(){
        Lexicon l = Tools.LoadFunctions.loadLex("files/lexicons/freeling/es", "eagles", "es");
//        Lexicon l = Tools.LoadFunctions.loadLex("files/lexicons/freeling/en", "penn", "en");
        
        ArrayList<String> prepsS = LoadFunctions.loadList("files/verbPrepEs");
//        ArrayList<String> prepsE = LoadFunctions.loadList("files/verbPrepEng");
//        ArrayList<String> prashal = LoadFunctions.loadList("files/phrasalEng");
//        
//        prepsE.addAll(prashal);
        
//        String s = "qué primavera pasar varios año .";
        String s = "el madre estar aquel día.";
        String p = "determiner noun verb determiner noun punctuation";
        
//        String corpus = "Cuentos_ENG_Lem";
        String corpus = "CriticasCine";
        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS,"");
        System.out.println(s);
      
//        s = "qué cosa faltar el flor .";
//        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS);
//        System.out.println(s);
////        
//        s = "el estrella brillar su belleza .";
//        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS);
//        System.out.println(s);
////        
//        s = "qué primavera pasar varios año .";
//        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS);
//        System.out.println(s);
//        
//        
//        s = "su mujer quedar el mundo .";
//        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS);
//        System.out.println(s);
//        
//        s = "el lado yacer su rayo .";
//        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS);
//        System.out.println(s);
//        
//        s = "el flor ser el destino .";
//        s = Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS);
//        System.out.println(s);
    
        
    }
    
    public void testVerbTenseEnglish(){
        String verb = "look";
        Lexicon l = Tools.LoadFunctions.loadLex("files/lexicons/freeling/en", "penn", "en");
        
        System.out.println("infinitive: "+ Inflect.getVerbTenseEnglish(verb, "infinitive", "", l,new ArrayList<String>()));
        System.out.println("past simple: "+ Inflect.getVerbTenseEnglish(verb, "past", "1s", l,new ArrayList<String>()));
        System.out.println("present simple: "+ Inflect.getVerbTenseEnglish(verb, "present", "2s", l,new ArrayList<String>()));
        System.out.println("future simple: "+ Inflect.getVerbTenseEnglish(verb, "future", "", l,new ArrayList<String>()));
        System.out.println("past continuous: "+ Inflect.getVerbTenseEnglish(verb, "pastC", "1s", l,new ArrayList<String>()));
        System.out.println("present continuous: "+ Inflect.getVerbTenseEnglish(verb, "presentC", "1s", l,new ArrayList<String>()));
        System.out.println("future continuous: "+ Inflect.getVerbTenseEnglish(verb, "futureC", "", l,new ArrayList<String>()));
        System.out.println("past perfect: "+ Inflect.getVerbTenseEnglish(verb, "pastP", "", l,new ArrayList<String>()));
        System.out.println("present perfect: "+ Inflect.getVerbTenseEnglish(verb, "presentP", "1p", l,new ArrayList<String>()));
        System.out.println("future perfect: "+ Inflect.getVerbTenseEnglish(verb, "futureP", "", l,new ArrayList<String>()));
        System.out.println("past p.continuous: "+ Inflect.getVerbTenseEnglish(verb, "pastPC", "", l,new ArrayList<String>()));
        System.out.println("present p.continuous: "+ Inflect.getVerbTenseEnglish(verb, "presentPC", "3s", l,new ArrayList<String>()));
        System.out.println("future p.continuous: "+ Inflect.getVerbTenseEnglish(verb, "futurePC", "", l,new ArrayList<String>()));
    }
    
    public void testSentiment(int stopw,int useGram,String fich, String emosent){
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String linea = "";
        String datos = "SentimentFull sentiment "+emosent+" 3 prepeng stopeng s freeling gramen";
        int nos = 10;
        String featG = "s";
        try{
            ArrayList<String> dataExp = new ArrayList<>();
            //solo para ingles
            ArrayList<String> prepsE = LoadFunctions.loadList("files/verbPrepEng");
            ArrayList<String> prashal = LoadFunctions.loadList("files/phrasalEng");
            prepsE.addAll(prashal);
            //---------------------------------

            
            Rank r = new Rank();
            s.init(datos);
            
            if(useGram == 0){
                s.useGram = false;
            }
            
            for(SeedFeature f: s.seedFeatures){
                s.feature = f;
                s.bag = new BagW();
                if(stopw == 0)
                    s.bag.setHasStop(false);
                s.fillBOW();
                Sentiment sen = (Sentiment)f;
                //Generación de cuento
                BufferedWriter writer = new BufferedWriter(new FileWriter(fich+sen.getSentiment()+"LOG"));
                BufferedWriter writer2 = new BufferedWriter(new FileWriter(fich+sen.getSentiment()));
                Sentence sent = new Sentence();
                String nextS = "";

                ArrayList<Sentence> generated = new ArrayList<>();
                ArrayList<String> genString = new ArrayList<>();
                ArrayList<String> generatedIn = new ArrayList<>();
                ArrayList<String> genNumMacro =  new ArrayList<>();
                for(int i = 0; i<nos; i++){
                    ArrayList<Sentence> sentences = s.generateTri((!s.useGram?20:s.gram.length), s.langM, true,nextS,genString,false);
                    writer.append("Generated sentences");
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");

                    for(Sentence sentence: sentences){
                        writer.append(sentence.getSentence() + " " + Arrays.toString(sentence.getSentiment()));
                        writer.append("\n");
                    }
                    sent = r.combinedRankSentence(sentences, s.corpus, s.langM, s.feature,s.featureGen,genString,nextS,false);
                    //Lexicalizacion!!

                    generated.add(sent);
                    genString.add(sent.getSentence());
//                    if(!(aux[2].contains("h1.") || aux[2].contains("h2.")))
                        nextS = s.nextSub(sent.getSentence());
                    
//                    String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,new ArrayList<String>(),s.langM);
                    //Solo para ingles
                    if(!featG.equals("s")){
                        String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,new ArrayList<String>(),s.langM);
                        generatedIn.add(sInflected);
                        writer.append("\n");
                        writer.append(sInflected);
                    }

                    writer.append("\n");
                    writer.append(nextS);
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");
                    s.bag = s.feature.changeFeature(s.bag,s.featureGen);
                    s.gram = AuxFunctions.randGrammar(s.gramlist);
                }
                
                writer.append("\n");
                writer.append("\n");
                for(int j = 0; j< generated.size(); j++){

                     if(featG.equals("s")){
                        writer.append(generated.get(j).getSentence());
                        writer.append("\n");
                        writer2.append(generated.get(j).getSentence());
                        writer2.append("\n");
                        ArrayList<Sentence> gensent  = s.genFromSynset(generated.get(j).getSentence()); 
                        for(Sentence senaux: gensent){
                            writer.append(senaux.getSentence());
                            writer.append("\n");
                            writer2.append(senaux.getSentence());
                            writer2.append("\n");
                        } 
                     }
                     else{
                        writer.append(generatedIn.get(j));
                        writer.append("\n");
                        writer2.append(generatedIn.get(j));
                        writer2.append("\n");
                     }
                }

                writer2.close();
                
                writer.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
            
        
        
    }
    
    public String macroNumString(String cad,SeedFeaturesV2 s){
        String nmacro = "";
        
        if(!cad.equals("")){
            String[] aux2 = cad.split(" ");
            float nvimp = 0;
            if(aux2.length == 6){
                if(!aux2[2].equals("")){
                    String lemma = AuxFunctions.getSentenceInfoFreeling(aux2[2], s.corpus, s.language).get(0);
                    String[] aux3 = lemma.split(" ");
                    aux2[2] = aux3[2];
                    StringBuilder strBuilder = new StringBuilder();
                    for (int k = 0; k < aux2.length; k++) {
                       strBuilder.append(aux2[k]+" ");
                    }
                    String newString = strBuilder.toString();
                    cad = strBuilder.toString();
                    nvimp = 3;
                }
            }
            else if(aux2.length == 8){
                if(!aux2[3].equals("")){
                    String lemma = AuxFunctions.getSentenceInfoFreeling(aux2[3], s.corpus, s.language).get(0);
                    String[] aux3 = lemma.split(" ");
                    aux2[3] = aux3[2];
                    StringBuilder strBuilder = new StringBuilder();
                    for (int k = 0; k < aux2.length; k++) {
                       strBuilder.append(aux2[k]+" ");
                    }
                    String newString = strBuilder.toString();
                    cad = strBuilder.toString();
                    nvimp = 5;
                }
            }
            else{
                if(aux2.length == 7 && !aux2[2].equals("") && !aux2[3].equals("")){
                    ArrayList<String> lemma = AuxFunctions.getSentenceInfoFreeling(aux2[2]+" "+aux2[3], s.corpus, s.language);
                    String[] aux3 = lemma.get(0).split(" ");
                    aux2[2] = aux3[2];
                    aux3 = lemma.get(1).split(" ");
                    aux2[3] = aux3[2];
                    StringBuilder strBuilder = new StringBuilder();
                    for (int k = 0; k < aux2.length; k++) {
                       strBuilder.append(aux2[k]+" ");
                    }
                    String newString = strBuilder.toString();
                    cad = strBuilder.toString();
                    nvimp = 4;
                }
            }
            int ncont = s.bag.numWordsContained(cad);
            nmacro = " nWordsfromMacro:"+ncont+" of Max:"+nvimp+" per:"+(Float)(ncont/nvimp);
        }
        
        return nmacro;
    }
    
    public void obtainMacroNum(String fich){
        SeedFeaturesV2 s = new SeedFeaturesV2();
        String linea = "";
        try{
            ArrayList<String> dataExp = new ArrayList<>();
            File archivo = new File("files/Exp/"+fich);
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);

            while ((linea = br.readLine()) != null) {
                    dataExp.add(linea);
            }
            
            Rank r = new Rank();
            String[] cad = dataExp.get(0).split(";");
            s.init(cad[1]);

            for(String c: dataExp){
                cad = c.split(";");
                String[] aux = cad[1].split(" ");
                System.out.println(cad[0]);
                if(aux.length==10)
                    s.langM = aux[9]+"/";
                else
                    s.langM = "";

            
                VImpW vimp = new VImpW();
                ArrayList<String> words = new ArrayList<>();
                    
                archivo = new File("files/" + aux[2]);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
                int nos = Integer.parseInt(br.readLine());
                while ((linea = br.readLine()) != null) {
                    words.add(linea);
                }
    
                
                vimp.setSentF(words);
                s.bag = vimp.changeFeature(new BagW(),s.featureGen);
                s.feature = vimp;
                
                
                //Generación de cuento
                BufferedWriter writer = new BufferedWriter(new FileWriter("files/"+cad[0]+"_MACRO_NPER"));
                Sentence sent = new Sentence();
                String nextS = "";

                ArrayList<String> generated = new ArrayList<>();
                ArrayList<String> generatedIn = new ArrayList<>();
                
                
                ArrayList<String> sents= new ArrayList<>();
                    
                archivo = new File("files/" + cad[0]);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
                while ((linea = br.readLine()) != null) {
                    if(linea.isEmpty())
                        sents.add("");
                    else
                        sents.add(linea);
                }

                
                for(int i = 0; i<nos; i++){
                    if(!sents.get(i).equals("")){
                        String[] aux2 = sents.get(i).split(" ");
                        float nvimp = 0;
                        if(aux2.length == 6){
                            if(!aux2[2].equals("")){
                                String lemma = AuxFunctions.getSentenceInfoFreeling(aux2[2], s.corpus, s.language).get(0);
                                String[] aux3 = lemma.split(" ");
                                aux2[2] = aux3[2];
                                StringBuilder strBuilder = new StringBuilder();
                                for (int k = 0; k < aux2.length; k++) {
                                   strBuilder.append(aux2[k]+" ");
                                }
                                String newString = strBuilder.toString();
                                sents.set(i, strBuilder.toString());
                                nvimp = 3;
                            }
                        }
                        else if(aux2.length == 8){
                            if(!aux2[3].equals("")){
                                String lemma = AuxFunctions.getSentenceInfoFreeling(aux2[3], s.corpus, s.language).get(0);
                                String[] aux3 = lemma.split(" ");
                                aux2[3] = aux3[2];
                                StringBuilder strBuilder = new StringBuilder();
                                for (int k = 0; k < aux2.length; k++) {
                                   strBuilder.append(aux2[k]+" ");
                                }
                                String newString = strBuilder.toString();
                                sents.set(i, strBuilder.toString());
                                nvimp = 5;
                            }
                        }
                        else{
                            if(!aux2[2].equals("") && !aux2[3].equals("")){
                                ArrayList<String> lemma = AuxFunctions.getSentenceInfoFreeling(aux2[2]+" "+aux2[3], s.corpus, s.language);
                                String[] aux3 = lemma.get(0).split(" ");
                                aux2[2] = aux3[2];
                                aux3 = lemma.get(1).split(" ");
                                aux2[3] = aux3[2];
                                StringBuilder strBuilder = new StringBuilder();
                                for (int k = 0; k < aux2.length; k++) {
                                   strBuilder.append(aux2[k]+" ");
                                }
                                String newString = strBuilder.toString();
                                sents.set(i, strBuilder.toString());
                                nvimp = 4;
                            }
                        }
                        int ncont = s.bag.numWordsContained(sents.get(i));
                        writer.append(sents.get(i)+" nWordsfromMacro:"+ncont+" of Max:"+nvimp+" per:"+(Float)(ncont/nvimp));
                        writer.append("\n");
                        s.bag = s.feature.changeFeature(s.bag,s.featureGen);
                        
                    }
                }
                
                writer.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void testMacroEXP(int useGram,int useRel,String fich){
         SeedFeaturesV2 s = new SeedFeaturesV2();
        String linea = "";
        try{
            ArrayList<String> dataExp = new ArrayList<>();
            //solo para ingles
            ArrayList<String> prepsE = LoadFunctions.loadList("files/verbPrepEng");
            ArrayList<String> prashal = LoadFunctions.loadList("files/phrasalEng");
            prepsE.addAll(prashal);
            //---------------------------------
            File archivo = new File(fich);
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);

            while ((linea = br.readLine()) != null) {
                    dataExp.add(linea);
            }
            
            Rank r = new Rank();
            String[] cad = dataExp.get(0).split(";");
            s.init(cad[1]);
            
            if(useGram == 0){
                s.useGram = false;
            }
            
            for(String c: dataExp){
                cad = c.split(";");
                String[] aux = cad[1].split(" ");
                System.out.println(cad[0]);
                if(aux.length==10)
                    s.langM = aux[9]+"/";
                else
                    s.langM = "";

            
                VImpW vimp = new VImpW();
                ArrayList<String> words = new ArrayList<>();
                    
                archivo = new File("files/" + aux[2]);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
                int nos = Integer.parseInt(br.readLine());
                while ((linea = br.readLine()) != null) {
           
                    words.add((linea.contains("-")?linea.replace("-", "_"):linea));
                }
                
                vimp.setSentF(words);
                s.bag = vimp.changeFeature(new BagW(),s.featureGen);
                s.feature = vimp;
                
                
                //Generación de cuento
                BufferedWriter writer = new BufferedWriter(new FileWriter("files/"+cad[0]+"LOG"));
                BufferedWriter writer2 = new BufferedWriter(new FileWriter("files/"+cad[0]));
                Sentence sent = new Sentence();
                String nextS = "";
                
                ArrayList<String> generated = new ArrayList<>();
                ArrayList<String> generatedIn = new ArrayList<>();
                ArrayList<String> genNumMacro =  new ArrayList<>();
                for(int i = 0; i<nos; i++){
                    ArrayList<Sentence> sentences = s.generateTri((!s.useGram?20:s.gram.length), s.langM, true,nextS,generated,false);
                    writer.append("Generated sentences");
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");

                    for(Sentence sen: sentences){
                        writer.append(sen.getSentence());
                        writer.append("\n");
                    }
                    sent = r.combinedRankSentence(sentences, s.corpus, s.langM, s.feature,s.featureGen,generated,nextS,false);
                    //Lexicalizacion!!

                    generated.add(sent.getSentence());
                    if(useRel==1)
                        nextS = s.nextSub(sent.getSentence());
                    
//                     genNumMacro.add(macroNumString(sent.getSentence(), s));
//                    String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,new ArrayList<String>(),s.langM);
                    //Solo para ingles
//                    String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,prepsE,s.langM);
                    
                    //-----------------------
                    
                    writer.append("Selected");
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");
                    writer.append(sent.getSentence());
                    if(!s.featureGen.equals("s")){
                        String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,new ArrayList<String>(),s.langM);
                       
                        generatedIn.add(sInflected);
                        writer.append("\n");
                        writer.append(sInflected);
                    }

                    writer.append("\n");
                    writer.append(nextS);
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");
                    s.bag = s.feature.changeFeature(s.bag,s.featureGen);
                    s.gram = AuxFunctions.randGrammar(s.gramlist);
                }
                
                writer.append("\n");
                writer.append("\n");
                for(int j = 0; j< generated.size(); j++){

                     if(s.featureGen.equals("s")){
                        writer.append(generated.get(j));
                        writer.append("\n");
                        writer2.append(generated.get(j));
                        writer2.append("\n");
                        ArrayList<Sentence> gensent  = s.genFromSynset(generated.get(j)); 
                        for(Sentence senaux: gensent){
                            writer.append(senaux.getSentence());
                            writer.append("\n");
                            writer2.append(senaux.getSentence());
                            writer2.append("\n");
                        } 
                     }
                     else{
                        writer.append(generatedIn.get(j));
                        writer.append("\n");
                        writer2.append(generatedIn.get(j));
                        writer2.append("\n");
                     }
                }

                writer2.close();
                
                writer.close();
                   
//                    generatedIn.add(sInflected);
//                    genNumMacro.add(macroNumString(sInflected, s));
//                    writer.append("Selected");
//                    writer.append("\n");
//                    writer.append("-------------------");
//                    writer.append("\n");
//                    writer.append(sent.getSentence());
//                    writer.append("\n");
//                    writer.append(sInflected);
//                    writer.append("\n");
//                    writer.append(nextS);
//                    writer.append("\n");
//                    writer.append("-------------------");
//                    writer.append("\n");
//                    s.bag = s.feature.changeFeature(s.bag);
//                    s.gram = AuxFunctions.randGrammar(s.gramlist);
//                }
//                
//                writer.append("\n");
//                writer.append("\n");
//                for(int j = 0; j< generatedIn.size(); j++){
//                    writer.append(generatedIn.get(j)+genNumMacro.get(j));
//                    writer.append("\n");
//                    writer2.append(generatedIn.get(j));
//                    writer2.append("\n");
//                }
//
//                writer2.close();
//                
//                writer.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void genSentences(int useGram,int useRel,int numS,boolean noFLM,String featG,String fich){
         SeedFeaturesV2 s = new SeedFeaturesV2();
        String linea = "";
        try{
            //solo para ingles
            ArrayList<String> prepsE = LoadFunctions.loadList("files/verbPrepEng");
            ArrayList<String> prashal = LoadFunctions.loadList("files/phrasalEng");
            prepsE.addAll(prashal);
            //---------------------------------
            
            Rank r = new Rank();
            String cad = "CuentosENSynset importantW Exp/Synsets/h2.len3.s25.cuento_0_en.plmOut a prepeng stopeng " +featG+ " freeling gramen";
            s.init(cad);
            
            if(useGram == 0){
                s.useGram = false;
            }
            
            
                String[] aux = cad.split(" ");
                if(aux.length==10)
                    s.langM = aux[9]+"/";
                else
                    s.langM = "";

            
                VImpW vimp = new VImpW();
                ArrayList<String> words = new ArrayList<>();
                    
//                File archivo = new File("files/" + aux[2]);
//                FileReader fr = new FileReader(archivo);
//                BufferedReader br = new BufferedReader(fr);
//                int nos = Integer.parseInt(br.readLine());
//                while ((linea = br.readLine()) != null) {
//                    words.add(linea);
//                }
                
                
                vimp.setSentF(words);
                s.bag = vimp.changeFeature(new BagW(),s.featureGen);
                s.feature = vimp;
                
                //Generación de cuento
                BufferedWriter writer = new BufferedWriter(new FileWriter(fich+"LOG"));
                BufferedWriter writer2 = new BufferedWriter(new FileWriter(fich));
                Sentence sent = new Sentence();
                String nextS = "";
                
                ArrayList<String> generated = new ArrayList<>();
                ArrayList<String> generatedIn = new ArrayList<>();
                for(int i = 0; i<numS; i++){
                    ArrayList<Sentence> sentences = s.generateTri((!s.useGram?20:s.gram.length), s.langM, true,nextS,generated,noFLM);
                    writer.append("Generated sentences");
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");

                    for(Sentence sen: sentences){
                        writer.append(sen.getSentence());
                        writer.append("\n");
                    }
//                    sent = r.combinedRankSentence(sentences, s.corpus, s.langM, s.feature,s.featureGen,generated,nextS,noFLM);
                    //Lexicalizacion!!

                    generated.add(sentences.get(0).getSentence());
                    if(useRel==1)
                        nextS = s.nextSub(sent.getSentence());

                    writer.append("Selected");
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");
                    writer.append(sent.getSentence());
                    if(!featG.equals("s")){
                        if(noFLM)
                            sent.calcSentencePOS(s.corpus, s.lex);
                        String sInflected =  Inflect.inflectSentence(sent, s.lex, s.corpus,new ArrayList<String>(),s.langM);
                        generatedIn.add(sInflected);
                        writer.append("\n");
                        writer.append(sInflected);
                    }

                    writer.append("\n");
                    writer.append(nextS);
                    writer.append("\n");
                    writer.append("-------------------");
                    writer.append("\n");
                    s.bag = s.feature.changeFeature(s.bag,s.featureGen);
                    s.gram = AuxFunctions.randGrammar(s.gramlist);
                }
                
                writer.append("\n");
                writer.append("\n");
                for(int j = 0; j< generated.size(); j++){

                     if(featG.equals("s")){
                        writer.append(generated.get(j));
                        writer.append("\n");
                        writer2.append(generated.get(j));
                        writer2.append("\n");
                        ArrayList<Sentence> gensent  = s.genFromSynset(generated.get(j)); 
                        for(Sentence senaux: gensent){
                            writer.append(senaux.getSentence());
                            writer.append("\n");
                            writer2.append(senaux.getSentence());
                            writer2.append("\n");
                        } 
                     }
                     else{
                        writer.append(generatedIn.get(j));
                        writer.append("\n");
                        writer2.append(generatedIn.get(j));
                        writer2.append("\n");
                     }
                }

                writer2.close();
                
                writer.close();
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void inflectSeveralSent(String path, String pathout){
        
        ArrayList<String> sentences =  LoadFunctions.loadList(path);
        
        Lexicon l = Tools.LoadFunctions.loadLex("files/lexicons/freeling/es", "eagles", "es");      
        ArrayList<String> prepsS = LoadFunctions.loadList("files/verbPrepEs");

        String p = "determiner noun verb determiner noun punctuation";

        String corpus = "CuentosES";
        
        
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(pathout));

            for(String s: sentences){
                if(!s.contains("Phoneme")){
                    writer.append(Inflect.inflectSentence(new Sentence(s,p), l, corpus,prepsS,""));
                }
                else{
                    writer.append(s);
                }
                
                writer.append("\n");
            }
            
            writer.close();
            
        }catch (Exception e) {
            e.printStackTrace();
        } 
        
    }
    
    public void testWN() throws IOException{
        // construct the URL to the Wordnet dictionary directory
        
//        String path = "files/libraries/spanish_wordnet_3.0-1.0";
        String path = "files/libraries/WNdict";
        URL url = new URL("file", null , path ) ;
//        URL url2 = new URL("file", null , path2 ) ;
        
        // construct the dictionary object and open it
        IDictionary dict = new Dictionary( url ) ;
//        IDictionary dict2 = new Dictionary( url2 ); 
        dict.open () ;
//        dict2.open();
        
        // look up first sense of the word "dog "
        IIndexWord idxWord = dict.getIndexWord("dog", POS.NOUN);
        IWordID wordID = idxWord.getWordIDs().get(0) ;
        IWord word = dict.getWord( wordID ) ;

        ISynsetID s = SynsetID.parseSynsetID("SID-01079480‑V");
        ISynset syn = dict.getSynset(s);
        
        IWord w = syn.getWords().get(0);
        
        
        
        ISenseKey isen = syn.getWords().get(0).getSenseKey();
        
        
        System.out.println(w.getVerbFrames().get(0).getTemplate());
        System.out.println(w.getID().getSynsetID().toString());
        
        System.out.println ("Id = " + syn.getWords().get(0).getSenseKey());
        System.out.println (" Lemma = " + word.getLemma() ) ;
        System.out.println (" Gloss = " + word.getSynset().getGloss()) ;
        dict.close();
 
        // make a url pointing to the Verbnet data
        String pathToVerbnet = "files/libraries/new_vn" ;
        URL url2 = new URL ( "file" , null , pathToVerbnet ) ;
        // construct the index and open it
         IVerbIndex index = new VerbIndex ( url2 ) ;
        index . open () ;
        // look up a verb class and print out some info


                
        IVerbClass verb = index.getVerb("hit-18.1");
        IMember member = verb.getMembers().get(0);
        Set<IWordnetKey> keys = member.getWordnetTypes().keySet() ;
        IFrame frame = verb.getFrames().get(0) ;
        FrameType type = frame.getPrimaryType() ;
        String example = frame.getExamples().get(0) ;
        Iterator<IWordnetKey> it = keys.iterator();
        
        
        WordnetKey wk = WordnetKey.parseKey("meet%2:33:00");
        IWordnetKey iwk = wk;
        Set<IMember> m = index.getMembers(iwk);
        
        
        
        System.out.println (" id : " + verb.getID()) ;
                System.out.println (" id : " +m.iterator().next().getVerbClass().getFrames().get(0).getPrimaryType().getID()) ;
        System.out.println (" first wordnet keys : " + keys) ;
        System.out.println (" first frame type : " + type.getID()) ;
        System.out.println (" first example : " + example) ;
        
        
        
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException{
        // TODO code application logic here
        TestFunctions t = new TestFunctions();
         
        if(args[0].equals("-m")){
            t.testMacroEXP(Integer.parseInt(args[1]),Integer.parseInt(args[2]),args[3]);
        }else if(args[0].equals("-s")){
            t.testSentiment(Integer.parseInt(args[1]),Integer.parseInt(args[2]),args[3],args[4]);
        }
        else if(args[0].equals("-g")){
            t.genSentences(Integer.parseInt(args[1]),Integer.parseInt(args[2]),Integer.parseInt(args[3]),args[4].equals("1"),args[5],args[6]);
        }
        
//        t.testWN();
        
        
//        t.testLoadLex();
//          t.testVerbTenseEnglish();
        
//        t.testSpaPhonemesSeveral("l");
//        t.testEngPhonemesSeveral("l");
//        t.testRank();
//        t.getNumberPhonemeFich("files/CtesteoSROverParsear", "fesp");
//        t.getNumberPhonemeFich("files/CtesteoOverEnglish", "feng");

//          t.testMacroEXP("CDormir/ExperimentoNLDB/InputExps25_aux");
//          t.obtainMacroNum("CDormir/ExperimentoNLDB/InputExps25");
            
            

//         t.inflectSeveralSent("files/testSpaCUENTOS_Several3", "files/SpaCUENTOS_SeveralInfl");
        
        
    }
}
