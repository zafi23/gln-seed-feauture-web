/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import seedfeaturesv.pkg2.Features.Sentence;
import seedfeaturesv.pkg2.Features.Polarity;
import seedfeaturesv.pkg2.Features.Word;

/**
 *
 * @author cristina
 */
public class Inflect {
    
    public static String inflectSentence(Sentence sent, Lexicon lex, String corpus,ArrayList<String> preps,String lm){
        String infSent = "";  
        if(!sent.getSentence().isEmpty()){
            ArrayList<Sentence> sentences = new ArrayList<>();
            //encontrar sujeto y extraer caracteristicas
            ArrayList<String> sentInfo =  AuxFunctions.getSentenceInfoFreeling(sent.getSentence(), corpus, lex.getLanguage());
            ArrayList<String> subS, objS;
            subS = new ArrayList<>();
            objS = new ArrayList<>();
            HashMap<String,String> subjF, objF;
            subjF = getFeatures(sentInfo, corpus, lex.getLanguage(), "subj");
            objF = getFeatures(sentInfo, corpus, lex.getLanguage(), "obj");
            splitSentenceFeatures(sentInfo, subS, objS, corpus, lex.getLanguage());
//            subjF.put("num","P");
    //        subjF.put("tense","P");
//            subjF.put("mood","I");
    //        subjF.remove("person");
//    objF.put("num", "P");
    

            for(int i = 0; i < sent.getLength(); i++){
                ArrayList<Word> words = new ArrayList<>();
                 if(!sent.getPOSAt(i).equals("punctuation")){
                     if(subS.contains((i+1)+sent.getWordAt(i))){
                         if(sent.getPOSAt(i).equals("verb") && lex.getLanguage().equals("en")){
                             String person = "";
                             if(subjF.containsKey("person"))
                                 person+=subjF.get("person");
                             if(subjF.containsKey("num"))
                                 person+=subjF.get("num").toLowerCase();
                             words = getAllVTenseEnglish(sent.getWordAt(i), lex, person, preps);
                         }else {
                             if(!sent.getPOSAt(i).equals("verb"))
                                words = lex.getWordsFeatures(sent.getWordAt(i), sent.getPOSAt(i), subjF,preps,true);
                         }
                     }
                     else if(objS.contains((i+1)+sent.getWordAt(i))){
                         if(sent.getPOSAt(i).equals("verb") && lex.getLanguage().equals("en")){
                             String person = "";
                             if(subjF.containsKey("person"))
                                 person+=subjF.get("person");
                             if(subjF.containsKey("num"))
                                 person+=subjF.get("num").toLowerCase();
                             words = getAllVTenseEnglish(sent.getWordAt(i), lex, person, preps);
                         }else {
                             if(!sent.getPOSAt(i).equals("verb"))
                                words = lex.getWordsFeatures(sent.getWordAt(i), sent.getPOSAt(i), objF,preps,true);
                         }
                     }
                 }
                 else{                     
                        words.add(new Word(sent.getWordAt(i), sent.getPOSAt(i)));
                 }

                 //TEMPORAL, HAY QUE PONER LO DE DENERO O CON APERTIUM
                if(words.isEmpty()){
//                    if(subS.contains((i+1)+sent.getWordAt(i))){
//                        words.addAll(lex.getSimilarWordsFea(sent.getWordAt(i), sent.getPOSAt(i), subjF));
//                     }
//                     else if(objS.contains((i+1)+sent.getWordAt(i))){
//                        words.addAll(lex.getSimilarWordsFea(sent.getWordAt(i), sent.getPOSAt(i), objF));
//                     }
//                    words.addAll(lex.getSimilarWordsFea(sent.getWordAt(i), sent.getPOSAt(i), subjF));
//                    words.addAll(lex.getSimilarWordsFea(sent.getWordAt(i), sent.getPOSAt(i), objF));
                    
                    if(words.isEmpty()){
                        if(sent.getPOSAt(i).equals("verb") && lex.getLanguage().equals("en"))
                             words = getAllVTenseEnglish(sent.getWordAt(i), lex, "3", preps);
                        else
                            words.add(new Word(sent.getWordAt(i), sent.getPOSAt(i)));
                    }
                         
                } 

                if(i == 0){
                    for(Word w: words)
                        sentences.add(new Sentence(w.getWord(),w.getPosTAG()));
                }else{
                    ArrayList<Sentence> saux = new ArrayList<>();
                    for(Sentence s: sentences){
                        for(Word w: words)
                            saux.add(new Sentence(s.getSentence()+" "+w.getWord(),s.getSentencePOS()+" "+w.getPosTAG()));
                    }
                    sentences = (ArrayList<Sentence>)saux.clone();
                }

            }
            
//            for(Sentence saux: sentences){
//                saux.setSentencePOS(sent.getSentencePOS());
//            }

            //con las caracteristicas del sujeto, inflexionar el resto de cosas por separado

            //si no se encuentra una flexion en el lexicon.. acceder al programa de denero


            //¿Inflexionar para frases con distinto tiempo ?
//            Rank r = new Rank();
//            Sentence s = r.combinedRankSentence(sentences, corpus, lm,new Polarity(),"w",new ArrayList<String>(),"");
//            infSent = s.getSentence();
              infSent = sentences.get(0).getSentence();
        }
        
        return infSent;
    }
    
    public static void splitSentenceFeatures(ArrayList<String> sentInfo , ArrayList<String> subS, ArrayList<String> objS, String corpus, String lang){
        String sindex, oindex;
        sindex = oindex = "-1";
        String subjInfo = AuxFunctions.extractSubjectInfo(sentInfo, corpus, lang);
        String objInfo = AuxFunctions.extractObjectInfo(sentInfo, corpus, lang);
        String[] sInf = subjInfo.split("\\s+");
        String[] oInf = objInfo.split("\\s+");
        sindex = sInf[0];
        oindex = oInf[0];

        
        for(String s: sentInfo){
            String[] auxc = s.split("\\s+");
            if(auxc[9].equals(sindex) || auxc[0].equals(sindex) || auxc[5].contains("pos=verb"))
                subS.add(auxc[0]+auxc[2]);
            else if(auxc[9].equals(oindex) || auxc[0].equals(oindex))
                objS.add(auxc[0]+auxc[2]);
        }

        
    }
    
    //funcion de encontrar sujeto; en caso de k no se encontrarse sujeto, coger el primer sustantivo
    public static HashMap<String,String> getFeatures(ArrayList<String> sentInfo, String corpus, String language, String type){
        HashMap<String,String> subjFea =  new HashMap<>();
        String subjInfo = (type.equals("subj"))?AuxFunctions.extractSubjectInfo(sentInfo, corpus, language):AuxFunctions.extractObjectInfo(sentInfo, corpus, language);
        String[] cadInf = subjInfo.split("\\s+");
//        String posInfo = cadInf[3];
        if(!(subjInfo.isEmpty() ||subjInfo.equals(""))){
            subjFea.put("lemma", cadInf[2]);
            if(cadInf[5].contains("pos=noun|")){
                String[] info = cadInf[5].split("\\|");
                for(String s: info){
                    if(s.contains("gen=")){
                        subjFea.put("gen", String.valueOf(s.toUpperCase().charAt(4)));
                    }
                    if(s.contains("num=")){
                        subjFea.put("num", String.valueOf(s.toUpperCase().charAt(4)));
                    }
                }
                subjFea.put("person","3");
            }else if(cadInf[5].contains("pos=pronoun|") && cadInf[5].contains("type=personal")){
                String[] info = cadInf[5].split("\\|");
                if(language.equals("es")){
                    for(String s: info){
                            if(s.contains("gen=")){
                                subjFea.put("gen", String.valueOf(s.toUpperCase().charAt(4)));
                            }
                            if(s.contains("num=")){
                                subjFea.put("num", String.valueOf(s.toUpperCase().charAt(4)));
                            }
                            if(s.contains("person=")){
                                subjFea.put("person", String.valueOf(s.toUpperCase().charAt(6)));
                            }
                        }               
                }
                else if(language.equals("en")){
                    if(cadInf[2].equals("he") || cadInf[2].equals("she") || cadInf[2].equals("it"))
                        subjFea.put("person","3s");
                }
            }
        }
        
        
        return subjFea;
    }
    
    public static ArrayList<Word> getAllVTenseEnglish(String verb, Lexicon lex,String person, ArrayList<String> preps){
        ArrayList<Word> vtenses = new ArrayList<>();
//        String[] tenses = {"infinitive","past","present","future","pastC","presentC","futureC","pastP","presentP","futureP","pastPC","presentPC","futurePC"};
        String[] tenses = {"past"};
        for(String s: tenses){
            String wordF = getVerbTenseEnglish(verb, s, person, lex, preps);
            Word w = new Word(wordF,AuxFunctions.obtainPos(wordF, verb+"_"+s, lex.getLanguage()));
            vtenses.add(w);
        }
        
        return vtenses;
    }
    
    public static String getVerbTenseEnglish(String verb, String tense, String person,Lexicon lex,ArrayList<String> preps){
        String vtense = "";
        HashMap<String,String> features = new HashMap<>();
        ArrayList<Word> words = new ArrayList<>();
        
        switch(tense){
            case "infinitive":
                features.put("vform", "I");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "to "+words.get(0).getWord();
                break;
            case "past":
                features.put("vform", "Ps");
                if(person.equals("3s"))
                    features.put("person", "3");
                if(person.equals("1s"))
                    features.put("person", "1");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = words.get(0).getWord();
                break;
            case "present":
                features.put("vform", "Pe");
                if(person.equals("3s"))
                    features.put("person", "3");
                if(person.equals("1s"))
                    features.put("person", "1");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = words.get(0).getWord();
                break;
            case "future":
                features.put("vform", "I");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "will "+ words.get(0).getWord();
                break;
            case "pastC":
                features.put("vform", "G");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0){
                    if(person.contains("s"))
                        vtense = "was " + words.get(0).getWord();
                    else
                        vtense = "were " + words.get(0).getWord();               
                }
                break;
            case "presentC":
                features.put("vform", "G");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0){
                    if(person.equals("3s") || person.equals("1s")){
                        if(person.contains("1"))
                             vtense = "am " + words.get(0).getWord();
                        else
                            vtense = "is " + words.get(0).getWord();
                        
                    }else
                        vtense = "are " + words.get(0).getWord();               
                }
                break;
            case "futureC":
                features.put("vform", "G");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "will be "+ words.get(0).getWord();
                break;
            case "pastP":
                features.put("vform", "Pa");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "had "+ words.get(0).getWord();
                break;
            case "presentP":
                features.put("vform", "Pa");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0){
                    if(person.equals("3s"))
                        vtense = "has " + words.get(0).getWord();
                    else
                        vtense = "have " + words.get(0).getWord();               
                }
                break;
            case "futureP":
                features.put("vform", "Pa");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "will have "+ words.get(0).getWord();
                break;
            case "pastPC":
                features.put("vform", "G");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "had been "+ words.get(0).getWord();
                break;
            case "presentPC":
                features.put("vform", "G");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0){
                    if(person.equals("3s"))
                        vtense = "has been " + words.get(0).getWord();
                    else
                        vtense = "have been " + words.get(0).getWord();               
                }
                break;
            case "futurePC":
                features.put("vform", "G");
                words = lex.getWordsFeatures(verb, "verb", features,preps,false);
                if(words.size()>0)
                    vtense = "will have been "+ words.get(0).getWord();
                break;
        }
        
        
        return vtense;
    }
    
    public static Sentence contracSentSpanish(Sentence sent){
        String saux = "";
        String paux = "";
        for(int i = 1; i < sent.getLength(); i++){
            if(sent.getWordAt(i).equals("el") && sent.getWordAt(i-1).equals("a")){
                saux = saux.substring(0, saux.lastIndexOf(" "));
                saux+=" al";
                paux = paux.substring(0, paux.lastIndexOf(" "));
                paux+=" adposition"; 
            }
            else if(sent.getWordAt(i).equals("el") && sent.getWordAt(i-1).equals("de")){
                saux = saux.substring(0, saux.lastIndexOf(" "));
                saux+=" del";
                paux = paux.substring(0, paux.lastIndexOf(" "));
                paux+=" adposition";
            }
            else{
                if(i == 1){
                    saux+= sent.getWordAt(i-1)+" ";
                    paux+= sent.getPOSAt(i-1)+" ";
                }
                saux+= " " +sent.getWordAt(i);
                paux+= " " +sent.getPOSAt(i);
            }
            
        } 
        
        return(new Sentence(saux, paux));
    }
}
