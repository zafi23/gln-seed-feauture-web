/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.util.ArrayList;
import java.util.HashMap;
import seedfeaturesv.pkg2.Features.Word;

/**
 *
 * @author cristina
 */
public class Lexicon {
    private ArrayList<Word> words;
    private String language;

    public Lexicon() {
        words = new ArrayList<>();
        language = "";
    }

    public Lexicon(ArrayList<Word> words, String language, String path) {
        this.words = words;
        this.language = language;
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public void setWords(ArrayList<Word> words) {
        this.words = words;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public void addWord(Word w){
//        if(!words.contains(w))
            words.add(w);
    }
    
    public ArrayList<Word> getWordsPOS(String pos,int max){
        ArrayList<Word> wpos = new ArrayList<>();
        int count = 0;
        
        for(Word w: words){
            if(w.getPosTAG().equals(pos)){
                wpos.add(w);
                if(max!=-1)
                    count++;
                if(count == max)
                    break;
            }
        }
        
        return wpos;
    }
    
    
    
    //Busqueda por lema
    public ArrayList<Word> getWordsfromLemaPOS(String lema, String pos){
        ArrayList<Word> lemaW = new ArrayList<>();
        
        for(Word w:words){
            if(lema.equals(w.getInfo().getLemma()) && pos.equals(w.getPosTAG()))
                lemaW.add(w);
        }
        
        return lemaW;
    }
    
    //Busqueda por caracteristicas y lema
    public ArrayList<Word> getWordsFeatures(String word, String pos, HashMap<String,String> features,ArrayList<String> preps,Boolean cpos){
        ArrayList<Word> lemaW = new ArrayList<>();
        ArrayList<String> lemaWprep = new ArrayList<>();
        String lema = AuxFunctions.obtainLemma(word, language);
        for(Word w:words){
            if(w.getInfo().haveFeatures(pos, lema, features)){
                if(pos.equals("verb")){
//                   lemaWprep = AuxFunctions.getPreps(word, preps);
//                    if(!lemaWprep.isEmpty()){
//                        for(String s: lemaWprep){
//                            Word w2 = new Word(w);
//                            w2.setWord(w.getWord()+" "+s);
//                            if(cpos)
//                                w2.setPosTAG(w2.getPosTAG()+AuxFunctions.obtainPos(s, s+"_"+pos, language));
//                            lemaW.add(w2);
//                        }
//                    }
//                    else
                        lemaW.add(w); 
                }
                else
                    lemaW.add(w);
                
            }
        }
        
        return lemaW;
    }
    
    public ArrayList<Word> getSimilarWordsFea(String word,String pos,HashMap<String,String> features){
        int max = 5;
        int num = 0;
        ArrayList<Word> lemaW = new ArrayList<>();
        ArrayList<Word> wpos = getWordsPOS(pos,-1);
        ArrayList<String> infow = AuxFunctions.getSentenceInfoFreeling(word, word+"_"+pos, language);
        Word wd = (language.equals("es")?LoadFunctions.disEAGLES(infow.get(0).substring(2), 3):LoadFunctions.disPENN(infow.get(0).substring(2),language, 3));
        HashMap f = (HashMap)features.clone();
        if(wd.getInfo().getMorphFeatures().containsKey("type"))
            f.put("type", wd.getInfo().getMorphFeatures().get("type"));
        if(!pos.equals("verb"))
                f.remove("person");
        for(Word w:wpos){
            if(w.getInfo().similarFeatures(f) && num<max){
               lemaW.add(w);
               num++;
            }else if(num == max)
                break;
        }
        return lemaW;
    }
    
}
