/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import java.util.ArrayList;
import seedfeaturesv.pkg2.BagW;

/**
 *
 * @author musi
 */
public class Polarity extends SeedFeature {
    private String polarity;
    private ArrayList<String> polarityWords;
    private ArrayList<String> notDesirable;
    
    public Polarity(){
        polarity = "";
        polarityWords = new ArrayList<>();
        notDesirable = new ArrayList<>();
    }
    
    public void setPolarity(String p){
        polarity = p;
    }
    
    public void setPolWords(ArrayList<String> p){
        polarityWords = p;
    }

    public void setNotDesirable(ArrayList<String> p) {
        notDesirable = p;
    }
    
    
    
    @Override
    public Boolean hasFeature(String p){
        return polarityWords.contains(p.toLowerCase());
    }
    
    @Override
    public String getFeatureID(){
        return polarity;
    }
    
    @Override
    public String getFilterCrit(){
        return "";
    }
    
    @Override
    public Boolean wordNotDesirable(String word){
        return notDesirable.contains(word);
    }
    
      @Override
    public BagW changeFeature(BagW bag,String featGen){
        return bag;
    }

}
