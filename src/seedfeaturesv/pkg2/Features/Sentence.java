/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import Tools.AuxFunctions;
import Tools.Lexicon;
import Tools.LoadFunctions;
import java.util.ArrayList;

/**
 *
 * @author cristina
 */
public class Sentence {
    private String sentence;
    private String sentencePOS;
    private int length;
    private int[] sentiment;
    //Almacenar info del analisis de freeling??

    public Sentence() {
        sentence = "";
        sentencePOS = "";
        length = 0;
        sentiment = new int[6];
    }

    public Sentence(String sentence, String sentencePOS) {
        this.sentence = sentence;
        this.sentencePOS = sentencePOS;
        String[] p = sentence.split("\\s+");
        this.length = p.length;
        sentiment = new int[6];
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getSentencePOS() {
        return sentencePOS;
    }

    public void setSentencePOS(String sentencePOS) {
        this.sentencePOS = sentencePOS;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int[] getSentiment() {
        return sentiment;
    }

    public void setSentiment(int[] sentiment) {
        this.sentiment = sentiment;
    }
    
    public String getWordAt(int index){
        String[] s = sentence.split("\\s+");
        return s[index];
    }
    
    public String getPOSAt(int index){
        String[] s = sentencePOS.split("\\s+");
        return s[index];
    }
    
    public void addSentiment(int id){
        if(id>=0 && id<sentiment.length)
            sentiment[id]++;
    }
    
    public void calcSentencePOS(String corpus, Lexicon lex){
        if(sentence.contains("'"))
            sentence = sentence.replace("'", "\\'");
        
        ArrayList<String> sentInfo =  AuxFunctions.getSentenceInfoFreeling(sentence, corpus, lex.getLanguage());
        
        sentencePOS = "";
        
        for(int i = 0; i < sentInfo.size(); i++){
            Word w = (lex.getLanguage().equals("es")?LoadFunctions.disEAGLES(sentInfo.get(i),3):LoadFunctions.disPENN(sentInfo.get(i), lex.getLanguage(), 3)); 
            sentencePOS+=(i == sentInfo.size()-1?w.getPosTAG():w.getPosTAG()+" ");
        }
        
    }
    
    
}
