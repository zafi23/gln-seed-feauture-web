/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import java.util.Objects;


/**
 *
 * @author musi
 */
public class Word {
    
    private String word;
    private WordInfo info;
    private SeedFeature feature;
    private Float frec;
    
    public Word()
    {
        word = "";
        frec = 0f;
        feature = null; 
        info = new WordInfo();
    }
    
    public Word(String p, Float f)
    {
        word = p;
        frec = f;
    }
    
    public Word(String p, Float f, SeedFeature fea, String pos)
    {
        word = p;
        frec = f;
        feature = fea;
        info = new WordInfo();
        info.setPosTAG(pos);
    }
    
    public Word(String p, String pos)
    {
        word = p;
        info = new WordInfo();
        info.setPosTAG(pos);
    }
    
    public Word(Word w){
        word = w.word;
        frec = w.frec;
        feature = w.feature;
        info = w.info;
    }
    
    public String getWord()
    {
        return word;
    }
            
    public Float getFrequency()
    {
        return frec;
    }
    
    public void setWord(String p)
    {
        word = p;
    }
    
    public void setFrequency(Float f)
    {
        frec = f;
    }
    
     public SeedFeature getFeature() {
        return feature;
    }

    public void setFeature(SeedFeature seed) {
        this.feature = seed;
    }

    public String getPosTAG() {
        return info.getPosTAG();
    }

    public void setPosTAG(String posTAG) {
        this.info.setPosTAG(posTAG);
    }

    public WordInfo getInfo() {
        return info;
    }

    public void setInfo(WordInfo info) {
        this.info = info;
    }
   
    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Word))return false;
        Word w = (Word) other;
        if(word.equals(w.word) && frec == w.frec && info.equals(w.info)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.word);
        hash = 79 * hash + Objects.hashCode(this.info);
        hash = 79 * hash + Objects.hashCode(this.frec);;
        return hash;
    }
    
    
}
