/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import seedfeaturesv.pkg2.BagW;

/**
 *
 * @author musi
 */
public abstract class SeedFeature {
    
    abstract public Boolean hasFeature(String p);
    abstract public String getFeatureID();
    abstract public String getFilterCrit();
    abstract public Boolean wordNotDesirable(String word);
    abstract public BagW changeFeature(BagW bag,String featGen);
}
