/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import java.util.ArrayList;
import seedfeaturesv.pkg2.BagW;

/**
 *
 * @author cristina
 */
public class Sentiment extends SeedFeature{
    private String type;
    private String sentiment;
    private int id;
    private ArrayList<String> wordsSentiment;

    public Sentiment(){
        this.sentiment = "";
        this.id = 6;
        this.wordsSentiment = new ArrayList<>();
        this.type = "";  
    }
    
    
    public Sentiment(String sentiment, int id, ArrayList<String> wordsSentiment, String type) {
        this.sentiment = sentiment;
        this.id = id;
        this.wordsSentiment = wordsSentiment;
        this.type = type;
    }

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<String> getWordsSentiment() {
        return wordsSentiment;
    }

    public void setWordsSentiment(ArrayList<String> wordsSentiment) {
        this.wordsSentiment = wordsSentiment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
     @Override
    public Boolean hasFeature(String p){
        return wordsSentiment.contains(p.toLowerCase());
    }
    
    @Override
    public String getFeatureID(){
        return type;
    }
    
    @Override
    public String getFilterCrit(){
        return "";
    }
    
    @Override
    public Boolean wordNotDesirable(String word){
        return false;
    }
    
      @Override
    public BagW changeFeature(BagW bag,String featGen){
        return bag;
    }
    
    
}
