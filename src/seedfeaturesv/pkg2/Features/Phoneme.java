/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package seedfeaturesv.pkg2.Features;

import java.util.ArrayList;
import seedfeaturesv.pkg2.BagW;


/**
 *
 * @author cristina
 */
public class Phoneme extends SeedFeature{
    
    private String phoneme;
    private ArrayList<Letter> letters;
    
    public Phoneme()
    {
        phoneme = "";
        letters = new ArrayList<>();
    }
    
    public void setFonema(String f)
    {
        phoneme = f;
    }
    
    public void setLetras(ArrayList<Letter> a)
    {
        letters = a;
    }
    
    @Override
    public String getFeatureID()
    {
        return phoneme;
    }
    
    public ArrayList<Letter> getLetras()
    {
        return letters;
    }
    
    @Override
    public Boolean hasFeature(String p)
    {
        Boolean contain = false;
        
        for(Letter l: letters)
        {
            if(l.getRemaining())
            {
                for(int i = 0; i < l.getNext().size(); i++)
                {
                    char c = (p.indexOf(l.getLetter())== p.length()-1) ?'0': p.charAt(p.indexOf(l.getLetter())+1) ;
              
                    if(l.getNext().get(i).equals("con") && isConsonant(c))
                    {
                        contain = true;
                        break;
                    }
                    else if(p.contains(l.getLetter()+l.getNext().get(i)))
                    {
                        contain = true;
                        break;
                    }
                    
                    
                }
            }
            else if(p.contains(l.getLetter()))
            {
                contain = true;
                break;
            }
        }
        
        return contain;
    }
    
    @Override
    public String getFilterCrit(){
        String crit = " | grep ";
        
        for (int i = 0; i < letters.size(); i++) {
                crit += " -e \"" + letters.get(i).getLetter() + "\"";
            }
        
        return crit;
    }
    
    @Override
    public Boolean wordNotDesirable(String word){
        return false;
    }
    
    
    public  boolean isConsonant(char c){
        String cons = "bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ";
        return (cons.indexOf(c)>0);
    }
    
    @Override
    public BagW changeFeature(BagW bag,String featGen){
        return bag;
    }

    
}
