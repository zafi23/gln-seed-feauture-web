/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 *
 * @author cristina
 */
public class WordInfo {
    private String lemma;
    private String posTAG;
    private HashMap<String, String> morphFeatures;

    public WordInfo() {
        lemma = "";
        posTAG = "";
        morphFeatures = new HashMap<>();
    }

    public WordInfo(String lemma, String posTAG, HashMap<String, String> morphFeatures) {
        this.lemma = lemma;
        this.posTAG = posTAG;
        this.morphFeatures = morphFeatures;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getPosTAG() {
        return posTAG;
    }

    public void setPosTAG(String posTAG) {
        this.posTAG = posTAG;
    }

    public HashMap<String, String> getMorphFeatures() {
        return morphFeatures;
    }

    public void setMorphFeatures(HashMap<String, String> morphFeatures) {
        this.morphFeatures = morphFeatures;
    }
    
    public String getWordFeature(String pos, String morphF){
        String feature = "";
        
        if(posTAG.equals(pos)){
            feature = morphFeatures.get(morphF);
        }
        
        return feature;
    }
    
    public Boolean haveFeatures(String pos, String lema,HashMap features){
        Boolean haveF = true;
        HashMap f = (HashMap)features.clone();
        if(posTAG.equals(pos) && lemma.equals(lema)){
            if(!pos.equals("verb"))
                f.remove("person");
            else
                f.put("gen", "0");
            Iterator it = f.entrySet().iterator();
            while (it.hasNext() && haveF) {
            Map.Entry e = (Map.Entry)it.next();
            String key = (String)e.getKey();
            String value = (String)e.getValue();
            
               if(morphFeatures.containsKey(key) && !morphFeatures.get(key).equals(value)){
                   haveF = false;
                   if(key.equals("gen")&& ((value.equals("C") || morphFeatures.get(key).equals("C"))))
                       haveF = true;
                   if(key.equals("num")&& ((value.equals("N") || morphFeatures.get(key).equals("N"))))
                       haveF = true;
                   
               } 
               if(pos.equals("verb")){
                   if(key.equals("person")){
                        if((morphFeatures.containsKey("person2")) &&morphFeatures.get("person2").equals(value))
                            haveF = true; 
                   } 
                   if(morphFeatures.containsKey("person") && !f.containsKey("person"))
                       haveF = false;
               }
                   
            }
        }
        else
            haveF = false;

        return haveF;
    }
    
    public Boolean similarFeatures(HashMap features){
        Boolean haveF = true;
        HashMap f = (HashMap)features.clone();
        Iterator it = f.entrySet().iterator();
        while (it.hasNext() && haveF) {
            Map.Entry e = (Map.Entry)it.next();
            String key = (String)e.getKey();
            String value = (String)e.getValue();

            if(morphFeatures.containsKey(key) && !morphFeatures.get(key).equals(value)){
                haveF = false;
                if(key.equals("gen")&& ((value.equals("C") || morphFeatures.get(key).equals("C"))))
                    haveF = true;
                if(key.equals("num")&& ((value.equals("N") || morphFeatures.get(key).equals("N"))))
                    haveF = true;

            } 
        }


        return haveF;
    }
    
    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Word))return false;
        WordInfo w = (WordInfo) other;
        if(lemma.equals(w.lemma) && posTAG.equals(w.posTAG)){
            Iterator it = w.morphFeatures.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry)it.next();
                if(!morphFeatures.get(e.getKey()).equals(e.getValue()))
                    return false;
            }
            return true;
        }
        return false;
    }   

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.lemma);
        hash = 89 * hash + Objects.hashCode(this.posTAG);
        hash = 89 * hash + Objects.hashCode(this.morphFeatures);
        return hash;
    }
    
    
}
