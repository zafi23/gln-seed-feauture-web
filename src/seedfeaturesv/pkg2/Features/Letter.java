/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package seedfeaturesv.pkg2.Features;
import java.util.ArrayList;
import java.util.Arrays;


/**
 *
 * @author cristina
 */
public class Letter {
    private String letter;
    private ArrayList next;
    private Boolean remaining;
    
    public Letter()
    {
        letter = "";
        next = new ArrayList();
        remaining = false;
    }
    
    public Letter(String l)
    {
        String[] cadAux = l.split("_");
                   
        if(cadAux.length > 1)
        {
            next = new ArrayList();
            String[] sigAux = cadAux[1].split(",");
            next.addAll(Arrays.asList(sigAux));

            letter = cadAux[0];
            remaining = true;
        }
        else
        {
            letter = l;
            remaining = false;
        }
    }
    
    public void setLetter(String f)
    {
        letter = f;
    }
    
    public void setNext(ArrayList a)
    {
        next = a;
    }
    
    public void setRemaining(Boolean r)
    {
        remaining = r;
    }
    
    public String getLetter()
    {
        return letter;
    }
    
    public ArrayList getNext()
    {
        return next;
    }
    
    public Boolean getRemaining()
    {
        return remaining;
    }
    
}
