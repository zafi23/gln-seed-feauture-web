/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2.Features;

import Tools.LoadFunctions;
import java.util.ArrayList;
import seedfeaturesv.pkg2.BagW;

/**
 *
 * @author cristina
 * VeryImportantWords class
 */
public class VImpW extends SeedFeature{
    
    private String id = "VIW";
    private ArrayList<String> viw;
    private ArrayList<String> sentF;
    private int nsent;
    
    public VImpW(){
        id = "";
        nsent = -1;
        viw = new ArrayList<>();
        sentF =  new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getViw() {
        return viw;
    }

    public void setViw(ArrayList<String> viw) {
        this.viw = viw;
    }

    public ArrayList<String> getSentF() {
        return sentF;
    }

    public void setSentF(ArrayList<String> sentF) {
        this.sentF = sentF;
    }

    public int getNsent() {
        return nsent;
    }

    public void setNsent(int nsent) {
        this.nsent = nsent;
    }

    
    
    @Override
    public Boolean hasFeature(String p){
        return viw.contains(p.toLowerCase());
    }
    
    @Override
    public String getFeatureID(){
        return id;
    }
    
    @Override
    public String getFilterCrit(){
        return "";
    }
    
    @Override
    public Boolean wordNotDesirable(String word){
        return false;
    }
    
    @Override
    public BagW changeFeature(BagW bag,String featGen){
        if(nsent < sentF.size()-1){
            BagW b = new BagW();
            ArrayList<Word> words = new ArrayList<>();
            nsent++;
            String[] featTg = sentF.get(nsent).split(":");
            String[] feat = featTg[1].split(";");
            for(String s:feat){
                String[] cad = s.split(",");
                viw.add(cad[0]);
            }
            for(String s:feat){
                String[] cad = s.split(",");
                Word w = (cad[0].contains("|")? new Word(cad[0],cad[0].split("\\|")[0]):LoadFunctions.disPENN(cad[0]+" "+cad[0]+" "+cad[1], "en", 2));
                w.setFeature(this);
                if(cad.length==3)
                    w.setFrequency(Float.parseFloat(cad[2]));
                if(featGen.equals("s")){
                    w.setWord(w.getPosTAG()+"|"+w.getWord());
                }
                
                if(!w.getWord().equals("verb|02604760_v"))
                    b.addWord(w);
                
            }
            return b;
        }
        else
            return bag;
    }

}
