/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2;

import Tools.AuxFunctions;
import Tools.Inflect;
import Tools.Lexicon;
import Tools.LoadFunctions;
import Tools.Rank;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.SynsetID;
import seedfeaturesv.pkg2.Features.Phoneme;
import seedfeaturesv.pkg2.Features.Word;
import seedfeaturesv.pkg2.Features.Letter;
import seedfeaturesv.pkg2.Features.SeedFeature;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import seedfeaturesv.pkg2.Features.Sentence;
import seedfeaturesv.pkg2.Features.Polarity;
import seedfeaturesv.pkg2.Features.Sentiment;
import seedfeaturesv.pkg2.Features.VImpW;

/**
 *
 * @author cristina
 */
public class SeedFeaturesV2 {

    public ArrayList<SeedFeature> seedFeatures;
    public BagW bag;
    public Lexicon lex;
    public ArrayList<String> prepos;
    public ArrayList<String> stopw;
    public String corpus;
    public SeedFeature feature;
    public String featureGen;
    public String language;
    public String[] gram;
    public ArrayList<String> gramlist;
    public String langM;
    public Boolean useGram;
    
    public void init(String datos) {
        try {
            String[] s = datos.split("\\s+");
            corpus = s[0];
            String featureType = s[1];
            seedFeatures = new ArrayList<>();
            bag = new BagW();
            prepos = new ArrayList<>();
            stopw = new ArrayList<>();
            gramlist = new ArrayList<>();
            useGram = true;
            String feat = s[3];
            String fich = "files/" + s[2];
            String fichPre = "files/" + s[4];
            String fichStop = "files/" + s[5];
            String fichGram = "files/" + s[8];
            
            AuxFunctions.fstpEn.load("files/apAnalyser/en-es.automorf.bin");
            AuxFunctions.fstpSp.load("files/apAnalyser/es-en.automorf.bin");
            
            if(s.length==10)
                langM = s[9]+"/";
            else
                langM = "";
            
            if(s[4].contains("esp"))
                language = "es";
            else
                language = "en";
            featureGen = s[6];
            String lexPath  = "files/lexicons/"+s[7]+"/"+language+"/";
            File archivo = null;
            FileReader fr = null;
            BufferedReader br = null;
            String linea;
            try {
                
                archivo = new File(fichPre);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
                
                while ((linea = br.readLine()) != null) {
                    prepos.add(linea);
                }
                br.close();
                fr.close();
                
                archivo = new File(fichStop);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
                
                while ((linea = br.readLine()) != null) {
                    stopw.add(linea);
                }
                
                br.close();
                fr.close();
                
                switch(featureType){
                    case "phoneme":
                        archivo = new File(fich);
                        fr = new FileReader(archivo);
                        br = new BufferedReader(fr);
                        
                        
                        while ((linea = br.readLine()) != null) {
                            String[] cad = linea.split("\\s+");
                            Phoneme f = new Phoneme();
                            
                            String fonem = cad[0];
                            ArrayList<Letter> lets = new ArrayList<>();
                            
                            for (int i = 1; i < cad.length; i++) {
                                Letter l = new Letter(cad[i]);
                                
                                lets.add(l);
                            }
                            
                            f.setFonema(fonem);
                            f.setLetras(lets);
                            
                            if (fonem.equals(feat)) {
                                feature = new Phoneme();
                                feature = f;
                            }
                            
                            seedFeatures.add(f);
                        }
                        
                        br.close();
                        fr.close();
                        
                        fillBOW();
                        break;
                    case "polarity":
                        Polarity sent = new Polarity();
                        String[] pols = feat.split("-");
                        
                        sent.setPolarity(pols[0]);
                        ArrayList<String> sentWords = new ArrayList<>();
                        ArrayList<String> noSentWords = new ArrayList<>();
                        
                        if(s[2].contains(";")){
                            String[] fichs = s[2].split(";");
                            String fichSent = "files/"+fichs[0];
                            String fichNoSent = "files/"+fichs[1];
                            
                            archivo = new File(fichSent);
                            fr = new FileReader(archivo);
                            br = new BufferedReader(fr);
                            
                            while ((linea = br.readLine()) != null) {
                                if(!linea.contains(";") && !linea.isEmpty()){
                                    sentWords.add(linea);
                                }
                            }
                            
                            br.close();
                            fr.close();
                            
                            archivo = new File(fichNoSent);
                            fr = new FileReader(archivo);
                            br = new BufferedReader(fr);
                            
                            while ((linea = br.readLine()) != null) {
                                if(!linea.contains(";") && !linea.isEmpty()){
                                    noSentWords.add(linea);
                                }
                            }
                            
                            br.close();
                            fr.close();
                            
                        }
                        else{
                            fillWordsSentimentXML(fich, pols, sentWords, noSentWords);
                        }
                        
                        sent.setPolWords(sentWords);
                        sent.setNotDesirable(noSentWords);
                        
                        feature = sent;
                        fillBOW();
                        break;
                    case "importantW":
                        VImpW vimp = new VImpW();
                        ArrayList<String> words = new ArrayList<>();
                        
                        archivo = new File(fich);
                        fr = new FileReader(archivo);
                        br = new BufferedReader(fr);
                        linea = br.readLine();
                        while ((linea = br.readLine()) != null) {
                            words.add(linea);
                        }
                        br.close();
                        fr.close();
                        vimp.setSentF(words);
                        bag = vimp.changeFeature(new BagW(),featureGen);
                        feature = vimp;
                        break;
                    case "sentiment":
                        archivo = new File(fich);
                        fr = new FileReader(archivo);
                        br = new BufferedReader(fr);
                        ArrayList<ArrayList<String>> sentiments = new ArrayList<>();
                        ArrayList<Sentiment> arSents = new ArrayList<>();
                        
                        linea = br.readLine();
                        String[] cad = linea.split(";");
                        for(int i = 1; i < cad.length;i++){
                            ArrayList<String> sentAux = new ArrayList<>();
                            sentiments.add(sentAux);                            
                            Sentiment sentiment = new Sentiment(cad[i], i-1, new ArrayList<String>(), "sentiment");
                            arSents.add(sentiment);
                        }
                        
                        
                        while ((linea = br.readLine()) != null) {
                            cad = linea.split(";");
                            for(int i = 1; i < cad.length;i++){
                                if(cad[i].equals("1"))
                                    sentiments.get(i-1).add(cad[0]);
                            }
                        }
                        
                        for(int i = 0; i < sentiments.size(); i++){
                            arSents.get(i).setWordsSentiment(sentiments.get(i));
                            seedFeatures.add(arSents.get(i));
                            if(arSents.get(i).getId() == Integer.parseInt(feat))
                                feature = arSents.get(i);
                        }
                        
                        fillBOW();
                        br.close();
                        fr.close();
                        break;
                }
                
                
                
                archivo = new File(fichGram);
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
                
                while ((linea = br.readLine()) != null) {
                    gramlist.add(linea);
                }
                
                br.close();
                fr.close();
                gram = AuxFunctions.randGrammar(gramlist);
                
                lex = LoadFunctions.loadLex(lexPath,(language.equals("es"))?"eagles":"penn", language);
                
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (null != fr) {
                        fr.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(SeedFeaturesV2.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void fillWordsSentimentXML(String fich, String[] pols, ArrayList<String> sentWords, ArrayList<String> noSentWords){
        
        try{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(fich));
            Element document = doc.getDocumentElement();
            NodeList nodeList1 = document.getElementsByTagName(pols[0]);
            for(int x=0,size= nodeList1.getLength(); x<size; x++) {
                Node node = nodeList1.item(x);
                Element e = (Element) node;
                NodeList nodeList = e.getElementsByTagName("lemma");

                for(int i=0, size2 = nodeList.getLength(); i< size2; i++){

                sentWords.add(nodeList.item(i).getChildNodes().item(0)
                                                .getNodeValue().replaceAll("\\s+",""));
                }
            }

            nodeList1 = document.getElementsByTagName(pols[1]);
            for(int x=0,size= nodeList1.getLength(); x<size; x++) {
                Node node = nodeList1.item(x);
                Element e = (Element) node;
                NodeList nodeList = e.getElementsByTagName("lemma");

                for(int i=0, size2 = nodeList.getLength(); i< size2; i++){

                noSentWords.add(nodeList.item(i).getChildNodes().item(0)
                                                .getNodeValue().replaceAll("\\s+",""));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
                                
    }
    
    
    
    public void fillBOW() {
        try {

            String comando = " cat files/corpus/" + corpus + "/corpus/*  | tr -s \" \" \"\\n\"";

            comando+= feature.getFilterCrit();

            comando += " | tr '[:upper:]' '[:lower:]'| sort | uniq -i -c";

            String[] cmd = {
                "/bin/sh",
                "-c",
                comando
            };

            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String s = stdInput.readLine();
            int index = 0;
            String[] caux0 = s.split("\\s+");
            String[] caux = caux0[2].split(":");
            for(int i = 0; i < caux.length; i++ ){
                if(caux[i].contains(featureGen+"-")){
                    index = i;
                    break;
                }
            }
            
            
            do{
           
                String[] cad = s.split("\\s+");
                String[] cadAux = cad[2].split(":");
                String[] cadAux2 = cadAux[index].split("-");
                String[] cadAux3 = cadAux[0].split("-");
                String cadena = "";
                if(cadAux2.length==2)
                    cadena = cadAux2[1];
                Float frec = Float.parseFloat(cad[1]);
                String pos = cadAux3[1];

//                && !stopw.contains(cadena.toLowerCase())
                if (feature.hasFeature(cadena) && !(!bag.hasStop() && stopw.contains(cadena.toLowerCase()))) {
                    Word p = new Word(cadena, frec, feature, pos);

                    bag.addWord(p);
                }

            } while ((s = stdInput.readLine()) != null); 
            stdInput.close();
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }



    public ArrayList<String> generateBi(int length, String lm, Boolean overG,String sub) {
        ArrayList<String> output = new ArrayList<>();
        ArrayList<String> lastWord = new ArrayList<>();
        ArrayList<String> lastPosTag = new ArrayList<>();
        String[] wordCod = new String[2];

        for (int i = 0; i < length; i++) {

            if (i == 0) {

                if (overG) {
                    selectWordsOver(lastWord, lastPosTag, lm, 1,sub,false);
                    output = (ArrayList<String>) lastWord.clone();
                } else {
                    wordCod = nextWord("<s>", "", lm, "", 1, false,sub,i,new ArrayList<String>());
                    lastWord.add(wordCod[1]);
                    lastPosTag.add("p-" + wordCod[0]);
                    output.add(lastWord.get(0));
                }

            } else {

                for (int j = 0; j < lastWord.size(); j++) {
                    if (!(lastWord.get(j).equals("</s>") || lastWord.get(j).isEmpty() || lastWord.get(j).equals("."))) {
                        wordCod = nextWord(featureGen + "-" + lastWord.get(j), "p-" + lastPosTag.get(j), lm, output.get(j), 1, (i == length - 1),sub,i,new ArrayList<String>());

                        if (!wordCod[1].equals("")) {
                            lastPosTag.set(j, wordCod[0]);
                            String sf = output.get(j) + " " + wordCod[1];
                            output.set(j, sf);
                            String[] aux = output.get(j).split("\\s+");
                            lastWord.set(j, aux[aux.length - 1]);
                        } else {
                            lastWord.set(j, "");
                        }
                    }
                }

            }

        }

        return output;
    }

    public ArrayList<Sentence> generateTri(int length, String lm, Boolean overG, String sub, ArrayList<String> lastSentences,boolean noFLM) {
        ArrayList<Sentence> outputSent = new ArrayList<>();
        ArrayList<String> output = new ArrayList<>();
        ArrayList<String> outputPos = new ArrayList<>();
        ArrayList<String> lastWord = new ArrayList<>();
        ArrayList<String> lastPosTag = new ArrayList<>();
        String[] wordCod = new String[2];
        String[] poslw = new String[3];
        poslw[0] = poslw[1] = poslw [2] = "";
        

        for (int i = 0; i < length; i++) {
            if(!noFLM){
                if (i == 0) {

                    if (overG) {
                        selectWordsOver(lastWord, lastPosTag, lm+"2gram", 1,sub,noFLM);
                        output = (ArrayList<String>) lastWord.clone();
                        outputPos = (ArrayList<String>) lastPosTag.clone();
                        //anyadir los p- y featureGen+"-"
                        for (int k = 0; k < lastWord.size(); k++) {
                            lastWord.set(k, "<s> " + featureGen + "-" + lastWord.get(k));
                            lastPosTag.set(k, "<s> p-" + lastPosTag.get(k));
                        }
                    } else {
                        wordCod = nextWord("<s>", "", lm+"2gram", "", 1, false,sub,i,lastSentences);
                        lastWord.add("<s> " + featureGen + "-" + wordCod[1]);
                        lastPosTag.add("<s> p-" + wordCod[0]);
                        output.add(wordCod[1]);
                        outputPos.add(wordCod[0]);
                    }

                } else {
    //                if(i == 5)
    //                    System.out.println("");

                    for (int j = 0; j < lastWord.size(); j++) { 
                        if (!(lastWord.get(j).contains("</s>") || lastWord.get(j).isEmpty() || lastWord.get(j).contains("."))) {
                            ArrayList<String> allWords = new ArrayList<>();
                            allWords.addAll(lastSentences);
                            allWords.addAll(output);
//                            wordCod = nextWord(lastWord.get(j), lastPosTag.get(j), lm+"3gram", output.get(j), 2, (i == length - 1),sub,i,lastSentences);
                             wordCod = nextWord(lastWord.get(j), lastPosTag.get(j), lm+"3gram", output.get(j), 2, (i == length - 1),sub,i,allWords);

                            if (!wordCod[1].equals("")) {
                                String sf = output.get(j) + " " + wordCod[1];
                                String sp = outputPos.get(j) + " " + wordCod[0];
                                output.set(j, sf);
                                outputPos.set(j, sp);
                                String[] aux = output.get(j).split("\\s+");
                                String[] aux2 = lastPosTag.get(j).split("\\s+");
                                lastWord.set(j, featureGen + "-" + aux[aux.length - 2] + " " + featureGen + "-" + aux[aux.length - 1]);
                                lastPosTag.set(j, aux2[aux2.length - 1] + " p-" + wordCod[0]);

                            } else {
                                lastWord.set(j, "");
                            }
                        }
                    }

                }
            }
            else{
                
                //Para cuando no se usa FLM
                if (i == 0) {
                    //falta ajustar esto
                    if (overG) {
                        selectWordsOver(lastWord, lastPosTag, lm+"2gram", 1,sub,noFLM);
                        output = (ArrayList<String>) lastWord.clone();
                        for (int k = 0; k < lastWord.size(); k++) {
                            lastWord.set(k, "<s> " + featureGen + "-" + lastWord.get(k));
                        }
                    } else {
                        wordCod = searchLM("<s>", "", lm+"2gram", "" ,1 , (i == length - 1), poslw, 0, false,sub,i,lastSentences,noFLM);
//                        wordCod = nextWord("<s>", "", lm+"2gram", "", 1, false,sub,i,lastSentences);
                        lastWord.add("<s> " + featureGen + "-" + wordCod[1]);
                        output.add(wordCod[1]);
                    }

                } else {
                    for (int j = 0; j < lastWord.size(); j++) { 
                        if (!(lastWord.get(j).contains("</s>") || lastWord.get(j).isEmpty() || lastWord.get(j).contains("."))) {
                            wordCod = searchLM(lastWord.get(j), lastPosTag.get(j), lm+"3gram", output.get(j), 2 , (i == length - 1), poslw, 0, false,sub,i,lastSentences,noFLM);


                            if (!wordCod[1].equals("")) {
                                String sf = output.get(j) + " " + wordCod[1];
                                output.set(j, sf);
                                String[] aux = output.get(j).split("\\s+");
                                lastWord.set(j, featureGen + "-" + aux[aux.length - 2] + " " + featureGen + "-" + aux[aux.length - 1]);

                            } else {
                                
                                String sf = output.get(j) + " .";
                                output.set(j, sf);
                                String[] aux = output.get(j).split("\\s+");
                                lastWord.set(j, featureGen + "-" + aux[aux.length - 2] + " " + featureGen + "-" + aux[aux.length - 1]);
                                
//                                lastWord.set(j, "");
                            }
                        }
                    }

                }
            }

        }
        
        for (int k = 0; k < output.size(); k++) {
            Sentence saux = new Sentence(output.get(k),(noFLM?"":outputPos.get(k)));
            saux.setSentiment(AuxFunctions.obtainSentimentSentence(saux.getSentence(), seedFeatures, feature));
            outputSent.add(saux);
        }
        
        
        return outputSent;
    }

    public String[] getPosMostProb(String word, String post, String lm, int i) {
        String[] pos = new String[3];
        String[] aux = new String[3];
        String[] aux2 = new String[3];
        String[] aux3 = new String[3];
        String pos1, pos2;
        pos1 = pos2 = "";
        float prob1, prob2;
        prob1 = prob2 = -Float.MAX_VALUE;

        try {

            //Get posTag with the highest probability of being with word 
            String command = " grep -e [$'\t']\"" + word + " \" files/corpus/" + corpus + "/lm/" + lm + "P" + featureGen.toUpperCase() + "1.lm | sort";

            String[] cmd = {
                "/bin/sh",
                "-c",
                command
            };

            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String s = stdInput.readLine();

            if (s != null) {
                do{
                    aux = s.split("\\t+");
                    aux2 = aux[1].split("\\s+");
                    aux3 = aux2[i].split("-");
                    if(aux3.length!=2)
                        s = stdInput.readLine();
                }while(aux3.length!=2);
                if(aux3.length!=2 && aux2[i].contains("</s>"))
                    pos1 = "puntuacion";
                else
                    pos1 = aux3[1];
                prob1 = Float.parseFloat(aux[0]);
            }
            stdInput.close();
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();
            //Get posTag with the highest probability of being with post
            command = " grep -e [$'\t']\"" + post + " \" files/corpus/" + corpus + "/lm/" + lm + "PP1.lm | sort";
            cmd[2] = command;

            process = Runtime.getRuntime().exec(cmd);
            stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

            s = stdInput.readLine();

            if (s != null) {
                do{
                    aux = s.split("\\t+");
                    aux2 = aux[1].split("\\s+");
                    aux3 = aux2[i].split("-");
                    if(aux3.length!=2)
                        s = stdInput.readLine();
                }while(aux3.length!=2);
                if(aux3.length!=2 && aux2[i].contains("</s>"))
                    pos2 = "puntuacion";
                else
                    pos2 = aux3[1];
                prob2 = Float.parseFloat(aux[0]);
            }

            if (pos1.equals(pos2)) {
                pos[0] = pos1;
            } else if (prob1 > prob2) {
                pos[0] = pos1;
            } else {
                pos[0] = pos2;
            }

            if (pos1.equals("") || pos2.equals("")) {
                pos1 = pos2 = pos[0];
            }

            pos[1] = pos1;
            pos[2] = pos2;
            stdInput.close();
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }

        return pos;
    }
    
    public String[] getRandomPos(){
        String[] pos = new String[3];
        String[] severalPos = {"adjetivo","adverbio","conjuncion","determinante","nombre","preposicion","pronombre","verbo"};
        
        Random dice = new Random(); 
        pos[0] = severalPos[dice.nextInt(severalPos.length)];
        pos[1] = severalPos[dice.nextInt(severalPos.length)];
        pos[2] = severalPos[dice.nextInt(severalPos.length)];
            
        return pos;
    }
    
    public String[] getNextPos(int i){
        String[] pos = new String[3];
        
        pos[0] = gram[i];
        pos[1] = gram[i];
        pos[2] = gram[i];
            
        return pos;
    }



    public ArrayList cleanLastProb(ArrayList l, float f, float limit) {
        Iterator<String> lIterator = l.iterator();

        while (lIterator.hasNext()) {
            String[] cad = lIterator.next().split(",");

            if (Float.parseFloat(cad[1]) + limit < f) {
                lIterator.remove();
            }
        }

        return l;
    }

    public ArrayList cleanArrayProb(ArrayList l) {
        ListIterator lIterator = l.listIterator();

        while (lIterator.hasNext()) {
            String[] cad = ((String) lIterator.next()).split(",");
            lIterator.set(cad[0]);
        }

        return l;
    }

    public String[] nextWord(String lw, String lpt, String lm, String sentence, int pos, Boolean endW,String sub,int it,ArrayList<String> genSentences) {
        String[] nextW = new String[2];
        boolean found = false;
        nextW[0] = nextW[1] = "";
        float prob = -Float.MAX_VALUE;
        List<String> sentWords = new ArrayList<>(Arrays.asList(sentence.split("\\s+")));
        
        for(String s: genSentences)
            sentWords.addAll(new ArrayList<>(Arrays.asList(s.split("\\s+"))));
        
        String[] poslw = new String[3];
        Float f = 0f;
        Word first = new Word();

        try {
//            if (lw.equals("<s>")) {
//                poslw = getPosMostProb(lw, lpt, lm, pos);
//            } else {
//                poslw = getPosMostProb(lw, lpt, lm, pos);
//            }
            if(!useGram){
               poslw = getPosMostProb(lw, lpt, lm, pos);  
            }else{
               poslw = getNextPos(it); 
            }
//            poslw = getNextPos(it);
            
//            if(poslw[0].equals("noun") && !sub.isEmpty() && !sentWords.contains(sub)){
              if((poslw[0].equals("noun") || poslw[0].equals("pronoun")) && it == 1 && !sub.isEmpty()){
//                  if((poslw[0].equals("noun") || poslw[0].equals("pronoun")) && !sub.isEmpty() && !sentWords.contains(sub)){
//                String comando = " grep -e [$'\t']\"" + lw + " " + featureGen + "-" + sub + "\" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm";
//
//                String[] cmd = {
//                    "/bin/sh",
//                    "-c",
//                    comando
//                };
//
//                Process process = Runtime.getRuntime().exec(cmd);
//                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
//                String s = stdInput.readLine();

//                if (s != null) {
                    nextW[0] = poslw[0];
                    nextW[1] = sub;
                    found = true;
//                }
                
            }
            if(poslw[0].equals("punctuation") || poslw[0].equals(".")){
                nextW[0] = "punctuation";
                    nextW[1] = ".";
                    found = true;
            }
            if(!found){
                ArrayList<Word> words = bag.getWordsPOS(poslw[0]);
                
                // Poner una restriccion para bigramas!!!
                for (Word w : words) {
                    
                    String comando = " grep -e [$'\t']\"" + lw + " " + featureGen + "-" + w.getWord() + "\" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm";

                    String[] cmd = {
                        "/bin/sh",
                        "-c",
                        comando
                    };

                    Process process = Runtime.getRuntime().exec(cmd);
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    String s = stdInput.readLine();

                    if (s != null ){
                        if(first.getWord().equals(""))
                            first = w;
                        
                        if(!sentWords.contains(w.getWord())){ 
                        String[] aux = s.split("\\t+");
                        String[] aux1 = aux[1].split("\\s+");
                        String[] aux2 = aux1[pos].split("-");
                        if (w.getWord().equals(aux2[1]) && !(endW && prepos.contains(w.getWord())) && !feature.wordNotDesirable(aux2[1])) {

                            float probAux = Float.parseFloat(aux[0]);

                            if (probAux > prob) {
                                prob = probAux;
                                nextW[0] = w.getPosTAG();
                                nextW[1] = w.getWord();
                                f = w.getFrequency();
                            } else if (probAux == prob && f <= w.getFrequency()) {
                                prob = probAux;
                                nextW[0] = w.getPosTAG();
                                nextW[1] = w.getWord();
                                f = w.getFrequency();
                            }
                        }

                        }
                    }else if(pos == 2){
                        stdInput.close();
                        process.getOutputStream().close();
                        process.getInputStream().close();
                        process.getErrorStream().close();
                        process.destroy();
                        
                        String[] lwaux = lw.split("\\s+");
                        
                        comando = "grep -e [$'\t']\"" + lwaux[1] + " "+ featureGen + "-" + w.getWord() + "\" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm";

                        cmd[2] = comando;
                    

                        process = Runtime.getRuntime().exec(cmd);
                        stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        s = stdInput.readLine();
                        
                        if (s != null ){
                            if(!sentWords.contains(w.getWord())){ 
                                String[] aux = s.split("\\t+");
                                String[] aux1 = aux[1].split("\\s+");
                                String[] aux2 = aux1[pos-1].split("-");
                                if (w.getWord().equals(aux2[1]) && !(endW && prepos.contains(w.getWord())) && !feature.wordNotDesirable(aux2[1])) {

                                    float probAux = Float.parseFloat(aux[0]);

                                    if (probAux > prob) {
                                        prob = probAux;
                                        nextW[0] = w.getPosTAG();
                                        nextW[1] = w.getWord();
                                        f = w.getFrequency();
                                    } else if (probAux == prob && f <= w.getFrequency()) {
                                        prob = probAux;
                                        nextW[0] = w.getPosTAG();
                                        nextW[1] = w.getWord();
                                        f = w.getFrequency();
                                    }
                                }

                            }
                        }
                        
                    }
                    
                    stdInput.close();
                    process.getOutputStream().close();
                    process.getInputStream().close();
                    process.getErrorStream().close();
                    process.destroy();
                }

                if (nextW[1].equals("")) {
//                    if(!first.getWord().equals("")){
//                        nextW[0] = first.getPosTAG();
//                        nextW[1] = first.getWord();
//                    }
//                    else
                        nextW = searchLM(lw, lpt, lm, sentence, pos, endW, poslw, 0, false,sub,it,genSentences,false);

                }
            }
        } catch (IOException ioe) {
            System.out.println(ioe);
        }

        return nextW;
    }

    public void selectWordsOver(ArrayList<String> lastWord, ArrayList<String> lastPosTag, String lm, int pos, String sub,boolean noFLM) {
        float prob = -Float.MAX_VALUE;
        String[] poslw = new String[3];
        boolean found = false;
        int f = 0;

        try {
            if(!noFLM){
                if(!useGram){
                   poslw = getPosMostProb("<s>", "<s>", lm, pos);  
                }else{
                   poslw = getNextPos(0); 
                }
            }
            else
                poslw[0] = poslw[1] = poslw[2] = "";
//            poslw = getPosMostProb("<s>", "", lm, pos);
//            poslw = getRandomPos();
//              poslw = getNextPos(0);
            
            if(poslw[0].equals("nombre") && !sub.isEmpty()){
                String comando = " grep -e [$'\t']\"<s> " + featureGen + "-" + sub + "\" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm";

                String[] cmd = {
                    "/bin/sh",
                    "-c",
                    comando
                };

                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String s = stdInput.readLine();

                if (s != null) {
                    found = true;
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();
            }

            ArrayList<Word> words = bag.getWordsPOS(poslw[0]);
            
            for (Word w : words) {
                String comando = " grep -e [$'\t']\"<s> " + featureGen + "-" + w.getWord() + "\" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm";

                String[] cmd = {
                    "/bin/sh",
                    "-c",
                    comando
                };

                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String s = stdInput.readLine();

                if (s != null) {
                    String[] aux = s.split("\\t+");
                    String[] aux1 = aux[1].split("\\s+");
                    String[] aux2 = aux1[pos].split("-");
                    if (w.getWord().equals(aux2[1]) && !feature.wordNotDesirable(aux2[1])) {

                        float probAux = Float.parseFloat(aux[0]);

                        if (probAux > prob) {
                            prob = probAux;
                            lastWord = cleanLastProb(lastWord, prob, 1f);
                            lastWord.add(w.getWord() + "," + prob);
                        } else if (probAux > prob - 0.5f) {
                            lastWord.add(w.getWord() + "," + probAux);
                        }
                    }
                }
                stdInput.close();
                process.getOutputStream().close();
                process.getInputStream().close();
                process.getErrorStream().close();
                process.destroy();
            }

            cleanArrayProb(lastWord);
            
            if(found)
                lastWord.add(sub);
            // Coger un numero máximo de frases del LM
            int maxSentLM = 20;
            int numSentLM = 0;
            int auxNumSentLM = 0;
            ArrayList<String> auxSent = new ArrayList<>();
            File file = null;
            FileReader fr = null;
            BufferedReader br = null;
            ArrayList<String> wtagCorpus = new ArrayList<>();
            if(!noFLM)
                wtagCorpus = AuxFunctions.getWordPosCorpus(poslw[0],corpus,featureGen);

//Opcion 1
            Random r = new Random();
//            String temp = "files/tempFiles/tempLMres"+r.nextInt()+".tmp";
            file = File.createTempFile("temp", null, new File("files/tempFiles/"));
            file.deleteOnExit();
            String comando = "grep -e [$'\t']\"<s> \" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm | sort >" + file.getAbsolutePath();

            String[] cmd = {
                "/bin/sh",
                "-c",
                comando
            };

            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();

//            file = new File(temp);
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            
            String s = br.readLine();
           
            do{

                if (s.contains(featureGen + "-")) {
                    String[] aux;
                    aux = s.split("\\t+");
                    if(aux.length==2){
                        String[] aux1 = aux[1].split("\\s+");
                        String[] aux2 = aux1[pos].split("-");

                        if ((noFLM?true:wtagCorpus.contains(aux2[(aux2.length>1)?1:0])) && feature.hasFeature(aux2[(aux2.length>1)?1:0]) && !lastWord.contains(aux2[(aux2.length>1)?1:0]) && !feature.wordNotDesirable(aux2[(aux2.length>1)?1:0])) {
                            lastWord.add(aux2[(aux2.length>1)?1:0]);
                            numSentLM++;
                        }
                        else if((noFLM?true:wtagCorpus.contains(aux2[(aux2.length>1)?1:0])) && !lastWord.contains(aux2[(aux2.length>1)?1:0]) && auxNumSentLM<maxSentLM && !feature.wordNotDesirable(aux2[(aux2.length>1)?1:0])){
                            auxSent.add(aux2[(aux2.length>1)?1:0]);
    //                        lastWord.add(aux2[(aux2.length>1)?1:0]);
                            auxNumSentLM++;
                        }

                    }
                }

            } while(numSentLM<maxSentLM && (s = br.readLine()) != null ); 
            
            if(numSentLM==0){
                lastWord.addAll(auxSent);
                if(auxSent.size() == 1 && auxSent.get(0).equals("determiner|none"))
                {
                    for(int i = 0; i < maxSentLM; i++)
                        lastWord.addAll(auxSent);
                }
            }
   
       
            
//Opcion 2
            /*
        Random dice = new Random();
        for(int i = 0; i < maxSentLM; i++){
            lastWord.add(wtagCorpus.get(dice.nextInt(wtagCorpus.size())));
        }
 */                  
            
            for (int i = 0; i < lastWord.size(); i++) 
                lastPosTag.add(poslw[0]);
//          
            if(lastPosTag.size()!=lastWord.size())
                System.out.println("");
            
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();
            
            fr.close();
            br.close();
//            file.delete();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }catch (Exception e){
            System.out.println(e);
        }

    }

    public String[] searchLM(String lw, String lpt, String lm, String sentence, int pos, Boolean endW, String[] poslw, int i, Boolean tested,String sub, int it,ArrayList<String> genSentences,boolean noFLM) {
        String[] nextW = new String[2];
        nextW[0] = nextW[1] = "";
        ArrayList<String> sentWords;
        sentWords = new ArrayList<>(Arrays.asList(sentence.split("\\s+")));
        String c = "";
        String firstS = "";
        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        Random r  = new Random();
        ArrayList<String> wtagCorpus = new ArrayList<>();

        try {
            if(!noFLM)
                wtagCorpus = AuxFunctions.getWordPosCorpus(poslw[i],corpus,featureGen);
            file = File.createTempFile("temp", null, new File("files/tempFiles/"));
            file.deleteOnExit();
            String comando = "grep -e [$'\t']\"" + lw + " \" files/corpus/" + corpus + "/lm/" + lm + featureGen.toUpperCase() + featureGen.toUpperCase() + "1.lm | sort >" + file.getAbsolutePath();

            String[] cmd = {
                "/bin/sh",
                "-c",
                comando
            };

            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();

//            file = new File(temp);
            fr = new FileReader(file);
            br = new BufferedReader(fr);

            String s = br.readLine();
            //caso de encontrar w_i+1 con postag de mayor probabilidad
            c = s;
            if (s != null && s.contains(featureGen + "-")) {
                String[] aux;
                float p = Float.MIN_VALUE;

                aux = s.split("\\t+");
                String[] aux1 = aux[1].split("\\s+");
                    String[] aux2 = aux1[pos].split("-");
                    firstS = aux2[(aux2.length>1)?1:0];
                    
                    
                    if (AuxFunctions.filterCond(featureGen, sentWords, aux2, wtagCorpus, feature, poslw, i, corpus,genSentences,noFLM)){
                        nextW[1] = aux2[(aux2.length>1)?1:0];
                        nextW[0] = poslw[i];
                        p = Float.parseFloat(aux[0]);
                    }

                    if(!noFLM)
                    {
                        while ((s = br.readLine()) != null && s.contains(featureGen + "-")) {
                            c = s;
                            aux = s.split("\\t+");
                            aux1 = aux[1].split("\\s+");
                            if(aux1.length == 3){
                                aux2 = aux1[pos].split("-");                            
                                if (nextW[1].equals("")) {
                                    if (AuxFunctions.filterCond(featureGen, sentWords, aux2, wtagCorpus, feature, poslw, i, corpus,genSentences,noFLM)){
                                        if (!(endW && prepos.contains(aux2[1]))) {
                                            nextW[1] = aux2[(aux2.length>1)?1:0];
                                            nextW[0] = poslw[i];
                                            p = Float.parseFloat(aux[0]);
                                        }
                                    }
                                } 
    //                            else {
    //                                if (AuxFunctions.filterCond(featureGen, sentWords, aux2, wtagCorpus, feature, poslw, i, corpus)){
    //                                    if (!(endW && !prepos.contains(aux2[(aux2.length>1)?1:0]))) {
    //                                        if (feature.hasFeature(nextW[1]) && Float.parseFloat(aux[0]) < p) {
    //                                            break;
    //                                        } else {
    //                                            nextW[1] = aux2[(aux2.length>1)?1:0];
    //                                            nextW[0] = poslw[i];
    //                                            p = Float.parseFloat(aux[0]);
    //                                        }
    //                                    }
    //                                }
    //                            }
                            }
                        }
                    }
                
            } 
//            else {
//                if (!tested) {
//                    if (!poslw[i].equals(poslw[1])) {
//                        nextW = searchLM(lw, lpt, lm, sentence, pos, endW, poslw, 1, true,sub,it);
//                    } else {
//                        nextW = searchLM(lw, lpt, lm, sentence, pos, endW, poslw, 2, true,sub,it);
//                    }
//                } else {
//                    if (pos == 2) {
//                        String[] aux = lw.split("\\s+");
//                        String[] aux2 = lpt.split("\\s+");
//                        nextW = nextWord(aux[aux.length - 1], aux2[aux2.length - 1], "3gram", sentence, 1, endW,sub,it);
//                    }
//                }
//
//            }
            
//            file.delete();
            //caso de encontrar w_i+1 con postag de mas probable a la palabra en caso de que no fuese el most probable
            if (nextW[1].equals("")) {

//                if (firstS.equals("")) {
                    if (pos == 2) {
                        String[] aux = lw.split("\\s+");
                        String[] aux2 = lpt.split("\\s+");
                        nextW = nextWord(aux[aux.length - 1], aux2[aux2.length - 1], lm, sentence, 1, endW,sub,it,genSentences);
                    }
//                } else {
//                    nextW[1] = firstS;
//                    nextW[0] = getPosOfWord(firstS);
//                }
            
            }
            process.getOutputStream().close();
            process.getInputStream().close();
            process.getErrorStream().close();
            process.destroy();

        } catch (Exception ioe) {
            System.out.println(ioe);
        } finally {
            try {
                if (null != fr) {
                    br.close();
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        return nextW;
    }

    public void generateSentence(String data) {
        init(data);
        ArrayList<Sentence> s = generateTri(6, "3gram", true,"",new ArrayList<String>(),false);
        Rank r = new Rank();
        
        System.out.println("Generated sentences");
        System.out.println("-------------------");
        
        for(Sentence sent: s){
            System.out.println(sent.getSentence());
        }
        
        System.out.println();
        System.out.println();
        
        Sentence sentence = r.combinedRankSentence(s, corpus, "gramLP1.lm", feature,featureGen,new ArrayList<String>(),"",false);
        
        System.out.println("Selected sentence");
        System.out.println("-------------------");
        
        
        System.out.println(sentence.getSentence());
    }
    
    public void generateSeveralSentences(String data, int nos){
        init(data);
        Sentence sent = new Sentence();
        String nextS = "";
        Rank r = new Rank();
        ArrayList<String> generated = new ArrayList<>();
        
        
        for(int i = 0; i<nos; i++){
            ArrayList<Sentence> s = generateTri((!useGram?20:gram.length), langM, true,nextS,generated,true);
            System.out.println("Generated sentences");
            System.out.println("-------------------");

            for(Sentence sen: s){
                System.out.println(sen.getSentence() + " " + Arrays.toString(sen.getSentiment()));
            }
            //cambiar el ranking
            sent = r.combinedRankSentence(s, corpus, langM, feature,featureGen,generated,nextS,true);
            //Lexicalizacion!!
            
            generated.add(sent.getSentence());
//            nextS = nextSub(sent.getSentence());
            
            System.out.println("Selected");
            System.out.println("-------------------");
            System.out.println(sent.getSentence()+ " " + Arrays.toString(sent.getSentiment()));
            if(!featureGen.equals("s")){
                if(true)
                    sent.calcSentencePOS(corpus, lex);
                String sInflected =  Inflect.inflectSentence(sent, lex, corpus,new ArrayList<String>(),langM);
                System.out.println(sInflected);
            }
            System.out.println(nextS);
            System.out.println("-------------------");
            bag = feature.changeFeature(bag,featureGen);
            gram = AuxFunctions.randGrammar(gramlist);
//            genFromSynset(sent.getSentence());
        }        
    }
    
    public String nextSub(String sentence){
        String word = "";
        if(!sentence.isEmpty()){
            ArrayList<String> sInfo = AuxFunctions.getSentenceInfoFreeling(sentence, corpus, language);
            String subjInfo = AuxFunctions.extractSubjectInfo(sInfo, corpus, language);
//            String subjInfo = AuxFunctions.extractObjectInfo(sInfo, corpus, language);
//            String subjInfo = AuxFunctions.extractNounInfo(sInfo, corpus, language,bag);
            if(!(subjInfo.isEmpty() || subjInfo.equals(""))){
                String[] info = subjInfo.split("\\s+");
                word = info[2]; 
            }
            
        }

        return word;
    }
    
    public ArrayList<Sentence> genFromSynset(String sentence){
        ArrayList<Sentence> gensent = new ArrayList<>();
        ArrayList<Sentence> gensent2 = new ArrayList<>();
        String[] words = sentence.split("\\s+");
        int max = 3;
        try{
            String path = "files/libraries/WNdict30";
            URL url = new URL("file", null , path ) ;

            // construct the dictionary object and open it
            IDictionary dict = new Dictionary( url ) ;
            dict.open () ;

            for(int i = 0 ; i < words.length; i++){
                String[] aux = words[i].split("\\|");
                ArrayList<Word> w = new ArrayList<>();
                if(aux.length>1){
                    if(aux[1].equals("none")){
//                       
                        
                        if(aux[0].equals("determiner")){
//                          w.add(new Word("a", "determiner"));
                          w.add(new Word("the", "determiner")); 
                        } else
                             w =  lex.getWordsPOS(aux[0],max);
    
                    }
                    else{
                        String synaux = "SID-"+aux[1].replace("_", "-").toUpperCase();
                        ISynsetID synsetID = SynsetID.parseSynsetID(synaux);
                        ISynset syn = dict.getSynset(synsetID);
                        if(syn.getWords().size()>=max){
                            for(int j = 0; j < max; j++){
                                w.add(new Word(syn.getWords().get(j).getLemma(), aux[0]));
                            }
                        }
                        else{
                            for(IWord iw: syn.getWords()){
                                w.add(new Word(iw.getLemma(), aux[0]));
                            }
                        }
                        
                    }
                }
                else
                    w.add(new Word(".", "punctuation"));

                 if(i == 0){
                    for(Word ws: w)
                        gensent.add(new Sentence(ws.getWord(),ws.getPosTAG()));
                }else{
                    ArrayList<Sentence> saux = new ArrayList<>();
                    for(Sentence s: gensent){
                        for(Word ws: w)
                            saux.add(new Sentence(s.getSentence()+" "+ws.getWord(),s.getSentencePOS()+" "+ws.getPosTAG()));
                    }
                    gensent = (ArrayList<Sentence>)saux.clone();
                }
                
            }
            
//            System.out.println("Sentences = "+gensent.size());
            
            Rank r = new Rank();
            Sentence s = r.combinedRankSentence(gensent, corpus, "gramLP1.lm", feature,"l",new ArrayList<String>(),"",false);
            gensent2.add(s);
            
        
        }catch(Exception e){
            e.printStackTrace();
        }
        return gensent2;
    }
    
    public String genService(String[] args){
        String genText = "";
        String input = "";
        String lang = args[4];
        
        //prepos input
        input+=args[0]+" ";
        input+=args[1]+" ";
        switch(args[1]){
            case "phoneme": input+= (lang.equals("en")?"feng ":"fesp ") + args[2]+" ";
                break;
            case "polarity": input+= (lang.equals("en")?"senticon.en.xml ":"senticon.es.xml ");
                    input+= (args[2].equals("positive")?"positive-negative ":"negative-positive ");
                break;
            case "sentiment": input += "emosenticnet.csv ";  
                    switch(args[2]){
                        case "anger": input += "0 ";
                            break;
                        case "disgust": input += "1 ";
                            break;
                        case "fear": input += "2 ";
                            break;
                        case "joy": input += "3 ";
                            break;
                        case "sadness": input += "4 ";
                            break;
                        case "surprise": input += "5 ";
                            break;
                    }
                
                break;
            case "importantW": input += "h2.len3.s25.cuento_0_en.plmOut 2 ";
                break;
        }        
        input+= (lang.equals("en")?"prepeng stopeng ":"prepesp stopesp ");
        input += args[3] + " freeling ";
        input+= (lang.equals("en")?"gramen":"gramesp");
        
        
        int nos = Integer.parseInt(args[8]);
        int useRel= Integer.parseInt(args[5]);
        int useG= Integer.parseInt(args[6]);
        int useStop= Integer.parseInt(args[7]);
        
        //gen
        init(input);
        Sentence sent = new Sentence();
        String nextS = "";
        Rank r = new Rank();
        ArrayList<String> generated = new ArrayList<>();
        ArrayList<String> generatedIn = new ArrayList<>();
        
        if(useG == 0)
                useGram = false;
        
        if(useStop == 0){
           bag.setHasStop(false);
           fillBOW();
        }
        

        for(int i = 0; i<nos; i++){
            ArrayList<Sentence> s = generateTri((!useGram?20:gram.length), langM, true,nextS,generated,true);
            //cambiar el ranking
            sent = r.combinedRankSentence(s, corpus, langM, feature,featureGen,generated,nextS,true);
            //Lexicalizacion!!
            
            generated.add(sent.getSentence());
            if(useRel == 1)
                nextS = nextSub(sent.getSentence());
            
//            System.out.println(sent.getSentence()+ " " + Arrays.toString(sent.getSentiment()));
            if(!featureGen.equals("s")){
                if(true)
                    sent.calcSentencePOS(corpus, lex);
                String sInflected =  Inflect.inflectSentence(sent, lex, corpus,new ArrayList<String>(),langM);
                generatedIn.add(sInflected);
            }
            else{
                ArrayList<Sentence> gensent  = genFromSynset(sent.getSentence());
                generatedIn.add(gensent.get(0).getSentence());
            }
            bag = feature.changeFeature(bag,featureGen);
            gram = AuxFunctions.randGrammar(gramlist);
        }        
        
        for(String s: generatedIn){
            genText+=s + '\n';
        }

        return genText;
    } 
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SeedFeaturesV2 seed = new SeedFeaturesV2();
//        seed.generateSeveralSentences(args[0],10);
//        seed.genFromSynset("determiner|none noun|09917593_n verb|00056930_v determiner|none noun|05833840_n .");
        String[] options = args[0].split("\\s+");
        
        if(options.length==9){
            String s = seed.genService(options);
            System.out.println(s);
        }
    }

}