/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seedfeaturesv.pkg2;

import seedfeaturesv.pkg2.Features.Word;
import java.util.ArrayList;

/**
 *
 * @author cristina
 */
public class BagW {
    
    private ArrayList<Word> words;
    private Boolean hasStop;
    
    public BagW(){
        words = new ArrayList<>();
        hasStop = true;
    }
    
    public ArrayList<Word> getWords() {
        return words;
    }

    public void setWords(ArrayList<Word> palabras) {
        this.words = palabras;
    }

    public Boolean hasStop() {
        return hasStop;
    }

    public void setHasStop(Boolean hasStop) {
        this.hasStop = hasStop;
    }
    
    public void addWord(Word p)
    {
        words.add(p);
    }
    
    public ArrayList<Word> getWordsPOS(String pos){
        ArrayList pal = new ArrayList<>();
        
        for(Word s: words){
            if(s.getPosTAG().equals(pos)){
                pal.add(s);
            }
        }
        return pal;
    }
    
    public Boolean containWord(String word){
        Boolean contained = false;
        for(Word s: words){
            if(s.getWord().equals(word)){
                contained = true;
                break;
            }
        }
        return contained;
    }
    
    public int numWordsContained(String sentence){
        int num = 0;
        String[] s = sentence.split(" ");
        for(int i = 0; i < s.length;i++){
            if(!s[i].equals(".")){
                if(containWord(s[i]))
                    num++;
            }
        }
        return num;
    }
    
}
